use tracing::subscriber::set_global_default;
use tracing_bunyan_formatter::{BunyanFormattingLayer, JsonStorageLayer};
use tracing_subscriber::{layer::SubscriberExt, EnvFilter, Registry};

/// Inits a simple log based tracing with a JSON format.
pub async fn init_tracing() {
    let subscriber = Registry::default()
        .with(EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info")))
        .with(JsonStorageLayer)
        .with(BunyanFormattingLayer::new("api".into(), std::io::stdout));

    set_global_default(subscriber).expect("failed to set subscriber");
}
