use secrecy::SecretString;

#[derive(Clone)]
pub struct AuthContext {
    token: Option<SecretString>,
}

impl AuthContext {
    pub fn new(token: Option<SecretString>) -> Self {
        Self { token }
    }

    pub fn token(&self) -> &Option<SecretString> {
        &self.token
    }
}
