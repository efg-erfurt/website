use crate::tracing::init_tracing;
use ::tracing::info;

/// This method is called during the first cold start.
pub async fn init_cold() {
    init_tracing().await;

    info!("started with cold start");
}
