use crate::auth_context::AuthContext;
use async_graphql::{
    http::{receive_batch_body, MultipartOptions},
    BatchRequest, BatchResponse, EmptySubscription, ParseRequestError, Schema,
};
use http::{
    header::{AUTHORIZATION, CACHE_CONTROL},
    Method, StatusCode,
};
use lambda_http::{
    http::header::CONTENT_TYPE, Body, Body::Text, Error, Request, RequestExt, Response,
};
use secrecy::SecretString;
use tracing::{info_span, warn, Instrument};

/// This function validates and transforms the incoming http request for async_graphql.
/// A valid request is resolved in async_graphql. The response is transformed back to the lambda reponse format.
/// This integration flow is described here: <https://github.com/async-graphql/async-graphql/blob/master/integrations/README.md>
pub async fn run_function_with_schema<Q, M>(
    http_event: &Request,
    schema: &Schema<Q, M, EmptySubscription>,
) -> Result<Response<Body>, Error>
where
    Q: 'static + async_graphql::ObjectType,
    M: 'static + async_graphql::ObjectType,
{
    let context = http_event.lambda_context();
    let request_id = context.request_id;
    let xray_trace_id = context.xray_trace_id;
    let trace_id =
        get_trace_value(&xray_trace_id, "Root").expect("root missing in trace id header");
    let parent_id =
        get_trace_value(&xray_trace_id, "Parent").expect("parent missing in trace id header");

    async {
        match execute_request(http_event, schema).await {
            Ok(value) => value,
            Err(value) => value,
        }
    }
    .instrument(info_span!(
        "warm_run",
        request_id = request_id.as_str(),
        trace_id = trace_id.as_str(),
        parent_id = parent_id.as_str()
    ))
    .await
}

async fn execute_request<Q, M>(
    http_event: &http::Request<Body>,
    schema: &Schema<Q, M, EmptySubscription>,
) -> Result<Result<Response<Body>, Error>, Result<Response<Body>, Error>>
where
    Q: 'static + async_graphql::ObjectType,
    M: 'static + async_graphql::ObjectType,
{
    // validate request
    let has_expected_method = matches!(http_event.method(), &Method::POST);
    if !has_expected_method {
        warn!("request has invalid method");
        return Err(map_status_code_to_error_response(
            StatusCode::METHOD_NOT_ALLOWED,
        ));
    }

    // map event to request
    let request = map_event_to_request(http_event).await;
    if request.is_none() {
        warn!("request has no payload");
        return Err(map_status_code_to_error_response(StatusCode::BAD_REQUEST));
    }

    // check parsed request
    let existing_request = request.unwrap();
    if let Err(error) = existing_request {
        warn!("request parsing failed: {:?}", error);
        return Err(match error {
            ParseRequestError::PayloadTooLarge => {
                map_status_code_to_error_response(StatusCode::PAYLOAD_TOO_LARGE)
            }
            _ => map_status_code_to_error_response(StatusCode::BAD_REQUEST),
        });
    }

    let auth_context = build_auth_context(http_event);

    // execute request
    let result = schema
        .execute_batch(existing_request.unwrap().data(auth_context))
        .await;

    // map result to response
    Ok(map_result_to_response(result))
}

fn get_trace_value(header: &Option<String>, key: &str) -> Option<String> {
    header.as_ref().and_then(|x| {
        x.split(';')
            .map(|val| val.split('='))
            .filter(|key_value| key_value.clone().next().unwrap().eq(key))
            .map(|key_value| key_value.clone().nth(1).unwrap().to_string())
            .next()
    })
}

fn build_auth_context(http_event: &http::Request<Body>) -> AuthContext {
    let token = http_event
        .headers()
        .get(AUTHORIZATION)
        .map(|val| val.to_str().unwrap().to_string())
        .and_then(|val| {
            let parts = val.split(' ').map(|x| x.to_string()).collect::<Vec<_>>();
            if parts.len() != 2 {
                return None;
            }

            Some(parts.get(1).unwrap().to_string())
        });

    AuthContext::new(token.map(SecretString::from))
}

async fn map_event_to_request(
    http_event: &Request,
) -> Option<Result<BatchRequest, ParseRequestError>> {
    match http_event.body() {
        Text(payload) => {
            if payload.is_empty() {
                return None;
            }

            let content_type = http_event
                .headers()
                .get(CONTENT_TYPE)
                .map(|e| e.to_str().unwrap_or("application/json"));
            let request = receive_batch_body(
                content_type,
                payload.as_bytes(),
                MultipartOptions::default(),
            )
            .await;

            Some(request)
        }
        _ => None,
    }
}

fn map_result_to_response(gql_response: BatchResponse) -> Result<Response<Body>, Error> {
    // prepare response with status code and body
    let mut response = Response::builder()
        .status(StatusCode::OK.as_u16())
        .body(serde_json::to_string(&gql_response)?.into())?;

    // always add Content-Type header
    response
        .headers_mut()
        .append(CONTENT_TYPE, "application/json".parse()?);

    // add headers from schema
    let headers = gql_response.http_headers();
    for (key, value) in &headers {
        response.headers_mut().append(key, value.clone());
    }

    // add Cache-Control header for responses without errors
    if gql_response.is_ok() {
        let cache_control_value = gql_response.cache_control().value().unwrap_or_default();
        response
            .headers_mut()
            .append(CACHE_CONTROL, cache_control_value.parse()?);
    }

    Ok(response)
}

fn map_status_code_to_error_response(status_code: StatusCode) -> Result<Response<Body>, Error> {
    let response = Response::builder()
        .status(status_code.as_u16())
        .body(status_code.as_str().into())?;
    Ok(response)
}

#[cfg(test)]
mod tests {
    use crate::{auth_context::AuthContext, function::run_function_with_schema};
    use async_graphql::{EmptyMutation, EmptySubscription, Object, Schema};
    use http::{
        header::{AUTHORIZATION, CACHE_CONTROL},
        HeaderMap, HeaderValue, Method, Request,
    };
    use insta::assert_json_snapshot;
    use lambda_http::{
        http::header::CONTENT_TYPE, lambda_runtime::Config, Body::Text, Context, RequestExt,
    };
    use secrecy::ExposeSecret;
    use serde_json::Value;
    use std::sync::Arc;

    pub struct QueryRoot;

    #[Object(cache_control(max_age = 60))]
    impl QueryRoot {
        async fn value(&self) -> i32 {
            100
        }

        async fn user<'ctx>(&self, ctx: &async_graphql::Context<'ctx>) -> Option<String> {
            let auth_context = ctx.data_unchecked::<AuthContext>();
            auth_context
                .token()
                .as_ref()
                .filter(|t| (t.expose_secret()).eq("abcdefg12345"))
                .map(|_| "user".to_string())
        }
    }

    #[tokio::test]
    async fn test_should_return_valid_response_for_single_request() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .header(CONTENT_TYPE, "application/json")
            .body("{\"query\": \"{ value }\"}".into())
            .unwrap()
            .with_lambda_context(build_context());

        // when
        let result = run_function_with_schema(&request, &build_example_schema()).await;

        // then
        assert!(result.is_ok());

        let response = result.unwrap();
        assert_eq!(200, response.status());

        let headers = response.headers();
        assert_eq!(2, headers.len());
        assert_eq!(
            "application/json",
            headers.get(CONTENT_TYPE).unwrap().to_str().unwrap()
        );
        assert_eq!(
            "max-age=60",
            headers.get(CACHE_CONTROL).unwrap().to_str().unwrap()
        );

        let body = match response.body() {
            Text(payload) => payload,
            _ => panic!(""),
        };
        let response_body: Value = serde_json::from_str(body).unwrap();
        assert_json_snapshot!(response_body);
    }

    #[tokio::test]
    async fn test_should_return_valid_response_for_authorized_request() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .header(CONTENT_TYPE, "application/json")
            .header(AUTHORIZATION, "Bearer abcdefg12345")
            .body("{\"query\": \"{ user }\"}".into())
            .unwrap()
            .with_lambda_context(build_context());

        // when
        let result = run_function_with_schema(&request, &build_example_schema()).await;

        // then
        let response = result.unwrap();
        assert_eq!(200, response.status());

        let body = match response.body() {
            Text(payload) => payload,
            _ => panic!(""),
        };
        let response_body: Value = serde_json::from_str(body).unwrap();
        assert_json_snapshot!(response_body);
    }

    #[tokio::test]
    async fn test_should_return_valid_response_for_unauthorized_request() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .header(CONTENT_TYPE, "application/json")
            .body("{\"query\": \"{ user }\"}".into())
            .unwrap()
            .with_lambda_context(build_context());

        // when
        let result = run_function_with_schema(&request, &build_example_schema()).await;

        // then
        let response = result.unwrap();
        assert_eq!(200, response.status());

        let body = match response.body() {
            Text(payload) => payload,
            _ => panic!(""),
        };
        let response_body: Value = serde_json::from_str(body).unwrap();
        assert_json_snapshot!(response_body);
    }

    #[tokio::test]
    async fn test_should_return_valid_response_for_invalid_authorization_header() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .header(CONTENT_TYPE, "application/json")
            .header(AUTHORIZATION, "Something")
            .body("{\"query\": \"{ user }\"}".into())
            .unwrap()
            .with_lambda_context(build_context());

        // when
        let result = run_function_with_schema(&request, &build_example_schema()).await;

        // then
        let response = result.unwrap();
        assert_eq!(200, response.status());

        let body = match response.body() {
            Text(payload) => payload,
            _ => panic!(""),
        };
        let response_body: Value = serde_json::from_str(body).unwrap();
        assert_json_snapshot!(response_body);
    }

    #[tokio::test]
    async fn test_should_return_valid_response_for_batch_request() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .header(CONTENT_TYPE, "application/json")
            .body("[{\"query\": \"{ value }\"}, {\"query\": \"{ value }\"}]".into())
            .unwrap()
            .with_lambda_context(build_context());

        // when
        let result = run_function_with_schema(&request, &build_example_schema()).await;

        // then
        assert!(result.is_ok());

        let response = result.unwrap();
        assert_eq!(200, response.status());

        let headers = response.headers();
        assert_eq!(2, headers.len());
        assert_eq!(
            "application/json",
            headers.get(CONTENT_TYPE).unwrap().to_str().unwrap()
        );
        assert_eq!(
            "max-age=60",
            headers.get(CACHE_CONTROL).unwrap().to_str().unwrap()
        );

        let body = match response.body() {
            Text(payload) => payload,
            _ => panic!(""),
        };
        let response_body: Value = serde_json::from_str(body).unwrap();
        assert_json_snapshot!(response_body);
    }

    #[tokio::test]
    async fn test_should_return_error_for_malformed_request() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .body("{ value".into())
            .unwrap()
            .with_lambda_context(build_context());

        // when
        let result = run_function_with_schema(&request, &build_example_schema()).await;

        // then
        assert!(result.is_ok());

        let response = result.unwrap();
        assert_eq!(400, response.status());
    }

    #[tokio::test]
    async fn test_should_return_error_for_invalid_request() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .body("{\"query\": \"{ invalid }\"}".into())
            .unwrap()
            .with_lambda_context(build_context());

        // when
        let result = run_function_with_schema(&request, &build_example_schema()).await;

        // then
        assert!(result.is_ok());

        let response = result.unwrap();
        assert_eq!(200, response.status());

        let headers = response.headers();
        assert_eq!(1, headers.len());
        assert_eq!(
            "application/json",
            headers.get(CONTENT_TYPE).unwrap().to_str().unwrap()
        );

        let body = match response.body() {
            Text(payload) => payload,
            _ => panic!(""),
        };
        let response_body: Value = serde_json::from_str(body).unwrap();
        assert_eq!(
            response_body["errors"][0]["message"],
            "Unknown field \"invalid\" on type \"QueryRoot\"."
        );
        assert_json_snapshot!(response_body);
    }

    #[tokio::test]
    async fn test_should_return_unsupported_method_for_non_post_request() {
        // given
        let request = Request::builder()
            .method(Method::PUT)
            .body("{value}".into())
            .unwrap()
            .with_lambda_context(build_context());

        // when
        let result = run_function_with_schema(&request, &build_example_schema()).await;

        // then
        assert!(result.is_ok());

        let response = result.unwrap();
        assert_eq!(405, response.status());
    }

    #[tokio::test]
    async fn test_should_return_unsupported_method_for_empty_post_request() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .body("".into())
            .unwrap()
            .with_lambda_context(build_context());

        // when
        let result = run_function_with_schema(&request, &build_example_schema()).await;

        // then
        assert!(result.is_ok());

        let response = result.unwrap();
        assert_eq!(400, response.status());
    }

    fn build_context() -> Context {
        let mut headers = HeaderMap::new();
        headers.insert(
            "lambda-runtime-aws-request-id",
            HeaderValue::from_static("6f3c103e-f0a5-4982-934c-dabedda0065e"),
        );
        headers.insert(
            "lambda-runtime-deadline-ms",
            HeaderValue::from_static("123"),
        );
        headers.insert(
            "lambda-runtime-invoked-function-arn",
            HeaderValue::from_static("arn::myarn"),
        );
        headers.insert(
            "lambda-runtime-trace-id",
            HeaderValue::from_static(
                "Root=1-621e8596-13fa204f3ea98d584b6af5cd;Parent=06cf37c30d0d8959;Sampled=1",
            ),
        );
        Context::new("id", Arc::new(Config::default()), &headers).expect("building context failed")
    }

    fn build_example_schema() -> Schema<QueryRoot, EmptyMutation, EmptySubscription> {
        Schema::build(QueryRoot, EmptyMutation, EmptySubscription).finish()
    }
}
