use std::{
    error::Error,
    fmt::{Debug, Display},
};
use tracing::error;

#[derive(Debug)]
pub struct AppError {
    message: String,
}

impl Error for AppError {}

impl AppError {
    pub fn from_error<E>(error: &E, context_message: &str) -> Self
    where
        E: Display + Debug,
    {
        AppError {
            message: format!("{:?} ({})", &error, context_message),
        }
    }

    pub fn new(error: impl Into<String>) -> Self {
        AppError {
            message: error.into(),
        }
    }

    pub fn get_message(&self) -> &str {
        &self.message
    }
}

impl Display for AppError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // show technical details only for debug builds
        if cfg!(debug_assertions) {
            write!(f, "Internal Error: {}", self.message)
        } else {
            error!("{}", self.message);
            write!(f, "Internal Error. We are really sorry.")
        }
    }
}
