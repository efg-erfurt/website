use crate::{db::DbClient, db_item::DbItem};
use async_trait::async_trait;
use aws_sdk_dynamodb::{
    operation::{
        batch_get_item::BatchGetItemOutput, delete_item::DeleteItemOutput, get_item::GetItemOutput,
        put_item::PutItemOutput, query::QueryOutput, scan::ScanOutput,
    },
    types::{AttributeValue, KeysAndAttributes, Select},
    Client,
};
use aws_types::sdk_config::SdkConfig;
use efg_lambda_base::errors::AppError;
use std::{
    collections::{BTreeMap, HashMap},
    fmt::{Debug, Display},
    marker::PhantomData,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use tracing::{debug_span, Instrument};

#[derive(Clone)]
pub struct DynamoDbClient<U>
where
    U: 'static + DbItem<U> + Send + Sync,
{
    db: Client,
    table: String,
    ttl: Option<Duration>,
    phantom: PhantomData<U>,
}

impl<U> DynamoDbClient<U>
where
    U: 'static + DbItem<U> + Send + Sync,
{
    pub fn new(table: &str, aws_config: &SdkConfig, ttl: Option<Duration>) -> Self {
        if table.is_empty() {
            panic!("missing table name");
        }

        let db = Client::new(aws_config);

        DynamoDbClient {
            db,
            ttl,
            table: table.to_string(),
            phantom: PhantomData,
        }
    }

    fn map_dynamo_error_to_app_error<E>(&self, err: &E) -> AppError
    where
        E: Display + Debug,
    {
        AppError::from_error(&err, "DynamoDB")
    }

    async fn scan(&self) -> Result<ScanOutput, AppError> {
        async {
            self.db
                .scan()
                .table_name(&self.table)
                .select(Select::AllAttributes)
                .send()
                .await
                .map_err(|err| self.map_dynamo_error_to_app_error(&err))
        }
        .instrument(debug_span!("scan", table = self.table))
        .await
    }

    async fn query(&self, id: &str) -> Result<QueryOutput, AppError> {
        async {
            self.db
                .query()
                .table_name(&self.table)
                .key_condition_expression("#key = :value".to_string())
                .expression_attribute_names("#key".to_string(), "id".to_string())
                .expression_attribute_values(
                    ":value".to_string(),
                    AttributeValue::S(id.to_string()),
                )
                .select(Select::AllAttributes)
                .send()
                .await
                .map_err(|err| self.map_dynamo_error_to_app_error(&err))
        }
        .instrument(debug_span!("query", table = self.table))
        .await
    }

    async fn get_item(&self, id: &str) -> Result<GetItemOutput, AppError> {
        async {
            self.db
                .get_item()
                .table_name(&self.table)
                .key("id", AttributeValue::S(id.to_string()))
                .send()
                .await
                .map_err(|err| self.map_dynamo_error_to_app_error(&err))
        }
        .instrument(debug_span!("get_item", table = self.table, id = id))
        .await
    }

    async fn put_item(&self, item: U) -> Result<PutItemOutput, AppError> {
        async {
            let raw = item.to_raw();

            let mut request_builder = self.db.put_item().table_name(&self.table);
            for (key, value) in raw.iter() {
                request_builder = request_builder.item(key.to_string(), value.clone());
            }

            if let Some(ttl) = self.ttl {
                let ttl_timestamp = SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .expect("time invalid, should never happen")
                    .as_secs()
                    + ttl.as_secs();
                request_builder =
                    request_builder.item("ttl", AttributeValue::N(ttl_timestamp.to_string()));
            }

            let id = item.get_id();
            request_builder
                .item("id", AttributeValue::S(id))
                .send()
                .await
                .map_err(|err| self.map_dynamo_error_to_app_error(&err))
        }
        .instrument(debug_span!(
            "put_item",
            table = self.table,
            id = item.get_id()
        ))
        .await
    }

    async fn delete_item(&self, id: &str) -> Result<DeleteItemOutput, AppError> {
        async {
            self.db
                .delete_item()
                .table_name(&self.table)
                .key("id", AttributeValue::S(id.to_string()))
                .send()
                .await
                .map_err(|err| self.map_dynamo_error_to_app_error(&err))
        }
        .instrument(debug_span!("delete_item", table = self.table, id = id))
        .await
    }

    async fn batch_get_item(&self, ids: &[String]) -> Result<BatchGetItemOutput, AppError> {
        async {
            let keys = ids
                .iter()
                .map(|k| {
                    let mut request_map = HashMap::new();
                    request_map.insert("id".to_string(), AttributeValue::S(k.to_string()));
                    request_map
                })
                .collect();

            let mut request_items = HashMap::new();
            request_items.insert(
                self.table.to_string(),
                KeysAndAttributes::builder()
                    .set_keys(Some(keys))
                    .build()
                    .unwrap(),
            );

            self.db
                .batch_get_item()
                .set_request_items(Some(request_items))
                .send()
                .await
                .map_err(|err| self.map_dynamo_error_to_app_error(&err))
        }
        .instrument(debug_span!(
            "batch_get_item",
            table = self.table,
            ids_count = ids.len()
        ))
        .await
    }
}

#[async_trait]
impl<U> DbClient<U> for DynamoDbClient<U>
where
    U: 'static + DbItem<U> + Send + Sync,
{
    async fn get_value_by_id(&self, id: &str) -> Result<Option<U>, AppError> {
        let result = self.get_item(id).await?;

        if result.item().is_none() {
            return Ok(None);
        }

        Ok(Some(U::from_raw(result.item().unwrap())?))
    }

    async fn get_values_by_id(&self, id: &str) -> Result<Vec<U>, AppError> {
        let result = self.query(id).await?;

        let matches = result
            .items()
            .iter()
            .map(|m| U::from_raw(m))
            .collect::<Vec<_>>();
        let has_any_errors = matches.iter().any(|i| i.is_err());
        if has_any_errors {
            return Err(AppError::new("transforming entities failed"));
        }

        Ok(matches
            .iter()
            .map(|i| i.as_ref().unwrap().clone())
            .collect())
    }

    async fn batch_get_values_by_ids(
        &self,
        ids: &[String],
    ) -> Result<BTreeMap<String, U>, AppError> {
        let result = self.batch_get_item(ids).await?;

        if result.responses().is_none() || result.responses().unwrap().is_empty() {
            return Ok(BTreeMap::new());
        }

        let (_, table_items) = result.responses().unwrap().iter().next().unwrap();

        let matches = table_items
            .iter()
            .map(|m| U::from_raw(m))
            .collect::<Vec<_>>();
        let has_any_errors = matches.iter().any(|i| i.is_err());
        if has_any_errors {
            return Err(AppError::new("transforming entities failed"));
        }

        Ok(matches
            .iter()
            .map(|i| {
                let item = i.as_ref().unwrap().clone();

                (item.get_id(), item)
            })
            .collect::<BTreeMap<String, U>>())
    }

    async fn get_all_values(
        &self,
        _sort_column: &str,
        page: usize,
        size: usize,
    ) -> Result<Vec<U>, AppError> {
        let result = self.scan().await?;

        let matches = result
            .items()
            .iter()
            .cloned()
            .map(|m| U::from_raw(&m))
            .skip(page * size)
            .take(size)
            .collect::<Vec<_>>();

        let has_any_errors = matches.iter().any(|i| i.is_err());
        if has_any_errors {
            return Err(AppError::new("transforming entities failed"));
        }

        Ok(matches
            .iter()
            .map(|i| i.as_ref().unwrap().clone())
            .collect())
    }

    async fn put_item(&self, item: U) -> Result<(), AppError> {
        let _ = self.put_item(item).await?;

        Ok(())
    }

    async fn delete_item(&self, id: &str) -> Result<(), AppError> {
        let _ = self.delete_item(id).await?;

        Ok(())
    }

    async fn delete_all(&self) -> Result<(), AppError> {
        let all = self.get_all_values("", 0, 99999).await?;

        for x in all.iter() {
            self.delete_item(&x.get_id()).await?;
        }

        Ok(())
    }
}
