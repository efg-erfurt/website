use aws_sdk_dynamodb::types::AttributeValue;
use efg_lambda_base::errors::AppError;
use std::collections::HashMap;

/// Trait for persistable entities.
pub trait DbItem<U>: Clone {
    /// Creates an Item from a DynamoDB raw map.
    fn from_raw(raw: &HashMap<String, AttributeValue>) -> Result<U, AppError>;

    /// Creates a DynamoDB raw map from an item.
    fn to_raw(&self) -> HashMap<String, AttributeValue>;

    /// Returns the technical id of an item.
    fn get_id(&self) -> String;
}
