use crate::db_item::DbItem;
use async_trait::async_trait;
use efg_lambda_base::errors::AppError;
use std::collections::BTreeMap;

pub type DbService<U> = Box<dyn DbClient<U> + Send + Sync>;

#[async_trait]
pub trait DbClient<U>: Send + Sync
where
    U: 'static + DbItem<U> + Send + Sync,
{
    /// Retrieves items only by a primary key. It will fail for combined keys with sorting parts.
    async fn get_value_by_id(&self, _id: &str) -> Result<Option<U>, AppError> {
        unimplemented!("get_value_by_id")
    }

    /// Retrieves items by a primary key. It might return multiple items for tables with sort keys.
    async fn get_values_by_id(&self, _id: &str) -> Result<Vec<U>, AppError> {
        unimplemented!("get_values_by_id")
    }

    /// Retrieves multiple items by primary keys.
    async fn batch_get_values_by_ids(
        &self,
        _ids: &[String],
    ) -> Result<BTreeMap<String, U>, AppError> {
        unimplemented!("batch_get_values_by_ids")
    }

    /// Scans the whole table and returns a sorted page.
    async fn get_all_values(
        &self,
        _sort_column: &str,
        _page: usize,
        _size: usize,
    ) -> Result<Vec<U>, AppError> {
        unimplemented!("get_all_values")
    }

    /// Creates or updates an item.
    async fn put_item(&self, _item: U) -> Result<(), AppError> {
        unimplemented!("put_item")
    }

    /// Deletes all entries.
    async fn delete_all(&self) -> Result<(), AppError> {
        unimplemented!("delete_all")
    }

    /// Deletes an item  ID.
    async fn delete_item(&self, _id: &str) -> Result<(), AppError> {
        unimplemented!("delete_item")
    }
}
