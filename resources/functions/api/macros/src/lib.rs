use proc_macro::TokenStream;
use syn::{parse_macro_input, ItemFn};
extern crate proc_macro;
use quote::quote;

#[proc_macro_attribute]
pub fn track_mock_calls(_args: TokenStream, stream: TokenStream) -> TokenStream {
    let input = parse_macro_input!(stream as ItemFn);
    let ItemFn {
        attrs,
        vis,
        sig,
        block,
    } = input;
    let fn_name = sig.ident.to_string();

    let self_arg = sig
        .inputs
        .iter()
        .flat_map(|i| match i {
            syn::FnArg::Receiver(s) => Some(s.clone()),
            syn::FnArg::Typed(_) => None,
        })
        .map(|r| r.self_token)
        .next()
        .unwrap();

    let expanded = quote! {
        #(#attrs)* #vis #sig {
            // inform about call
            #self_arg.call_sender.send(Some(#fn_name.to_string())).unwrap();

            #block
        }
    };

    TokenStream::from(expanded)
}
