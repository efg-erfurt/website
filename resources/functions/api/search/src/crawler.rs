use quick_xml::Reader;
use std::{collections::BTreeMap, str::from_utf8};
use unicode_segmentation::UnicodeSegmentation;

#[derive(Default)]
pub struct Crawler;

impl Crawler {
    pub fn parse_page(&self, page: &str) -> BTreeMap<String, Vec<String>> {
        let mut reader = Reader::from_str(page);
        reader.config_mut().check_end_names = false;

        let mut current_path = Vec::new();
        let mut page_elements: BTreeMap<String, Vec<String>> = BTreeMap::new();

        loop {
            match reader.read_event().unwrap() {
                quick_xml::events::Event::Start(e) => {
                    let value = from_utf8(e.name().as_ref()).unwrap().to_string();
                    current_path.push(value);
                }
                quick_xml::events::Event::End(_) => {
                    current_path.pop();
                }
                quick_xml::events::Event::Text(e) => {
                    let content = e.unescape().unwrap_or_default().to_string();
                    // filter readable tags as last tag to ignore elements for plain text
                    let current_tag = current_path
                        .iter()
                        .filter(|p| {
                            !matches!(p.as_str(), "div" | "span" | "a" | "b" | "i" | "strong")
                        })
                        .last()
                        .cloned();

                    if let Some(tag) = current_tag.as_ref() {
                        match tag.as_str() {
                            "h1" | "h2" | "h3" | "h4" | "h5" | "p" | "title" => {
                                if !page_elements.contains_key(tag) {
                                    page_elements.insert(tag.to_string(), Vec::new());
                                }
                                page_elements.get_mut(tag).unwrap().push(content);
                            }
                            _ => {
                                // ignore non textual tags
                            }
                        }
                    }
                }
                quick_xml::events::Event::Eof => break,
                _ => (),
            }
        }

        page_elements
    }

    pub fn calculate_score(
        &self,
        page_elements: &BTreeMap<String, Vec<String>>,
        query: &str,
    ) -> f32 {
        let normalized_query = self.normalize_element(query);

        let score = page_elements
            .iter()
            .map(|(tag, elements)| {
                let boost = match tag.as_str() {
                    "h1" => 15.0,
                    "h2" => 12.0,
                    "h3" => 9.0,
                    "h4" => 6.0,
                    "h5" => 3.0,
                    _ => 1.0,
                };

                elements
                    .iter()
                    .map(|e| self.normalize_element(e))
                    .collect::<Vec<_>>()
                    .join(" ")
                    .matches(&normalized_query)
                    .count() as f32
                    * boost
            })
            .sum::<f32>();

        score
    }

    fn normalize_element(&self, element: &str) -> String {
        element
            .unicode_words()
            .collect::<Vec<_>>()
            .join(" ")
            .to_lowercase()
            .replace('ä', "a")
            .replace('ö', "o")
            .replace('ü', "u")
            .replace('é', "e")
            .replace('á', "a")
    }
}

#[cfg(test)]
mod tests {
    use super::Crawler;
    use std::collections::BTreeMap;

    #[test]
    fn test_should_parse_page() {
        // given
        let crawler = Crawler;

        // when
        let page = crawler
            .parse_page("<html><title>Test</title><body><p>Content<br />Abc</p></body></html>");

        // then
        assert_eq!(2, page.len());
        assert_eq!(["Test"], page.get("title").unwrap().as_slice());
        assert_eq!(["Content", "Abc"], page.get("p").unwrap().as_slice());
    }

    #[test]
    fn test_should_ignore_container_tags() {
        // given
        let crawler = Crawler;

        // when
        let page = crawler
            .parse_page("<html><body><div><p>Content with <strong>strong</strong>, <i>i</i>, <b>b</b> and <span>span</span></p></div></body></html>");

        // then
        assert_eq!(1, page.len());
        assert_eq!(
            [
                "Content with ",
                "strong",
                ", ",
                "i",
                ", ",
                "b",
                " and ",
                "span"
            ],
            page.get("p").unwrap().as_slice()
        );
    }

    #[test]
    fn test_should_ignore_unsupported_tags() {
        // given
        let crawler = Crawler;

        // when
        let page = crawler.parse_page("<html><body><svg>abc</svg><div>some</div></body></html>");

        // then
        assert!(page.is_empty());
    }

    #[test]
    fn test_should_normalize_text() {
        // given
        let crawler = Crawler;

        // when
        let element = crawler.normalize_element("Test. Abc: xyz! 123 1% (abc) äöüá");

        // then
        assert_eq!("test abc xyz 123 1 abc aoua", element);
    }

    #[test]
    fn test_should_calculate_score_for_paragraph() {
        // given
        let crawler = Crawler;
        let mut page = BTreeMap::new();
        page.insert(
            "p".to_string(),
            vec![
                "abc".to_string(),
                "ABC".to_string(),
                "AbcAbc!".to_string(),
                "xyz".to_string(),
            ],
        );

        // when
        let score = crawler.calculate_score(&page, "abc");

        // then
        assert_eq!(4.0, score);
    }
}
