use crate::{
    components::{footer::Footer, text::Text},
    exif::generate_exif_from_config,
    io::import::{load_fonts, load_image_from_http, load_image_from_memory},
};
use cosmic_text::{Color, FontSystem, SwashCache};
use image::{
    codecs::jpeg::JpegEncoder, imageops, DynamicImage, GenericImage, GenericImageView, Rgb,
    RgbImage, Rgba,
};
use img_parts::{ImageEXIF, ImageICC};

const ICC_SRGB: &[u8] = include_bytes!("../../resources/icc/sRGB-v4.icc");

pub struct ColorBackground {
    pub color: Color,
}

pub enum BackgroundAlign {
    Zoom,
}

pub enum Mirroring {
    None,
    Horizontal,
    Vertical,
}

pub struct ImageBackground {
    pub path: String,
    pub align: BackgroundAlign,
    pub mirroring: Mirroring,
    pub contrast: f32,
    pub brightness: f32,
    pub offset_x: f32,
    pub offset_y: f32,
    pub blur: u32,
}

impl Default for ImageBackground {
    fn default() -> Self {
        Self {
            path: "".to_string(),
            align: BackgroundAlign::Zoom,
            mirroring: Mirroring::None,
            brightness: 0.0,
            contrast: 0.0,
            offset_x: 0.0,
            offset_y: 0.0,
            blur: 0,
        }
    }
}

pub enum CanvasBackground {
    Image(ImageBackground),
    Color(ColorBackground),
}

pub struct Margins {
    pub top: u32,
    pub bottom: u32,
    pub left: u32,
    pub right: u32,
}

impl Margins {
    pub fn top(&self) -> u32 {
        self.top
    }

    pub fn bottom(&self) -> u32 {
        self.bottom
    }

    pub fn right(&self) -> u32 {
        self.right
    }

    pub fn left(&self) -> u32 {
        self.left
    }
}

pub struct Canvas {
    size: (u32, u32),
    background: CanvasBackground,
    texts: Vec<Text>,
    footer: Option<Footer>,
    margins: Margins,
    config: String,
    sharpen: f32,
}

impl Default for Canvas {
    fn default() -> Self {
        Self {
            sharpen: 0.5,
            size: (1280, 720),
            background: CanvasBackground::Color(ColorBackground {
                color: Color::rgb(10u8, 30u8, 230u8),
            }),
            texts: Vec::new(),
            footer: Some(Footer::default()),
            margins: Margins {
                top: 100,
                bottom: 100,
                left: 100,
                right: 100,
            },
            config: "".to_string(),
        }
    }
}

impl Canvas {
    pub fn set_config(&mut self, config: &str) {
        self.config = config.to_string();
    }

    pub fn set_size(&mut self, width: u32, height: u32) {
        self.size = (width, height);
    }

    pub fn width(&self) -> u32 {
        self.size.0
    }

    pub fn height(&self) -> u32 {
        self.size.1
    }

    pub fn texts(&self) -> &[Text] {
        &self.texts
    }

    pub fn texts_mut(&mut self) -> &mut Vec<Text> {
        &mut self.texts
    }

    pub fn set_footer(&mut self, footer: Option<Footer>) {
        self.footer = footer;
    }

    pub fn footer(&self) -> &Option<Footer> {
        &self.footer
    }

    pub fn set_margins(&mut self, margins: Margins) {
        self.margins = margins;
    }

    pub fn margins(&self) -> &Margins {
        &self.margins
    }

    pub fn set_background(&mut self, background: CanvasBackground) {
        self.background = background;
    }

    pub fn background(&self) -> &CanvasBackground {
        &self.background
    }

    pub async fn export_as_jpeg_file(
        &mut self,
        destination: &str,
    ) -> Result<(), Box<dyn std::error::Error + std::marker::Send + Sync>> {
        let img = self.export_as_jpeg_bytes().await?;

        std::fs::write(destination, img)?;

        Ok(())
    }

    pub async fn export_as_jpeg_bytes(
        &mut self,
    ) -> Result<Vec<u8>, Box<dyn std::error::Error + std::marker::Send + Sync>> {
        let img = self.render_image().await;

        // encode raw image as jpeg
        let mut jpeg_bytes = Vec::new();
        let mut encoder = JpegEncoder::new_with_quality(&mut jpeg_bytes, 97);
        encoder.encode(
            &img.into_bytes(),
            self.width(),
            self.height(),
            image::ExtendedColorType::Rgb8,
        )?;

        // build jpeg withe EXIF and ICC data
        let mut jpeg = img_parts::jpeg::Jpeg::from_bytes(jpeg_bytes.into())?;
        let exif_bytes = generate_exif_from_config(&self.config)?;
        jpeg.set_exif(Some(exif_bytes.into()));
        jpeg.set_icc_profile(Some(ICC_SRGB.to_vec().into()));

        Ok(jpeg.encoder().bytes().into())
    }

    async fn render_image(&mut self) -> DynamicImage {
        let mut img = match &self.background {
            CanvasBackground::Image(image_background) => {
                self.load_background_image(image_background).await
            }
            CanvasBackground::Color(color_background) => {
                self.load_background_color(color_background)
            }
        };

        let mut font_system = load_fonts();
        let mut swash_cache = SwashCache::new();

        // draw footer
        if let Some(footer) = self.footer() {
            footer.draw(&mut img, &mut font_system, &mut swash_cache, self);
            self.margins.bottom +=
                ((footer.logo_size + footer.padding * 2.0) * footer.scale) as u32;
        }

        // draw texts
        for text in self.texts.iter() {
            text.draw(&mut img, &mut font_system, &mut swash_cache, self);
        }

        self.sharpen_image(img)
    }

    fn sharpen_image(&mut self, img: DynamicImage) -> DynamicImage {
        let border = 5;
        let mut expanded_image = DynamicImage::ImageRgb8(RgbImage::from_pixel(
            img.width() + border * 2,
            img.height() + border * 2,
            Rgb([0, 0, 0]),
        ));

        for x in 0..img.width() + border * 2 {
            for y in 0..img.height() + border * 2 {
                if x < border
                    || y < border
                    || x >= img.width() + border
                    || y >= img.height() + border
                {
                    expanded_image.put_pixel(x, y, Rgba([255u8, 255u8, 255u8, 255u8]));
                } else {
                    expanded_image.put_pixel(x, y, img.get_pixel(x - border, y - border));
                }
            }
        }

        expanded_image
            .filter3x3(&[
                0.0,
                -1.0 * self.sharpen / 4.0,
                0.0,
                -1.0 * self.sharpen / 4.0,
                1.0 + self.sharpen,
                -1.0 * self.sharpen / 4.0,
                0.0,
                -1.0 * self.sharpen / 4.0,
                0.0,
            ])
            .crop_imm(border, border, img.width(), img.height())
    }

    fn load_background_color(&self, color_background: &ColorBackground) -> DynamicImage {
        DynamicImage::ImageRgb8(RgbImage::from_pixel(
            self.width(),
            self.height(),
            Rgb([
                color_background.color.r(),
                color_background.color.g(),
                color_background.color.b(),
            ]),
        ))
    }

    async fn load_background_image(&self, image_background: &ImageBackground) -> DynamicImage {
        let mut background = match image_background.path.as_str() {
            "alps.jpg" => load_image_from_memory(include_bytes!("../../resources/images/alps.jpg")),
            "desert.jpg" => {
                load_image_from_memory(include_bytes!("../../resources/images/desert.jpg"))
            }
            path => load_image_from_http(path).await,
        };
        //  load_image(&image_background.path);
        match &image_background.mirroring {
            Mirroring::Horizontal => {
                background = background.fliph();
            }
            Mirroring::Vertical => {
                background = background.flipv();
            }
            _ => {}
        }

        background = background
            .adjust_contrast(image_background.contrast)
            .brighten(image_background.brightness as i32)
            .blur(image_background.blur as f32);

        match image_background.align {
            BackgroundAlign::Zoom => background.resize_to_fill(
                self.width(),
                self.height(),
                imageops::FilterType::CatmullRom,
            ),
        }
    }
}

pub trait Drawable {
    fn draw(
        &self,
        img: &mut DynamicImage,
        font_system: &mut FontSystem,
        swash_cache: &mut SwashCache,
        canvas: &Canvas,
    );
}

#[cfg(test)]
mod tests {
    use super::{
        BackgroundAlign, Canvas, CanvasBackground, ColorBackground, ImageBackground, Margins,
    };
    use crate::components::{
        footer::Footer,
        text::{Text, TextPosition},
    };
    use cosmic_text::{Color, Family, Style, Weight};
    use image_hasher::HasherConfig;
    use std::vec;

    const BG_COLOR: &Color = &Color::rgb(120u8, 150u8, 180u8);
    const TEST_RENDERS_FOLDER: &str = "test_renders";

    #[tokio::test]
    async fn should_render_quad_preview_with_background() {
        // given
        let mut canvas = Canvas::default();
        canvas.set_size(400, 400);
        canvas.set_footer(None);
        canvas.set_background(CanvasBackground::Color(ColorBackground {
            color: *BG_COLOR,
        }));

        // when & then
        assert_canvas_export("quad_preview_with_background", &mut canvas).await;
    }

    #[tokio::test]
    async fn should_render_quad_preview_with_zoomed_and_low_contrast_image() {
        // given
        let mut canvas = Canvas::default();
        canvas.set_size(400, 400);
        canvas.set_footer(None);
        canvas.set_background(CanvasBackground::Image(ImageBackground {
            path: "alps.jpg".to_string(),
            contrast: -20.0,
            ..Default::default()
        }));

        // when & then
        assert_canvas_export(
            "quad_preview_with_zoomed_and_low_contrast_image",
            &mut canvas,
        )
        .await;
    }

    #[tokio::test]
    async fn should_render_insta_story_with_background() {
        // given
        let mut canvas = Canvas::default();
        canvas.set_size(1080, 1920);
        canvas.set_footer(None);
        canvas.set_background(CanvasBackground::Color(ColorBackground {
            color: *BG_COLOR,
        }));
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Farsi-Bibelkreis".to_string()],
            font_size: 96.0,
            font_weight: Weight::BOLD,
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec![
                "Medium Words\nAbc\nXyz".to_string(),
                "another line".to_string(),
            ],
            font_size: 72.0,
            line_spacing: 1.8,
            position: TextPosition::Middle,
            ..Default::default()
        });

        // when & then
        assert_canvas_export("insta_story_with_background", &mut canvas).await;
    }

    #[tokio::test]
    async fn should_render_infoscreen_dark_with_background() {
        // given
        let mut canvas = Canvas::default();
        canvas.set_size(1920, 1080);
        canvas.set_footer(None);
        canvas.set_background(CanvasBackground::Image(ImageBackground {
            path: "desert.jpg".to_string(),
            align: BackgroundAlign::Zoom,
            mirroring: super::Mirroring::Horizontal,
            brightness: -40.0,
            ..Default::default()
        }));
        canvas.texts_mut().push(Text {
            paragraphs: vec!["So wie".to_string(), "ich bin...".to_string()],
            font_size: 160.0,
            line_spacing: 1.2,
            font_weight: Weight::BOLD,
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Begeistert. Begabt. Einzigartig. äöüß".to_string()],
            font_size: 80.0,
            position: TextPosition::Bottom,
            ..Default::default()
        });

        // when & then
        assert_canvas_export("infoscreen_dark_with_background", &mut canvas).await;
    }

    #[tokio::test]
    async fn should_render_infoscreen_with_http_background() {
        // given
        let mut canvas = Canvas::default();
        canvas.set_size(600, 600);
        canvas.set_footer(None);
        canvas.set_background(CanvasBackground::Image(ImageBackground {
            path:
                "https://www.efg-erfurt.de/public/assets/72cec5c3-9564-45c0-92b0-5abf6c006df5.jpg"
                    .to_string(),
            ..Default::default()
        }));

        // when & then
        assert_canvas_export("infoscreen_with_http_background", &mut canvas).await;
    }

    #[tokio::test]
    async fn should_render_insta_story_with_zoomed_and_blurred_image() {
        // given
        let mut canvas = Canvas::default();
        canvas.set_size(1080, 1920);
        canvas.set_margins(Margins {
            top: 30,
            left: 30,
            right: 30,
            bottom: 30,
        });
        canvas.set_footer(None);
        canvas.set_background(CanvasBackground::Image(ImageBackground {
            path: "alps.jpg".to_string(),
            blur: 8,
            brightness: 40.0,
            ..Default::default()
        }));
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Farsi-Bibelkreis".to_string()],
            font_size: 128.0,
            font_color: Color::rgb(30u8, 30u8, 30u8),
            font_weight: Weight::BOLD,
            position: TextPosition::Top,
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Middle with Line, which should break".to_string()],
            font_size: 72.0,
            line_spacing: 1.0,
            position: TextPosition::Middle,
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Bottom".to_string()],
            font_size: 72.0,
            position: TextPosition::Bottom,
            ..Default::default()
        });

        // when & then
        assert_canvas_export("insta_story_with_zoomed_and_blurred_image", &mut canvas).await;
    }

    #[tokio::test]
    async fn should_render_infoscreen_with_background() {
        // given
        let mut canvas = Canvas::default();
        canvas.set_size(1920, 1080);
        canvas.set_background(CanvasBackground::Color(ColorBackground {
            color: *BG_COLOR,
        }));
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Extra Bold".to_string()],
            font_size: 80.0,
            font_style: Style::Normal,
            font_weight: Weight::EXTRA_BOLD,
            offset_y: 100.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Bold".to_string()],
            font_size: 80.0,
            font_style: Style::Normal,
            font_weight: Weight::BOLD,
            offset_y: 200.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Medium".to_string()],
            font_size: 80.0,
            font_style: Style::Normal,
            font_weight: Weight::MEDIUM,
            offset_y: 300.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Normal".to_string()],
            font_size: 80.0,
            font_style: Style::Normal,
            font_weight: Weight::NORMAL,
            offset_y: 400.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Light".to_string()],
            font_size: 80.0,
            font_style: Style::Normal,
            font_weight: Weight::LIGHT,
            offset_y: 500.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Extra Bold".to_string()],
            font_size: 80.0,
            font_style: Style::Italic,
            offset_x: 500.0,
            font_weight: Weight::EXTRA_BOLD,
            offset_y: 100.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Bold".to_string()],
            font_size: 80.0,
            font_style: Style::Italic,
            offset_x: 500.0,
            font_weight: Weight::BOLD,
            offset_y: 200.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Medium".to_string()],
            font_size: 80.0,
            font_style: Style::Italic,
            offset_x: 500.0,
            font_weight: Weight::MEDIUM,
            offset_y: 300.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Normal".to_string()],
            font_size: 80.0,
            font_style: Style::Italic,
            offset_x: 500.0,
            font_weight: Weight::NORMAL,
            offset_y: 400.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Light".to_string()],
            font_size: 80.0,
            font_style: Style::Italic,
            offset_x: 500.0,
            font_weight: Weight::LIGHT,
            offset_y: 500.0,
            font_family: Family::Name("Open Sans"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["EFG Erfurt".to_string()],
            font_size: 80.0,
            font_style: Style::Normal,
            offset_x: 1000.0,
            font_weight: Weight::BOLD,
            offset_y: 100.0,
            font_family: Family::Name("Vollkorn"),
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["EFG Erfurt".to_string()],
            font_size: 80.0,
            font_style: Style::Normal,
            offset_x: 1000.0,
            font_weight: Weight::MEDIUM,
            offset_y: 200.0,
            font_family: Family::Name("Vollkorn"),
            ..Default::default()
        });

        // when & then
        assert_canvas_export("infoscreen_with_background", &mut canvas).await;
    }

    #[tokio::test]
    async fn should_render_infoscreen_with_zoomed_image() {
        // given
        let mut canvas = Canvas::default();
        canvas.set_footer(Some(Footer {
            scale: 2.0,
            padding: 5.0,
            logo_size: 100.0,
        }));
        canvas.set_size(1920, 1080);
        canvas.set_background(CanvasBackground::Image(ImageBackground {
            path: "alps.jpg".to_string(),
            ..Default::default()
        }));
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Some Headline".to_string()],
            font_size: 96.0,
            position: TextPosition::Top,
            font_style: Style::Normal,
            font_weight: Weight::BOLD,
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Points...".to_string()],
            font_size: 36.0,
            position: TextPosition::Middle,
            ..Default::default()
        });
        canvas.texts_mut().push(Text {
            paragraphs: vec!["Ansprechpartner: XYZ".to_string()],
            font_size: 36.0,
            position: TextPosition::Bottom,
            ..Default::default()
        });

        // when & then
        assert_canvas_export("infoscreen_with_zoomed_image", &mut canvas).await;
    }

    #[tokio::test]
    async fn should_render_custom_with_scaled_footer() {
        // given
        let footer = Footer {
            // TODO check why scaling for scale < 0.4 leads to an overflow!
            scale: 0.4,
            ..Default::default()
        };

        let mut canvas = Canvas::default();
        canvas.set_config("{}");
        canvas.set_size(800, 600);
        canvas.set_footer(Some(footer));
        canvas.set_background(CanvasBackground::Color(ColorBackground {
            color: *BG_COLOR,
        }));

        // when & then
        assert_canvas_export("custom_with_scaled_footer", &mut canvas).await;
    }

    async fn assert_canvas_export(name: &str, canvas: &mut Canvas) {
        let path = format!("{TEST_RENDERS_FOLDER}/{name}.jpg");

        // when
        canvas
            .export_as_jpeg_file(&path)
            .await
            .expect("writing image failed");

        // then

        // save image to test renders and assert snapshot of image
        let image = image::open(path.clone()).unwrap();
        assert_eq!(canvas.width(), image.width(), "image width differs");
        assert_eq!(canvas.height(), image.height(), "image height differs");

        let hasher = HasherConfig::new().to_hasher();
        let hash = hasher.hash_image(&image);
        insta::assert_snapshot!(name, hash.to_base64());

        // load image and assert snapshot of all EXIF data
        let file = std::fs::File::open(path).unwrap();
        let mut bufreader = std::io::BufReader::new(&file);
        let exif_reader = exif::Reader::new();
        let exif = exif_reader.read_from_container(&mut bufreader).unwrap();

        let mut exif_data = Vec::new();
        for f in exif.fields() {
            exif_data.push(format!(
                "{} ({}): {}",
                f.tag,
                f.ifd_num,
                f.display_value().with_unit(&exif)
            ));
        }
        insta::assert_snapshot!(format!("{}_exif", name), exif_data.join("\n"));
    }
}
