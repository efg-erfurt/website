use cosmic_text::FontSystem;
use image::DynamicImage;

pub fn load_image_from_memory(bytes: &[u8]) -> DynamicImage {
    image::load_from_memory(bytes).unwrap()
}

pub fn load_image(path: &str) -> DynamicImage {
    image::open(path).unwrap()
}

pub async fn load_image_from_http(url: &str) -> DynamicImage {
    let resp = reqwest::get(url).await.unwrap();
    let bytes = resp.bytes().await.unwrap();
    image::load_from_memory(&bytes).unwrap()
}

pub fn load_fonts() -> FontSystem {
    let mut font_system = FontSystem::new();

    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/vollkorn/Vollkorn-Bold.ttf").to_vec(),
    );
    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/vollkorn/Vollkorn-Regular.ttf").to_vec(),
    );
    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/open-sans/OpenSans-Regular.ttf").to_vec(),
    );
    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/open-sans/OpenSans-Italic.ttf").to_vec(),
    );
    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/open-sans/OpenSans-Bold.ttf").to_vec(),
    );
    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/open-sans/OpenSans-BoldItalic.ttf").to_vec(),
    );
    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/open-sans/OpenSans-ExtraBold.ttf").to_vec(),
    );
    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/open-sans/OpenSans-ExtraBoldItalic.ttf").to_vec(),
    );
    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/open-sans/OpenSans-Light.ttf").to_vec(),
    );
    font_system.db_mut().load_font_data(
        include_bytes!("../../resources/fonts/open-sans/OpenSans-LightItalic.ttf").to_vec(),
    );

    font_system
}
