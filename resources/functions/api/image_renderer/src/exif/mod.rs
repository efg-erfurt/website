use std::io::Cursor;

use exif::{In, Tag};
use img_parts::ImageEXIF;

pub async fn load_config_from_image_exif(
    url: &str,
) -> Result<Option<String>, Box<dyn std::error::Error + std::marker::Send + Sync>> {
    let response = reqwest::get(url).await?;
    let image_bytes = response.bytes().await?;

    let image = img_parts::DynImage::from_bytes(image_bytes)?;
    if image.is_none() {
        return Ok(None);
    }
    let image = image.unwrap();
    let exif = image.exif();
    if exif.is_none() {
        return Ok(None);
    }
    let exif_reader = exif::Reader::new();
    let exif = exif_reader.read_raw(exif.unwrap().to_vec())?;

    let field = exif.get_field(Tag::MakerNote, In::PRIMARY);

    let config = field.map(|field| field.value.display_as(Tag::MakerNote).to_string());

    Ok(config)
}

pub fn generate_exif_from_config(
    config: &str,
) -> Result<Vec<u8>, Box<dyn std::error::Error + std::marker::Send + Sync>> {
    let mut writer = exif::experimental::Writer::new();
    let mut buf = Cursor::new(Vec::new());
    let image_desc = exif::Field {
        tag: exif::Tag::MakerNote,
        ifd_num: exif::In::PRIMARY,
        value: exif::Value::Ascii(vec![config.bytes().collect()]),
    };
    writer.push_field(&image_desc);
    let software = exif::Field {
        tag: exif::Tag::Software,
        ifd_num: exif::In::PRIMARY,
        value: exif::Value::Ascii(vec![b"EFG Erfurt".to_vec()]),
    };
    writer.push_field(&software);
    writer.write(&mut buf, false)?;

    Ok(buf.into_inner().to_vec())
}
