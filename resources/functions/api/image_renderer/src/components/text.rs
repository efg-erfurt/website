use crate::canvas::{Canvas, Drawable};
use cosmic_text::{
    Attrs, AttrsList, BidiParagraphs, BorrowedWithFontSystem, Buffer, BufferLine, Color, Family,
    Metrics, Shaping, Stretch, Style, SwashCache, Weight,
};
use image::{DynamicImage, GenericImage, GenericImageView, Pixel, Rgba};

pub enum TextPosition {
    Top,
    Middle,
    Bottom,
}

pub struct Text {
    pub paragraphs: Vec<String>,
    pub font_size: f32,
    pub line_spacing: f32,
    pub font_color: Color,
    pub font_family: Family<'static>,
    pub font_weight: Weight,
    pub font_style: Style,
    pub font_stretch: Stretch,
    pub offset_y: f32,
    pub offset_x: f32,
    pub position: TextPosition,
}

impl Default for Text {
    fn default() -> Self {
        Self {
            paragraphs: Default::default(),
            font_size: 32.0,
            line_spacing: 1.0,
            font_color: Color::rgb(250u8, 250u8, 250u8),
            font_family: Family::Name("Open Sans"),
            font_style: Style::Italic,
            font_weight: Weight::NORMAL,
            font_stretch: Stretch::Normal,
            offset_y: 0.0,
            offset_x: 0.0,
            position: TextPosition::Top,
        }
    }
}

impl Drawable for Text {
    fn draw(
        &self,
        img: &mut image::DynamicImage,
        font_system: &mut cosmic_text::FontSystem,
        swash_cache: &mut cosmic_text::SwashCache,
        canvas: &Canvas,
    ) {
        let metrics = Metrics::new(self.font_size, self.font_size * self.line_spacing);

        let attrs = Attrs::new()
            .family(self.font_family)
            .weight(self.font_weight)
            .stretch(self.font_stretch)
            .style(self.font_style);

        let mut buffer = Buffer::new(font_system, metrics);
        let mut buffer = buffer.borrow_with(font_system);

        buffer.set_size(
            Some(
                if img.width() >= canvas.margins().left + canvas.margins().right {
                    img.width() - (canvas.margins().left + canvas.margins().right)
                } else {
                    img.width()
                } as f32,
            ),
            Some(
                if img.height() >= canvas.margins().top + canvas.margins().bottom {
                    img.height() - (canvas.margins().top + canvas.margins().bottom)
                } else {
                    img.height()
                } as f32,
            ),
        );
        buffer.lines.clear();

        for paragraph in self.paragraphs.iter() {
            for line in BidiParagraphs::new(paragraph) {
                // let mut attrs_list = AttrsList::new(attrs);
                // TODO support advanced styling
                // attrs = attrs.color(Color::rgb(255u8, 0u8, 0u8));
                // attrs_list.add_span(std::ops::Range { start: 0, end: 10 }, attrs);
                buffer.lines.push(BufferLine::new(
                    line.to_string(),
                    cosmic_text::LineEnding::Lf,
                    AttrsList::new(attrs),
                    Shaping::Advanced,
                ));
            }
        }

        buffer.shape_until_scroll(false);

        let tmp_image = render_cropped_buffer(img, self.font_color, &mut buffer, swash_cache);

        blend_image(
            tmp_image,
            canvas,
            &self.position,
            self.offset_x,
            self.offset_y,
            img,
        );
    }
}

fn blend_image(
    tmp_image: DynamicImage,
    canvas: &Canvas,
    position: &TextPosition,
    offset_x: f32,
    offset_y: f32,
    img: &mut DynamicImage,
) {
    let offset_y = (match position {
        TextPosition::Top => canvas.margins().top(),
        TextPosition::Middle => {
            (if canvas.margins().top() + img.height()
                >= tmp_image.height() + canvas.margins().bottom()
            {
                canvas.margins().top() + img.height()
                    - tmp_image.height()
                    - canvas.margins().bottom()
            } else {
                canvas.margins().top() + img.height()
            }) / 2
        }
        TextPosition::Bottom => {
            if img.height() >= tmp_image.height() + canvas.margins().bottom {
                img.height() - tmp_image.height() - canvas.margins().bottom()
            } else {
                img.height()
            }
        }
    } as f32
        + offset_y) as u32;
    let offset_x = canvas.margins().left() + offset_x as u32;

    for x in 0..tmp_image.width() {
        for y in 0..tmp_image.height() {
            let pixel_to_copy = tmp_image.get_pixel(x, y);

            let x = x + offset_x;
            let y = y + offset_y;
            if x >= img.width() || y >= img.height() {
                continue;
            }

            let mut existing_pixel = img.get_pixel(x, y);
            existing_pixel.blend(&pixel_to_copy);
            img.put_pixel(x, y, existing_pixel);
        }
    }
}

fn render_cropped_buffer(
    img: &DynamicImage,
    font_color: Color,
    buffer: &mut BorrowedWithFontSystem<'_, Buffer>,
    swash_cache: &mut SwashCache,
) -> DynamicImage {
    let mut tmp_image = DynamicImage::new_rgba8(img.width(), img.height());

    buffer.draw(swash_cache, font_color, |x, y, _w, _h, color| {
        // somehow they can be -1
        if x < 0 || y < 0 {
            return;
        }

        let x = x as u32;
        let y = y as u32;

        let new_pixel = Rgba([color.r(), color.g(), color.b(), color.a()]);
        let mut existing_pixel = tmp_image.get_pixel(x, y);
        existing_pixel.blend(&new_pixel);
        tmp_image.put_pixel(x, y, existing_pixel);
    });

    // crop from bottom to top
    let mut end_y = img.height();
    for y in (0..tmp_image.height()).rev() {
        let mut is_empty_line = true;
        for x in 0..tmp_image.width() {
            let is_used = tmp_image.get_pixel(x, y)[3] > 0u8;
            if is_used {
                is_empty_line = false;
                break;
            }
        }
        if !is_empty_line {
            break;
        } else {
            end_y = y;
        }
    }

    // crop from right to left
    let mut end_x = img.width();
    for x in (0..tmp_image.width()).rev() {
        let mut is_empty_line = false;
        for y in 0..tmp_image.height() {
            let is_used = tmp_image.get_pixel(x, y)[3] > 0u8;
            if is_used {
                is_empty_line = false;
                break;
            }
        }
        if !is_empty_line {
            break;
        } else {
            end_x = x;
        }
    }

    tmp_image.crop_imm(0, 0, end_x, end_y)
}
