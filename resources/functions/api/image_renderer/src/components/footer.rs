use crate::{
    canvas::{Canvas, Drawable},
    io::import::load_image_from_memory,
};
use cosmic_text::{Attrs, Buffer, Color, Family, FontSystem, Metrics, Shaping, SwashCache, Weight};
use image::{imageops::FilterType, DynamicImage, GenericImage, GenericImageView, Pixel, Rgba};

#[derive(Clone, Copy)]
pub struct Footer {
    pub scale: f32,
    pub padding: f32,
    pub logo_size: f32,
}

impl Default for Footer {
    fn default() -> Self {
        Self {
            scale: 1.0,
            padding: 10.0,
            logo_size: 100.0,
        }
    }
}

impl Drawable for Footer {
    fn draw(
        &self,
        img: &mut DynamicImage,
        font_system: &mut FontSystem,
        swash_cache: &mut SwashCache,
        _canvas: &Canvas,
    ) {
        // draw background
        let scaled_logo_size = (self.logo_size * self.scale) as u32;
        let logo_margin = (2.0 * self.padding * self.scale) as u32;
        let start_y = if img.height() >= scaled_logo_size + logo_margin {
            img.height() - scaled_logo_size - logo_margin
        } else {
            img.height()
        };
        for x in 0..img.width() {
            let alpha = 255u8 - (150 * x / img.width()) as u8;
            for y in start_y..img.height() {
                let mut existing_pixel = img.get_pixel(x, y);
                existing_pixel.blend(&Rgba([255u8, 255u8, 255u8, alpha]));
                img.put_pixel(x, y, existing_pixel);
            }
        }

        let logo = self.draw_logo(img);

        // shared
        let font_size = 28.0 * self.scale;
        let color = Color::rgb(0u8, 0u8, 0u8);
        let font_attrs = Attrs::new()
            .family(Family::Name("Open Sans"))
            .weight(Weight::NORMAL);
        let offset_x = logo.width() + (2.0 * self.padding * self.scale) as u32;

        // Name
        let offset_y = img.height() - scaled_logo_size + (self.padding * self.scale / 2.0) as u32;
        self.draw_text(
            "Evangelisch-Freikirchliche Gemeinde Erfurt (Baptisten)",
            font_attrs,
            &color,
            font_size,
            offset_x,
            offset_y,
            img,
            font_system,
            swash_cache,
        );

        // Address
        let offset_y = offset_y + (self.scale * self.padding) as u32 + font_size as u32;
        self.draw_text(
            "Magdeburger Allee 10, Erfurt",
            font_attrs,
            &color,
            font_size,
            offset_x,
            offset_y,
            img,
            font_system,
            swash_cache,
        );
    }
}

impl Footer {
    fn draw_logo(&self, img: &mut DynamicImage) -> DynamicImage {
        let scaled_logo_size = (self.logo_size * self.scale) as u32;
        let mut logo = load_image_from_memory(include_bytes!("../../resources/images/logo.png"));
        logo = logo.resize(scaled_logo_size, scaled_logo_size, FilterType::CatmullRom);

        let offset_x = (self.scale * self.padding) as u32;
        let offset_y = img.height() - scaled_logo_size - (self.padding * self.scale) as u32;
        for x in 0..logo.width() {
            for y in 0..logo.height() {
                let logo_pixel = logo.get_pixel(x, y);
                let mut existing_pixel = img.get_pixel(x + offset_x, y + offset_y);
                existing_pixel.blend(&logo_pixel);
                img.put_pixel(x + offset_x, y + offset_y, existing_pixel);
            }
        }

        logo
    }

    fn draw_text(
        &self,
        text: &str,
        font_attrs: Attrs<'_>,
        color: &Color,
        font_size: f32,
        offset_x: u32,
        offset_y: u32,
        img: &mut DynamicImage,
        font_system: &mut FontSystem,
        swash_cache: &mut SwashCache,
    ) {
        let metrics = Metrics::new(font_size, font_size);
        let mut buffer = Buffer::new(font_system, metrics);
        let mut buffer = buffer.borrow_with(font_system);
        buffer.set_size(Some(img.width() as f32), Some(font_size));
        buffer.set_text(text, font_attrs, Shaping::Advanced);

        buffer.draw(swash_cache, *color, |x, y, _w, _h, color| {
            let x = x as u32 + offset_x;
            let y = y as u32 + offset_y;

            // skip rendering of image borders
            if x < img.width() && y < img.height() {
                let mut existing_pixel = img.get_pixel(x, y);

                existing_pixel.blend(&Rgba([color.r(), color.g(), color.b(), color.a()]));

                img.put_pixel(x, y, existing_pixel);
            } else {
                // TODO handle rendering over edges
            }
        });
    }
}
