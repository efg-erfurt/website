#![forbid(unsafe_code)]

use async_graphql::{EmptySubscription, Schema};
use efg_lib::schema::{mutation::Mutation, query::QueryRoot};
use insta::assert_snapshot;
use std::{fs::File, io::Write};

const SCHEMA_FILE: &str = "schema.gql";

#[test]
fn test_should_verify_up_to_date_sdl() {
    // given
    let schema = Schema::build(QueryRoot, Mutation, EmptySubscription).finish();

    // when
    let new_sdl = schema.sdl();

    let mut new_file = File::create(SCHEMA_FILE).expect("opening file failed");
    new_file
        .write_all(new_sdl.as_bytes())
        .expect("writing failed");

    // then
    assert_snapshot!(new_sdl);
}
