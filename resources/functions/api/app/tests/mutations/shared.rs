use crate::{query::build_schema, util::build_auth_context};
use async_trait::async_trait;
use efg_lambda_base::errors::AppError;
use efg_lib::{
    clients::{
        auth::AuthService,
        cache::{cache_entry::CacheEntry, CacheClientImpl, CacheService},
        mail::{MailClient, MailService},
    },
    create_auth_client_mock,
    schema::types::send_message_input::SendMessageInput,
};
use efg_testing::create_db_mock;
use insta::assert_json_snapshot;

#[tokio::test]
async fn test_should_clear_cache() {
    // given
    let (cache_mock, mut cache_mock_collector) = create_db_mock!(
        CacheEntry,
        MockCacheClient,
        async fn delete_all(&self) -> Result<(), AppError> {
            Ok(())
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;

    let schema = build_schema()
        .data::<CacheService>(Box::new(CacheClientImpl::new(cache_mock)))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema.execute("mutation { clearCache }").await;

    // then
    assert_json_snapshot!(result);
    cache_mock_collector.collect_calls();
    assert_eq!(1, cache_mock_collector.get_calls_by_method("delete_all"));
}

#[tokio::test]
async fn test_should_send_message_with_name_and_mail() {
    // given
    struct MockMailClient;

    #[async_trait]
    impl MailClient for MockMailClient {
        async fn send_mail(&self, _message_input: &SendMessageInput) -> Result<(), AppError> {
            Ok(())
        }
    }

    let mock_mail_client: MailService = Box::new(MockMailClient {});
    let schema = build_schema().data(mock_mail_client).finish();

    // when
    let result = schema
        .execute(
            "mutation { sendMessage(message: {
                name: \"Some Body\",
                mail: \"test@test.de\",
                destination: \"buero\",
                message: \"Some important message!\",
        })}",
        )
        .await;

    // then
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_send_message_without_name_and_mail() {
    // given
    struct MockMailClient;

    #[async_trait]
    impl MailClient for MockMailClient {
        async fn send_mail(&self, _message_input: &SendMessageInput) -> Result<(), AppError> {
            Ok(())
        }
    }

    let mock_mail_client: MailService = Box::new(MockMailClient {});
    let schema = build_schema().data(mock_mail_client).finish();

    // when
    let result = schema
        .execute(
            "mutation { sendMessage(message: {
                destination: \"buero\",
                message: \"Some important message!\",
        })}",
        )
        .await;

    // then
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_not_send_message_without_message() {
    // given
    struct MockMailClient;

    #[async_trait]
    impl MailClient for MockMailClient {}

    let mock_mail_client: MailService = Box::new(MockMailClient {});
    let schema = build_schema().data(mock_mail_client).finish();

    // when
    let result = schema
        .execute(
            "mutation { sendMessage(message: {
                destination: \"buero\",
        })}",
        )
        .await;

    // then
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_not_send_message_without_destination() {
    // given
    struct MockMailClient;

    #[async_trait]
    impl MailClient for MockMailClient {}

    let mock_mail_client: MailService = Box::new(MockMailClient {});
    let schema = build_schema().data(mock_mail_client).finish();

    // when
    let result = schema
        .execute(
            "mutation { sendMessage(message: {
                message: \"Some important message!\",
        })}",
        )
        .await;

    // then
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_not_send_message_with_invalid_mail() {
    // given
    struct MockMailClient;

    #[async_trait]
    impl MailClient for MockMailClient {}

    let mock_mail_client: MailService = Box::new(MockMailClient {});
    let schema = build_schema().data(mock_mail_client).finish();

    // when
    let result = schema
        .execute(
            "mutation { sendMessage(message: {
                destination: \"buero\",
                mail: \"test@test\",
                message: \"Some important message!\",
        })}",
        )
        .await;

    // then
    assert_json_snapshot!(result);
}
