use crate::{query::build_schema, util::build_auth_context};
use efg_db_client::db::DbService;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
use efg_lib::{
    clients::{auth::AuthService, ct::CtService, http::HttpService},
    create_auth_client_mock, create_ct_rest_mock, create_http_client_mock,
    create_object_client_mock,
    schema::types::content::Content,
    types::ct::{CtWhoAmIResponse, CtWhoAmIResponseData},
};
use efg_object_client::db::ObjectService;
use efg_testing::create_db_mock;
use insta::assert_json_snapshot;
use std::{collections::BTreeMap, fs::File, sync::Arc};
use time::OffsetDateTime;

#[tokio::test]
async fn test_should_edit_monitor_internally() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn put_item(&self, item: Content) -> Result<(), AppError> {
            if !item.key.eq("monitor.INTERNAL.paths") {
                panic!("invalid key \"{}\"", item.key)
            } else if !item.content.eq("a.jpg,b.jpg") {
                panic!("invalid content")
            } else {
                Ok(())
            }
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_current_user(
            &self,
            _auth_context: &AuthContext,
        ) -> Result<Option<CtWhoAmIResponse>, AppError> {
            Ok(Some(CtWhoAmIResponse {
                data: CtWhoAmIResponseData {
                    id: 123,
                    firstName: "X".to_string(),
                    lastName: "Y".to_string(),
                    imageUrl: Some("img".to_string()),
                },
            }))
        }
    );

    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute(
            "mutation { editMonitor(visibility: \"INTERNAL\", newSlides:[\"a.jpg\", \"b.jpg\"]) }",
        )
        .await;

    // then
    content_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_edit_monitor_externally() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn put_item(&self, item: Content) -> Result<(), AppError> {
            if !item.key.eq("monitor.EXTERNAL.paths") {
                panic!("invalid key \"{}\"", item.key)
            } else if !item.content.eq("") {
                panic!("invalid content")
            } else {
                Ok(())
            }
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_current_user(
            &self,
            _auth_context: &AuthContext,
        ) -> Result<Option<CtWhoAmIResponse>, AppError> {
            Ok(Some(CtWhoAmIResponse {
                data: CtWhoAmIResponseData {
                    id: 123,
                    firstName: "X".to_string(),
                    lastName: "Y".to_string(),
                    imageUrl: Some("".to_string()),
                },
            }))
        }
    );

    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("mutation { editMonitor(visibility: \"EXTERNAL\", newSlides:[]) }")
        .await;

    // then
    content_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_update_monitor() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn put_item(&self, item: Content) -> Result<(), AppError> {
            if !item.key.eq("monitor.EXTERNAL.paths") && !item.key.eq("monitor.INTERNAL.paths") {
                panic!("invalid key \"{}\"", item.key)
            } else {
                let slides = item
                    .content
                    .split(',')
                    .map(|x| x.to_string())
                    .collect::<Vec<_>>();
                let has_first_slide_with_order = slides
                    .iter()
                    .any(|p| p.starts_with("i1-") || p.starts_with("e1-"));
                if slides.len() == 21 && has_first_slide_with_order {
                    Ok(())
                } else {
                    panic!("unexpected slides: {:?}", slides)
                }
            }
        },
        async fn get_value_by_id(&self, id: &str) -> Result<Option<Content>, AppError> {
            if id.eq("de.monitor.EXTERNAL.paths") {
                return Ok(Some(Content {
                    key: "monitor.EXTERNAL.paths".to_string(),
                    content: "".to_string(),
                    language: "de".to_string(),
                    date: OffsetDateTime::now_utc(),
                }));
            } else if id.eq("de.monitor.INTERNAL.paths") {
                return Ok(Some(Content {
                    key: "monitor.INTERNAL.paths".to_string(),
                    content: "abc.png,xyz.jpg".to_string(),
                    language: "de".to_string(),
                    date: OffsetDateTime::now_utc(),
                }));
            } else {
                panic!("unexpected id {}", id)
            }
        },
        async fn batch_get_values_by_ids(
            &self,
            _ids: &[String],
        ) -> Result<BTreeMap<String, Content>, AppError> {
            let mut results = BTreeMap::new();
            results.insert(
                "de.general.info.imagesUrl".to_string(),
                Content {
                    key: "general.info.screensUrl".to_string(),
                    content: "https://localhost/external.zip".to_string(),
                    language: "de".to_string(),
                    date: OffsetDateTime::now_utc(),
                },
            );
            results.insert(
                "de.general.info.screensUrl".to_string(),
                Content {
                    key: "general.info.screensUrl".to_string(),
                    content: "https://localhost/internal.zip".to_string(),
                    language: "de".to_string(),
                    date: OffsetDateTime::now_utc(),
                },
            );
            Ok(results)
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_current_user(
            &self,
            _auth_context: &AuthContext,
        ) -> Result<Option<CtWhoAmIResponse>, AppError> {
            Ok(Some(CtWhoAmIResponse {
                data: CtWhoAmIResponseData {
                    id: 123,
                    firstName: "X".to_string(),
                    lastName: "Y".to_string(),
                    imageUrl: Some("".to_string()),
                },
            }))
        }
    );
    let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
        async fn fetch_file_to_tmp(&self, uri: &str) -> Result<File, AppError> {
            if !uri.eq("https://localhost/internal.zip")
                && !uri.eq("https://localhost/external.zip")
            {
                panic!("unexpected uri {}", uri)
            }

            let file = File::open("../app/resources/221204_InfoScreen innen.zip")
                .map_err(|err| AppError::from_error(&err, "reading tmp file failed"))?;
            Ok(file)
        }
    )
    .await;
    let (object_client_mock, mut object_client_mock_collector) = create_object_client_mock!(
        async fn upload(&self, _key: &str, _data: Vec<u8>, _mime: &str) -> Result<(), AppError> {
            Ok(())
        },
        async fn delete(&self, _key: &str) -> Result<(), AppError> {
            Ok(())
        }
    )
    .await;

    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data::<HttpService>(Arc::new(http_client_mock))
        .data::<ObjectService>(Box::new(object_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("mutation { updateMonitor(language: \"GERMAN\") }")
        .await;

    // then
    object_client_mock_collector.collect_calls();
    http_client_mock_collector.collect_calls();
    content_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);

    assert_eq!(2, content_mock_collector.get_calls_by_method("put_item"));
    assert_eq!(
        2,
        content_mock_collector.get_calls_by_method("get_value_by_id")
    );
    assert_eq!(
        1,
        content_mock_collector.get_calls_by_method("batch_get_values_by_ids")
    );
    assert_eq!(
        2,
        http_client_mock_collector.get_calls_by_method("fetch_file_to_tmp")
    );
    assert_eq!(
        42,
        object_client_mock_collector.get_calls_by_method("upload")
    );
    assert_eq!(
        2,
        object_client_mock_collector.get_calls_by_method("delete")
    );
}
