use crate::{query::build_schema, util::build_auth_context};
use async_trait::async_trait;
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use efg_lib::{
    clients::auth::AuthService, create_auth_client_mock, schema::types::content::Content,
};
use efg_testing::create_db_mock;
use insta::assert_json_snapshot;
use time::macros::datetime;

#[tokio::test]
async fn test_should_update_content() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn get_value_by_id(&self, id: &str) -> Result<Option<Content>, AppError> {
            Ok(if id.eq("de.key") {
                Some(Content {
                    content: "content".to_string(),
                    date: datetime!(2022-06-12 06:21:50 UTC),
                    language: "de".to_string(),
                    key: "key".to_string(),
                })
            } else {
                None
            })
        },
        async fn put_item(&self, _item: Content) -> Result<(), AppError> {
            Ok(())
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;

    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute(
            "mutation { updateContent(
            language: \"GERMAN\",
            key: \"key\",
            content: \"new content\"
        )}",
        )
        .await;

    // then
    content_mock_collector.collect_calls();
    assert_json_snapshot!(result, { ".data.updateContent.date"=> "[iso-date]" });
}
