use crate::{query::build_schema, util::build_auth_context};
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use efg_lib::{
    clients::auth::AuthService, create_auth_client_mock, schema::types::content::Content,
};
use efg_testing::create_db_mock;
use insta::assert_json_snapshot;

#[tokio::test]
async fn test_should_fill_german_properties() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn put_item(&self, _item: Content) -> Result<(), AppError> {
            Ok(())
        },
        async fn get_value_by_id(&self, _id: &str) -> Result<Option<Content>, AppError> {
            Ok(None)
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Admin]).await;

    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("mutation { fillProperties(language: \"GERMAN\") { imported, total } }")
        .await;

    // then
    content_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_fill_english_properties() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn put_item(&self, _item: Content) -> Result<(), AppError> {
            Ok(())
        },
        async fn get_value_by_id(&self, _id: &str) -> Result<Option<Content>, AppError> {
            Ok(None)
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Admin]).await;

    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("mutation { fillProperties(language: \"ENGLISH\") { imported, total } }")
        .await;

    // then
    content_mock_collector.collect_calls();
    assert_json_snapshot!(result);
    assert_eq!(188, content_mock_collector.get_calls_by_method("put_item"));
    assert_eq!(
        188,
        content_mock_collector.get_calls_by_method("get_value_by_id")
    );
}
