use crate::{query::build_schema, util::build_auth_context};
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use efg_lib::{clients::auth::AuthService, create_auth_client_mock, schema::types::sermon::Sermon};
use efg_testing::create_db_mock;
use insta::{assert_json_snapshot, dynamic_redaction};
use time::macros::datetime;

#[tokio::test]
async fn test_should_create_sermon() {
    // given
    let (sermon_mock, mut sermon_mock_collector) = create_db_mock!(
        Sermon,
        MockSermonClient,
        async fn put_item(&self, _item: Sermon) -> Result<(), AppError> {
            Ok(())
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;

    let schema = build_schema()
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute(
            "mutation { createSermon(sermon: {
            title: \"abc\",
            language: \"GERMAN\",
            talkDate: \"2022-06-12T06:21:50.498Z\",
            speaker: \"Some Body\",
            tags: [\"some\", \"tag\"]
        }) { id, title, language, talkDate, speaker, tags, publishDate } }",
        )
        .await;

    // then
    sermon_mock_collector.collect_calls();
    assert_json_snapshot!(result, {
        ".data.createSermon.id" => dynamic_redaction(|value, _path| {
            // assert that the value looks like a uuid here
            assert_eq!(value
                .as_str()
                .unwrap()
                .len(),
                36
            );
            "[short-id]"}),
        ".data.createSermon.publishDate" => "[iso-date]"}
    );
}

#[tokio::test]
async fn test_should_update_sermon() {
    // given
    let (sermon_mock, mut sermon_mock_collector) = create_db_mock!(
        Sermon,
        MockSermonClient,
        async fn get_value_by_id(&self, id: &str) -> Result<Option<Sermon>, AppError> {
            Ok(if id.eq("id") {
                Some(Sermon {
                    audio_url: None,
                    text_url: None,
                    translation_url: None,
                    comment: None,
                    id: "id".to_string(),
                    language: "de".to_string(),
                    publish_date: datetime!(2022-06-12 06:21:50 UTC),
                    speaker: "speaker".to_string(),
                    tags: Vec::new(),
                    talk_date: datetime!(2019-01-01 01:23:45 UTC),
                    title: "title".to_string(),
                    video_url: None,
                })
            } else {
                None
            })
        },
        async fn put_item(&self, _item: Sermon) -> Result<(), AppError> {
            Ok(())
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;

    let schema = build_schema()
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute(
            "mutation { updateSermon(sermon: {
            id: \"id\",
            title: \"abc\",
            language: \"GERMAN\",
            audioUrl: \"https://audio\",
            videoUrl: \"https://video\",
            translationUrl: \"https://translation\",
            comment: \"some comment\",
            talkDate: \"2022-01-05T06:21:50.000Z\",
            speaker: \"Some Body\",
            tags: [\"some\", \"tag\"]
        }) { id, title, language, talkDate, speaker, tags, publishDate, audioUrl, videoUrl, translationUrl, comment } }",
        )
        .await;

    // then
    sermon_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_delete_sermon() {
    // given
    let (sermon_mock, mut sermon_mock_collector) = create_db_mock!(
        Sermon,
        MockSermonClient,
        async fn delete_item(&self, id: &str) -> Result<(), AppError> {
            if id.eq("id") {
                Ok(())
            } else {
                Err(AppError::new("unexpected"))
            }
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;

    let schema = build_schema()
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("mutation { deleteSermon(id: \"id\") }")
        .await;

    // then
    sermon_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}
