use efg_lambda_base::auth_context::AuthContext;
use lambda_http::{Body, Response};
use secrecy::SecretString;
use serde_json::Value;

/// Parses the incoming JSON response and asserts the expected status code for equalness.
///
/// # Examples
///
/// ```
/// use util::parse_response;
///
/// let response = Reponse::default();
/// parse_response(response);
/// ```
pub fn parse_response(response: &Response<Body>) -> Value {
    match response.body() {
        Body::Text(payload) => serde_json::from_str::<Value>(payload).unwrap(),
        _ => panic!(""),
    }
}

pub fn build_auth_context() -> AuthContext {
    AuthContext::new(Some(SecretString::from("token".to_string())))
}
