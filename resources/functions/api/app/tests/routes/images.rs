use crate::routes::QueryRoot;
use async_graphql::{EmptyMutation, EmptySubscription, Schema};
use base64::Engine;
use efg_lib::{
    create_http_client_mock, create_object_client_mock,
    function::run_function,
    routes::model::image_config::{
        BackgroundConfig, ColorConfig, FooterConfig, ImageConfig, Position, TextConfig, TextStyle,
        TextWidth,
    },
};
use http::{
    header::{CACHE_CONTROL, CONTENT_TYPE},
    method::Method,
    Request,
};
use insta::{assert_json_snapshot, assert_snapshot};
use lambda_http::{aws_lambda_events::query_map::QueryMap, Body, RequestExt};
use std::sync::Arc;

#[tokio::test]
async fn test_should_render_image_with_missing_config() {
    // when
    let response = render_image_with_config(&None).await;

    // then
    assert_eq!(404, response.status());
}

#[tokio::test]
async fn test_should_render_image_with_invalid_config() {
    // given
    let config = "abc".to_string();

    // when
    let response = render_image_with_config(&Some(config)).await;

    // then
    assert_eq!(400, response.status());
}

#[tokio::test]
async fn test_should_render_image_with_empty_config() {
    // given
    let encoded = base64::engine::general_purpose::STANDARD.encode(
        "{
            \"texts\":[],
            \"background\": {
                \"type\":\"color\",
                \"color\": {\"r\":100,\"g\":189,\"b\":250}
            }
        }",
    );

    // when
    let response = render_image_with_config(&Some(encoded)).await;

    // then
    assert_eq!(200, response.status());
}

#[tokio::test]
async fn test_should_render_image_with_minimal_config() {
    // given
    let config = serde_json::to_string(&ImageConfig {
        footer: None,
        h: None,
        w: None,
        margins: None,
        background: BackgroundConfig::Color {
            color: ColorConfig {
                r: 100,
                g: 80,
                b: 200,
            },
        },
        texts: Vec::new(),
    })
    .unwrap();
    let encoded = base64::engine::general_purpose::STANDARD.encode(config);

    // when
    let response = render_image_with_config(&Some(encoded.clone())).await;

    // then
    assert_eq!(200, response.status());
    assert_snapshot!(encoded);
    assert_eq!(
        "image/jpeg",
        response
            .headers()
            .get(CONTENT_TYPE)
            .unwrap()
            .to_str()
            .unwrap()
    );
    assert_eq!(
        "public,max-age=604800",
        response
            .headers()
            .get(CACHE_CONTROL)
            .unwrap()
            .to_str()
            .unwrap()
    );
}

#[tokio::test]
async fn test_should_render_image_with_full_config() {
    // given
    let config = serde_json::to_string(&ImageConfig {
        footer: Some(FooterConfig {
            logo_size: Some(100.0),
            padding: Some(20.0),
            scale: Some(1.0),
        }),
        h: Some(123),
        w: Some(456),
        margins: Some(20),
        background: BackgroundConfig::Color {
            color: ColorConfig {
                r: 200,
                g: 80,
                b: 200,
            },
        },
        texts: vec![TextConfig {
            font_size: Some(12.0),
            line_spacing: Some(1.5),
            offset_x: None,
            offset_y: Some(2.0),
            position: Some(Position::Bottom),
            style: Some(TextStyle::Normal),
            width: Some(TextWidth::Normal),
            color: Some(ColorConfig {
                r: 100,
                g: 0,
                b: 100,
            }),
            paragraphs: vec!["abc".to_string()],
        }],
    })
    .unwrap();
    let encoded = base64::engine::general_purpose::STANDARD.encode(config);

    // when
    let response = render_image_with_config(&Some(encoded.clone())).await;

    // then
    assert_eq!(200, response.status());
    assert_snapshot!(encoded);
    assert_eq!(
        "image/jpeg",
        response
            .headers()
            .get(CONTENT_TYPE)
            .unwrap()
            .to_str()
            .unwrap()
    );
    assert_eq!(
        "public,max-age=604800",
        response
            .headers()
            .get(CACHE_CONTROL)
            .unwrap()
            .to_str()
            .unwrap()
    );
}

#[tokio::test]
async fn test_should_render_image_with_special_characters() {
    // given
    let config = serde_json::to_string(&ImageConfig {
        footer: None,
        h: None,
        w: None,
        margins: None,
        background: BackgroundConfig::Color {
            color: ColorConfig {
                r: 200,
                g: 80,
                b: 200,
            },
        },
        texts: vec![TextConfig {
            font_size: None,
            line_spacing: None,
            offset_x: None,
            offset_y: None,
            position: None,
            style: None,
            width: None,
            color: None,
            paragraphs: vec!["äüöß".to_string()],
        }],
    })
    .unwrap();
    let encoded = base64::engine::general_purpose::STANDARD.encode(config);

    // when
    let response = render_image_with_config(&Some(encoded.clone())).await;

    // then
    assert_eq!(200, response.status());
}

#[tokio::test]
async fn test_should_render_alps_image() {
    // given
    let encoded = build_encoded_image_request_with_page("alps.jpg");

    // when
    let response = render_image_with_config(&Some(encoded.clone())).await;

    // then
    assert_eq!(200, response.status());
}

#[tokio::test]
async fn test_should_render_desert_image() {
    // given
    let encoded = build_encoded_image_request_with_page("desert.jpg");

    // when
    let response = render_image_with_config(&Some(encoded.clone())).await;

    // then
    assert_eq!(200, response.status());
}

#[tokio::test]
async fn test_should_throw_error_for_unallowed_hostname() {
    // given
    let encoded = build_encoded_image_request_with_page("https://google.de/img.jpg");

    // when
    let response = render_image_with_config(&Some(encoded.clone())).await;

    // then
    assert_eq!(400, response.status());
}

#[tokio::test]
async fn test_should_support_external_image() {
    // given
    let encoded = build_encoded_image_request_with_page(
        "https://www.efg-erfurt.de/public/assets/72cec5c3-9564-45c0-92b0-5abf6c006df5.jpg",
    );

    // when
    let response = render_image_with_config(&Some(encoded.clone())).await;

    // then
    assert_eq!(200, response.status());
}

#[tokio::test]
async fn test_should_load_config() {
    // given
    let url = "https://www.efg-erfurt.de/api/images?c=eyJ0ZXh0cyI6W3siZm9udFNpemUiOjI0LCJwYXJhZ3JhcGhzIjpbIlRoaXMgaW1hZ2UgY29udGFpbnMgdGhlIGNvbmZpZyJdLCJzdHlsZSI6IkJvbGQifSx7ImZvbnRTaXplIjo3MiwicGFyYWdyYXBocyI6WyJFWElGIEZUVyJdLCJwb3NpdGlvbiI6Ik1pZGRsZSIsImNvbG9yIjp7InIiOjAsImciOjIwNSwiYiI6MjU1fSwic3R5bGUiOiJCb2xkSXRhbGljIn1dLCJ3Ijo1MDAsImgiOjUwMCwiYmFja2dyb3VuZCI6eyJ0eXBlIjoiY29sb3IiLCJjb2xvciI6eyJyIjoyNDYsImciOjE4MywiYiI6NjB9fSwibWFyZ2lucyI6MzB9";

    // when
    let response = get_config_from_image(url).await;

    // then
    assert_eq!(200, response.status());
    assert_json_snapshot!(response.body());
}

#[tokio::test]
async fn test_should_load_no_config() {
    // given
    let url = "https://www.efg-erfurt.de/public/assets/6a6eaf09-181a-498f-ae47-9c8155e34c1b.jpg";

    // when
    let response = get_config_from_image(url).await;

    // then
    assert_eq!(404, response.status());
}

#[tokio::test]
async fn test_should_load_no_config_because_from_other_host() {
    // given
    let url = "https://efg-erfurt.church.tools/images/17942/1b17ce045c7ea94cc55737cd14d87381597e18e3de5ffcc5e26cf6abf16bab82?w=50&h=50&q=85";

    // when
    let response = get_config_from_image(url).await;

    // then
    assert_eq!(400, response.status());
}

fn build_encoded_image_request_with_page(path: &str) -> String {
    let config = serde_json::to_string(&ImageConfig {
        footer: None,
        h: Some(100),
        w: Some(100),
        margins: None,
        background: BackgroundConfig::Image {
            path: path.to_string(),
        },
        texts: vec![TextConfig {
            font_size: None,
            line_spacing: None,
            offset_x: None,
            offset_y: None,
            position: None,
            style: None,
            width: None,
            color: None,
            paragraphs: vec!["äüöß".to_string()],
        }],
    })
    .unwrap();

    base64::engine::general_purpose::STANDARD.encode(config)
}

async fn render_image_with_config(config: &Option<String>) -> http::response::Response<Body> {
    let (http_client_mock, ..) = create_http_client_mock!().await;
    let (object_client_mock, ..) = create_object_client_mock!().await;

    let mut request = Request::builder()
        .uri("/api/images")
        .method(Method::GET)
        .body(().into())
        .unwrap();
    if let Some(config) = config {
        request = request
            .with_query_string_parameters(format!("c={config}").parse::<QueryMap>().unwrap());
    }

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    return result.unwrap();
}

async fn get_config_from_image(url: &str) -> http::response::Response<Body> {
    let (http_client_mock, ..) = create_http_client_mock!().await;
    let (object_client_mock, ..) = create_object_client_mock!().await;

    let request = Request::builder()
        .uri("/api/image-config")
        .method(Method::GET)
        .body(().into())
        .unwrap()
        .with_query_string_parameters(format!("url={url}").parse::<QueryMap>().unwrap());

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    return result.unwrap();
}
