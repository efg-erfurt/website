use crate::{
    routes::{build_context, QueryRoot},
    util::parse_response,
};
use async_graphql::{EmptyMutation, EmptySubscription, Schema};
use efg_lambda_base::errors::AppError;
use efg_lib::{create_http_client_mock, create_object_client_mock, function::run_function};
use http::{
    header::{CONTENT_TYPE, SET_COOKIE},
    method::Method,
    Request, Response, StatusCode,
};
use insta::assert_json_snapshot;
use lambda_http::RequestExt;
use std::sync::Arc;

#[tokio::test]
async fn test_should_handle_login() {
    // given
    let (http_client_mock, mut collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            if request.uri().to_string().eq("https://efg-erfurt.church.tools/api/login") &&
                request.body().eq(&Some("{\"username\":\"my-user\",\"password\":\"s3cr3t\",\"rememberMe\":false}".to_string())) {
                let response = Response::builder()
                    .status(StatusCode::OK)
                    .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                    .body(include_str!("../../tests/query/responses/ct/login.json").to_string())
                    .expect("building response failed");
                return Ok(response);
            } else if request.uri().to_string().eq("https://efg-erfurt.church.tools/api/persons/123/groups?show_overdue_groups=false&show_inactive_groups=false") {
                let response = Response::builder()
                    .status(StatusCode::OK)
                    .body(include_str!("../../tests/query/responses/ct/personalGroups.json").to_string())
                    .expect("building response failed");
                return Ok(response);
            }

            Err(AppError::new(format!("unexpected request, uri={}, body={:?}", &request.uri(), &request.body())))
        }
    )
    .await;
    let (object_client_mock, _) = create_object_client_mock!().await;

    let request = Request::builder()
        .uri("/api/login")
        .method(Method::POST)
        .header(CONTENT_TYPE, "application/json")
        .body("{\"user\":\"my-user\", \"pw\":\"s3cr3t\"}".into())
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    let response = result.unwrap();
    let headers = response.headers();
    assert_eq!(1, headers.len());
    assert_eq!(
        "application/json",
        headers.get(CONTENT_TYPE).unwrap().to_str().unwrap()
    );

    assert_json_snapshot!(parse_response(&response));
    assert_eq!(200, response.status());
    collector.collect_calls();
    assert_eq!(2, collector.total_calls());
}

#[tokio::test]
async fn test_should_handle_login_for_invalid_pw() {
    // given
    let (http_client_mock, mut collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            if request
                .uri()
                .to_string()
                .eq("https://efg-erfurt.church.tools/api/login")
            {
                let response = Response::builder()
                    .status(StatusCode::BAD_REQUEST)
                    .body("".to_string())
                    .unwrap();
                return Ok(response);
            }

            Err(AppError::new(format!(
                "unexpected request, uri={}, body={:?}",
                &request.uri(),
                &request.body()
            )))
        }
    )
    .await;
    let (object_client_mock, _) = create_object_client_mock!().await;

    let request = Request::builder()
        .uri("/api/login")
        .method(Method::POST)
        .header(CONTENT_TYPE, "application/json")
        .body("{\"user\":\"my-user\", \"pw\":\"s3cr3t\"}".into())
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    let response = result.unwrap();
    let headers = response.headers();
    assert_eq!(1, headers.len());
    assert_eq!(
        "application/json",
        headers.get(CONTENT_TYPE).unwrap().to_str().unwrap()
    );

    assert_json_snapshot!(parse_response(&response), { ".id"=> "[uuid]" });
    assert_eq!(401, response.status());
    collector.collect_calls();
    assert_eq!(1, collector.total_calls());
}

#[tokio::test]
async fn test_should_handle_login_for_missing_group() {
    // given
    let (http_client_mock, mut collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            if request.uri().to_string().eq("https://efg-erfurt.church.tools/api/login") &&
                request.body().eq(&Some("{\"username\":\"my-user\",\"password\":\"s3cr3t\",\"rememberMe\":false}".to_string())) {
                let response = Response::builder()
                    .status(StatusCode::OK)
                    .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                    .body(include_str!("../../tests/query/responses/ct/login.json").to_string())
                    .expect("building response failed");
                return Ok(response);
            } else if request.uri().to_string().eq("https://efg-erfurt.church.tools/api/persons/123/groups?show_overdue_groups=false&show_inactive_groups=false") {
                let response = Response::builder()
                    .status(StatusCode::OK)
                    .body(include_str!("../../tests/query/responses/ct/personalGroups_unauthorized.json").to_string())
                    .expect("building response failed");
                return Ok(response);
            }

            Err(AppError::new(format!("unexpected request, uri={}, body={:?}", &request.uri(), &request.body())))
        }
    )
    .await;
    let (object_client_mock, _) = create_object_client_mock!().await;

    let request = Request::builder()
        .uri("/api/login")
        .method(Method::POST)
        .header(CONTENT_TYPE, "application/json")
        .body("{\"user\":\"my-user\", \"pw\":\"s3cr3t\"}".into())
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    let response = result.unwrap();
    let headers = response.headers();
    assert_eq!(1, headers.len());
    assert_eq!(
        "application/json",
        headers.get(CONTENT_TYPE).unwrap().to_str().unwrap()
    );

    assert_json_snapshot!(parse_response(&response), { ".id"=> "[uuid]" });
    assert_eq!(403, response.status());
    collector.collect_calls();
    assert_eq!(2, collector.total_calls());
}

#[tokio::test]
async fn test_should_handle_login_with_empty_payload() {
    // given
    let (http_client_mock, ..) = create_http_client_mock!().await;
    let (object_client_mock, ..) = create_object_client_mock!().await;

    let request = Request::builder()
        .uri("/api/login")
        .method(Method::POST)
        .header(CONTENT_TYPE, "application/json")
        .body(().into())
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    let response = result.unwrap();
    assert_json_snapshot!(parse_response(&response), { ".id"=> "[uuid]" });
    assert_eq!(400, response.status());
}

#[tokio::test]
async fn test_should_handle_login_with_invalid_payload() {
    // given
    let (http_client_mock, ..) = create_http_client_mock!().await;
    let (object_client_mock, ..) = create_object_client_mock!().await;

    let request = Request::builder()
        .method(Method::POST)
        .uri("/api/login")
        .header(CONTENT_TYPE, "application/json")
        .body("{".into())
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    let response = result.unwrap();
    assert_json_snapshot!(parse_response(&response), { ".id"=> "[uuid]" });
    assert_eq!(400, response.status());
}

#[tokio::test]
async fn test_should_handle_unknown_path() {
    // given
    let (http_client_mock, ..) = create_http_client_mock!().await;
    let (object_client_mock, ..) = create_object_client_mock!().await;

    let request = Request::builder()
        .uri("/api/something")
        .body(().into())
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    let response = result.unwrap();
    let headers = response.headers();
    assert_eq!(1, headers.len());
    assert_eq!(
        "application/json",
        headers.get(CONTENT_TYPE).unwrap().to_str().unwrap()
    );
    assert_json_snapshot!(parse_response(&response), { ".id"=> "[uuid]" });
    assert_eq!(405, response.status())
}

#[tokio::test]
async fn test_should_handle_gql() {
    // given
    let (http_client_mock, ..) = create_http_client_mock!().await;
    let (object_client_mock, ..) = create_object_client_mock!().await;

    let request = Request::builder()
        .uri("/api/gql")
        .method(Method::POST)
        .header(CONTENT_TYPE, "application/json")
        .body("{\"query\": \"{ value }\"}".into())
        .unwrap()
        .with_lambda_context(build_context());

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    let response = result.unwrap();
    assert_json_snapshot!(parse_response(&response));
    assert_eq!(200, response.status())
}
