use crate::{routes::QueryRoot, util::parse_response};
use async_graphql::{EmptyMutation, EmptySubscription, Schema};
use efg_lambda_base::errors::AppError;
use efg_lib::{create_http_client_mock, create_object_client_mock, function::run_function};
use http::{
    header::{AUTHORIZATION, CONTENT_TYPE, SET_COOKIE},
    method::Method,
    Request, Response, StatusCode,
};
use insta::assert_json_snapshot;
use std::sync::Arc;

#[tokio::test]
async fn test_should_handle_image_upload() {
    // given
    let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            if request
            .uri()
            .to_string()
            .eq("https://efg-erfurt.church.tools/api/whoami")
        {
            let response = Response::builder()
                .status(StatusCode::OK)
                .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                .body(include_str!("../../tests/query/responses/ct/whoami_authorized.json").to_string())
                .expect("building response failed");
            return Ok(response);
        } else if request.uri().to_string().eq("https://efg-erfurt.church.tools/api/persons/123/groups?show_overdue_groups=false&show_inactive_groups=false") {
                let response = Response::builder()
                    .status(StatusCode::OK)
                    .body(include_str!("../../tests/query/responses/ct/personalGroups.json").to_string())
                    .expect("building response failed");
                return Ok(response);
            }

            Err(AppError::new(format!("unexpected request, uri={}, body={:?}", &request.uri(), &request.body())))
        }
    )
    .await;
    let (object_client_mock, mut object_client_mock_collector) = create_object_client_mock!(
        async fn upload(&self, _key: &str, _data: Vec<u8>, _mime: &str) -> Result<(), AppError> {
            Ok(())
        }
    )
    .await;

    let start = "--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"data\"; filename=\"test.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n"
    .as_bytes();
    let image = include_bytes!("test.jpg");
    let end = "\r\n\r\n--X-BOUNDARY--\r\n".as_bytes();

    let request = Request::builder()
        .uri("/api/files")
        .method(Method::POST)
        .header(CONTENT_TYPE, "multipart/form-data; boundary=X-BOUNDARY")
        .header(AUTHORIZATION, "Bearer token")
        .body([start, image, end].concat().into())
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    http_client_mock_collector.collect_calls();
    object_client_mock_collector.collect_calls();
    let response = result.unwrap();
    let headers = response.headers();
    assert_eq!(1, headers.len());
    assert_eq!(
        "application/json",
        headers.get(CONTENT_TYPE).unwrap().to_str().unwrap()
    );

    assert_json_snapshot!(parse_response(&response), { ".id" => "[uuid]", ".path" => "/public/assets/[uuid]" });
    assert_eq!(200, response.status());
    assert_eq!(2, http_client_mock_collector.total_calls());
    assert_eq!(
        1,
        object_client_mock_collector.get_calls_by_method("upload")
    );
}

#[tokio::test]
async fn test_should_handle_image_upload_with_unexpected_type() {
    // given
    let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            if request
            .uri()
            .to_string()
            .eq("https://efg-erfurt.church.tools/api/whoami")
        {
            let response = Response::builder()
                .status(StatusCode::OK)
                .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                .body(include_str!("../../tests/query/responses/ct/whoami_authorized.json").to_string())
                .expect("building response failed");
            return Ok(response);
        } else if request.uri().to_string().eq("https://efg-erfurt.church.tools/api/persons/123/groups?show_overdue_groups=false&show_inactive_groups=false") {
                let response = Response::builder()
                    .status(StatusCode::OK)
                    .body(include_str!("../../tests/query/responses/ct/personalGroups.json").to_string())
                    .expect("building response failed");
                return Ok(response);
            }

            Err(AppError::new(format!("unexpected request, uri={}, body={:?}", &request.uri(), &request.body())))
        }
    )
    .await;
    let (object_client_mock, mut object_client_mock_collector) = create_object_client_mock!().await;

    let body = "--X-BOUNDARY\r\nContent-Disposition: form-data; name=\"data\"; filename=\"test.json\"\r\nContent-Type: application/json\r\n\r\n\"test\"\r\n\r\n--X-BOUNDARY--\r\n".as_bytes();

    let request = Request::builder()
        .uri("/api/files")
        .method(Method::POST)
        .header(CONTENT_TYPE, "multipart/form-data; boundary=X-BOUNDARY")
        .header(AUTHORIZATION, "Bearer token")
        .body(body.into())
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    http_client_mock_collector.collect_calls();
    object_client_mock_collector.collect_calls();
    let response = result.unwrap();

    assert_json_snapshot!(parse_response(&response), { ".id" => "[uuid]" });
    assert_eq!(400, response.status());
    assert_eq!(2, http_client_mock_collector.total_calls());
}

#[tokio::test]
async fn test_should_handle_image_upload_with_missing_body() {
    // given
    let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            if request
            .uri()
            .to_string()
            .eq("https://efg-erfurt.church.tools/api/whoami")
        {
            let response = Response::builder()
                .status(StatusCode::OK)
                .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                .body(include_str!("../../tests/query/responses/ct/whoami_authorized.json").to_string())
                .expect("building response failed");
            return Ok(response);
        } else if request.uri().to_string().eq("https://efg-erfurt.church.tools/api/persons/123/groups?show_overdue_groups=false&show_inactive_groups=false") {
                let response = Response::builder()
                    .status(StatusCode::OK)
                    .body(include_str!("../../tests/query/responses/ct/personalGroups.json").to_string())
                    .expect("building response failed");
                return Ok(response);
            }

            Err(AppError::new(format!("unexpected request, uri={}, body={:?}", &request.uri(), &request.body())))
        }
    )
    .await;
    let (object_client_mock, mut object_client_mock_collector) = create_object_client_mock!().await;

    let request = Request::builder()
        .uri("/api/files")
        .method(Method::POST)
        .header(CONTENT_TYPE, "multipart/form-data; boundary=X-BOUNDARY")
        .header(AUTHORIZATION, "Bearer token")
        .body(lambda_http::Body::Empty)
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    http_client_mock_collector.collect_calls();
    object_client_mock_collector.collect_calls();
    let response = result.unwrap();

    assert_json_snapshot!(parse_response(&response), { ".id" => "[uuid]" });
    assert_eq!(400, response.status());
    assert_eq!(2, http_client_mock_collector.total_calls());
}
