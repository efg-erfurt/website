mod files;
mod gql;
mod images;
mod login;

use crate::util::parse_response;
use async_graphql::{EmptyMutation, EmptySubscription, Object, Schema};
use async_trait::async_trait;
use efg_lib::{create_http_client_mock, create_object_client_mock, function::run_function};
use http::{header::CONTENT_TYPE, HeaderMap, HeaderValue, Request};
use insta::assert_json_snapshot;
use lambda_http::{lambda_runtime::Config, Context};
use std::sync::Arc;

pub struct QueryRoot;

#[Object()]
impl QueryRoot {
    async fn value(&self) -> i32 {
        100
    }
}

pub fn build_context() -> Context {
    let mut headers = HeaderMap::new();
    headers.insert(
        "lambda-runtime-aws-request-id",
        HeaderValue::from_static("6f3c103e-f0a5-4982-934c-dabedda0065e"),
    );
    headers.insert(
        "lambda-runtime-deadline-ms",
        HeaderValue::from_static("123"),
    );
    headers.insert(
        "lambda-runtime-invoked-function-arn",
        HeaderValue::from_static("arn::myarn"),
    );
    headers.insert(
        "lambda-runtime-trace-id",
        HeaderValue::from_static(
            "Root=1-621e8596-13fa204f3ea98d584b6af5cd;Parent=06cf37c30d0d8959;Sampled=1",
        ),
    );
    Context::new("id", Arc::new(Config::default()), &headers).expect("building context failed")
}

#[tokio::test]
async fn test_should_handle_unknown_path() {
    // given
    let (http_client_mock, mut collector) = create_http_client_mock!().await;
    let (object_client_mock, _) = create_object_client_mock!().await;
    let request = Request::builder()
        .uri("/api/something")
        .body(().into())
        .unwrap();

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    let response = result.unwrap();
    let headers = response.headers();
    assert_eq!(1, headers.len());
    assert_eq!(
        "application/json",
        headers.get(CONTENT_TYPE).unwrap().to_str().unwrap()
    );
    assert_json_snapshot!(parse_response(&response), { ".id"=> "[uuid]" });
    assert_eq!(405, response.status());
    collector.collect_calls();
}
