use crate::{
    routes::{build_context, QueryRoot},
    util::parse_response,
};
use async_graphql::{EmptyMutation, EmptySubscription, Schema};
use efg_lib::{create_http_client_mock, create_object_client_mock, function::run_function};
use http::{header::CONTENT_TYPE, method::Method, Request};
use insta::assert_json_snapshot;
use lambda_http::RequestExt;
use std::sync::Arc;

#[tokio::test]
async fn test_should_handle_gql() {
    // given
    let (http_client_mock, mut collector) = create_http_client_mock!().await;
    let (object_client_mock, _) = create_object_client_mock!().await;
    let request = Request::builder()
        .uri("/api/gql")
        .method(Method::POST)
        .header(CONTENT_TYPE, "application/json")
        .body("{\"query\": \"{ value }\"}".into())
        .unwrap()
        .with_lambda_context(build_context());

    // when
    let result = run_function(
        request,
        Schema::build(QueryRoot {}, EmptyMutation, EmptySubscription).finish(),
        Arc::new(http_client_mock),
        Arc::new(object_client_mock),
    )
    .await;

    // then
    let response = result.unwrap();
    assert_json_snapshot!(parse_response(&response));
    assert_eq!(200, response.status());
    collector.collect_calls();
}
