use crate::query::build_schema;
use efg_lambda_base::errors::AppError;
use efg_lib::{clients::ct::CtService, create_ct_rest_mock, types::ct::CtGroupsResponse};
use insta::assert_json_snapshot;
use std::sync::Arc;

#[tokio::test]
async fn test_should_return_signups() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_group_by_id(&self, _group_id: &str) -> Result<CtGroupsResponse, AppError> {
            Ok(
                serde_json::from_str(include_str!("responses/ct/grouphomepages.json"))
                    .expect("reading mock data failed"),
            )
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute(
            "{ signups(groupsId: \"abc\") { id, name, time, day, note, imageUrl, slotsTotal, slotsLeft, slotsUsed } }",
        )
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}
