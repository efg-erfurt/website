use std::sync::Arc;

use crate::query::build_schema;
use efg_lambda_base::errors::AppError;
use efg_lib::{
    clients::ct::CtService,
    create_ct_rest_mock,
    types::ct::{CtGetPersonResponse, CtGroupsResponse},
};
use insta::assert_json_snapshot;

#[tokio::test]
async fn test_should_return_groups() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_group_by_id(&self, _group_id: &str) -> Result<CtGroupsResponse, AppError> {
            Ok(serde_json::from_str(include_str!(
                "../../tests/query/responses/ct/getGroups.json"
            ))
            .expect("reading mock data failed"))
        },
        async fn get_user_by_id(
            &self,
            user_id: &str,
        ) -> Result<Option<CtGetPersonResponse>, AppError> {
            Ok(Some(if user_id.eq("1240") {
                serde_json::from_str(include_str!(
                    "../../tests/query/responses/ct/getPerson.json"
                ))
                .expect("parsing mock data failed")
            } else {
                serde_json::from_str(include_str!(
                    "../../tests/query/responses/ct/getPerson_notFound.json"
                ))
                .expect("parsing mock data failed")
            }))
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute(
            "{ groups(id: \"abc\") { id, name, time, day, place, note, imageUrl, ages, canBeContacted, contactUrl, leaders { firstName, lastName, id } } }",
        )
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}
