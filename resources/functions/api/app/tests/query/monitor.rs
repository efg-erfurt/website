use crate::query::build_schema;
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use efg_lib::schema::types::content::Content;
use efg_testing::create_db_mock;
use insta::assert_json_snapshot;
use time::OffsetDateTime;

#[tokio::test]
async fn test_should_return_pages() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn get_value_by_id(&self, id: &str) -> Result<Option<Content>, AppError> {
            Ok(if id.eq("de.monitor.INTERNAL.paths") {
                Some(Content {
                    content: "abc.jpg,xyz.jpg".to_string(),
                    date: OffsetDateTime::now_utc(),
                    key: "monitor-pages".to_string(),
                    language: "de".to_string(),
                })
            } else {
                None
            })
        }
    );
    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .finish();

    // when
    let result = schema
        .execute("{ monitor { pages(visibility: \"INTERNAL\") } }")
        .await;

    // then
    content_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_pages_with_default_externally() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn get_value_by_id(&self, id: &str) -> Result<Option<Content>, AppError> {
            Ok(if id.eq("de.monitor.EXTERNAL.paths") {
                Some(Content {
                    content: "abc.jpg,xyz.jpg".to_string(),
                    date: OffsetDateTime::now_utc(),
                    key: "monitor-pages".to_string(),
                    language: "de".to_string(),
                })
            } else {
                None
            })
        }
    );
    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .finish();

    // when
    let result = schema.execute("{ monitor { pages } }").await;

    // then
    content_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}
