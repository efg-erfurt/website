use async_graphql::{EmptySubscription, Schema, SchemaBuilder};
use efg_lib::schema::{mutation::Mutation, query::QueryRoot};

pub mod content;
pub mod events;
pub mod groups;
pub mod monitor;
pub mod post;
pub mod search;
pub mod sermons;
pub mod signups;
pub mod users;

pub fn build_schema() -> SchemaBuilder<QueryRoot, Mutation, EmptySubscription> {
    Schema::build(QueryRoot, Mutation, EmptySubscription)
}
