use crate::{query::build_schema, util::build_auth_context};
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
use efg_lib::{
    clients::ct::CtService,
    create_ct_rest_mock,
    types::ct::{CtGetPersonResponse, CtWhoAmIResponse},
};
use insta::assert_json_snapshot;
use std::sync::Arc;

#[tokio::test]
async fn test_should_return_current_user() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_current_user(
            &self,
            _auth_context: &AuthContext,
        ) -> Result<Option<CtWhoAmIResponse>, AppError> {
            Ok(Some(
                serde_json::from_str(include_str!("responses/ct/whoami_authorized.json"))
                    .expect("parsing mock data failed"),
            ))
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("{ currentUser { id, firstName, lastName, imageUrl } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_current_user_for_anonymous() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_current_user(
            &self,
            _auth_context: &AuthContext,
        ) -> Result<Option<CtWhoAmIResponse>, AppError> {
            Ok(None)
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data(AuthContext::new(None))
        .finish();

    // when
    let result = schema
        .execute("{ currentUser { id, firstName, lastName, imageUrl } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_user_by_id() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_user_by_id(
            &self,
            _user_id: &str,
        ) -> Result<Option<CtGetPersonResponse>, AppError> {
            Ok(Some(
                serde_json::from_str(include_str!("responses/ct/getPerson.json"))
                    .expect("parsing mock data failed"),
            ))
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("{ user(id: \"1\") { id, firstName, lastName, imageUrl } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_updated_user_by_id() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_user_by_id(
            &self,
            _user_id: &str,
        ) -> Result<Option<CtGetPersonResponse>, AppError> {
            Ok(Some(
                serde_json::from_str(include_str!("responses/ct/getPerson_updated.json"))
                    .expect("parsing mock data failed"),
            ))
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("{ user(id: \"1\") { id, firstName, lastName, imageUrl } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_no_user_by_id_because_of_allowlist() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!();
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("{ user(id: \"123\") { id, firstName, lastName, imageUrl } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
    assert_eq!(0, ct_rest_mock_collector.total_calls());
}

#[tokio::test]
async fn test_should_return_no_user_by_id() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_user_by_id(
            &self,
            _user_id: &str,
        ) -> Result<Option<CtGetPersonResponse>, AppError> {
            Ok(Some(
                serde_json::from_str(include_str!("responses/ct/getPerson_notFound.json"))
                    .expect("parsing mock data failed"),
            ))
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("{ user(id: \"13\") { id, firstName, lastName, imageUrl } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
    assert_eq!(1, ct_rest_mock_collector.total_calls());
}
