use crate::query::build_schema;
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use efg_lib::{
    clients::{ct::CtService, http::HttpService},
    create_ct_rest_mock, create_http_client_mock,
    schema::{
        mutation::Language,
        types::{post::Post, sermon::Sermon},
    },
    types::ct::{CtEventsOverviewResponse, CtEventsResponse},
};
use efg_testing::create_db_mock;
use http::{Request, Response};
use insta::assert_json_snapshot;
use std::sync::Arc;
use time::macros::datetime;

/// Macro for generating mock ct rest client implementation with only a custom categories file.
#[macro_export]
macro_rules! create_ct_events_mock {
    () => {{
        create_ct_rest_mock!(
            async fn get_categories(&self) -> Result<CtEventsOverviewResponse, AppError> {
                Ok(serde_json::from_str(
                    &include_str!("responses/ct/getMasterData.json").to_string(),
                )
                .expect("reading mock data failed"))
            },
            async fn get_calendars_by_category_ids(
                &self,
                _category_ids: &[String],
            ) -> Result<CtEventsResponse, AppError> {
                Ok(serde_json::from_str(
                    &include_str!("responses/ct/getCalPerCategory.json").to_string(),
                )
                .expect("reading mock data failed"))
            }
        )
    }};
}

#[tokio::test]
async fn test_should_search_for_pages() {
    // given
    let (http_client_mock, mut collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            let response = Response::builder()
                .body(
                    if request.uri().eq("https://www.efg-erfurt.de/de/donate") {
                        include_str!("responses/efg/donate.html")
                    } else if request.uri().eq("https://www.efg-erfurt.de/de/contribute") {
                        include_str!("responses/efg/contribute.html")
                    } else if request.uri().eq("https://www.efg-erfurt.de/de/about-us") {
                        include_str!("responses/efg/about-us.html")
                    } else {
                        ""
                    }
                    .to_string(),
                )
                .unwrap();
            Ok(response)
        }
    )
    .await;
    let (post_mock, mut post_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Post>, AppError> {
            Ok(Vec::new())
        }
    );
    let (sermon_mock, mut sermon_mock_collector) = create_db_mock!(
        Sermon,
        MockSermonClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Sermon>, AppError> {
            Ok(Vec::new())
        }
    );
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_events_mock!();

    // when
    let schema = build_schema()
        .data::<HttpService>(Arc::new(http_client_mock))
        .data::<DbService<Post>>(Box::new(post_mock))
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute("{ search(query: \"Nordlicht\", language: \"GERMAN\") { id, query, language, pages { title, path, score } } }")
        .await;

    // then
    collector.collect_calls();
    post_mock_collector.collect_calls();
    sermon_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result, { ".data.search.id" => "[uuid]" });
}

#[tokio::test]
async fn test_should_search_for_events() {
    // given
    let (http_client_mock, mut collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            _request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            let response = Response::builder().body("".to_string()).unwrap();
            Ok(response)
        }
    )
    .await;
    let (post_mock, mut post_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Post>, AppError> {
            Ok(Vec::new())
        }
    );
    let (sermon_mock, mut sermon_mock_collector) = create_db_mock!(
        Sermon,
        MockSermonClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Sermon>, AppError> {
            Ok(Vec::new())
        }
    );
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_events_mock!();

    // when
    let schema = build_schema()
        .data::<HttpService>(Arc::new(http_client_mock))
        .data::<DbService<Post>>(Box::new(post_mock))
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute("{ search(query: \"Gottesdienst\", language: \"GERMAN\", startDate: \"2022-11-27T19:30:00Z\") { id, query, language, events { id, name, date } } }")
        .await;

    // then
    collector.collect_calls();
    post_mock_collector.collect_calls();
    sermon_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result, { ".data.search.id" => "[uuid]" });
}

#[tokio::test]
async fn test_should_search_for_sermons() {
    // given
    let (http_client_mock, mut collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            _request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            let response = Response::builder().body("".to_string()).unwrap();
            Ok(response)
        }
    )
    .await;
    let (post_mock, mut post_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Post>, AppError> {
            Ok(Vec::new())
        }
    );
    let (sermon_mock, mut sermon_mock_collector) = create_db_mock!(
        Sermon,
        MockSermonClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Sermon>, AppError> {
            Ok(vec![Sermon {
                id: "abc".to_string(),
                language: Language::German.to_str(),
                speaker: "Speaker".to_string(),
                title: "abc xyz".to_string(),
                audio_url: None,
                comment: None,
                text_url: None,
                translation_url: None,
                video_url: None,
                publish_date: datetime!(2022-03-31 10:30 UTC),
                talk_date: datetime!(2022-03-31 10:30 UTC),
                tags: Vec::new(),
            }])
        }
    );
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_events_mock!();

    // when
    let schema = build_schema()
        .data::<HttpService>(Arc::new(http_client_mock))
        .data::<DbService<Post>>(Box::new(post_mock))
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute("{ search(query: \"ABC\", language: \"GERMAN\") { id, query, language, sermons { id, title } } }")
        .await;

    // then
    collector.collect_calls();
    post_mock_collector.collect_calls();
    sermon_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result, { ".data.search.id" => "[uuid]" });
}

#[tokio::test]
async fn test_should_search_for_posts() {
    // given
    let (http_client_mock, mut collector) = create_http_client_mock!(
        async fn send_request(
            &self,
            _request: Request<Option<String>>,
        ) -> Result<Response<String>, AppError> {
            let response = Response::builder().body("".to_string()).unwrap();
            Ok(response)
        }
    )
    .await;
    let (post_mock, mut post_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Post>, AppError> {
            Ok(vec![Post {
                id: "abc".to_string(),
                title: "Something important".to_string(),
                content: "ABC xyz".to_string(),
                language: Language::German.to_str(),
                user_id: "user".to_string(),
                date: datetime!(2022-03-31 10:30 UTC),
                publish_date: datetime!(2022-03-31 10:30 UTC),
                image_url: None,
                summary: None,
                tags: Vec::new(),
            }])
        }
    );
    let (sermon_mock, mut sermon_mock_collector) = create_db_mock!(
        Sermon,
        MockSermonClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Sermon>, AppError> {
            Ok(Vec::new())
        }
    );
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_events_mock!();

    // when
    let schema = build_schema()
        .data::<HttpService>(Arc::new(http_client_mock))
        .data::<DbService<Post>>(Box::new(post_mock))
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute("{ search(query: \"ABC\", language: \"GERMAN\") { id, query, language, posts { id, title, content } } }")
        .await;

    // then
    collector.collect_calls();
    post_mock_collector.collect_calls();
    sermon_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result, { ".data.search.id" => "[uuid]" });
}
