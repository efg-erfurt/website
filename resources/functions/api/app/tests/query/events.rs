use crate::query::build_schema;
use efg_lambda_base::errors::AppError;
use efg_lib::{
    clients::ct::CtService,
    create_ct_rest_mock,
    types::ct::{CtEventsOverviewResponse, CtEventsResponse},
};
use insta::assert_json_snapshot;
use std::sync::Arc;

#[tokio::test]
async fn test_should_return_jet() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_categories(&self) -> Result<CtEventsOverviewResponse, AppError> {
            Ok(
                serde_json::from_str(include_str!("responses/ct/getMasterData.json"))
                    .expect("reading mock data failed"),
            )
        },
        async fn get_calendars_by_category_ids(
            &self,
            _category_ids: &[String],
        ) -> Result<CtEventsResponse, AppError> {
            Ok(
                serde_json::from_str(include_str!("responses/ct/getCalPerCategory_jet.json"))
                    .expect("reading mock data failed"),
            )
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute("{ events(start: \"2024-02-10T04:00:19.12345Z\") { id, name, date } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_latest_events() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_categories(&self) -> Result<CtEventsOverviewResponse, AppError> {
            Ok(
                serde_json::from_str(include_str!("responses/ct/getMasterData.json"))
                    .expect("reading mock data failed"),
            )
        },
        async fn get_calendars_by_category_ids(
            &self,
            _category_ids: &[String],
        ) -> Result<CtEventsResponse, AppError> {
            Ok(
                serde_json::from_str(include_str!("responses/ct/getCalPerCategory.json"))
                    .expect("reading mock data failed"),
            )
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute(
            "{ events(start: \"2021-01-12T04:00:19.12345Z\",maxEntries: 24) { id, name, date, place, description, category { id, name, textColor, backgroundColor } } }",
        )
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_event_categories() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_categories(&self) -> Result<CtEventsOverviewResponse, AppError> {
            Ok(
                serde_json::from_str(include_str!("responses/ct/getMasterData.json"))
                    .expect("reading mock data failed"),
            )
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute("{ eventCategories { id, name, textColor, backgroundColor } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_event_category_by_id() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_categories(&self) -> Result<CtEventsOverviewResponse, AppError> {
            Ok(
                serde_json::from_str(include_str!("responses/ct/getMasterData.json"))
                    .expect("reading mock data failed"),
            )
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    ct_rest_mock_collector.collect_calls();
    let result = schema
        .execute("{ eventCategory(id: \"56\") { id, name, textColor, backgroundColor } }")
        .await;

    // then
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_no_event_category_by_id() {
    // given
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_categories(&self) -> Result<CtEventsOverviewResponse, AppError> {
            Ok(
                serde_json::from_str(include_str!("responses/ct/getMasterData.json"))
                    .expect("reading mock data failed"),
            )
        }
    );
    let schema = build_schema()
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute("{ eventCategory(id: \"987\") { id, name, textColor, backgroundColor } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}
