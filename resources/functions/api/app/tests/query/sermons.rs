use crate::query::build_schema;
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use efg_lib::schema::types::sermon::Sermon;
use efg_testing::create_db_mock;
use insta::assert_json_snapshot;
use time::macros::datetime;

#[tokio::test]
async fn test_should_return_sermons() {
    // given
    let (sermon_mock, mut ct_rest_mock_collector) = create_db_mock!(
        Sermon,
        MockContentClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Sermon>, AppError> {
            Ok(create_test_sermons())
        }
    );

    let schema = build_schema()
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .finish();

    // when
    let result = schema.execute("{ sermons(language: \"GERMAN\", pagination: {}) { count, page, totalPages, totalCount, content { id, title, comment, language, speaker, tags, textUrl, translationUrl, videoUrl, audioUrl, talkDate, publishDate } } }").await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_sermons_with_speaker() {
    // given
    let (sermon_mock, mut ct_rest_mock_collector) = create_db_mock!(
        Sermon,
        MockContentClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Sermon>, AppError> {
            Ok(create_test_sermons())
        }
    );
    let schema = build_schema()
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .finish();

    // when
    let result = schema.execute("{ sermons(language: \"GERMAN\", speaker:\"speaker A\", pagination: {}) { count, page, totalPages, totalCount, content { id, title, comment, language, speaker, tags, textUrl, translationUrl, videoUrl, audioUrl, talkDate, publishDate } } }").await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_no_sermons_with_different_language() {
    // given
    let (sermon_mock, mut ct_rest_mock_collector) = create_db_mock!(
        Sermon,
        MockContentClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Sermon>, AppError> {
            Ok(create_test_sermons())
        }
    );
    let schema = build_schema()
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .finish();

    // when
    let result = schema.execute("{ sermons(language: \"ENGLISH\", pagination: {}) { count, page, totalPages, totalCount, content { id, title, comment, language, speaker, tags, textUrl, translationUrl, videoUrl, audioUrl, talkDate, publishDate } } }").await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_latest_sermons() {
    // given
    let (sermon_mock, mut ct_rest_mock_collector) = create_db_mock!(
        Sermon,
        MockContentClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Sermon>, AppError> {
            Ok(create_test_sermons())
        }
    );
    let schema = build_schema()
        .data::<DbService<Sermon>>(Box::new(sermon_mock))
        .finish();

    // when
    let result = schema.execute("{ latestSermons(language: \"GERMAN\", pagination: { size: 5, page: 0 }) { count, page, totalPages, totalCount, content { id, title, comment, language, speaker, tags, textUrl, translationUrl, videoUrl, audioUrl, talkDate, publishDate } } }").await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

fn create_test_sermons() -> Vec<Sermon> {
    let a = Sermon {
        id: "id-a".to_string(),
        title: "title A".to_string(),
        comment: None,
        language: "de".to_string(),
        speaker: "speaker A".to_string(),
        tags: vec!["a".to_string(), "b".to_string()],
        text_url: None,
        translation_url: None,
        video_url: None,
        audio_url: None,
        publish_date: datetime!(2020-12-13 19:30 UTC),
        talk_date: datetime!(2020-11-20 19:30 UTC),
    };
    let b = Sermon {
        id: "id-b".to_string(),
        title: "title B".to_string(),
        comment: Some("comment".to_string()),
        language: "de".to_string(),
        speaker: "speaker B".to_string(),
        tags: Vec::new(),
        text_url: Some("text.txt".to_string()),
        translation_url: Some("en.txt".to_string()),
        video_url: Some("video.mp4".to_string()),
        audio_url: Some("audio.mp3".to_string()),
        publish_date: datetime!(2020-12-13 19:30 UTC),
        talk_date: datetime!(2020-11-27 19:30 UTC),
    };
    let c = Sermon {
        id: "id-c".to_string(),
        title: "title C".to_string(),
        comment: Some("comment".to_string()),
        language: "de".to_string(),
        speaker: "speaker C".to_string(),
        tags: Vec::new(),
        text_url: Some("text.txt".to_string()),
        translation_url: Some("en.txt".to_string()),
        video_url: Some("video.mp4".to_string()),
        audio_url: Some("audio.mp3".to_string()),
        publish_date: datetime!(2020-08-13 19:30 UTC),
        talk_date: datetime!(2020-08-27 19:30 UTC),
    };
    vec![a, b, c]
}
