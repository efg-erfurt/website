use crate::query::build_schema;
use efg_db_client::db::DbService;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
use efg_lib::{
    clients::ct::CtService,
    create_ct_rest_mock,
    schema::types::content::Content,
    types::ct::{CtWhoAmIResponse, CtWhoAmIResponseData},
};
use efg_testing::create_db_mock;
use insta::assert_json_snapshot;
use std::{collections::BTreeMap, sync::Arc};
use time::OffsetDateTime;

#[tokio::test]
async fn test_should_return_content_by_key_and_language() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn get_value_by_id(&self, id: &str) -> Result<Option<Content>, AppError> {
            Ok(if id.eq("de.key") {
                Some(Content {
                    content: "abc".to_string(),
                    date: OffsetDateTime::now_utc(),
                    key: "key".to_string(),
                    language: "de".to_string(),
                })
            } else {
                None
            })
        }
    );
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_current_user(
            &self,
            _auth_context: &AuthContext,
        ) -> Result<Option<CtWhoAmIResponse>, AppError> {
            Ok(Some(CtWhoAmIResponse {
                data: CtWhoAmIResponseData {
                    id: 123,
                    firstName: "X".to_string(),
                    lastName: "Y".to_string(),
                    imageUrl: Some("img".to_string()),
                },
            }))
        }
    );
    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute("{ content(key: \"key\",language: \"GERMAN\") { key, language, content, date } }")
        .await;

    // then
    content_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result, { ".data.content.date" => "[iso-date]" });
}

#[tokio::test]
async fn test_should_return_no_content_by_key() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn get_value_by_id(&self, _id: &str) -> Result<Option<Content>, AppError> {
            Ok(None)
        }
    );
    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .finish();

    // when
    let result = schema
        .execute("{ content(key: \"key\",language: \"GERMAN\") { key } }")
        .await;

    // then
    content_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_contents_by_keys_and_language() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn batch_get_values_by_ids(
            &self,
            ids: &[String],
        ) -> Result<BTreeMap<String, Content>, AppError> {
            Ok(if ids.len() == 1 && ids.get(0).unwrap().eq("de.key") {
                let mut result = BTreeMap::new();
                result.insert(
                    "de.key".to_string(),
                    Content {
                        content: "abc".to_string(),
                        date: OffsetDateTime::now_utc(),
                        key: "key".to_string(),
                        language: "de".to_string(),
                    },
                );
                result
            } else {
                BTreeMap::new()
            })
        }
    );
    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .finish();

    // when
    let result = schema
        .execute("{ contents(keys: [\"key\"],language: \"GERMAN\") { key } }")
        .await;

    // then
    content_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_no_contents_by_keys_and_language() {
    // given
    let (content_mock, mut content_mock_collector) = create_db_mock!(
        Content,
        MockContentClient,
        async fn batch_get_values_by_ids(
            &self,
            _ids: &[String],
        ) -> Result<BTreeMap<String, Content>, AppError> {
            Ok(BTreeMap::new())
        }
    );
    let schema = build_schema()
        .data::<DbService<Content>>(Box::new(content_mock))
        .finish();

    // when
    let result = schema
        .execute("{ contents(keys: [\"key\"],language: \"GERMAN\") { key } }")
        .await;

    // then
    content_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}
