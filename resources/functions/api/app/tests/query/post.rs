use std::sync::Arc;

use crate::{query::build_schema, util::build_auth_context};
use efg_db_client::db::DbService;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
use efg_lib::{
    clients::{auth::AuthService, ct::CtService},
    create_auth_client_mock, create_ct_rest_mock,
    schema::types::post::Post,
    types::ct::{CtGetPersonResponse, CtWhoAmIResponse},
};
use efg_testing::create_db_mock;
use insta::{assert_json_snapshot, dynamic_redaction};
use time::{macros::datetime, OffsetDateTime};

#[tokio::test]
async fn test_should_return_post_by_id() {
    // given
    let (post_mock, mut post_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn get_value_by_id(&self, id: &str) -> Result<Option<Post>, AppError> {
            Ok(if id.eq("specific-id") {
                Some(Post {
                    id: "specific-id".to_string(),
                    date: OffsetDateTime::now_utc(),
                    publish_date: OffsetDateTime::now_utc(),
                    title: "title".to_string(),
                    summary: None,
                    image_url: None,
                    language: "de".to_string(),
                    content: "abc".to_string(),
                    user_id: "123".to_string(),
                    tags: Vec::new(),
                })
            } else {
                None
            })
        }
    );
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_user_by_id(
            &self,
            _user_id: &str,
        ) -> Result<Option<CtGetPersonResponse>, AppError> {
            Ok(Some(
                serde_json::from_str(include_str!(
                    "../../tests/query/responses/ct/getPerson.json"
                ))
                .expect("parsing mock data failed"),
            ))
        }
    );
    let schema = build_schema()
        .data::<DbService<Post>>(Box::new(post_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .finish();

    // when
    let result = schema
        .execute("{ post(id: \"specific-id\") { id, date, publishDate, title, summary, imageUrl, content, tags, language, creator { id } } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    post_mock_collector.collect_calls();
    assert_json_snapshot!(result, { ".data.post.date" => "[iso-date]", ".data.post.publishDate" => "[iso-date]" });
}

#[tokio::test]
async fn test_should_return_no_post_by_id() {
    // given
    let (post_mock, mut ct_rest_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn get_value_by_id(&self, _id: &str) -> Result<Option<Post>, AppError> {
            Ok(None)
        }
    );
    let schema = build_schema()
        .data::<DbService<Post>>(Box::new(post_mock))
        .finish();

    // when
    let result = schema
        .execute("{ post(id: \"specific-id\") { id, date, publishDate, title, summary, imageUrl, content, tags } }")
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_latests_posts() {
    // given
    let (post_mock, mut ct_rest_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Post>, AppError> {
            Ok(vec![Post {
                id: "abc".to_string(),
                content: "test".to_string(),
                date: datetime!(2022-02-13 10:30 UTC),
                publish_date: datetime!(2022-02-17 10:30 UTC),
                image_url: None,
                language: "de".to_string(),
                summary: None,
                tags: Vec::new(),
                title: "title".to_string(),
                user_id: "user".to_string(),
            }])
        }
    );
    let schema = build_schema()
        .data::<DbService<Post>>(Box::new(post_mock))
        .finish();

    // when
    let result = schema
        .execute(
            "{ latestPosts(pagination: {}) { count, page, totalPages, totalCount, content { id, date, publishDate, title, summary, imageUrl, content, tags } } }",
        )
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_return_latests_posts_filtered_by_tag() {
    // given
    let (post_mock, mut ct_rest_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn get_all_values(
            &self,
            _sort_column: &str,
            _page: usize,
            _size: usize,
        ) -> Result<Vec<Post>, AppError> {
            Ok(vec![
                Post {
                    id: "abc".to_string(),
                    content: "test".to_string(),
                    date: datetime!(2022-02-13 10:30 UTC),
                    publish_date: datetime!(2022-02-17 10:30 UTC),
                    image_url: None,
                    language: "de".to_string(),
                    summary: None,
                    tags: Vec::new(),
                    title: "title".to_string(),
                    user_id: "user".to_string(),
                },
                Post {
                    id: "xyz".to_string(),
                    content: "xyz".to_string(),
                    date: datetime!(2022-02-13 10:30 UTC),
                    publish_date: datetime!(2022-02-17 10:30 UTC),
                    image_url: None,
                    language: "de".to_string(),
                    summary: None,
                    tags: vec!["some-tag".to_string(), "other-tag".to_string()],
                    title: "xyz".to_string(),
                    user_id: "user".to_string(),
                },
            ])
        }
    );
    let schema = build_schema()
        .data::<DbService<Post>>(Box::new(post_mock))
        .finish();

    // when
    let result = schema
        .execute(
            "{ latestPosts(pagination: {}, tag: \"some-tag\") { count, content { id, tags } } }",
        )
        .await;

    // then
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_create_post() {
    // given
    let (post_mock, mut post_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn put_item(&self, _item: Post) -> Result<(), AppError> {
            Ok(())
        }
    );
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_current_user(
            &self,
            _auth_context: &AuthContext,
        ) -> Result<Option<CtWhoAmIResponse>, AppError> {
            Ok(Some(
                serde_json::from_str(include_str!(
                    "../../tests/query/responses/ct/getPerson.json"
                ))
                .expect("parsing mock data failed"),
            ))
        },
        async fn get_user_by_id(
            &self,
            _user_id: &str,
        ) -> Result<Option<CtGetPersonResponse>, AppError> {
            Ok(Some(
                serde_json::from_str(include_str!(
                    "../../tests/query/responses/ct/getPerson.json"
                ))
                .expect("parsing mock data failed"),
            ))
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;

    let schema = build_schema()
        .data::<DbService<Post>>(Box::new(post_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute(
            "mutation { createPost(post: {
            title: \"abc\",
            content: \"content\",
            language: \"GERMAN\",
            tags: [\"some\", \"tag\"]
        }) { id, title, language, content, tags, publishDate, creator { id } } }",
        )
        .await;

    // then
    post_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result, {
        ".data.createPost.id" => dynamic_redaction(|value, _path| {
            // assert that the value looks like a uuid here
            assert_eq!(value
                .as_str()
                .unwrap()
                .len(),
                36
            );
            "[short-id]"}),
        ".data.createPost.publishDate" => "[iso-date]"}
    );
}

#[tokio::test]
async fn test_should_update_post() {
    // given
    let (post_mock, mut post_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn get_value_by_id(&self, _id: &str) -> Result<Option<Post>, AppError> {
            Ok(Some(Post {
                id: "id".to_string(),
                content: "some".to_string(),
                date: datetime!(2022-02-13 10:30 UTC),
                image_url: None,
                language: "de".to_string(),
                publish_date: datetime!(2022-02-13 10:30 UTC),
                summary: None,
                tags: Vec::new(),
                title: "title".to_string(),
                user_id: "user".to_string(),
            }))
        },
        async fn put_item(&self, _item: Post) -> Result<(), AppError> {
            Ok(())
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;
    let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
        async fn get_current_user(
            &self,
            _auth_context: &AuthContext,
        ) -> Result<Option<CtWhoAmIResponse>, AppError> {
            Ok(Some(
                serde_json::from_str(include_str!(
                    "../../tests/query/responses/ct/getPerson.json"
                ))
                .expect("parsing mock data failed"),
            ))
        },
        async fn get_user_by_id(
            &self,
            _user_id: &str,
        ) -> Result<Option<CtGetPersonResponse>, AppError> {
            Ok(Some(
                serde_json::from_str(include_str!(
                    "../../tests/query/responses/ct/getPerson.json"
                ))
                .expect("parsing mock data failed"),
            ))
        }
    );

    let schema = build_schema()
        .data::<DbService<Post>>(Box::new(post_mock))
        .data::<CtService>(Arc::new(ct_rest_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute(
            "mutation { updatePost(post: {
            id: \"id\",
            title: \"abc\",
            content: \"content\",
            language: \"GERMAN\",
            tags: [\"some\", \"tag\"]
        }) { id, title, language, content, tags, publishDate } }",
        )
        .await;

    // then
    post_mock_collector.collect_calls();
    ct_rest_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}

#[tokio::test]
async fn test_should_delete_post() {
    // given
    let (post_mock, mut post_mock_collector) = create_db_mock!(
        Post,
        MockPostClient,
        async fn delete_item(&self, id: &str) -> Result<(), AppError> {
            if id.ne("test") {
                panic!("invalid id arg")
            }
            Ok(())
        }
    );
    let (auth_client_mock, _) = create_auth_client_mock!(vec![Role::Editor]).await;

    let schema = build_schema()
        .data::<DbService<Post>>(Box::new(post_mock))
        .data::<AuthService>(Box::new(auth_client_mock))
        .data(build_auth_context())
        .finish();

    // when
    let result = schema
        .execute("mutation { deletePost(id: \"test\") }")
        .await;

    // then
    post_mock_collector.collect_calls();
    assert_json_snapshot!(result);
}
