use crate::{
    clients::http::HttpClient,
    routes::{
        files::handle_upload_file,
        image_generator::handle_image_generator,
        images::{handle_image_config, handle_images},
        login::handle_login,
        util::{respond_error, response_method_not_allowed},
    },
};
use async_graphql::{EmptySubscription, Schema};
use efg_lambda_base::function::run_function_with_schema;
use efg_object_client::db::ObjectClient;
use http::{Method, StatusCode};
use lambda_http::{Body, Error, Request, Response};
use std::sync::Arc;
use tracing::{error, warn};

/// HTTP router to route REST and GQL routes as well as handle unexpected errors with a 500.
pub async fn run_function<Q, M, H, S>(
    http_event: Request,
    schema: Schema<Q, M, EmptySubscription>,
    http_client: Arc<H>,
    object_client: Arc<S>,
) -> Result<Response<Body>, Error>
where
    Q: 'static + async_graphql::ObjectType,
    M: 'static + async_graphql::ObjectType,
    H: HttpClient + Sync + Send + 'static,
    S: ObjectClient + Send + Sync,
{
    // TODO set env for AWS_LAMBDA_FUNCTION_NAME and _X_AMZN_TRACE_ID to connect traces
    let result = match http_event.uri().path() {
        "/api/login" => match *http_event.method() {
            Method::POST => handle_login(&http_event, http_client).await,
            _ => response_method_not_allowed(),
        },
        "/api/gql" => match *http_event.method() {
            Method::POST => run_function_with_schema(&http_event, &schema).await,
            _ => response_method_not_allowed(),
        },
        "/api/images" => match *http_event.method() {
            Method::GET => handle_images(&http_event).await,
            _ => response_method_not_allowed(),
        },
        "/api/image-config" => match *http_event.method() {
            Method::GET => handle_image_config(&http_event).await,
            _ => response_method_not_allowed(),
        },
        "/api/image-generator" => match *http_event.method() {
            Method::GET => handle_image_generator(&http_event).await,
            _ => response_method_not_allowed(),
        },
        "/api/files" => match *http_event.method() {
            Method::POST => handle_upload_file(http_event, http_client, object_client).await,
            _ => response_method_not_allowed(),
        },
        _ => {
            warn!(
                "unexpected incoming request {} {}",
                http_event.method(),
                http_event.uri().path()
            );
            response_method_not_allowed()
        }
    };

    if let Err(err) = result.as_ref() {
        error!("processing rquest failed with error {}", err);
    }

    // handle unexpected errors with a 500 and error code
    match result {
        Err(err) => respond_error(
            "UNEXPECTED_ERROR",
            if cfg!(debug_assertions) {
                Some(format!("{}", &err))
            } else {
                None
            },
            StatusCode::INTERNAL_SERVER_ERROR,
        ),
        valid_response => valid_response,
    }
}
