use super::require_env_var;

pub struct ObjectsConfig {
    public_bucket: String,
}

impl ObjectsConfig {
    pub fn new() -> Self {
        ObjectsConfig {
            public_bucket: require_env_var("BUCKET_PUBLIC", "S3 bucket for public assets"),
        }
    }

    pub fn public_bucket(&self) -> &str {
        &self.public_bucket
    }
}

impl Default for ObjectsConfig {
    fn default() -> Self {
        Self::new()
    }
}
