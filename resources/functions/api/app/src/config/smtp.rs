use super::require_env_var;
use secrecy::SecretString;

pub struct SmtpConfig {
    host: String,
    mail: String,
    password: SecretString,
    user: String,
}

impl SmtpConfig {
    pub fn new() -> Self {
        SmtpConfig {
            host: require_env_var("SMTP_HOST", "SMTP hostname to send mails"),
            mail: require_env_var("SMTP_MAIL", "SMTP mail adress to send mails"),
            password: SecretString::from(require_env_var("SMTP_PW", "SMTP password to send mails")),
            user: require_env_var("SMTP_USER", "SMTP username to send mails"),
        }
    }

    pub fn password(&self) -> &SecretString {
        &self.password
    }

    pub fn user(&self) -> &str {
        &self.user
    }

    pub fn mail(&self) -> &str {
        &self.mail
    }

    pub fn host(&self) -> &str {
        &self.host
    }
}

impl Default for SmtpConfig {
    fn default() -> Self {
        Self::new()
    }
}
