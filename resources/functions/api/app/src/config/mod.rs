use self::{churchtools::CtConfig, database::DbConfig, objects::ObjectsConfig, smtp::SmtpConfig};
use std::env::var;

pub mod churchtools;
pub mod database;
pub mod objects;
pub mod smtp;

pub struct AppConfig {
    db: DbConfig,
    ct: CtConfig,
    smtp: SmtpConfig,
    objects: ObjectsConfig,
}

impl AppConfig {
    pub fn new() -> Self {
        AppConfig {
            db: DbConfig::new(),
            ct: CtConfig::new(),
            smtp: SmtpConfig::new(),
            objects: ObjectsConfig::new(),
        }
    }

    pub fn db(&self) -> &DbConfig {
        &self.db
    }

    pub fn ct(&self) -> &CtConfig {
        &self.ct
    }

    pub fn smtp(&self) -> &SmtpConfig {
        &self.smtp
    }

    pub fn objects(&self) -> &ObjectsConfig {
        &self.objects
    }
}

impl Default for AppConfig {
    fn default() -> Self {
        Self::new()
    }
}

/// Tries to read the environment variable by name and will panic with the name and reason of this variable in case it is missing.
fn require_env_var(name: &str, description: &str) -> String {
    var(name).unwrap_or_else(|_| panic!("missing environment variable: {name} ({description})"))
}
