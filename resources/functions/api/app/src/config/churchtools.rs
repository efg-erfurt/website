use super::require_env_var;
use secrecy::SecretString;

pub struct CtConfig {
    user: String,
    password: SecretString,
}

impl CtConfig {
    pub fn new() -> Self {
        CtConfig {
            password: SecretString::from(require_env_var("CT_PW", "password of CT user")),
            user: require_env_var("CT_USER", "username of CT user"),
        }
    }

    pub fn password(&self) -> &SecretString {
        &self.password
    }

    pub fn user(&self) -> &str {
        &self.user
    }
}

impl Default for CtConfig {
    fn default() -> Self {
        Self::new()
    }
}
