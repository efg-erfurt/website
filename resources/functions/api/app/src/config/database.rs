use super::require_env_var;

pub struct DbConfig {
    content_table: String,
    post_table: String,
    sermon_table: String,
    cache_table: String,
}

impl DbConfig {
    pub fn new() -> Self {
        DbConfig {
            content_table: require_env_var("TABLE_CONTENT", "content DynamoDB table name"),
            post_table: require_env_var("TABLE_POST", "posts DynamoDB table name"),
            sermon_table: require_env_var("TABLE_SERMON", "sermons DynamoDB table name"),
            cache_table: require_env_var("TABLE_CACHE", "cache DynamoDB table name"),
        }
    }

    pub fn content_table(&self) -> &str {
        &self.content_table
    }

    pub fn post_table(&self) -> &str {
        &self.post_table
    }

    pub fn sermon_table(&self) -> &str {
        &self.sermon_table
    }

    pub fn cache_table(&self) -> &str {
        &self.cache_table
    }
}

impl Default for DbConfig {
    fn default() -> Self {
        Self::new()
    }
}
