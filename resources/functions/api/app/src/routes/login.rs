use super::util::{build_json_response, respond_error};
use crate::clients::{
    ct::client::{fetch_token, is_authorized},
    http::HttpClient,
};
use efg_lambda_base::errors::AppError;
use http::StatusCode;
use lambda_http::{Body, Error, Request, Response};
use secrecy::{ExposeSecret, SecretString};
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use tracing::{info, warn};

#[derive(Deserialize, Clone)]
struct LoginRequest {
    pub user: String,
    pub pw: String,
}

#[derive(Serialize, Clone)]
struct LoginResponse {
    pub status: String,
    pub token: String,
}

const ERROR_MESSAGE: &str = "LOGIN_FAILED";

pub async fn handle_login<H>(
    http_event: &Request,
    http_client: Arc<H>,
) -> Result<Response<Body>, Error>
where
    H: HttpClient + Sync + Send,
{
    if let Body::Empty = http_event.body() {
        warn!("login without body");
        return respond_error(
            ERROR_MESSAGE,
            Some("empty payload".to_string()),
            StatusCode::BAD_REQUEST,
        );
    } else if let Body::Binary(_) = http_event.body() {
        warn!("login with binary body");
        return respond_error(
            ERROR_MESSAGE,
            Some("invalid payload".to_string()),
            StatusCode::BAD_REQUEST,
        );
    }

    let body = match http_event.body() {
        Body::Text(text) => Ok(text.to_string()),
        _ => Err(AppError::new("parsing request body failed")),
    }?;

    let payload = serde_json::from_str::<LoginRequest>(&body);
    if payload.is_err() {
        warn!("parsing login body failed");
        return respond_error(
            ERROR_MESSAGE,
            Some("invalid payload".to_string()),
            StatusCode::BAD_REQUEST,
        );
    }
    let payload = payload.unwrap();

    let login_result = fetch_token(
        &payload.user,
        &SecretString::from(payload.pw.to_string()),
        http_client.clone(),
    )
    .await?;

    if login_result.is_none() {
        warn!("login by {} failed", payload.user);
        return respond_error(
            ERROR_MESSAGE,
            Some("login failed, maybe invalid username or password".to_string()),
            StatusCode::UNAUTHORIZED,
        );
    }

    let (user, token) = login_result.unwrap();

    let is_authorized = is_authorized(&token, user.data.personId, http_client).await?;
    if !is_authorized {
        warn!("user {} is not authorized", payload.user);
        return respond_error(
            ERROR_MESSAGE,
            Some("user is unauthorized".to_string()),
            StatusCode::FORBIDDEN,
        );
    }

    info!("login of {} succeeded", payload.user);

    build_json_response(LoginResponse {
        status: "success".to_string(),
        token: token.expose_secret().to_string(),
    })
}
