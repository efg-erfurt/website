use super::util::{build_json_response, check_auth, respond_error, AuthCheckResult};
use crate::clients::http::HttpClient;
use async_graphql::futures_util::stream::once;
use efg_lambda_base::errors::AppError;
use efg_object_client::db::ObjectClient;
use http::{header::CONTENT_TYPE, StatusCode};
use lambda_http::{Body, Error, Request, Response};
use multer::Multipart;
use serde::{Deserialize, Serialize};
use std::{convert::Infallible, sync::Arc};
use tracing::{info, warn};
use uuid::Uuid;

#[derive(Deserialize, Clone)]
struct UploadFileRequest {}

#[derive(Serialize, Clone)]
struct UploadFileResponse {
    pub id: String,
    pub path: String,
}

pub async fn handle_upload_file<H, S>(
    http_event: Request,
    http_client: Arc<H>,
    s3_client: Arc<S>,
) -> Result<Response<Body>, Error>
where
    H: HttpClient + Sync + Send,
    S: ObjectClient + Send + Sync,
{
    let result = check_auth(&http_event, http_client).await?;

    match result {
        AuthCheckResult::AllowedUser(user) => {
            let content_type = http_event.headers().get(CONTENT_TYPE).cloned();
            let (_, body) = http_event.into_parts();
            match body {
                Body::Empty => {
                    warn!("tried to upload without body");
                    respond_error("missing payload", None, StatusCode::BAD_REQUEST)
                }
                Body::Text(_) => {
                    warn!("tried to upload with text payload");
                    respond_error("unexpected payload type", None, StatusCode::BAD_REQUEST)
                }
                Body::Binary(payload) => {
                    let uuid = Uuid::new_v4();

                    // TODO add tests for all cases!

                    let boundary = content_type
                        .and_then(|ct| ct.to_str().ok().map(|x| x.to_string()))
                        .and_then(|ct| multer::parse_boundary(ct).ok());

                    if boundary.is_none() {
                        return respond_error(
                            "missing boundary of multipart body",
                            None,
                            StatusCode::BAD_REQUEST,
                        );
                    }

                    let stream = once(async move { Result::<Vec<u8>, Infallible>::Ok(payload) });
                    let mut multipart = Multipart::new(stream, boundary.unwrap());

                    if let Some(field) = multipart.next_field().await? {
                        let file_name = field
                            .file_name()
                            .map(|name| name.to_string())
                            .ok_or_else(|| AppError::new("missing file name"))?;
                        let mime = field
                            .content_type()
                            .cloned()
                            .ok_or_else(|| AppError::new("missing mime"))?;
                        let file_content = field.bytes().await?;

                        let extension = if mime.eq(&mime::IMAGE_PNG) {
                            "png"
                        } else if mime.eq(&mime::IMAGE_JPEG) {
                            "jpg"
                        } else {
                            return respond_error(
                                "unexpected file type",
                                None,
                                StatusCode::BAD_REQUEST,
                            );
                        };
                        let s3_key = format!("public/assets/{uuid}.{extension}");
                        s3_client
                            .upload(&s3_key, file_content.to_vec(), mime.as_ref())
                            .await?;

                        info!(
                            "user {} uploaded a file named {} with mime {} to {}",
                            user, &file_name, mime, &s3_key
                        );

                        build_json_response(UploadFileResponse {
                            id: uuid.to_string(),
                            path: format!("/{s3_key}"),
                        })
                    } else {
                        respond_error(
                            "missing file in multipart body",
                            None,
                            StatusCode::BAD_REQUEST,
                        )
                    }
                }
            }
        }
        AuthCheckResult::AuthError(auth_error) => {
            warn!("tried to upload file unauthorized");
            respond_error(
                &auth_error.message,
                Some(auth_error.details),
                auth_error.status,
            )
        }
    }
}
