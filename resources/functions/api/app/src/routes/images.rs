use super::model::image_config::{BackgroundConfig, ImageConfig, Position, TextStyle, TextWidth};
use base64::Engine;
use cosmic_text::{Color, Stretch, Style, Weight};
use efg_image_renderer::{
    canvas::{Canvas, CanvasBackground, ColorBackground, ImageBackground, Margins},
    components::{
        footer::Footer,
        text::{Text, TextPosition},
    },
    exif::load_config_from_image_exif,
};
use http::{
    header::{CACHE_CONTROL, CONTENT_TYPE},
    StatusCode,
};
use lambda_http::{Body, Error, Request, RequestExt, Response};

pub async fn handle_images(http_event: &Request) -> Result<Response<Body>, Error> {
    let config = http_event
        .query_string_parameters()
        .first("c")
        .map(|x| x.to_string());

    if config.is_none() {
        return return_not_found();
    }
    let config_str = config.unwrap();
    let config = decode_base64_config(&config_str);
    if config.is_err() {
        return return_bad_request();
    }

    let config = config.unwrap();
    if !is_allowed_background_image_path(&config) {
        return return_bad_request();
    }

    let mut canvas = Canvas::default();
    canvas.set_config(&config_str);
    canvas.set_size(config.w.unwrap_or(500), config.h.unwrap_or(500));
    canvas.set_footer(config.footer.map(|f| Footer {
        logo_size: f.logo_size.unwrap_or(100.0),
        padding: f.padding.unwrap_or(10.0),
        scale: f.scale.unwrap_or(1.0),
    }));
    canvas.set_background(match &config.background {
        BackgroundConfig::Color { color } => CanvasBackground::Color(ColorBackground {
            color: Color::rgb(color.r, color.g, color.b),
        }),
        BackgroundConfig::Image { path } => CanvasBackground::Image(ImageBackground {
            path: path.clone(),
            ..Default::default()
        }),
    });
    canvas.set_margins(Margins {
        top: config.margins.unwrap_or(10),
        bottom: config.margins.unwrap_or(10),
        left: config.margins.unwrap_or(10),
        right: config.margins.unwrap_or(10),
    });
    config.texts.iter().for_each(|t| {
        canvas.texts_mut().push(convert_text(t));
    });

    let raw_image = canvas.export_as_jpeg_bytes().await?;

    build_jpeg_response(raw_image)
}

pub async fn handle_image_config(http_event: &Request) -> Result<Response<Body>, Error> {
    let url = http_event
        .query_string_parameters()
        .first("url")
        .map(|x| x.to_string());

    if url.is_none() {
        return return_not_found();
    }

    let url = url.unwrap();
    if !is_efg_url(&url) {
        return return_bad_request();
    }

    let config = load_config_from_image_exif(&url).await?;
    if config.is_none() {
        return return_not_found();
    }

    let decoded_config = decode_base64_config(&config.unwrap());
    if decoded_config.is_err() {
        return return_bad_request();
    }

    build_json_response(&decoded_config.unwrap())
}

fn decode_base64_config(
    config_str: &str,
) -> Result<ImageConfig, Box<dyn std::error::Error + Send + Sync>> {
    let decoded: Result<Vec<u8>, base64::DecodeError> =
        base64::engine::general_purpose::STANDARD.decode(config_str.trim_matches('"'));
    if decoded.is_err() {
        return Err(format!(
            "Image config could not be decoded: {}",
            decoded.unwrap_err()
        )
        .into());
    }
    let decoded = decoded.unwrap();
    let config = serde_json::from_slice::<ImageConfig>(&decoded);
    if config.is_err() {
        return Err(format!(
            "Image config could not be deserialized: {}",
            config.unwrap_err()
        )
        .into());
    }

    Ok(config.unwrap())
}

fn is_allowed_background_image_path(config: &ImageConfig) -> bool {
    if let BackgroundConfig::Image { path } = &config.background {
        // allow two included images for testing
        if path.eq("alps.jpg") || path.eq("desert.jpg") {
            return true;
        }

        return is_efg_url(path);
    }

    true
}

fn is_efg_url(path: &str) -> bool {
    let url = path.parse::<reqwest::Url>();
    return match url {
        Ok(url) => match url.host() {
            // allow only images from www.efg-erfurt.de for now
            Some(host) => host.to_string().eq("www.efg-erfurt.de"),
            None => false,
        },
        Err(_) => false,
    };
}

fn convert_text(t: &super::model::image_config::TextConfig) -> Text {
    Text {
        paragraphs: t.paragraphs.clone(),
        line_spacing: t.line_spacing.unwrap_or(1.5),
        position: match &t.position {
            Some(position) => match position {
                Position::Top => TextPosition::Top,
                Position::Middle => TextPosition::Middle,
                Position::Bottom => TextPosition::Bottom,
            },
            None => TextPosition::Top,
        },
        offset_x: t.offset_x.unwrap_or(0.0),
        offset_y: t.offset_y.unwrap_or(0.0),
        font_size: t.font_size.unwrap_or(12.0),
        font_weight: match &t.style {
            Some(w) => match w {
                TextStyle::Normal => Weight::NORMAL,
                TextStyle::Bold => Weight::BOLD,
                TextStyle::BoldItalic => Weight::BOLD,
                TextStyle::Italic => Weight::NORMAL,
            },
            None => Weight::NORMAL,
        },
        font_style: match &t.style {
            Some(w) => match w {
                TextStyle::Italic => Style::Italic,
                TextStyle::BoldItalic => Style::Italic,
                _ => Style::Normal,
            },
            None => Style::Normal,
        },
        font_stretch: match &t.width {
            Some(w) => match w {
                TextWidth::Condensed => Stretch::Condensed,
                TextWidth::Normal => Stretch::Normal,
                TextWidth::Expanded => Stretch::Expanded,
            },
            None => Stretch::Normal,
        },
        font_color: t
            .color
            .as_ref()
            .map(|t| Color::rgb(t.r, t.g, t.b))
            .unwrap_or(Color::rgb(255, 255, 255)),
        ..Default::default()
    }
}

fn return_not_found() -> Result<Response<Body>, Error> {
    let response = Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body(Body::Empty)?;

    Ok(response)
}

fn return_bad_request() -> Result<Response<Body>, Error> {
    let response = Response::builder()
        .status(StatusCode::BAD_REQUEST)
        .body(Body::Empty)?;

    Ok(response)
}

fn build_jpeg_response(raw_image: Vec<u8>) -> Result<Response<Body>, Error> {
    let mut response: Response<Body> = Response::new(Body::Binary(raw_image));
    response
        .headers_mut()
        .append(CONTENT_TYPE, "image/jpeg".parse().unwrap());
    response
        .headers_mut()
        .append(CACHE_CONTROL, "public,max-age=604800".parse().unwrap());

    Ok(response)
}

fn build_json_response(config: &ImageConfig) -> Result<Response<Body>, Error> {
    let mut response: Response<Body> = Response::new(Body::Text(serde_json::to_string(config)?));
    response
        .headers_mut()
        .append(CONTENT_TYPE, "application/json".parse().unwrap());
    response
        .headers_mut()
        .append(CACHE_CONTROL, "public,max-age=2592000".parse().unwrap());

    Ok(response)
}
