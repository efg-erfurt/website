use http::header::{CACHE_CONTROL, CONTENT_TYPE};
use lambda_http::{Body, Error, Request, Response};

pub async fn handle_image_generator(_http_event: &Request) -> Result<Response<Body>, Error> {
    let generator = include_str!("../../resources/pages/image-generator.html").to_string();
    let mut response = Response::builder().body(Body::Text(generator)).unwrap();
    response
        .headers_mut()
        .append(CONTENT_TYPE, "text/html".parse().unwrap());
    response
        .headers_mut()
        .append(CACHE_CONTROL, "max-age=60".parse().unwrap());
    Ok(response)
}
