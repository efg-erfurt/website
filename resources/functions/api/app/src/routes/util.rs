use crate::{
    clients::{
        ct::client::{is_authorized, BASE_PATH, CT_COOKIE},
        http::HttpClient,
    },
    types::ct::CtWhoAmIResponse,
};
use http::{
    header::{AUTHORIZATION, CONTENT_TYPE, COOKIE},
    Method, StatusCode,
};
use lambda_http::{Body, Error, Request, Response};
use secrecy::SecretString;
use serde::Serialize;
use std::sync::Arc;
use tracing::log::error;
use uuid::Uuid;

#[derive(Serialize, Clone, Debug)]
struct ErrorResponse {
    pub message: String,
    pub details: Option<String>,
    pub id: String,
}

pub fn respond_error(
    message: &str,
    details: Option<String>,
    status_code: StatusCode,
) -> Result<Response<Body>, Error> {
    let id = Uuid::new_v4().to_string();
    error!(
        "error happened: id={}, message={}, details={:?}",
        &id, &message, &details
    );

    let mut response = build_json_response(ErrorResponse {
        id,
        message: message.to_string(),
        details,
    })?;
    *response.status_mut() = status_code;

    Ok(response)
}

pub fn build_json_response<T>(value: T) -> Result<Response<Body>, Error>
where
    T: Sized + Serialize,
{
    let json = serde_json::to_string(&value)?;

    let mut response = Response::new(Body::Text(json));
    response
        .headers_mut()
        .append(CONTENT_TYPE, "application/json".parse().unwrap());

    Ok(response)
}

pub fn response_method_not_allowed() -> Result<Response<Body>, Error> {
    respond_error(
        "INVALID_REQUEST",
        Some("request doesn't match any path or method".to_string()),
        StatusCode::METHOD_NOT_ALLOWED,
    )
}

pub struct AuthErrorContent {
    pub message: String,
    pub details: String,
    pub status: StatusCode,
}

pub enum AuthCheckResult {
    AllowedUser(i32),
    AuthError(AuthErrorContent),
}

const AUTH_FAILED_MESSAGE: &str = "AUTH_FAILED";

pub async fn check_auth<H>(
    http_event: &Request,
    http_client: Arc<H>,
) -> Result<AuthCheckResult, Error>
where
    H: HttpClient + Sync + Send,
{
    let auth_header = http_event.headers().get(AUTHORIZATION);
    if auth_header.is_none() {
        return Ok(AuthCheckResult::AuthError(AuthErrorContent {
            message: AUTH_FAILED_MESSAGE.to_string(),
            details: "missing authorization header".to_string(),
            status: StatusCode::UNAUTHORIZED,
        }));
    }

    let auth_header = auth_header
        .unwrap()
        .to_str()
        .expect("parsing auth header failed")
        .split(' ')
        .map(|v| v.to_string())
        .filter(|v| !v.trim().is_empty())
        .collect::<Vec<_>>();
    if auth_header.len() != 2 {
        return Ok(AuthCheckResult::AuthError(AuthErrorContent {
            message: AUTH_FAILED_MESSAGE.to_string(),
            details: "invalid authorization header".to_string(),
            status: StatusCode::UNAUTHORIZED,
        }));
    }

    let token = auth_header.get(1).unwrap().clone();
    let user_request = http::Request::builder()
        .method(Method::GET)
        .header(COOKIE, format!("{}={}", CT_COOKIE, &token))
        .uri(format!("{BASE_PATH}/api/whoami"))
        .body(None)
        .unwrap();

    let user_response = http_client.send_request(user_request).await?;

    let user = serde_json::from_str::<CtWhoAmIResponse>(user_response.body())?;

    let user_id = user.data.id;
    if user_id <= 0 {
        return Ok(AuthCheckResult::AuthError(AuthErrorContent {
            message: AUTH_FAILED_MESSAGE.to_string(),
            details: "unknown user".to_string(),
            status: StatusCode::UNAUTHORIZED,
        }));
    }

    let is_authorized = is_authorized(
        &SecretString::from(token),
        user_id.try_into().unwrap(),
        http_client,
    )
    .await?;

    if !is_authorized {
        return Ok(AuthCheckResult::AuthError(AuthErrorContent {
            message: AUTH_FAILED_MESSAGE.to_string(),
            details: "user is unauthorized".to_string(),
            status: StatusCode::FORBIDDEN,
        }));
    }

    Ok(AuthCheckResult::AllowedUser(user_id))
}

#[cfg(test)]
mod tests {
    use super::check_auth;
    use crate::{create_http_client_mock, routes::util::AuthCheckResult};
    use efg_lambda_base::errors::AppError;
    use http::{
        header::{AUTHORIZATION, SET_COOKIE},
        HeaderValue, Request, Response, StatusCode,
    };
    use std::sync::Arc;

    #[tokio::test]
    async fn test_should_authorize_successfully() {
        // given
        let mut request = lambda_http::Request::new(lambda_http::Body::Empty);
        request.headers_mut().append(
            AUTHORIZATION,
            HeaderValue::from_str("Bearer token").unwrap(),
        );

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                if request
                    .uri()
                    .to_string()
                    .eq("https://efg-erfurt.church.tools/api/whoami")
                {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                        .body(include_str!("../../tests/query/responses/ct/whoami_authorized.json").to_string())
                        .expect("building response failed");
                    return Ok(response);
                } else if request.uri().to_string().eq("https://efg-erfurt.church.tools/api/persons/123/groups?show_overdue_groups=false&show_inactive_groups=false") {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .body(include_str!("../../tests/query/responses/ct/personalGroups.json").to_string())
                        .expect("building response failed");
                    return Ok(response);
                }

                Err(AppError::new(format!(
                    "unexpected request: {:?}",
                    &request
                )))
            }
        ).await;

        // when
        let result = check_auth(&request, Arc::new(http_client_mock)).await;

        // then
        http_client_mock_collector.collect_calls();
        if let AuthCheckResult::AllowedUser(user_id) = result.unwrap() {
            assert_eq!(123, user_id);
        } else {
            panic!("auth failed");
        }
    }

    #[tokio::test]
    async fn test_should_fail_authorization_because_of_missing_group() {
        // given
        let mut request = lambda_http::Request::new(lambda_http::Body::Empty);
        request.headers_mut().append(
            AUTHORIZATION,
            HeaderValue::from_str("Bearer token").unwrap(),
        );

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                if request
                    .uri()
                    .to_string()
                    .eq("https://efg-erfurt.church.tools/api/whoami")
                {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                        .body(include_str!("../../tests/query/responses/ct/whoami_authorized.json").to_string())
                        .expect("building response failed");
                    return Ok(response);
                } else if request.uri().to_string().eq("https://efg-erfurt.church.tools/api/persons/123/groups?show_overdue_groups=false&show_inactive_groups=false") {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .body(include_str!("../../tests/query/responses/ct/personalGroups_unauthorized.json").to_string())
                        .expect("building response failed");
                    return Ok(response);
                }

                Err(AppError::new(format!(
                    "unexpected request: {:?}",
                    &request
                )))
            }
        ).await;

        // when
        let result = check_auth(&request, Arc::new(http_client_mock)).await;

        // then
        http_client_mock_collector.collect_calls();
        if let AuthCheckResult::AuthError(content) = result.unwrap() {
            assert_eq!("AUTH_FAILED", content.message);
            assert_eq!(StatusCode::FORBIDDEN, content.status);
        } else {
            panic!("auth suceeded");
        }
    }

    #[tokio::test]
    async fn test_should_fail_authorization_because_of_broken_token() {
        // given
        let mut request = lambda_http::Request::new(lambda_http::Body::Empty);
        request
            .headers_mut()
            .append(AUTHORIZATION, HeaderValue::from_str("Bearer ").unwrap());

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!().await;

        // when
        let result = check_auth(&request, Arc::new(http_client_mock)).await;

        // then
        http_client_mock_collector.collect_calls();
        if let AuthCheckResult::AuthError(content) = result.unwrap() {
            assert_eq!("AUTH_FAILED", content.message);
            assert_eq!(StatusCode::UNAUTHORIZED, content.status);
        } else {
            panic!("auth suceeded");
        }
    }

    #[tokio::test]
    async fn test_should_fail_authorization_because_of_broken_token2() {
        // given
        let mut request = lambda_http::Request::new(lambda_http::Body::Empty);
        request
            .headers_mut()
            .append(AUTHORIZATION, HeaderValue::from_str("Bearer").unwrap());

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!().await;

        // when
        let result = check_auth(&request, Arc::new(http_client_mock)).await;

        // then
        http_client_mock_collector.collect_calls();
        if let AuthCheckResult::AuthError(content) = result.unwrap() {
            assert_eq!("AUTH_FAILED", content.message);
            assert_eq!(StatusCode::UNAUTHORIZED, content.status);
        } else {
            panic!("auth suceeded");
        }
    }

    #[tokio::test]
    async fn test_should_fail_authorization_because_of_missing_token() {
        // given
        let request = lambda_http::Request::new(lambda_http::Body::Empty);

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!().await;

        // when
        let result = check_auth(&request, Arc::new(http_client_mock)).await;

        // then
        http_client_mock_collector.collect_calls();
        if let AuthCheckResult::AuthError(content) = result.unwrap() {
            assert_eq!("AUTH_FAILED", content.message);
            assert_eq!(StatusCode::UNAUTHORIZED, content.status);
        } else {
            panic!("auth suceeded");
        }
    }

    #[tokio::test]
    async fn test_should_fail_authorization_because_of_unknown_user() {
        // given
        let mut request = lambda_http::Request::new(lambda_http::Body::Empty);
        request.headers_mut().append(
            AUTHORIZATION,
            HeaderValue::from_str("Bearer token").unwrap(),
        );

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                if request
                    .uri()
                    .to_string()
                    .eq("https://efg-erfurt.church.tools/api/whoami")
                {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                        .body(include_str!("../../tests/query/responses/ct/whoami_anonymous.json").to_string())
                        .expect("building response failed");
                    return Ok(response);
                }

                Err(AppError::new(format!(
                    "unexpected request: {:?}",
                    &request
                )))
            }
        ).await;

        // when
        let result = check_auth(&request, Arc::new(http_client_mock)).await;

        // then
        http_client_mock_collector.collect_calls();
        if let AuthCheckResult::AuthError(content) = result.unwrap() {
            assert_eq!("AUTH_FAILED", content.message);
            assert_eq!(StatusCode::UNAUTHORIZED, content.status);
        } else {
            panic!("auth suceeded");
        }
    }
}
