use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ImageConfig {
    pub w: Option<u32>,
    pub h: Option<u32>,
    pub texts: Vec<TextConfig>,
    pub footer: Option<FooterConfig>,
    pub margins: Option<u32>,
    pub background: BackgroundConfig,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct FooterConfig {
    pub scale: Option<f32>,
    pub padding: Option<f32>,
    pub logo_size: Option<f32>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TextConfig {
    pub paragraphs: Vec<String>,
    pub font_size: Option<f32>,
    pub line_spacing: Option<f32>,
    pub offset_x: Option<f32>,
    pub offset_y: Option<f32>,
    pub position: Option<Position>,
    pub style: Option<TextStyle>,
    pub width: Option<TextWidth>,
    pub color: Option<ColorConfig>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ColorConfig {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum BackgroundConfig {
    Color { color: ColorConfig },
    Image { path: String },
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum TextStyle {
    Normal,
    Bold,
    Italic,
    BoldItalic,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Position {
    Top,
    Middle,
    Bottom,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum TextWidth {
    Condensed,
    Normal,
    Expanded,
}
