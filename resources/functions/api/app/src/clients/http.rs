use async_trait::async_trait;
use efg_lambda_base::errors::AppError;
use http::{HeaderName, HeaderValue, Request, Response};
use reqwest::Client;
use std::{
    fs::File,
    io::{copy, Cursor},
    str::FromStr,
    sync::Arc,
    time::Duration,
};
use tracing::{debug, debug_span, Instrument};

pub type HttpService = Arc<dyn HttpClient>;

#[async_trait]
pub trait HttpClient: Send + Sync {
    async fn send_request(
        &self,
        _request: Request<Option<String>>,
    ) -> Result<Response<String>, AppError> {
        unimplemented!("send_request")
    }

    async fn fetch_file_to_tmp(&self, _uri: &str) -> Result<File, AppError> {
        unimplemented!("fetch_file_to_tmp")
    }
}

#[derive(Clone)]
pub struct HttpClientImpl {
    reqwest_client: Client,
}

impl HttpClientImpl {
    pub fn new() -> Self {
        HttpClientImpl {
            reqwest_client: Client::builder()
                .timeout(Duration::from_secs(20))
                .build()
                .expect("building reqwest client failed"),
        }
    }

    async fn map_reqwest_response(
        &self,
        response: reqwest::Response,
    ) -> Result<Response<String>, AppError> {
        let mut response_builder = Response::builder();
        response_builder = response_builder.status(response.status().as_u16());

        for (name, value) in response.headers().iter() {
            response_builder = response_builder.header(
                HeaderName::from_str(name.as_str()).unwrap(),
                HeaderValue::from_str(value.to_str().unwrap_or_default()).unwrap(),
            );
        }

        let body = response
            .text()
            .await
            .map_err(|err| AppError::from_error(&err, "reading reqwest reponse as text"))?;

        let final_response = response_builder
            .body(body)
            .map_err(|err| AppError::from_error(&err, "transforming response"))?;

        Ok(final_response)
    }
}

#[async_trait]
impl HttpClient for HttpClientImpl {
    async fn send_request(
        &self,
        request: Request<Option<String>>,
    ) -> Result<Response<String>, AppError> {
        async {
            // TODO simply again when reqwest supports http 1.0
            let method = reqwest::Method::from_str(request.method().as_str()).unwrap();
            let mut headers = reqwest::header::HeaderMap::new();
            for (k, v) in request.headers().iter() {
                headers.append(
                    reqwest::header::HeaderName::from_str(k.as_str()).unwrap(),
                    reqwest::header::HeaderValue::from_str(v.to_str().unwrap()).unwrap(),
                );
            }

            let mut http_request = self
                .reqwest_client
                .request(method, request.uri().to_string())
                .headers(headers);
            if let Some(body) = request.body() {
                http_request = http_request.body(body.clone());
            }

            // send request
            let http_response = http_request
                .send()
                .await
                .map_err(|err| AppError::from_error(&err, "building reqwest request"))?;

            self.map_reqwest_response(http_response).await
        }
        .instrument(debug_span!(
            "send_request",
            uri = request.uri().to_string(),
            method = request.method().to_string()
        ))
        .await
    }

    async fn fetch_file_to_tmp(&self, uri: &str) -> Result<File, AppError> {
        async {
            // TODO simply again when reqwest supports http 1.0
            let http_request = self.reqwest_client.request(reqwest::Method::GET, uri);

            // send request
            let http_response = http_request
                .send()
                .await
                .map_err(|err| AppError::from_error(&err, "building reqwest request"))?;

            let mut file = tempfile::tempfile()
                .map_err(|err| AppError::from_error(&err, "writing temporary file failed"))?;

            let mut content = Cursor::new(
                http_response
                    .bytes()
                    .await
                    .map_err(|err| AppError::from_error(&err, "reading response failed"))?,
            );
            let bytes = copy(&mut content, &mut file).map_err(|err| {
                AppError::from_error(&err, "copying response into temporary file failed")
            })?;

            debug!(
                "wrote temporary file with {} bytes from {} successfully",
                bytes, uri
            );

            Ok(file)
        }
        .instrument(debug_span!("fetch_file_to_tmp", uri = uri.to_string()))
        .await
    }
}

impl Default for HttpClientImpl {
    fn default() -> Self {
        Self::new()
    }
}
