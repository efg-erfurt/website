use super::AuthClient;
use crate::{auth::Role, clients::ct::CtService};
use async_trait::async_trait;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};

const ADMIN_USER: i32 = 1168;

pub struct AuthClientImpl {
    ct: CtService,
}

impl AuthClientImpl {
    pub fn new(ct: CtService) -> Self {
        Self { ct }
    }
}

#[async_trait]
impl AuthClient for AuthClientImpl {
    async fn has_role(&self, role: Role, auth_context: &AuthContext) -> Result<bool, AppError> {
        let user = self.ct.get_current_user(auth_context).await?;

        if user.is_none() {
            return Ok(false);
        }

        match role {
            Role::Admin => {
                let is_admin = user.unwrap().data.id.eq(&ADMIN_USER);
                Ok(is_admin)
            }
            Role::Editor => {
                let group_ids = self
                    .ct
                    .get_group_ids_by_user_id(
                        user.unwrap()
                            .data
                            .id
                            .try_into()
                            .expect("id should be positive"),
                    )
                    .await?;
                let is_editor = group_ids.contains("133");
                Ok(is_editor)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        auth::Role,
        clients::auth::{client::AuthClientImpl, AuthClient},
        create_ct_rest_mock,
        types::ct::{CtWhoAmIResponse, CtWhoAmIResponseData},
    };
    use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
    use secrecy::SecretString;
    use std::{collections::BTreeSet, sync::Arc};

    #[tokio::test]
    async fn test_should_be_editor() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
            async fn get_current_user(
                &self,
                _auth_context: &AuthContext,
            ) -> Result<Option<CtWhoAmIResponse>, AppError> {
                Ok(Some(CtWhoAmIResponse {
                    data: CtWhoAmIResponseData {
                        id: 123,
                        firstName: "Test".to_string(),
                        lastName: "User".to_string(),
                        imageUrl: None,
                    },
                }))
            },
            async fn get_group_ids_by_user_id(
                &self,
                _user_id: usize,
            ) -> Result<BTreeSet<String>, AppError> {
                let mut ids = BTreeSet::new();
                ids.insert("123".to_string());
                ids.insert("133".to_string());
                Ok(ids)
            }
        );
        let auth_client = AuthClientImpl::new(Arc::new(ct_rest_mock));

        // when
        let has_role = auth_client
            .has_role(
                Role::Editor,
                &AuthContext::new(Some(SecretString::from("some".to_string()))),
            )
            .await
            .expect("checking role failed");

        // then
        ct_rest_mock_collector.collect_calls();

        assert!(has_role);
        assert_eq!(2, ct_rest_mock_collector.total_calls());
    }

    #[tokio::test]
    async fn test_should_be_admin() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
            async fn get_current_user(
                &self,
                _auth_context: &AuthContext,
            ) -> Result<Option<CtWhoAmIResponse>, AppError> {
                Ok(Some(CtWhoAmIResponse {
                    data: CtWhoAmIResponseData {
                        id: 1168,
                        firstName: "Test".to_string(),
                        lastName: "User".to_string(),
                        imageUrl: None,
                    },
                }))
            }
        );
        let auth_client = AuthClientImpl::new(Arc::new(ct_rest_mock));

        // when
        let has_role = auth_client
            .has_role(
                Role::Admin,
                &AuthContext::new(Some(SecretString::from("some".to_string()))),
            )
            .await
            .expect("checking role failed");

        // then
        ct_rest_mock_collector.collect_calls();

        assert!(has_role);
        assert_eq!(1, ct_rest_mock_collector.total_calls());
    }

    #[tokio::test]
    async fn test_should_not_be_admin() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
            async fn get_current_user(
                &self,
                _auth_context: &AuthContext,
            ) -> Result<Option<CtWhoAmIResponse>, AppError> {
                Ok(Some(CtWhoAmIResponse {
                    data: CtWhoAmIResponseData {
                        id: 123,
                        firstName: "Test".to_string(),
                        lastName: "User".to_string(),
                        imageUrl: None,
                    },
                }))
            }
        );
        let auth_client = AuthClientImpl::new(Arc::new(ct_rest_mock));

        // when
        let has_role = auth_client
            .has_role(
                Role::Admin,
                &AuthContext::new(Some(SecretString::from("some".to_string()))),
            )
            .await
            .expect("checking role failed");

        // then
        ct_rest_mock_collector.collect_calls();

        assert!(!has_role);
        assert_eq!(1, ct_rest_mock_collector.total_calls());
    }

    #[tokio::test]
    async fn test_should_not_be_editor() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
            async fn get_current_user(
                &self,
                _auth_context: &AuthContext,
            ) -> Result<Option<CtWhoAmIResponse>, AppError> {
                Ok(Some(CtWhoAmIResponse {
                    data: CtWhoAmIResponseData {
                        id: 123,
                        firstName: "Test".to_string(),
                        lastName: "User".to_string(),
                        imageUrl: None,
                    },
                }))
            },
            async fn get_group_ids_by_user_id(
                &self,
                _user_id: usize,
            ) -> Result<BTreeSet<String>, AppError> {
                let mut ids = BTreeSet::new();
                ids.insert("123".to_string());
                Ok(ids)
            }
        );
        let auth_client = AuthClientImpl::new(Arc::new(ct_rest_mock));

        // when
        let has_role = auth_client
            .has_role(
                Role::Editor,
                &AuthContext::new(Some(SecretString::from("some".to_string()))),
            )
            .await
            .expect("checking role failed");

        // then
        ct_rest_mock_collector.collect_calls();

        assert!(!has_role);
        assert_eq!(2, ct_rest_mock_collector.total_calls());
    }
}
