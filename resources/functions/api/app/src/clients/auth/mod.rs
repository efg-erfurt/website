use crate::auth::Role;
use async_trait::async_trait;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};

pub mod client;

pub type AuthService = Box<dyn AuthClient + Send + Sync>;

#[async_trait]
pub trait AuthClient: Send + Sync {
    async fn has_role(&self, _role: Role, _auth_context: &AuthContext) -> Result<bool, AppError> {
        unimplemented!("has_role")
    }
}
