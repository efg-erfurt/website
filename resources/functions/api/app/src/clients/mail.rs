use crate::{config::smtp::SmtpConfig, schema::types::send_message_input::SendMessageInput};
use async_trait::async_trait;
use efg_lambda_base::errors::AppError;
use lettre::{
    message::Mailbox, transport::smtp::authentication::Credentials, Message, SmtpTransport,
    Transport,
};
use secrecy::ExposeSecret;
use std::collections::BTreeMap;
use tracing::{info, log::warn};

pub type MailService = Box<dyn MailClient>;

#[async_trait]
pub trait MailClient: Send + Sync {
    async fn send_mail(&self, _message_input: &SendMessageInput) -> Result<(), AppError> {
        unimplemented!("send_mail")
    }
}

#[derive(Clone)]
pub struct MailClientImpl {
    mailer: SmtpTransport,
    sender_address: String,
    recipients: BTreeMap<String, String>,
}

impl MailClientImpl {
    pub fn new(smtp_config: &SmtpConfig) -> Self {
        let creds = Credentials::new(
            smtp_config.user().to_string(),
            smtp_config.password().expose_secret().to_string(),
        );

        let mailer = SmtpTransport::relay(smtp_config.host())
            .unwrap()
            .credentials(creds)
            .build();

        let recipients = vec![
            ("pastor", "ossa@efg-erfurt.de"),
            ("buero", "buero@efg-erfurt.de"),
            ("gemeindeleiter", "gemeindeleiter@efg-erfurt.de"),
            ("gebet", "gebet@efg-erfurt.de"),
            ("international", "international@efg-erfurt.de"),
            (
                "oeffentlichkeitsarbeit",
                "oeffentlichkeitsarbeit@efg-erfurt.de",
            ),
            ("finanzen", "finanzen@efg-erfurt.de"),
            ("verwaltung", "verwaltung@efg-erfurt.de"),
            ("kleingruppen", "gemeindeentwicklung@efg-erfurt.de"),
        ]
        .iter()
        .map(|(key, mail)| (key.to_string(), mail.to_string()))
        .collect();

        Self {
            mailer,
            recipients,
            sender_address: smtp_config.mail().to_string(),
        }
    }
}

#[async_trait]
impl MailClient for MailClientImpl {
    async fn send_mail(&self, message_input: &SendMessageInput) -> Result<(), AppError> {
        let recipient = self.recipients.get(&message_input.destination);
        if recipient.is_none() {
            return Err(AppError::new("unknown recipient"));
        }

        let email = Message::builder()
            .from(Mailbox::new(
                Some("Webseite".to_string()),
                self.sender_address.parse().expect("invalid mail address"),
            ))
            .reply_to(match message_input.mail.clone() {
                Some(mail) => {
                    let name = message_input
                        .name
                        .clone()
                        .unwrap_or_else(|| "Anonym".to_string());
                    Mailbox::new(Some(name), mail.parse().expect("invalid mail address"))
                }
                None => Mailbox::new(
                    Some("No Reply".to_string()),
                    self.sender_address.parse().expect("invalid mail address"),
                ),
            })
            .to(Mailbox::new(
                Some(message_input.destination.clone()),
                recipient.unwrap().parse().expect("invalid mail address"),
            ))
            .subject("Anfrage von Webseite")
            .body(String::from(&message_input.message))
            .unwrap();

        match self.mailer.send(&email) {
            Ok(res) => {
                if res.is_positive() {
                    info!("sent message successfully");
                } else {
                    warn!("sent message with negative result");
                }
                Ok(())
            }
            Err(err) => Err(AppError::from_error(&err, "sending message failed")),
        }
    }
}
