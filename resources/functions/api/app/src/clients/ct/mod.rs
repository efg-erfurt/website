use crate::types::ct::{
    CtEventsOverviewResponse, CtEventsResponse, CtGetPersonResponse, CtGroupsResponse,
    CtWhoAmIResponse,
};
use async_trait::async_trait;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
use std::{collections::BTreeSet, sync::Arc};

pub mod client;

pub type CtService = Arc<dyn CtClient>;

#[async_trait]
pub trait CtClient: Send + Sync {
    /// Returns the current user by token.
    async fn get_current_user(
        &self,
        _auth_context: &AuthContext,
    ) -> Result<Option<CtWhoAmIResponse>, AppError> {
        unimplemented!("get_current_user")
    }

    /// Returns a user by id.
    async fn get_user_by_id(
        &self,
        _user_id: &str,
    ) -> Result<Option<CtGetPersonResponse>, AppError> {
        unimplemented!("get_user_by_id")
    }

    async fn get_group_ids_by_user_id(
        &self,
        _user_id: usize,
    ) -> Result<BTreeSet<String>, AppError> {
        unimplemented!("get_group_ids_by_user_id")
    }

    async fn get_calendars_by_category_ids(
        &self,
        _category_ids: &[String],
    ) -> Result<CtEventsResponse, AppError> {
        unimplemented!("get_calendars_by_category_ids")
    }

    async fn get_categories(&self) -> Result<CtEventsOverviewResponse, AppError> {
        unimplemented!("get_categories")
    }

    async fn get_group_by_id(&self, _group_id: &str) -> Result<CtGroupsResponse, AppError> {
        unimplemented!("get_group_by_id")
    }
}
