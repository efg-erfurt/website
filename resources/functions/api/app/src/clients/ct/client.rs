use super::CtClient;
use crate::{
    clients::{
        cache::{cache_entry::CacheEntry, CacheClient},
        http::HttpClient,
    },
    types::ct::{
        CtCalPerCategoryRequest, CtEventsOverviewResponse, CtEventsResponse, CtFuncRequest,
        CtGetPersonResponse, CtGroupsResponse, CtLoginRequest, CtLoginResponse,
        CtPersonalGroupsResponse, CtWhoAmIResponse,
    },
};
use async_trait::async_trait;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
use http::{
    header::{AUTHORIZATION, CONTENT_LENGTH, CONTENT_TYPE, COOKIE, SET_COOKIE},
    Method, Request,
};
use lambda_http::http::StatusCode;
use md5::{Digest, Md5};
use secrecy::{ExposeSecret, SecretString};
use serde::{Deserialize, Serialize};
use std::{
    collections::BTreeSet,
    fmt::{Debug, Formatter},
    sync::Arc,
};
use tokio::sync::{mpsc, oneshot};
use tracing::{debug, info, info_span, warn, Instrument};

pub const BASE_PATH: &str = "https://efg-erfurt.church.tools";
pub const CT_COOKIE: &str = "ChurchTools_ct_efg-erfurt";
const CACHE_KEY_TOKEN: &str = "ct-token";

#[derive(Debug)]
pub struct TokenRequest {
    pub sender: oneshot::Sender<Result<SecretString, AppError>>,
}

pub async fn is_authorized<H>(
    token: &SecretString,
    user_id: usize,
    http_client: Arc<H>,
) -> Result<bool, AppError>
where
    H: HttpClient + Send + Sync,
{
    async {
        let request = Request::builder()
            .uri(&format!(
                "{BASE_PATH}/api/persons/{user_id}/groups?show_overdue_groups=false&show_inactive_groups=false"
            ))
            .method(Method::GET)
            .header(COOKIE, format!("{CT_COOKIE}={}", &token.expose_secret()))
            .body(None)
            .map_err(|err| AppError::from_error(&err, "building login request failed"))?;

        let response = http_client.send_request(request).await?;

        let personal_groups = serde_json::from_str::<CtPersonalGroupsResponse>(response.body())
            .map_err(|err| AppError::from_error(&err, "fetching personal groups failed"))?;

        let is_authorized = personal_groups
            .data
            .iter()
            .any(|g| g.group.domainIdentifier.eq("133") && g.group.domainType.eq("group"));

        Ok(is_authorized)
    }
    .instrument(info_span!("is_authorized", user_id = user_id))
    .await
}

pub async fn fetch_token<H>(
    username: &str,
    password: &SecretString,
    http_client: Arc<H>,
) -> Result<Option<(CtLoginResponse, SecretString)>, AppError>
where
    H: HttpClient + Send + Sync,
{
    let body = serde_json::to_string(&CtLoginRequest {
        username: username.to_string(),
        password: password.expose_secret().to_string(),
        rememberMe: false,
    })
    .unwrap();

    let request = Request::builder()
        .uri(&format!("{BASE_PATH}/api/login"))
        .method(Method::POST)
        .header(CONTENT_TYPE, "application/json")
        .header(CONTENT_LENGTH, body.len())
        .body(Some(body))
        .map_err(|err| AppError::from_error(&err, "building login request failed"))?;

    let response = http_client.send_request(request).await?;
    if response.status() != StatusCode::OK {
        warn!("unsuccessful status code {}", response.status());
        return Ok(None);
    }

    let user = serde_json::from_str::<CtLoginResponse>(response.body())
        .map_err(|err| AppError::from_error(&err, "parsing login response failed"))?;

    let session_cookie = response
        .headers()
        .get(SET_COOKIE)
        .ok_or_else(|| AppError::new("No cookie found"))?;

    let token = session_cookie
        .to_str()
        .map(|header| {
            header
                .split(';')
                .map(|header_value| header_value.trim())
                .filter(|header_value| header_value.starts_with(CT_COOKIE))
                .map(|ct_header| {
                    ct_header
                        .split('=')
                        .nth(1)
                        .expect("parsing CT token failed")
                })
        })
        .expect("reading cookie content failed")
        .next()
        .expect("missing Cookie header in CT response")
        .to_string();

    Ok(Some((user, SecretString::from(token))))
}

/// Wrapper service for item specific churchtools interfaces. This might be a REST connection or a mock implementation for example.
/// This service also serializes and deserializes responses from CT.
pub struct CtClientImpl<H, C>
where
    H: HttpClient + Send + Sync + 'static,
    C: CacheClient + Send + Sync + 'static,
{
    http_client: Arc<H>,
    cache_client: Arc<C>,
    sender: Option<mpsc::Sender<TokenRequest>>,
}

impl<H, C> CtClientImpl<H, C>
where
    H: HttpClient + Send + Sync + 'static,
    C: CacheClient + Send + Sync + 'static,
{
    /// Creates a new instance with the provided implementation.
    pub fn new(
        username: &str,
        password: &SecretString,
        cache_client: Arc<C>,
        http_client: Arc<H>,
    ) -> Self {
        let (sender, receiver) = mpsc::channel(1024);

        let client = CtClientImpl {
            sender: Some(sender),
            cache_client: cache_client.clone(),
            http_client: http_client.clone(),
        };

        client.handle_ct_token_requests(receiver, username, password, cache_client, http_client);

        client
    }

    fn handle_ct_token_requests(
        &self,
        mut token_request_receiver: mpsc::Receiver<TokenRequest>,
        ct_username: &str,
        ct_password: &SecretString,
        cache_client: Arc<C>,
        http_client: Arc<H>,
    ) {
        let local_username = ct_username.to_string();
        let local_password = ct_password.clone();

        // handle token requests in separate thread asynchronously
        tokio::spawn(async move {
            info!("waiting for token requests now...");

            while let Some(request) = token_request_receiver.recv().await {
                debug!("received token request");

                let token_retrieval_result: Result<SecretString, AppError> = async {
                    let used_token = match cache_client
                        .get_by_key(CACHE_KEY_TOKEN)
                        .await?
                        .map(|entry| SecretString::from(entry.value))
                    {
                        Some(existing_token) => existing_token,
                        None => {
                            let (_user, fresh_token) =
                                fetch_token(&local_username, &local_password, http_client.clone())
                                    .await?
                                    .expect("login failed, function needs to restart");
                            let exposed_token = fresh_token.expose_secret().to_string();

                            cache_client
                                .update_cache(CacheEntry::new(CACHE_KEY_TOKEN, &exposed_token))
                                .await?;

                            debug!("fetched fresh technical token");

                            fresh_token
                        }
                    };

                    Ok(used_token)
                }
                .await;

                // send fresh token to waiting request
                request
                    .sender
                    .send(token_retrieval_result)
                    .expect("sending token result failed");
            }
        });
    }

    async fn receive_token(&self) -> Result<SecretString, AppError> {
        if self.sender.is_none() {
            return Err(AppError::new("expected sender for token requests"));
        }

        let (sender, receiver) = oneshot::channel();

        // send token request
        self.sender
            .as_ref()
            .unwrap()
            .send(TokenRequest { sender })
            .await
            .map_err(|err| AppError::from_error(&err, "sending token request"))?;

        // wait for valid token
        receiver
            .await
            .map_err(|err| AppError::from_error(&err, "waiting for token response"))?
    }

    /// Sends a cached GET request to the specific path.
    /// The result is deserialized to T.
    async fn send_get<T>(
        &self,
        path: &str,
        custom_token: &Option<SecretString>,
    ) -> Result<T, AppError>
    where
        for<'de> T: Deserialize<'de>,
    {
        let request = self
            .create_request(path, Method::GET, None, custom_token)
            .await?;

        self.send_request_with_cache(request).await
    }

    /// Sends a cached POST request to the specific path.
    /// The payload is serialized into JSON from B.
    /// The result is deserialized to T.
    async fn send_post<T, B>(
        &self,
        path: &str,
        body: B,
        custom_token: &Option<SecretString>,
    ) -> Result<T, AppError>
    where
        for<'de> T: Deserialize<'de>,
        B: Serialize + Debug,
    {
        let serialized_body = serde_json::to_string(&body)
            .map_err(|err| AppError::from_error(&err, "serialization of request body"))?;

        let request = self
            .create_request(path, Method::POST, Some(serialized_body), custom_token)
            .await?;

        self.send_request_with_cache(request).await
    }

    async fn create_request(
        &self,
        path: &str,
        method: Method,
        body: Option<String>,
        custom_token: &Option<SecretString>,
    ) -> Result<http::Request<Option<String>>, AppError> {
        let mut req_builder = Request::builder()
            .method(method)
            .uri(format!("{BASE_PATH}{path}"));

        let used_token = if let Some(token) = custom_token {
            token.clone()
        } else {
            self.receive_token().await?
        };

        req_builder = req_builder.header(
            COOKIE,
            format!("{}={}", CT_COOKIE, &used_token.expose_secret()),
        );
        if body.is_some() {
            req_builder = req_builder
                .header(CONTENT_LENGTH, body.as_ref().unwrap().len())
                .header(CONTENT_TYPE, "application/json");
        }

        req_builder
            .body(body)
            .map_err(|err| AppError::from_error(&err, "building request"))
    }

    async fn send_request_with_cache<T>(
        &self,
        request: http::Request<Option<String>>,
    ) -> Result<T, AppError>
    where
        for<'de> T: Deserialize<'de>,
    {
        let cache_key = build_cache_key(&request);
        let cached_value = self.cache_client.get_by_key(&cache_key).await?;

        let serialized_body = match cached_value {
            Some(cached_body) => {
                info!("received response for key {} from cache", &cache_key);
                Ok(cached_body.value)
            }
            None => {
                let res = self.http_client.send_request(request).await?;

                if res.status().is_server_error() {
                    return Err(AppError::new("CT responded with error"));
                } else if !res.status().is_success() {
                    warn!("received non-success response status {}", res.status());
                }

                let body = res.body().clone();

                // cache result
                self.cache_client
                    .update_cache(CacheEntry {
                        key: cache_key.clone(),
                        value: body.clone(),
                    })
                    .await?;

                debug!("wrote response for key {} into cache", &cache_key);

                Ok(body)
            }
        }?;

        serde_json::from_str::<T>(&serialized_body)
            .map_err(|err| AppError::from_error(&err, "deserializing response"))
    }
}

/// Builds a cache key based on MD5 hashing the request URI as well as the optional payload and authorization header.
fn build_cache_key(request: &http::Request<Option<String>>) -> String {
    let mut hasher = Md5::new();

    // hash uri, method and payload
    hasher = hasher
        .chain_update(request.uri().to_string())
        .chain_update(request.method().to_string())
        .chain_update(request.body().as_ref().unwrap_or(&String::new()));

    // authorization and cookies for custom user tokens
    if let Some(auth_header) = request.headers().get(COOKIE) {
        let header_value = auth_header
            .to_str()
            .expect("reading cookie header for caching failed");
        hasher.update(header_value);
    } else if let Some(auth_header) = request.headers().get(AUTHORIZATION) {
        let header_value = auth_header
            .to_str()
            .expect("reading authorization header for caching failed");
        hasher.update(header_value);
    }

    let hash = hasher.finalize();

    base16ct::lower::encode_string(&hash)
}

#[async_trait]
impl<H, C> CtClient for CtClientImpl<H, C>
where
    H: HttpClient + Send + Sync + 'static,
    C: CacheClient + Send + Sync + 'static,
{
    /// Returns the current user by token.
    async fn get_current_user(
        &self,
        auth_context: &AuthContext,
    ) -> Result<Option<CtWhoAmIResponse>, AppError> {
        Ok(match auth_context.token() {
            Some(token) => {
                let response = self
                    .send_get::<CtWhoAmIResponse>("/api/whoami", &Some(token.clone()))
                    .await?;
                if response.data.id > 0 {
                    Some(response)
                } else {
                    None
                }
            }
            None => None,
        })
    }

    /// Returns a user by id.
    async fn get_user_by_id(&self, user_id: &str) -> Result<Option<CtGetPersonResponse>, AppError> {
        let response = self
            .send_get::<CtGetPersonResponse>(&format!("/api/persons/{user_id}"), &None)
            .await?;

        if response
            .messageKey
            .as_ref()
            .filter(|value| (*value).eq("error.notfound"))
            .is_some()
        {
            return Ok(None);
        }

        Ok(Some(response))
    }

    async fn get_group_ids_by_user_id(&self, user_id: usize) -> Result<BTreeSet<String>, AppError> {
        let response = self
            .send_get::<CtPersonalGroupsResponse>(&format!(
                "/api/persons/{user_id}/groups?show_overdue_groups=false&show_inactive_groups=false", 
            ),
            &None)
            .await?;

        Ok(response
            .data
            .iter()
            .filter(|data| data.group.domainType.eq("group"))
            .map(|data| data.group.domainIdentifier.to_string())
            .collect())
    }

    async fn get_calendars_by_category_ids(
        &self,
        category_ids: &[String],
    ) -> Result<CtEventsResponse, AppError> {
        // sort ids for stable request caching
        let mut sorted_ids = category_ids.to_vec();
        sorted_ids.sort();

        let response = self
            .send_post::<CtEventsResponse, CtCalPerCategoryRequest>(
                "/index.php?q=churchcal/ajax",
                CtCalPerCategoryRequest {
                    func: "getCalPerCategory".to_string(),
                    category_ids: sorted_ids,
                },
                &None,
            )
            .await?;

        Ok(response)
    }

    async fn get_categories(&self) -> Result<CtEventsOverviewResponse, AppError> {
        let response = self
            .send_post::<CtEventsOverviewResponse, CtFuncRequest>(
                "/index.php?q=churchcal/ajax",
                CtFuncRequest {
                    func: "getMasterData".to_string(),
                },
                &None,
            )
            .await?;

        Ok(response)
    }

    async fn get_group_by_id(&self, group_id: &str) -> Result<CtGroupsResponse, AppError> {
        let response = self
            .send_get::<CtGroupsResponse>(&format!("/api/grouphomepages/{group_id}"), &None)
            .await?;

        Ok(response)
    }
}

impl<H, C> Debug for CtClientImpl<H, C>
where
    H: HttpClient + Send + Sync + 'static,
    C: CacheClient + Send + Sync + 'static,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Ct").finish()
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        clients::{
            cache::{cache_entry::CacheEntry, CacheClientImpl},
            ct::client::{build_cache_key, CtClientImpl},
        },
        create_http_client_mock,
    };
    use efg_lambda_base::errors::AppError;
    use efg_testing::create_db_mock;
    use http::header::{CONTENT_LENGTH, CONTENT_TYPE, COOKIE, SET_COOKIE};
    use http::{Method, Request, Response, StatusCode};
    use secrecy::SecretString;
    use std::sync::Arc;

    #[test]
    fn test_should_build_stable_hash_key_for_get() {
        // given
        let request = Request::builder()
            .method(Method::GET)
            .uri("https://rust.org/path")
            .body(None)
            .expect("building request failed");

        // when
        let key = build_cache_key(&request);

        // then
        assert_eq!("75fdb25e046d0b1845ec59cadfd8d7cf", key);
    }

    #[test]
    fn test_should_build_stable_hash_key_for_get_with_other_uri() {
        // given
        let request = Request::builder()
            .method(Method::GET)
            .uri("https://google.de/path")
            .body(None)
            .expect("building request failed");

        // when
        let key = build_cache_key(&request);

        // then
        assert_eq!("63575529961befeb192b2a3d4fc0d023", key);
    }

    #[test]
    fn test_should_build_stable_hash_key_for_post() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .uri("https://rust.org/path")
            .body(None)
            .expect("building request failed");

        // when
        let key = build_cache_key(&request);

        // then
        assert_eq!("9c854d2f5f643fabc9aae7519a51fbd4", key);
    }

    #[test]
    fn test_should_build_stable_hash_key_for_post_with_body() {
        // given
        let request = Request::builder()
            .method(Method::POST)
            .uri("https://rust.org/path")
            .body(Some("body".to_string()))
            .expect("building request failed");

        // when
        let key = build_cache_key(&request);

        // then
        assert_eq!("28e2cfb0342954d128c0053577a06a5b", key);
    }

    #[tokio::test]
    async fn test_should_send_get() {
        // given
        let (cache_db_mock, mut cache_db_mock_collector) = create_db_mock!(
            CacheEntry,
            MockCacheClient,
            async fn get_value_by_id(&self, _id: &str) -> Result<Option<CacheEntry>, AppError> {
                Ok(None)
            },
            async fn put_item(&self, _item: CacheEntry) -> Result<(), AppError> {
                Ok(())
            }
        );

        let (http_client_mock, mut collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                if request
                    .uri()
                    .to_string()
                    .ne("https://efg-erfurt.church.tools/test")
                    || request.method().ne(&Method::GET)
                {
                    return Err(AppError::new(format!("unexpected request: {:?}", request)));
                }

                let response = Response::builder()
                    .status(StatusCode::OK)
                    .body("\"Successful\"".to_string())
                    .expect("building response failed");

                Ok(response)
            }
        )
        .await;

        // when
        let ct_client = CtClientImpl::new(
            "user",
            &SecretString::from("pw".to_string()),
            Arc::new(CacheClientImpl::new(cache_db_mock)),
            Arc::new(http_client_mock),
        );

        // then
        let response = ct_client
            .send_get::<String>("/test", &Some(SecretString::from("token".to_string())))
            .await
            .expect("sending request failed");

        cache_db_mock_collector.collect_calls();
        assert_eq!("Successful", &response);

        collector.collect_calls();
        assert_eq!(1, collector.total_calls());
    }

    #[tokio::test]
    async fn test_should_send_post() {
        // given
        let (cache_db_mock, mut cache_db_mock_collector) = create_db_mock!(
            CacheEntry,
            MockCacheClient,
            async fn get_value_by_id(&self, _id: &str) -> Result<Option<CacheEntry>, AppError> {
                Ok(None)
            },
            async fn put_item(&self, _item: CacheEntry) -> Result<(), AppError> {
                Ok(())
            }
        );

        let (http_client_mock, mut collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                if request
                    .uri()
                    .to_string()
                    .ne("https://efg-erfurt.church.tools/test-2")
                    || request.method().ne(&Method::POST)
                    || request.headers().get(CONTENT_LENGTH).unwrap().ne("6")
                    || request
                        .headers()
                        .get(CONTENT_TYPE)
                        .unwrap()
                        .ne("application/json")
                    || request.body().ne(&Some("\"body\"".to_string()))
                {
                    return Err(AppError::new(format!("unexpected request: {:?}", request)));
                }

                let response = Response::builder()
                    .status(StatusCode::OK)
                    .body("\"Successful\"".to_string())
                    .expect("building response failed");

                Ok(response)
            }
        )
        .await;

        // when
        let ct_client = CtClientImpl::new(
            "user",
            &SecretString::from("pw".to_string()),
            Arc::new(CacheClientImpl::new(cache_db_mock)),
            Arc::new(http_client_mock),
        );

        // then
        let response = ct_client
            .send_post::<String, String>(
                "/test-2",
                "body".to_string(),
                &Some(SecretString::from("token".to_string())),
            )
            .await
            .expect("sending request failed");

        cache_db_mock_collector.collect_calls();

        assert_eq!("Successful", &response);

        collector.collect_calls();
        assert_eq!(1, collector.total_calls());
    }

    #[tokio::test]
    async fn test_should_send_authorized_post() {
        // given
        let (cache_db_mock, mut cache_db_mock_collector) = create_db_mock!(
            CacheEntry,
            MockCacheClient,
            async fn get_value_by_id(&self, _id: &str) -> Result<Option<CacheEntry>, AppError> {
                Ok(None)
            },
            async fn put_item(&self, _item: CacheEntry) -> Result<(), AppError> {
                Ok(())
            }
        );

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                if request
                    .uri()
                    .to_string()
                    .eq("https://efg-erfurt.church.tools/test")
                    && request.method().eq(&Method::POST)
                    && request
                        .headers()
                        .get(COOKIE)
                        .unwrap()
                        .eq("ChurchTools_ct_efg-erfurt=abc-my-token")
                    && request.body().eq(&Some("\"body\"".to_string()))
                {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .body("\"Successful\"".to_string())
                        .expect("building response failed");
                    return Ok(response);
                } else if request
                    .uri()
                    .to_string()
                    .eq("https://efg-erfurt.church.tools/api/login")
                    && request.method().eq(&Method::POST)
                    && request
                        .body()
                        .eq(&Some("{\"username\":\"user\",\"password\":\"pw\",\"rememberMe\":false}".to_string()))
                {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                        .body(include_str!("../../../tests/query/responses/ct/login.json").to_string())
                        .expect("building response failed");
                    return Ok(response);
                }

                Err(AppError::new(format!(
                    "unexpected request: {:?}",
                    &request
                )))
            }
        ).await;

        // when
        let ct_client = CtClientImpl::new(
            "user",
            &SecretString::from("pw".to_string()),
            Arc::new(CacheClientImpl::new(cache_db_mock)),
            Arc::new(http_client_mock),
        );

        // then
        let response = ct_client
            .send_post::<String, String>("/test", "body".to_string(), &None)
            .await
            .expect("sending request failed");

        assert_eq!("Successful", &response);

        http_client_mock_collector.collect_calls();
        cache_db_mock_collector.collect_calls();
        assert_eq!(2, http_client_mock_collector.total_calls());
        assert_eq!(
            2,
            http_client_mock_collector.get_calls_by_method("send_request")
        );
        assert_eq!(4, cache_db_mock_collector.total_calls());
        assert_eq!(2, cache_db_mock_collector.get_calls_by_method("put_item"));
        assert_eq!(
            2,
            cache_db_mock_collector.get_calls_by_method("get_value_by_id")
        );
    }

    #[tokio::test]
    async fn test_should_send_authorized_get() {
        // given
        let (cache_db_mock, mut cache_db_mock_collector) = create_db_mock!(
            CacheEntry,
            MockCacheClient,
            async fn get_value_by_id(&self, _id: &str) -> Result<Option<CacheEntry>, AppError> {
                Ok(None)
            },
            async fn put_item(&self, _item: CacheEntry) -> Result<(), AppError> {
                Ok(())
            }
        );

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                if request
                    .uri()
                    .to_string()
                    .eq("https://efg-erfurt.church.tools/test")
                    && request.method().eq(&Method::GET)
                    && request
                        .headers()
                        .get(COOKIE)
                        .unwrap()
                        .eq("ChurchTools_ct_efg-erfurt=abc-my-token")
                {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .body("\"Successful\"".to_string())
                        .expect("building response failed");
                    return Ok(response);
                } else if request
                    .uri()
                    .to_string()
                    .eq("https://efg-erfurt.church.tools/api/login")
                    && request.method().eq(&Method::POST)
                    && request
                        .body()
                        .eq(&Some("{\"username\":\"user\",\"password\":\"pw\",\"rememberMe\":false}".to_string()))
                {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .header(SET_COOKIE, "ChurchTools_ct_efg-erfurt=abc-my-token; expires=Sun, 21-Aug-2022 15:15:42 GMT; Max-Age=604800; path=/; secure; HttpOnly; SameSite=None")
                        .body(include_str!("../../../tests/query/responses/ct/login.json").to_string())
                        .expect("building response failed");
                    return Ok(response);
                }

                Err(AppError::new(format!(
                    "unexpected request: {:?}",
                    &request
                )))
            }
        ).await;

        // when
        let ct_client = CtClientImpl::new(
            "user",
            &SecretString::from("pw".to_string()),
            Arc::new(CacheClientImpl::new(cache_db_mock)),
            Arc::new(http_client_mock),
        );

        // then
        let response = ct_client
            .send_get::<String>("/test", &None)
            .await
            .expect("sending request failed");

        cache_db_mock_collector.collect_calls();
        assert_eq!("Successful", &response);

        http_client_mock_collector.collect_calls();
        assert_eq!(2, http_client_mock_collector.total_calls());
        assert_eq!(
            2,
            http_client_mock_collector.get_calls_by_method("send_request")
        );
        assert_eq!(4, cache_db_mock_collector.total_calls());
        assert_eq!(2, cache_db_mock_collector.get_calls_by_method("put_item"));
        assert_eq!(
            2,
            cache_db_mock_collector.get_calls_by_method("get_value_by_id")
        );
    }

    #[tokio::test]
    async fn test_should_send_authorized_get_with_cached_token() {
        // given
        let (cache_db_mock, mut cache_db_mock_collector) = create_db_mock!(
            CacheEntry,
            MockCacheClient,
            async fn get_value_by_id(&self, id: &str) -> Result<Option<CacheEntry>, AppError> {
                if id.eq("ct-token") {
                    Ok(Some(CacheEntry {
                        key: "ct-token".to_string(),
                        value: "abc-my-token".to_string(),
                    }))
                } else {
                    Ok(None)
                }
            },
            async fn put_item(&self, _item: CacheEntry) -> Result<(), AppError> {
                Ok(())
            }
        );

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                if request
                    .uri()
                    .to_string()
                    .eq("https://efg-erfurt.church.tools/test")
                    && request.method().eq(&Method::GET)
                    && request
                        .headers()
                        .get(COOKIE)
                        .unwrap()
                        .eq("ChurchTools_ct_efg-erfurt=abc-my-token")
                {
                    let response = Response::builder()
                        .status(StatusCode::OK)
                        .body("\"Successful\"".to_string())
                        .expect("building response failed");
                    return Ok(response);
                }

                Err(AppError::new(format!("unexpected request: {:?}", &request)))
            }
        )
        .await;

        // when
        let ct_client = CtClientImpl::new(
            "user",
            &SecretString::from("pw".to_string()),
            Arc::new(CacheClientImpl::new(cache_db_mock)),
            Arc::new(http_client_mock),
        );

        // then
        let response = ct_client
            .send_get::<String>("/test", &None)
            .await
            .expect("sending request failed");

        cache_db_mock_collector.collect_calls();
        assert_eq!("Successful", &response);

        http_client_mock_collector.collect_calls();
        assert_eq!(1, http_client_mock_collector.total_calls());
        assert_eq!(
            1,
            http_client_mock_collector.get_calls_by_method("send_request")
        );
        assert_eq!(3, cache_db_mock_collector.total_calls());
        assert_eq!(
            2,
            cache_db_mock_collector.get_calls_by_method("get_value_by_id")
        );
        assert_eq!(1, cache_db_mock_collector.get_calls_by_method("put_item"));
    }

    #[tokio::test]
    async fn test_should_send_no_get_with_cache() {
        // given
        let (cache_db_mock, mut cache_db_mock_collector) = create_db_mock!(
            CacheEntry,
            MockCacheClient,
            async fn get_value_by_id(&self, id: &str) -> Result<Option<CacheEntry>, AppError> {
                if id.eq("ct-token") {
                    Ok(Some(CacheEntry {
                        key: "ct-token".to_string(),
                        value: "abc-my-token".to_string(),
                    }))
                } else if id.eq("91d1c70a43137da772fb6660f510b4ce") {
                    Ok(Some(CacheEntry {
                        key: "ct-token".to_string(),
                        value: "\"cached\"".to_string(),
                    }))
                } else {
                    Ok(None)
                }
            }
        );

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                _request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                Err(AppError::new("should send no request"))
            }
        )
        .await;

        // when
        let ct_client = CtClientImpl::new(
            "user",
            &SecretString::from("pw".to_string()),
            Arc::new(CacheClientImpl::new(cache_db_mock)),
            Arc::new(http_client_mock),
        );

        // then
        let response = ct_client
            .send_get::<String>("/test", &None)
            .await
            .expect("sending request failed");

        cache_db_mock_collector.collect_calls();
        assert_eq!("cached", &response);

        http_client_mock_collector.collect_calls();
        assert_eq!(0, http_client_mock_collector.total_calls());
        assert_eq!(2, cache_db_mock_collector.total_calls());
        assert_eq!(
            2,
            cache_db_mock_collector.get_calls_by_method("get_value_by_id")
        );
    }

    #[tokio::test]
    async fn test_should_send_no_post_with_cache() {
        // given
        let (cache_db_mock, mut cache_db_mock_collector) = create_db_mock!(
            CacheEntry,
            MockCacheClient,
            async fn get_value_by_id(&self, id: &str) -> Result<Option<CacheEntry>, AppError> {
                if id.eq("ct-token") {
                    Ok(Some(CacheEntry {
                        key: "ct-token".to_string(),
                        value: "abc-my-token".to_string(),
                    }))
                } else if id.eq("371129be3181d03bbd278372893410a0") {
                    Ok(Some(CacheEntry {
                        key: "ct-token".to_string(),
                        value: "\"cached\"".to_string(),
                    }))
                } else {
                    Ok(None)
                }
            }
        );

        let (http_client_mock, mut http_client_mock_collector) = create_http_client_mock!(
            async fn send_request(
                &self,
                _request: Request<Option<String>>,
            ) -> Result<Response<String>, AppError> {
                Err(AppError::new("should send no request"))
            }
        )
        .await;

        // when
        let ct_client = CtClientImpl::new(
            "user",
            &SecretString::from("pw".to_string()),
            Arc::new(CacheClientImpl::new(cache_db_mock)),
            Arc::new(http_client_mock),
        );

        // then
        let response = ct_client
            .send_post::<String, String>("/test", "\"body\"".to_string(), &None)
            .await
            .expect("sending request failed");

        cache_db_mock_collector.collect_calls();
        assert_eq!("cached", &response);

        http_client_mock_collector.collect_calls();
        assert_eq!(0, http_client_mock_collector.total_calls());
        assert_eq!(2, cache_db_mock_collector.total_calls());
        assert_eq!(
            2,
            cache_db_mock_collector.get_calls_by_method("get_value_by_id")
        );
    }
}
