use aws_sdk_dynamodb::types::AttributeValue;
use efg_db_client::db_item::DbItem;
use efg_lambda_base::errors::AppError;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct CacheEntry {
    pub key: String,
    pub value: String,
}

impl CacheEntry {
    pub fn new(key: &str, value: &str) -> Self {
        Self {
            key: key.to_string(),
            value: value.to_string(),
        }
    }
}

impl DbItem<CacheEntry> for CacheEntry {
    fn from_raw(raw: &HashMap<String, AttributeValue>) -> Result<CacheEntry, AppError> {
        let key = raw.get("id").unwrap().as_s().unwrap().clone();
        let value = raw.get("value").unwrap().as_s().unwrap().clone();
        let content = CacheEntry { key, value };

        Ok(content)
    }

    fn to_raw(&self) -> HashMap<String, AttributeValue> {
        let mut raw = HashMap::new();
        raw.insert("id".to_string(), AttributeValue::S(self.key.to_string()));
        raw.insert(
            "value".to_string(),
            AttributeValue::S(self.value.to_string()),
        );
        raw
    }

    fn get_id(&self) -> String {
        self.key.to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::CacheEntry;
    use aws_sdk_dynamodb::types::AttributeValue;
    use efg_db_client::db_item::DbItem;
    use std::collections::HashMap;

    #[tokio::test]
    async fn test_should_transform_entity_to_raw() {
        // given
        let sermon = CacheEntry {
            key: "my-key".to_string(),
            value: "some value".to_string(),
        };

        // when
        let raw = sermon.to_raw();

        // then
        assert_eq!("my-key", raw.get("id").unwrap().as_s().unwrap());
        assert_eq!("some value", raw.get("value").unwrap().as_s().unwrap());
    }

    #[tokio::test]
    async fn test_should_transform_raw_to_entity() {
        // given
        let mut raw = HashMap::new();
        raw.insert("id".to_string(), AttributeValue::S("my-key".to_string()));
        raw.insert(
            "value".to_string(),
            AttributeValue::S("some value".to_string()),
        );

        // when
        let entity = CacheEntry::from_raw(&raw).unwrap();

        // then
        assert_eq!("my-key", entity.key);
        assert_eq!("some value", entity.value);
    }
}
