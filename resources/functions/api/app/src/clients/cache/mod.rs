use self::cache_entry::CacheEntry;
use async_trait::async_trait;
use efg_db_client::db::{DbClient, DbService};
use efg_lambda_base::errors::AppError;

pub mod cache_entry;

pub type CacheService = Box<dyn CacheClient>;

#[async_trait]
pub trait CacheClient: Send + Sync {
    async fn get_by_key(&self, _key: &str) -> Result<Option<CacheEntry>, AppError> {
        unimplemented!("get_by_key")
    }

    async fn update_cache(&self, _entry: CacheEntry) -> Result<(), AppError> {
        unimplemented!("update_cache")
    }

    async fn clear_cache(&self) -> Result<(), AppError> {
        unimplemented!("clear_cache")
    }
}

pub struct CacheClientImpl {
    db_client: DbService<CacheEntry>,
}

impl CacheClientImpl {
    pub fn new<C>(client: C) -> Self
    where
        C: DbClient<CacheEntry> + 'static,
    {
        Self {
            db_client: Box::new(client),
        }
    }
}

#[async_trait]
impl CacheClient for CacheClientImpl {
    async fn get_by_key(&self, key: &str) -> Result<Option<CacheEntry>, AppError> {
        self.db_client.get_value_by_id(key).await
    }

    async fn update_cache(&self, entry: CacheEntry) -> Result<(), AppError> {
        self.db_client.put_item(entry).await
    }

    async fn clear_cache(&self) -> Result<(), AppError> {
        self.db_client.delete_all().await
    }
}
