/// Macro for generating mock ct rest client implementation. Each method is separated with commas. Unmocked methods use unimplemented!().
#[macro_export]
macro_rules! create_ct_rest_mock {
    ($($i:item), *) => {{
        use async_trait::async_trait;
        use efg_testing::call_collector::CallCollector;
        use efg_macros::track_mock_calls;
        use $crate::clients::ct::CtClient;

        #[derive(Clone)]
        struct MockCtRestClient {
            call_sender: std::sync::mpsc::SyncSender<Option<String>>,
        }

        #[async_trait]
        impl CtClient for MockCtRestClient {
            $(
                #[track_mock_calls]
                $i
            )*
        }

        let (call_sender, call_receiver) = std::sync::mpsc::sync_channel(1024);

        let mock_instance = MockCtRestClient {
            call_sender: call_sender.clone()
        };

        (mock_instance, CallCollector::new(call_receiver, call_sender))
    }};
}

/// Macro for generating mock HTTP client implementation. Each method is separated with commas. Unmocked methods use unimplemented!().
#[macro_export]
macro_rules! create_http_client_mock {
    ($($i:item), *) => {{
        async {
            use async_trait::async_trait;
            use $crate::clients::http::HttpClient;
            use efg_testing::call_collector::CallCollector;

            #[derive(Clone)]
            struct MockHttpClient {
                #[allow(dead_code)]
                call_sender: std::sync::mpsc::SyncSender<Option<String>>,
            }

            #[async_trait]
            impl HttpClient for MockHttpClient {
                $(
                    #[efg_macros::track_mock_calls]
                    $i
                )*
            }

            let (call_sender, call_receiver) = std::sync::mpsc::sync_channel(1024);

            let mock_instance = MockHttpClient {
                call_sender: call_sender.clone()
            };

            (mock_instance, CallCollector::new(call_receiver, call_sender))
        }
    }};
}

/// Macro for generating mock object client implementation. Each method is separated with commas. Unmocked methods use unimplemented!().
#[macro_export]
macro_rules! create_object_client_mock {
    ($($i:item), *) => {{
        async {
            use async_trait::async_trait;
            use efg_object_client::db::ObjectClient;
            use efg_macros::track_mock_calls;
            use efg_testing::call_collector::CallCollector;

            #[derive(Clone)]
            struct MockObjectClient {
                call_sender: std::sync::mpsc::SyncSender<Option<String>>,
            }

            #[async_trait]
            impl ObjectClient for MockObjectClient {
                $(
                    #[track_mock_calls]
                    $i
                )*
            }

            let (call_sender, call_receiver) = std::sync::mpsc::sync_channel(1024);

            let mock_instance = MockObjectClient {
                call_sender: call_sender.clone()
            };

            (mock_instance, CallCollector::new(call_receiver, call_sender))
        }
    }};
}

/// Macro for generating mock auth client implementation. Unmocked methods use unimplemented!().
#[macro_export]
macro_rules! create_auth_client_mock {
    ($stride:expr) => {{
        async {
            use async_trait::async_trait;
            use efg_lambda_base::auth_context::AuthContext;
            use efg_macros::track_mock_calls;
            use efg_testing::call_collector::CallCollector;
            use std::collections::BTreeSet;
            use $crate::auth::Role;
            use $crate::clients::auth::AuthClient;

            #[derive(Clone)]
            struct MockAuthClient {
                call_sender: std::sync::mpsc::SyncSender<Option<String>>,
                roles: BTreeSet<Role>,
            }

            #[async_trait]
            impl AuthClient for MockAuthClient {
                async fn has_role(
                    &self,
                    role: Role,
                    _auth_context: &AuthContext,
                ) -> Result<bool, AppError> {
                    Ok(self.roles.contains(&role))
                }
            }

            let (call_sender, call_receiver) = std::sync::mpsc::sync_channel(1024);

            let mock_instance = MockAuthClient {
                call_sender: call_sender.clone(),
                roles: BTreeSet::from_iter($stride.iter().cloned()),
            };

            (
                mock_instance,
                CallCollector::new(call_receiver, call_sender),
            )
        }
    }};
}
