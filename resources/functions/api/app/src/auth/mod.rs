use crate::clients::auth::AuthService;
use async_graphql::{Context, Enum, Error, Guard, Result};
use efg_lambda_base::auth_context::AuthContext;
use tracing::{info_span, Instrument};

#[derive(Enum, Eq, PartialEq, Copy, Clone, PartialOrd, Ord, Debug)]
pub enum Role {
    Admin,
    Editor,
}

pub struct RoleGuard {
    role: Role,
}

impl RoleGuard {
    pub fn new(role: Role) -> Self {
        Self { role }
    }
}

impl Guard for RoleGuard {
    async fn check(&self, ctx: &Context<'_>) -> Result<()> {
        async {
            let auth_client = ctx.data_unchecked::<AuthService>();
            let auth_context = ctx.data_unchecked::<AuthContext>();

            let has_role = auth_client.has_role(self.role, auth_context).await?;
            if has_role {
                Ok(())
            } else {
                Err(Error::new(format!("user is missing right {:?}", self.role)))
            }
        }
        .instrument(info_span!("check_role", role = format!("{:?}", self.role)))
        .await
    }
}
