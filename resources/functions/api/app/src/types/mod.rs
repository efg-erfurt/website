use async_graphql::SimpleObject;

pub mod ct;

#[derive(Clone, Debug, SimpleObject)]
pub struct FillPropertiesResult {
    pub imported: usize,
    pub total: usize,
}
