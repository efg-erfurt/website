use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Debug)]
pub struct CtFuncRequest {
    pub func: String,
}

#[derive(Serialize, Debug)]
#[allow(non_snake_case)]
pub struct CtLoginRequest {
    pub username: String,
    pub password: String,
    pub rememberMe: bool,
}

#[derive(Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct CtLoginResponse {
    pub data: CtLoginData,
}

#[derive(Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct CtLoginData {
    pub status: String,
    pub personId: usize,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGroupsResponse {
    pub data: Option<CtGroupsData>,
    pub messageKey: Option<String>,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGroupsData {
    pub id: usize,
    pub isEnabled: bool,
    pub parentGroup: usize,
    pub groups: Vec<CtGroup>,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGroup {
    pub id: usize,
    pub name: String,
    pub canSignUp: bool,
    pub information: CtGroupInformation,
    pub currentMemberCount: usize,
    pub maxMemberCount: Option<usize>,
    pub signUpConditions: Option<CtGroupSignUpConditions>,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGroupSignUpConditions {
    pub canContactLeader: bool,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGroupInformation {
    pub meetingTime: Option<String>,
    pub note: String,
    pub imageUrl: Option<String>,
    pub weekday: Option<CtWeekday>,
    pub leader: Vec<CtGroupLeader>,
    pub groupPlaces: Vec<CtGroupPlace>,
    pub targetGroup: Option<CtTargetGroup>,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGroupLeader {
    pub title: String,
    pub domainType: String,
    pub domainIdentifier: String,
    pub domainAttributes: CtGroupLeaderDomainAttributes,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGroupPlace {
    pub name: String,
    pub district: String,
    pub postalcode: String,
    pub city: String,
    pub markerUrl: String,
    pub geoLat: String,
    pub geoLng: String,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtTargetGroup {
    pub id: i32,
    pub name: String,
    pub nameTranslated: String,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGroupLeaderDomainAttributes {
    pub firstName: String,
    pub lastName: String,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtPersonalGroupsResponse {
    pub data: Vec<CtPersonalGroupOverview>,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtPersonalGroupOverview {
    pub group: CtPersonalGroup,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtPersonalGroup {
    pub title: String,
    pub domainIdentifier: String,
    pub domainType: String,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtWeekday {
    pub id: usize,
    pub name: String,
    pub nameTranslated: String,
}

#[derive(Serialize, Debug)]
pub struct CtCalPerCategoryRequest {
    pub func: String,
    pub category_ids: Vec<String>,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGetPersonResponse {
    pub data: Option<CtGetPersonResponseData>,
    pub messageKey: Option<String>,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtGetPersonResponseData {
    pub id: i32,
    pub firstName: String,
    pub lastName: String,
    pub imageUrl: Option<String>,
}

#[derive(Deserialize)]
pub struct CtWhoAmIResponse {
    pub data: CtWhoAmIResponseData,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtWhoAmIResponseData {
    pub id: i32,
    pub firstName: String,
    pub lastName: String,
    pub imageUrl: Option<String>,
}

#[derive(Deserialize)]
pub struct CtEventsOverviewResponse {
    pub data: Option<CtEventsOverviewResponseData>,
    pub status: String, // error, success
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtEventsOverviewResponseData {
    pub category: HashMap<String, CtCategory>,
    pub repeat: HashMap<String, CtRepeat>,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct CtCategory {
    pub id: String,
    pub bezeichnung: String,
    pub sortkey: String,
    pub color: String,
    pub oeffentlich_yn: String,
    pub ical_source_url: Option<String>,
    pub station_id: Option<String>,
    pub modified_date: String,
    pub modified_pid: String,
    pub textColor: String,
}

#[derive(Deserialize)]
pub struct CtRepeat {
    pub id: String,
    pub bezeichnung: String,
    pub sortkey: String,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum EitherReponse {
    Mapped(HashMap<String, CtEvent>),
    List(Vec<CtEvent>),
}

#[derive(Deserialize)]
pub struct CtEventsResponse {
    pub data: HashMap<String, EitherReponse>,
}

#[derive(Deserialize, Clone)]
pub struct CtEvent {
    pub id: String,
    pub bezeichnung: String,
    pub link: Option<String>,
    pub ort: Option<String>,
    pub notizen: Option<String>,
    pub intern_yn: String,
    pub startdate: String,
    pub enddate: String,
    pub category_id: String,
    pub repeat_id: String,
    pub repeat_frequence: Option<String>,
    pub repeat_until: Option<String>,
    pub repeat_option: Option<String>,
    pub csevents: Option<HashMap<String, CtCsEvent>>,
    pub exceptions: Option<HashMap<String, CtException>>,
    pub additions: Option<HashMap<String, CtAddition>>,
}

#[derive(Deserialize, Clone)]
pub struct CtCsEvent {
    pub id: String,
    pub startdate: String,
}

#[derive(Deserialize, Clone)]
pub struct CtAddition {
    pub id: String,
    pub add_date: String,
}

#[derive(Deserialize, Clone)]
pub struct CtException {
    pub id: String,
    pub except_date_start: String,
    pub except_date_end: String,
}
