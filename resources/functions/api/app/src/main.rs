#![forbid(unsafe_code)]

use aws_config::BehaviorVersion;
use efg_lambda_base::lifecycle::init_cold;
use efg_lib::{
    clients::http::HttpClientImpl, config::AppConfig, function::run_function, schema::init_schema,
};
use efg_object_client::s3_client::S3Client;
use lambda_http::{service_fn, Error};
use std::sync::Arc;

/// Main method for function execution.
/// Please note that this method will never terminate. Each warm run will only trigger the function provided as a service function.
#[tokio::main]
async fn main() -> Result<(), Error> {
    init_cold().await;

    let app_config = AppConfig::new();
    let aws_config = aws_config::load_defaults(BehaviorVersion::latest()).await;
    let http_client = Arc::new(HttpClientImpl::new());
    let object_client = Arc::new(S3Client::new(
        app_config.objects().public_bucket(),
        &aws_config,
    ));

    // build schema once, use it for all requests
    let schema = init_schema(&app_config, &aws_config).await;

    lambda_http::run(service_fn(|ev| {
        run_function(
            ev,
            schema.clone(),
            http_client.clone(),
            object_client.clone(),
        )
    }))
    .await?;

    Ok(())
}
