use super::{
    mutation::Language,
    resolvers::{
        contents::{resolve_content, resolve_contents},
        events::{resolve_event_categories, resolve_event_category, resolve_events},
        groups::resolve_groups,
        posts::{resolve_latest_posts, resolve_post},
        search::search,
        sermons::{resolve_latest_sermons, resolve_sermons},
        signups::resolve_signups,
        users::{resolve_current_user, resolve_user},
    },
    types::{
        content::Content, event::Event, event_category::EventCategory, group::Group,
        monitor::Monitor, pagination::Pagination, pagination_input::PaginationInput, post::Post,
        search_result::SearchResult, sermon::Sermon, signup::Signup, user::User,
    },
};
use crate::clients::{ct::CtService, http::HttpService};
use async_graphql::{Context, Object};
use efg_db_client::db::DbService;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
use std::collections::BTreeSet;
use time::OffsetDateTime;

pub struct QueryRoot;

#[Object]
impl QueryRoot {
    async fn content<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        key: String,
        language: Language,
    ) -> Result<Option<Content>, AppError> {
        let content_db = ctx.data_unchecked::<DbService<Content>>();
        resolve_content(content_db, &key, &language).await
    }

    async fn contents<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        keys: Vec<String>,
        language: Language,
    ) -> Result<Vec<Content>, AppError> {
        let content_db = ctx.data_unchecked::<DbService<Content>>();
        resolve_contents(content_db, &keys, &language).await
    }

    async fn search<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        start_date: Option<OffsetDateTime>,
        query: String,
        language: Language,
    ) -> Result<SearchResult, AppError> {
        let http_service = ctx.data_unchecked::<HttpService>();
        let post_db = ctx.data_unchecked::<DbService<Post>>();
        let sermon_db = ctx.data_unchecked::<DbService<Sermon>>();
        let ct_service = ctx.data_unchecked::<CtService>();

        let results = search(
            &query,
            &language,
            &start_date.unwrap_or_else(OffsetDateTime::now_utc),
            http_service,
            post_db,
            sermon_db,
            ct_service,
        )
        .await?;

        Ok(results)
    }

    async fn user<'ctx>(&self, ctx: &Context<'ctx>, id: String) -> Result<Option<User>, AppError> {
        let ct = ctx.data_unchecked::<CtService>();
        // list of user ids needed for website, other ids can't be accessed
        let id_allow_list = BTreeSet::from_iter(
            [
                1, 13, 32, 41, 241, 286, 338, 389, 407, 437, 1168, 1240, 3070,
            ]
            .iter()
            .map(|id| format!("{id}")),
        );
        if !id_allow_list.contains(&id) {
            return Ok(None);
        }

        resolve_user(ct, &id).await
    }

    async fn current_user<'ctx>(&self, ctx: &Context<'ctx>) -> Result<Option<User>, AppError> {
        let ct = ctx.data_unchecked::<CtService>();
        let auth_context = ctx.data_unchecked::<AuthContext>();
        resolve_current_user(ct, auth_context).await
    }

    async fn sermons<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        language: Language,
        speaker: Option<String>,
        pagination: PaginationInput,
    ) -> Result<Pagination<Sermon>, AppError> {
        let sermon_db = ctx.data_unchecked::<DbService<Sermon>>();
        resolve_sermons(sermon_db, &language, &speaker, &pagination).await
    }

    async fn latest_sermons<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        language: Language,
        pagination: PaginationInput,
    ) -> Result<Pagination<Sermon>, AppError> {
        let sermon_db = ctx.data_unchecked::<DbService<Sermon>>();
        resolve_latest_sermons(sermon_db, &language, &pagination).await
    }

    async fn event_categories<'ctx>(
        &self,
        ctx: &Context<'ctx>,
    ) -> Result<Vec<EventCategory>, AppError> {
        let ct = ctx.data_unchecked::<CtService>();
        resolve_event_categories(ct).await
    }

    async fn event_category<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        id: String,
    ) -> Result<Option<EventCategory>, AppError> {
        let ct = ctx.data_unchecked::<CtService>();
        resolve_event_category(ct, &id).await
    }

    async fn events<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        start: OffsetDateTime,
        end: Option<OffsetDateTime>,
        #[graphql(default = 20)] max_entries: usize,
        category_id: Option<String>,
    ) -> Result<Vec<Event>, AppError> {
        let ct = ctx.data_unchecked::<CtService>();
        resolve_events(ct, &start, &end, max_entries, &category_id).await
    }

    async fn latest_posts<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        language: Option<Language>,
        tag: Option<String>,
        pagination: PaginationInput,
    ) -> Result<Pagination<Post>, AppError> {
        let post_db = ctx.data_unchecked::<DbService<Post>>();
        resolve_latest_posts(
            post_db,
            &tag,
            &language.unwrap_or(Language::German),
            &pagination,
        )
        .await
    }

    async fn post<'ctx>(&self, ctx: &Context<'ctx>, id: String) -> Result<Option<Post>, AppError> {
        let post_db = ctx.data_unchecked::<DbService<Post>>();
        resolve_post(post_db, &id).await
    }

    async fn monitor(&self) -> Monitor {
        Monitor {}
    }

    async fn signups<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        groups_id: String,
    ) -> Result<Vec<Signup>, AppError> {
        let ct = ctx.data_unchecked::<CtService>();
        resolve_signups(&groups_id, ct).await
    }

    async fn groups<'ctx>(&self, ctx: &Context<'ctx>, id: String) -> Result<Vec<Group>, AppError> {
        let ct = ctx.data_unchecked::<CtService>();
        resolve_groups(&id, ct).await
    }
}
