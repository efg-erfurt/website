use super::{
    resolvers::{
        contents::update_content,
        posts::{create_post, delete_post, update_post},
        sermons::{create_sermon, delete_sermon, update_sermon},
    },
    types::{
        content::Content,
        create_post_input::CreatePostInput,
        create_sermon_input::CreateSermonInput,
        monitor::{Monitor, Visibility},
        post::Post,
        send_message_input::SendMessageInput,
        sermon::Sermon,
        update_post_input::UpdatePostInput,
        update_sermon_input::UpdateSermonInput,
    },
};
use crate::{
    auth::{Role, RoleGuard},
    clients::{cache::CacheService, ct::CtService, http::HttpService, mail::MailService},
    types::FillPropertiesResult,
};
use async_graphql::{Context, Enum, InputType, Object};
use efg_db_client::db::DbService;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
use efg_object_client::db::ObjectService;
use serde_json::Value;
use std::{collections::HashMap, ffi::OsStr, io::Read, path::Path};
use time::OffsetDateTime;
use tracing::{info, warn};
use uuid::Uuid;

#[derive(Enum, Eq, PartialEq, Copy, Clone, PartialOrd, Ord)]
pub enum Language {
    German,
    English,
}

impl Language {
    pub fn to_str(&self) -> String {
        match self {
            Language::German => "de".to_string(),
            Language::English => "en".to_string(),
        }
    }
}

pub struct Mutation;

#[Object]
impl Mutation {
    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn update_content<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        key: String,
        content: String,
        language: Language,
    ) -> Result<bool, AppError> {
        let content_db = ctx.data_unchecked::<DbService<Content>>();
        update_content(content_db, &key, &content, &language).await?;

        info!("updated content {} successfully", &key);

        Ok(true)
    }

    async fn send_message<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        message: SendMessageInput,
    ) -> Result<bool, AppError> {
        let mail_service = ctx.data_unchecked::<MailService>();

        mail_service.send_mail(&message).await?;

        info!("send mail successfully");

        Ok(true)
    }

    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn create_sermon<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        sermon: CreateSermonInput,
    ) -> Result<Sermon, AppError> {
        let sermon_db = ctx.data_unchecked::<DbService<Sermon>>();
        let new_sermon = create_sermon(sermon_db, sermon).await?;

        info!("created sermon {} successfully", new_sermon.id);

        Ok(new_sermon)
    }

    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn update_sermon<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        sermon: UpdateSermonInput,
    ) -> Result<Sermon, AppError> {
        let sermon_db = ctx.data_unchecked::<DbService<Sermon>>();
        let updated_sermon = update_sermon(sermon_db, sermon).await?;

        info!("updated sermon {} successfully", updated_sermon.id);

        Ok(updated_sermon)
    }

    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn delete_sermon<'ctx>(&self, ctx: &Context<'ctx>, id: String) -> Result<bool, AppError> {
        let sermon_db = ctx.data_unchecked::<DbService<Sermon>>();
        delete_sermon(sermon_db, &id).await?;

        info!("deleted sermon {} successfully", id);

        Ok(true)
    }

    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn create_post<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        post: CreatePostInput,
    ) -> Result<Post, AppError> {
        let post_db = ctx.data_unchecked::<DbService<Post>>();
        let auth_context = ctx.data_unchecked::<AuthContext>();
        let ct_service = ctx.data_unchecked::<CtService>();

        let new_post = create_post(post_db, ct_service, auth_context, post).await?;

        info!("updated post {} successfully", new_post.id);

        Ok(new_post)
    }

    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn update_post<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        post: UpdatePostInput,
    ) -> Result<Post, AppError> {
        let post_db = ctx.data_unchecked::<DbService<Post>>();
        let auth_context = ctx.data_unchecked::<AuthContext>();
        let ct_service = ctx.data_unchecked::<CtService>();

        let updated_post = update_post(post_db, ct_service, auth_context, post).await?;

        info!("updated post {} successfully", updated_post.id);

        Ok(updated_post)
    }

    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn delete_post<'ctx>(&self, ctx: &Context<'ctx>, id: String) -> Result<bool, AppError> {
        let post_db = ctx.data_unchecked::<DbService<Post>>();
        delete_post(post_db, &id).await?;

        info!("deleted post {} successfully", id);

        Ok(true)
    }

    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn edit_monitor<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        visibility: Visibility,
        new_slides: Vec<String>,
    ) -> Result<bool, AppError> {
        let auth_context = ctx.data_unchecked::<AuthContext>();
        let content_db = ctx.data_unchecked::<DbService<Content>>();
        let ct_service = ctx.data_unchecked::<CtService>();

        let user = ct_service
            .get_current_user(auth_context)
            .await?
            .expect("user should be existing");

        self.update_slides(visibility, &new_slides, user.data.id, content_db)
            .await?;

        Ok(true)
    }

    #[deprecated(note = "will be replaced by interactive editing and upload of images")]
    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn update_monitor<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        language: Language,
    ) -> Result<bool, AppError> {
        let auth_context = ctx.data_unchecked::<AuthContext>();
        let content_db = ctx.data_unchecked::<DbService<Content>>();
        let ct_service = ctx.data_unchecked::<CtService>();
        let s3_service = ctx.data_unchecked::<ObjectService>();
        let http_service = ctx.data_unchecked::<HttpService>();

        let user = ct_service
            .get_current_user(auth_context)
            .await?
            .expect("user should be existing");

        let external_url_id = Content::build_id("general.info.imagesUrl", &language.to_str());
        let internal_url_id = Content::build_id("general.info.screensUrl", &language.to_str());
        let urls = content_db
            .batch_get_values_by_ids(&[external_url_id.clone(), internal_url_id.clone()])
            .await?;

        if let Some(internal_url) = urls.get(&internal_url_id) {
            self.update_images_of_monitor(
                Visibility::Internal,
                &internal_url.content,
                user.data.id,
                content_db,
                http_service,
                s3_service,
            )
            .await?;
        } else {
            warn!("internal image url not defined yet");
        }

        if let Some(external_url) = urls.get(&external_url_id) {
            self.update_images_of_monitor(
                Visibility::External,
                &external_url.content,
                user.data.id,
                content_db,
                http_service,
                s3_service,
            )
            .await?;
        } else {
            warn!("external image url not defined yet");
        }

        Ok(true)
    }

    #[graphql(guard = "RoleGuard::new(Role::Editor)")]
    async fn clear_cache<'ctx>(&self, ctx: &Context<'ctx>) -> Result<bool, AppError> {
        let cache_client = ctx.data_unchecked::<CacheService>();
        cache_client.clear_cache().await?;

        info!("cleared cache successfully");

        Ok(true)
    }

    #[graphql(guard = "RoleGuard::new(Role::Admin)")]
    async fn fill_properties<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        language: Language,
    ) -> Result<FillPropertiesResult, AppError> {
        let content_db = ctx.data_unchecked::<DbService<Content>>();

        let default_data: Value = serde_json::from_str(&if language.eq(&Language::German) {
            include_str!("../../resources/i18n/de.json").to_string()
        } else if language.eq(&Language::English) {
            include_str!("../../resources/i18n/en.json").to_string()
        } else {
            return Err(AppError::new("unexpected language"));
        })
        .map_err(|err| AppError::from_error(&err, "reading properties file failed"))?;

        let mut properties = HashMap::new();
        let current_path = vec![];

        Mutation::build_properties(&default_data, current_path, &mut properties);

        let mut imported_count = 0;
        for (key, content) in properties.iter() {
            if content_db
                .get_value_by_id(&Content::build_id(key, &language.to_str()))
                .await?
                .is_none()
            {
                content_db
                    .put_item(Content {
                        key: key.to_string(),
                        content: content.to_string(),
                        language: language.to_str(),
                        date: OffsetDateTime::now_utc(),
                    })
                    .await?;
                imported_count += 1;
            }
        }

        info!(
            "filled {} of {} properties successfully",
            imported_count,
            properties.len()
        );

        Ok(FillPropertiesResult {
            imported: imported_count,
            total: properties.len(),
        })
    }
}

impl Mutation {
    fn build_properties(
        value: &Value,
        current_path: Vec<String>,
        results: &mut HashMap<String, String>,
    ) {
        match value {
            Value::Object(map) => {
                for (k, v) in map {
                    let mut new_path = current_path.clone();
                    new_path.push(k.to_owned());
                    Self::build_properties(v, new_path, results);
                }
            }
            Value::Array(array) => {
                for (i, v) in array.iter().enumerate() {
                    let mut new_path = current_path.clone();
                    new_path.push(i.to_string().to_owned());
                    Self::build_properties(v, new_path, results);
                }
            }
            Value::String(literal) => {
                let property = Self::build_property(&current_path);
                results.insert(property, literal.to_string());
            }
            _ => {
                warn!(
                    "ignored unexpected property {}",
                    Self::build_property(&current_path)
                );
            }
        }
    }

    fn build_property(path: &[String]) -> String {
        path.join(".")
    }

    async fn update_images_of_monitor(
        &self,
        visibility: Visibility,
        url: &str,
        user_id: i32,
        content_db: &DbService<Content>,
        http_service: &HttpService,
        s3_service: &ObjectService,
    ) -> Result<(), AppError> {
        let external_file = http_service.fetch_file_to_tmp(url).await?;

        let mut archive = zip::ZipArchive::new(external_file).unwrap();

        let mut images_to_upload = Vec::new();
        let mut valid_index = 1;
        for i in 0..archive.len() {
            let mut file = archive.by_index(i).unwrap();
            let file_path = match file.enclosed_name() {
                Some(path) => path.as_os_str().to_owned(),
                None => continue,
            };
            // skip hidden MacOS files
            if file_path.to_str().unwrap().starts_with("__MACOSX") {
                continue;
            }

            let extension = match Path::new(&file_path).extension().and_then(OsStr::to_str) {
                Some(ext) => ext,
                None => {
                    warn!("skipped file {:?} without extension", file_path);
                    continue;
                }
            };

            let file_name = format!(
                "{}{}-{}.{}",
                if visibility.eq(&Visibility::Internal) {
                    "i"
                } else {
                    "e"
                },
                valid_index,
                Uuid::new_v4(),
                extension
            );
            let key = format!("public/assets/{}", &file_name);

            let mut buffer = Vec::new();
            file.read_to_end(&mut buffer).expect("buffer overflow");

            let mime = if extension.eq("png") {
                mime::IMAGE_PNG
            } else {
                mime::IMAGE_JPEG
            }
            .to_string();
            images_to_upload.push((key, file_name, file_path, buffer, mime));

            valid_index += 1;
        }

        // upload images, maybe in parallel later
        let mut new_slides = Vec::new();
        for (key, file_name, file_path, buffer, mime) in images_to_upload {
            s3_service.upload(&key, buffer, &mime).await?;

            info!(
                "User {} uploaded {:?} as new file {}",
                user_id, file_path, &file_name
            );

            new_slides.push(file_name);
        }

        let old_paths = Monitor::resolve_pages(visibility, content_db).await?;
        self.update_slides(visibility, &new_slides, user_id, content_db)
            .await?;

        // delete replaced files (they are cached some more time by the CDN though)
        for p in old_paths.iter() {
            s3_service.delete(p).await?;
        }

        Ok(())
    }

    async fn update_slides(
        &self,
        visibility: Visibility,
        new_slides: &[String],
        user_id: i32,
        content_db: &DbService<Content>,
    ) -> Result<(), AppError> {
        let new_content = Content {
            key: format!("monitor.{}.paths", visibility.to_value()),
            content: new_slides.join(","),
            language: "de".to_string(),
            date: OffsetDateTime::now_utc(),
        };

        content_db.put_item(new_content).await?;

        info!(
            "{} monitor updated by {} successfully with {} images",
            visibility.to_value(),
            user_id,
            new_slides.len()
        );

        Ok(())
    }
}
