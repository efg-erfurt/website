use super::post::Post;
use super::sermon::Sermon;
use async_graphql::*;

#[derive(SimpleObject)]
#[graphql(concrete(name = "PaginatedPosts", params(Post)))]
#[graphql(concrete(name = "PaginatedSermons", params(Sermon)))]
pub struct Pagination<T: OutputType> {
    pub count: usize,
    pub page: usize,
    pub total_count: usize,
    pub total_pages: usize,
    pub content: Vec<T>,
}
