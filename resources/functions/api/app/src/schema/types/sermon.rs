use async_graphql::*;
use aws_sdk_dynamodb::types::AttributeValue;
use efg_db_client::db_item::DbItem;
use efg_lambda_base::errors::AppError;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use time::{format_description::well_known::Rfc3339, OffsetDateTime};

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
pub struct Sermon {
    pub id: String,
    pub title: String,
    pub language: String,
    pub audio_url: Option<String>,
    pub text_url: Option<String>,
    pub translation_url: Option<String>,
    pub video_url: Option<String>,
    pub publish_date: OffsetDateTime,
    pub talk_date: OffsetDateTime,
    pub comment: Option<String>,
    pub speaker: String,
    pub tags: Vec<String>,
}

impl DbItem<Sermon> for Sermon {
    fn from_raw(raw: &HashMap<String, AttributeValue>) -> Result<Sermon, AppError> {
        let id = raw.get("id").unwrap().as_s().unwrap().clone();
        let speaker = raw.get("speaker").unwrap().as_s().unwrap().clone();
        let comment = raw.get("comment").map(|s| s.as_s().unwrap().clone());
        let language = raw.get("language").unwrap().as_s().unwrap().clone();
        let video_url = raw.get("videoUrl").map(|s| s.as_s().unwrap().clone());
        let audio_url = raw.get("audioUrl").map(|s| s.as_s().unwrap().clone());
        let text_url = raw.get("textUrl").map(|s| s.as_s().unwrap().clone());
        let translation_url = raw.get("translationUrl").map(|s| s.as_s().unwrap().clone());
        let title = raw.get("title").unwrap().as_s().unwrap().clone();
        let publish_date = raw.get("publishDate").unwrap().as_s().unwrap().clone();
        let talk_date = raw.get("talkDate").unwrap().as_s().unwrap().clone();
        let tags = raw
            .get("tags")
            .map(|tags| tags.as_ss().unwrap().clone())
            .unwrap_or_default();
        let content = Sermon {
            id,
            title,
            tags,
            publish_date: OffsetDateTime::parse(&publish_date, &Rfc3339)
                .unwrap_or_else(|_| panic!("deserializing saved date failed: {}", &publish_date)),
            language,
            audio_url,
            text_url,
            translation_url,
            video_url,
            talk_date: OffsetDateTime::parse(&talk_date, &Rfc3339)
                .unwrap_or_else(|_| panic!("deserializing saved date failed: {}", &talk_date)),
            comment,
            speaker,
        };

        Ok(content)
    }

    fn to_raw(&self) -> HashMap<String, AttributeValue> {
        let mut raw = HashMap::new();
        raw.insert("id".to_string(), AttributeValue::S(self.id.to_string()));
        raw.insert(
            "speaker".to_string(),
            AttributeValue::S(self.speaker.to_string()),
        );
        raw.insert(
            "title".to_string(),
            AttributeValue::S(self.title.to_string()),
        );
        raw.insert(
            "language".to_string(),
            AttributeValue::S(self.language.to_string()),
        );
        if self.comment.is_some() {
            raw.insert(
                "comment".to_string(),
                AttributeValue::S(self.comment.clone().unwrap()),
            );
        }
        if self.audio_url.is_some() {
            raw.insert(
                "audioUrl".to_string(),
                AttributeValue::S(self.audio_url.clone().unwrap()),
            );
        }
        if self.text_url.is_some() {
            raw.insert(
                "textUrl".to_string(),
                AttributeValue::S(self.text_url.clone().unwrap()),
            );
        }
        if self.translation_url.is_some() {
            raw.insert(
                "translationUrl".to_string(),
                AttributeValue::S(self.translation_url.clone().unwrap()),
            );
        }
        if self.video_url.is_some() {
            raw.insert(
                "videoUrl".to_string(),
                AttributeValue::S(self.video_url.clone().unwrap()),
            );
        }
        if !self.tags.is_empty() {
            raw.insert("tags".to_string(), AttributeValue::Ss(self.tags.clone()));
        }
        raw.insert(
            "publishDate".to_string(),
            AttributeValue::S(OffsetDateTime::format(self.publish_date, &Rfc3339).unwrap()),
        );
        raw.insert(
            "talkDate".to_string(),
            AttributeValue::S(OffsetDateTime::format(self.talk_date, &Rfc3339).unwrap()),
        );
        raw
    }

    fn get_id(&self) -> String {
        self.id.to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::Sermon;
    use aws_sdk_dynamodb::types::AttributeValue;
    use efg_db_client::db_item::DbItem;
    use std::collections::HashMap;
    use time::macros::datetime;

    #[tokio::test]
    async fn test_should_transform_entity_to_raw() {
        // given
        let sermon = Sermon {
            id: "my-id".to_string(),
            speaker: "speaker".to_string(),
            language: "de".to_string(),
            talk_date: datetime!(2022-02-13 10:30 UTC),
            publish_date: datetime!(2020-12-13 19:30 UTC),
            audio_url: Some("audio.mp3".to_string()),
            text_url: Some("text.txt".to_string()),
            video_url: Some("video.mp4".to_string()),
            translation_url: Some("en.txt".to_string()),
            comment: Some("comment".to_string()),
            tags: vec!["a".to_string(), "b".to_string()],
            title: "title".to_string(),
        };

        // when
        let raw = sermon.to_raw();

        // then
        assert_eq!("comment", raw.get("comment").unwrap().as_s().unwrap());
        assert_eq!(
            "2022-02-13T10:30:00Z",
            raw.get("talkDate").unwrap().as_s().unwrap()
        );
        assert_eq!(
            "2020-12-13T19:30:00Z",
            raw.get("publishDate").unwrap().as_s().unwrap()
        );
        assert_eq!("my-id", raw.get("id").unwrap().as_s().unwrap());
        assert_eq!("audio.mp3", raw.get("audioUrl").unwrap().as_s().unwrap());
        assert_eq!("text.txt", raw.get("textUrl").unwrap().as_s().unwrap());
        assert_eq!("video.mp4", raw.get("videoUrl").unwrap().as_s().unwrap());
        assert_eq!("en.txt", raw.get("translationUrl").unwrap().as_s().unwrap());
        assert_eq!("speaker", raw.get("speaker").unwrap().as_s().unwrap());
        assert_eq!("title", raw.get("title").unwrap().as_s().unwrap());
        assert_eq!(
            vec!["a", "b"],
            raw.get("tags")
                .unwrap()
                .as_ss()
                .unwrap()
                .iter()
                .collect::<Vec<_>>()
        );
    }

    #[tokio::test]
    async fn test_should_transform_raw_to_entity() {
        // given
        let mut raw = HashMap::new();
        raw.insert("id".to_string(), AttributeValue::S("my-id".to_string()));
        raw.insert("language".to_string(), AttributeValue::S("en".to_string()));
        raw.insert(
            "textUrl".to_string(),
            AttributeValue::S("text.txt".to_string()),
        );
        raw.insert(
            "videoUrl".to_string(),
            AttributeValue::S("video.mp4".to_string()),
        );
        raw.insert(
            "audioUrl".to_string(),
            AttributeValue::S("audio.mp3".to_string()),
        );
        raw.insert(
            "translationUrl".to_string(),
            AttributeValue::S("en.txt".to_string()),
        );
        raw.insert(
            "comment".to_string(),
            AttributeValue::S("comment".to_string()),
        );
        raw.insert(
            "speaker".to_string(),
            AttributeValue::S("speaker".to_string()),
        );
        raw.insert("title".to_string(), AttributeValue::S("title".to_string()));
        raw.insert(
            "talkDate".to_string(),
            AttributeValue::S("2022-03-11T16:24:52.356Z".to_string()),
        );
        raw.insert(
            "publishDate".to_string(),
            AttributeValue::S("2022-11-13T12:34:56.000Z".to_string()),
        );
        raw.insert(
            "tags".to_string(),
            AttributeValue::Ss(vec!["a".to_string(), "b".to_string()]),
        );

        // when
        let entity = Sermon::from_raw(&raw).unwrap();

        // then
        assert_eq!("en", entity.language);
        assert_eq!("comment", entity.comment.unwrap());
        assert_eq!("my-id", entity.id);
        assert_eq!("speaker", entity.speaker);
        assert_eq!("title", entity.title);
        assert_eq!(
            "2022-03-11 16:24:52.356 +00:00:00",
            entity.talk_date.to_string()
        );
        assert_eq!(
            "2022-11-13 12:34:56.0 +00:00:00",
            entity.publish_date.to_string()
        );
        assert_eq!("audio.mp3", entity.audio_url.unwrap());
        assert_eq!("video.mp4", entity.video_url.unwrap());
        assert_eq!("en.txt", entity.translation_url.unwrap());
        assert_eq!("text.txt", entity.text_url.unwrap());
        assert_eq!(vec!["a", "b"], entity.tags);
    }
}
