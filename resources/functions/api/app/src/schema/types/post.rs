use super::user::User;
use crate::{clients::ct::CtService, schema::resolvers::users::resolve_user};
use async_graphql::*;
use aws_sdk_dynamodb::types::AttributeValue;
use efg_db_client::db_item::DbItem;
use efg_lambda_base::errors::AppError;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use time::{format_description::well_known::Rfc3339, OffsetDateTime};

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
#[graphql(complex)]
pub struct Post {
    pub id: String,
    pub date: OffsetDateTime,
    pub publish_date: OffsetDateTime,
    pub title: String,
    pub image_url: Option<String>,
    pub content: String,
    pub summary: Option<String>,
    pub tags: Vec<String>,
    #[graphql(visible = false, skip = true)]
    pub user_id: String,
    pub language: String,
}

#[ComplexObject]
impl Post {
    async fn creator<'ctx>(&self, ctx: &Context<'ctx>) -> Result<User, AppError> {
        let ct = ctx.data_unchecked::<CtService>();
        let user = resolve_user(ct, &self.user_id).await?.unwrap();

        Ok(user)
    }
}

impl DbItem<Post> for Post {
    fn from_raw(raw: &HashMap<String, AttributeValue>) -> Result<Post, AppError> {
        let id = raw.get("id").unwrap().as_s().unwrap().clone();
        let content = raw.get("content").unwrap().as_s().unwrap().clone();
        let summary = raw.get("summary").map(|s| s.as_s().unwrap().clone());
        let image_url = raw.get("imageUrl").map(|s| s.as_s().unwrap().clone());
        let title = raw.get("title").unwrap().as_s().unwrap().clone();
        let user_id = raw.get("userId").unwrap().as_s().unwrap().clone();
        let language = raw.get("language").unwrap().as_s().unwrap().clone();
        let date = raw.get("date").unwrap().as_s().unwrap().clone();
        let publish_date = raw.get("publishDate").unwrap().as_s().unwrap().clone();
        let tags = raw
            .get("tags")
            .map(|tags| tags.as_ss().unwrap().clone())
            .unwrap_or_default();
        let content = Post {
            id,
            content,
            title,
            summary,
            image_url,
            tags,
            user_id,
            language,
            publish_date: OffsetDateTime::parse(&publish_date, &Rfc3339)
                .unwrap_or_else(|_| panic!("deserializing saved date failed: {}", &date)),
            date: OffsetDateTime::parse(&date, &Rfc3339)
                .unwrap_or_else(|_| panic!("deserializing saved date failed: {}", &date)),
        };

        Ok(content)
    }

    fn to_raw(&self) -> HashMap<String, AttributeValue> {
        let mut raw = HashMap::new();
        raw.insert("id".to_string(), AttributeValue::S(self.id.to_string()));
        raw.insert(
            "content".to_string(),
            AttributeValue::S(self.content.to_string()),
        );
        if self.summary.is_some() {
            raw.insert(
                "summary".to_string(),
                AttributeValue::S(self.summary.clone().unwrap()),
            );
        }
        if self.image_url.is_some() {
            raw.insert(
                "imageUrl".to_string(),
                AttributeValue::S(self.image_url.clone().unwrap()),
            );
        }
        raw.insert(
            "title".to_string(),
            AttributeValue::S(self.title.to_string()),
        );
        raw.insert(
            "language".to_string(),
            AttributeValue::S(self.language.to_string()),
        );
        raw.insert(
            "userId".to_string(),
            AttributeValue::S(self.user_id.to_string()),
        );
        if !self.tags.is_empty() {
            raw.insert("tags".to_string(), AttributeValue::Ss(self.tags.clone()));
        }
        raw.insert(
            "date".to_string(),
            AttributeValue::S(OffsetDateTime::format(self.date, &Rfc3339).unwrap()),
        );
        raw.insert(
            "publishDate".to_string(),
            AttributeValue::S(OffsetDateTime::format(self.publish_date, &Rfc3339).unwrap()),
        );
        raw
    }

    fn get_id(&self) -> String {
        self.id.to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::Post;
    use aws_sdk_dynamodb::types::AttributeValue;
    use efg_db_client::db_item::DbItem;
    use std::collections::HashMap;
    use time::macros::datetime;

    #[tokio::test]
    async fn test_should_transform_entity_to_raw() {
        // given
        let content = Post {
            content: "abc".to_string(),
            date: datetime!(2022-02-13 10:30 UTC),
            id: "my-id".to_string(),
            image_url: Some("img.jpg".to_string()),
            publish_date: datetime!(2020-12-13 19:30 UTC),
            summary: Some("summary".to_string()),
            tags: vec!["a".to_string(), "b".to_string()],
            title: "title".to_string(),
            user_id: "user-id".to_string(),
            language: "de".to_string(),
        };

        // when
        let raw = content.to_raw();

        // then
        assert_eq!("abc", raw.get("content").unwrap().as_s().unwrap());
        assert_eq!(
            "2022-02-13T10:30:00Z",
            raw.get("date").unwrap().as_s().unwrap()
        );
        assert_eq!(
            "2020-12-13T19:30:00Z",
            raw.get("publishDate").unwrap().as_s().unwrap()
        );
        assert_eq!("my-id", raw.get("id").unwrap().as_s().unwrap());
        assert_eq!("de", raw.get("language").unwrap().as_s().unwrap());
        assert_eq!("img.jpg", raw.get("imageUrl").unwrap().as_s().unwrap());
        assert_eq!("summary", raw.get("summary").unwrap().as_s().unwrap());
        assert_eq!("title", raw.get("title").unwrap().as_s().unwrap());
        assert_eq!("user-id", raw.get("userId").unwrap().as_s().unwrap());
        assert_eq!(
            vec!["a", "b"],
            raw.get("tags")
                .unwrap()
                .as_ss()
                .unwrap()
                .iter()
                .collect::<Vec<_>>()
        );
    }

    #[tokio::test]
    async fn test_should_transform_raw_to_entity() {
        // given
        let mut raw = HashMap::new();
        raw.insert("id".to_string(), AttributeValue::S("my-id".to_string()));
        raw.insert(
            "imageUrl".to_string(),
            AttributeValue::S("img.jpg".to_string()),
        );
        raw.insert("content".to_string(), AttributeValue::S("abc".to_string()));
        raw.insert(
            "summary".to_string(),
            AttributeValue::S("summary".to_string()),
        );
        raw.insert("title".to_string(), AttributeValue::S("title".to_string()));
        raw.insert("language".to_string(), AttributeValue::S("de".to_string()));
        raw.insert(
            "userId".to_string(),
            AttributeValue::S("user-id".to_string()),
        );
        raw.insert(
            "date".to_string(),
            AttributeValue::S("2022-03-11T16:24:52.356Z".to_string()),
        );
        raw.insert(
            "publishDate".to_string(),
            AttributeValue::S("2022-11-13T12:34:56.000Z".to_string()),
        );
        raw.insert(
            "tags".to_string(),
            AttributeValue::Ss(vec!["a".to_string(), "b".to_string()]),
        );

        // when
        let entity = Post::from_raw(&raw).unwrap();

        // then
        assert_eq!("abc", entity.content);
        assert_eq!("my-id", entity.id);
        assert_eq!("summary", entity.summary.unwrap());
        assert_eq!("title", entity.title);
        assert_eq!("user-id", entity.user_id);
        assert_eq!("de", entity.language);
        assert_eq!("2022-03-11 16:24:52.356 +00:00:00", entity.date.to_string());
        assert_eq!(
            "2022-11-13 12:34:56.0 +00:00:00",
            entity.publish_date.to_string()
        );
        assert_eq!(vec!["a", "b"], entity.tags);
    }
}
