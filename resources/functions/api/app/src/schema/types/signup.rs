use async_graphql::*;

#[derive(SimpleObject)]
pub struct Signup {
    pub id: String,
    pub name: String,
    pub time: Option<String>,
    pub day: Option<String>,
    pub note: String,
    pub image_url: Option<String>,
    pub slots_total: Option<usize>,
    pub slots_left: Option<usize>,
    pub slots_used: usize,
}
