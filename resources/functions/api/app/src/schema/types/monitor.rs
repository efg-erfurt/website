use super::content::Content;
use async_graphql::*;
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use serde::{Deserialize, Serialize};

#[derive(Enum, Eq, PartialEq, Copy, Clone, PartialOrd, Ord)]
pub enum Visibility {
    External,
    Internal,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Monitor {}

fn external_default() -> Visibility {
    Visibility::External
}

#[Object]
impl Monitor {
    async fn pages<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        #[graphql(default_with = "external_default()")] visibility: Visibility,
    ) -> Result<Vec<String>, AppError> {
        let content_db = ctx.data_unchecked::<DbService<Content>>();

        Monitor::resolve_pages(visibility, content_db).await
    }
}

impl Monitor {
    pub async fn resolve_pages(
        visibility: Visibility,
        content_db: &DbService<Content>,
    ) -> Result<Vec<String>, AppError> {
        let urls = content_db
            .get_value_by_id(&Content::build_id(
                &format!("monitor.{}.paths", visibility.to_value()),
                "de",
            ))
            .await?
            .map(Monitor::map_content_to_urls)
            .unwrap_or_default();

        Ok(urls)
    }

    fn map_content_to_urls(content: Content) -> Vec<String> {
        content
            .content
            .split(',')
            .filter(|p| !p.is_empty())
            .map(|v| v.to_owned())
            .collect::<Vec<_>>()
    }
}
