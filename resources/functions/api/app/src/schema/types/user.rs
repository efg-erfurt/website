use async_graphql::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
#[graphql(complex)]
pub struct User {
    pub id: String,
    pub first_name: String,
    pub last_name: String,
    pub image_url: Option<String>,
}

#[ComplexObject]
impl User {
    async fn name(&self) -> String {
        format!("{} {}", self.first_name, self.last_name)
    }
}
