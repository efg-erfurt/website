use async_graphql::*;

#[derive(InputObject)]
pub struct PaginationInput {
    #[graphql(default = 0)]
    pub page: usize,
    #[graphql(default = 20)]
    pub size: usize,
}
