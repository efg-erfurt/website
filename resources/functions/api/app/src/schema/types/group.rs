use super::user::User;
use crate::{clients::ct::CtService, schema::resolvers::users::resolve_user};
use async_graphql::*;
use efg_lambda_base::errors::AppError;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
#[graphql(complex)]
pub struct Group {
    pub id: String,
    pub name: String,
    pub time: Option<String>,
    pub day: Option<String>,
    pub place: Option<String>,
    pub note: String,
    pub image_url: Option<String>,
    pub ages: Option<String>,
    pub contact_url: Option<String>,
    #[graphql(deprecation = true)]
    pub can_be_contacted: bool,
    #[graphql(visible = false, skip = true)]
    pub leader_ids: Vec<String>,
}

#[ComplexObject]
impl Group {
    async fn leaders<'ctx>(&self, ctx: &Context<'ctx>) -> Result<Vec<User>, AppError> {
        let ct = ctx.data_unchecked::<CtService>();

        let mut users = Vec::new();
        for id in self.leader_ids.iter() {
            if let Some(user) = resolve_user(ct, id).await? {
                users.push(user);
            }
        }

        Ok(users)
    }
}
