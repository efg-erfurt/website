use async_graphql::*;
use time::OffsetDateTime;

#[derive(InputObject)]
pub struct CreateSermonInput {
    pub title: String,
    pub language: String,
    pub audio_url: Option<String>,
    pub text_url: Option<String>,
    pub translation_url: Option<String>,
    pub video_url: Option<String>,
    pub talk_date: OffsetDateTime,
    pub comment: Option<String>,
    pub speaker: String,
    pub tags: Vec<String>,
}
