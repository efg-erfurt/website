use super::event_category::EventCategory;
use crate::{clients::ct::CtService, schema::resolvers::events::resolve_event_category};
use async_graphql::*;
use efg_lambda_base::errors::AppError;
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
#[graphql(complex)]
pub struct Event {
    pub id: String,
    pub name: String,
    pub date: OffsetDateTime,
    pub place: Option<String>,
    pub description: Option<String>,
    #[graphql(visible = false, skip = true)]
    pub category_id: String,
}

#[ComplexObject]
impl Event {
    async fn category<'ctx>(&self, ctx: &Context<'ctx>) -> Result<EventCategory, AppError> {
        let ct = ctx.data_unchecked::<CtService>();

        let category = resolve_event_category(ct, &self.category_id).await?;

        Ok(category.unwrap())
    }
}
