use async_graphql::*;

#[derive(InputObject)]
pub struct SendMessageInput {
    pub name: Option<String>,
    #[graphql(validator(email))]
    pub mail: Option<String>,
    pub destination: String,
    pub message: String,
}
