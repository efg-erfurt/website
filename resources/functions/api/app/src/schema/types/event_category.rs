use async_graphql::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
pub struct EventCategory {
    pub id: String,
    pub name: String,
    pub background_color: String,
    pub text_color: String,
}
