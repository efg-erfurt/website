use async_graphql::*;
use aws_sdk_dynamodb::types::AttributeValue;
use efg_db_client::db_item::DbItem;
use efg_lambda_base::errors::AppError;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use time::{format_description::well_known::Rfc3339, OffsetDateTime};

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
pub struct Content {
    pub key: String,
    pub language: String,
    pub content: String,
    pub date: OffsetDateTime,
}

impl Content {
    pub fn build_id(key: &str, language: &str) -> String {
        format!("{language}.{key}")
    }
}

impl DbItem<Content> for Content {
    fn from_raw(raw: &HashMap<String, AttributeValue>) -> Result<Content, AppError> {
        let key = raw.get("key").unwrap().as_s().unwrap().clone();
        let content = raw.get("content").unwrap().as_s().unwrap().clone();
        let language = raw.get("language").unwrap().as_s().unwrap().clone();
        let date = raw.get("date").unwrap().as_s().unwrap().clone();
        let content = Content {
            key,
            content,
            language,
            date: OffsetDateTime::parse(&date, &Rfc3339)
                .unwrap_or_else(|_| panic!("deserializing saved date failed: {}", &date)),
        };

        Ok(content)
    }

    fn to_raw(&self) -> HashMap<String, AttributeValue> {
        let mut raw = HashMap::new();
        raw.insert("key".to_string(), AttributeValue::S(self.key.to_string()));
        raw.insert(
            "content".to_string(),
            AttributeValue::S(self.content.to_string()),
        );
        raw.insert(
            "language".to_string(),
            AttributeValue::S(self.language.to_string()),
        );
        raw.insert(
            "date".to_string(),
            AttributeValue::S(OffsetDateTime::format(self.date, &Rfc3339).unwrap()),
        );
        raw
    }

    fn get_id(&self) -> String {
        Content::build_id(&self.key, &self.language)
    }
}

#[cfg(test)]
mod tests {
    use super::Content;
    use aws_sdk_dynamodb::types::AttributeValue;
    use efg_db_client::db_item::DbItem;
    use std::collections::HashMap;
    use time::macros::datetime;

    #[tokio::test]
    async fn test_should_transform_entity_to_raw() {
        // given
        let content = Content {
            content: "abc".to_string(),
            date: datetime!(2022-02-13 10:30 UTC),
            key: "key".to_string(),
            language: "de".to_string(),
        };

        // when
        let raw = content.to_raw();

        // then
        assert_eq!("key", raw.get("key").unwrap().as_s().unwrap());
        assert_eq!("abc", raw.get("content").unwrap().as_s().unwrap());
        assert_eq!("de", raw.get("language").unwrap().as_s().unwrap());
        assert_eq!(
            "2022-02-13T10:30:00Z",
            raw.get("date").unwrap().as_s().unwrap()
        );
    }

    #[tokio::test]
    async fn test_should_transform_raw_to_entity() {
        // given
        let mut raw = HashMap::new();
        raw.insert("key".to_string(), AttributeValue::S("key".to_string()));
        raw.insert("content".to_string(), AttributeValue::S("abc".to_string()));
        raw.insert("language".to_string(), AttributeValue::S("de".to_string()));
        raw.insert(
            "date".to_string(),
            AttributeValue::S("2022-03-11T16:24:52.356Z".to_string()),
        );

        // when
        let entity = Content::from_raw(&raw).unwrap();

        // then
        assert_eq!("key", entity.key);
        assert_eq!("abc", entity.content);
        assert_eq!("de", entity.language);
        assert_eq!("2022-03-11 16:24:52.356 +00:00:00", entity.date.to_string());
    }
}
