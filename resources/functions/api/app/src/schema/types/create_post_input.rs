use async_graphql::*;

#[derive(InputObject)]
pub struct CreatePostInput {
    pub title: String,
    pub content: String,
    pub image_url: Option<String>,
    pub tags: Vec<String>,
    pub language: String,
}
