use super::{event::Event, post::Post, sermon::Sermon};
use async_graphql::*;
use efg_lambda_base::errors::AppError;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
#[graphql(complex)]

pub struct SearchResult {
    pub id: String,
    pub query: String,
    pub language: String,
    #[graphql(visible = false, skip = true)]
    pub posts: Vec<Post>,
    #[graphql(visible = false, skip = true)]
    pub events: Vec<Event>,
    #[graphql(visible = false, skip = true)]
    pub sermons: Vec<Sermon>,
    #[graphql(visible = false, skip = true)]
    pub pages: Vec<PageResult>,
}

#[ComplexObject]
impl SearchResult {
    pub async fn posts(&self) -> Result<Vec<Post>, AppError> {
        Ok(self.posts.clone())
    }

    pub async fn events(&self) -> Result<Vec<Event>, AppError> {
        Ok(self.events.clone())
    }

    pub async fn sermons(&self) -> Result<Vec<Sermon>, AppError> {
        Ok(self.sermons.clone())
    }

    pub async fn pages(&self) -> Result<Vec<PageResult>, AppError> {
        Ok(self.pages.clone())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
#[graphql(complex)]
pub struct PageResult {
    pub title: String,
    pub path: String,
    pub score: f32,
}

#[ComplexObject]
impl PageResult {
    pub async fn matches(&self) -> Result<Vec<Match>, AppError> {
        // TODO EFG-11
        Ok(Vec::new())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, SimpleObject)]
#[graphql(complex)]
pub struct Match {
    pub content: String,
}

#[ComplexObject]
impl Match {
    pub async fn selection(&self) -> Result<Vec<MatchSelection>, AppError> {
        // TODO EFG-11
        Ok(Vec::new())
    }
}

#[derive(SimpleObject)]
pub struct MatchSelection {
    pub from: u32,
    pub to: u32,
    pub highlight: bool,
}
