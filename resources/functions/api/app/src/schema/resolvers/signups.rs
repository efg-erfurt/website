use crate::{clients::ct::CtService, schema::types::signup::Signup};
use efg_lambda_base::errors::AppError;

pub async fn resolve_signups(groups_id: &str, ct: &CtService) -> Result<Vec<Signup>, AppError> {
    let response = ct.get_group_by_id(groups_id).await?;

    let groups = response.data.map_or_else(Vec::new, |data| {
        data.groups
            .iter()
            .map(|g| Signup {
                id: format!("{}", g.id),
                name: g.name.clone(),
                time: g.information.meetingTime.clone(),
                day: g
                    .information
                    .weekday
                    .as_ref()
                    .map(|n| n.nameTranslated.clone()),
                note: g.information.note.clone(),
                image_url: g
                    .information
                    .imageUrl
                    .as_ref()
                    .filter(|val| !val.is_empty())
                    .cloned(),
                slots_total: g.maxMemberCount,
                slots_left: if g.maxMemberCount.is_some() {
                    Some(g.maxMemberCount.unwrap() - g.currentMemberCount)
                } else {
                    None
                },
                slots_used: g.currentMemberCount,
            })
            .collect()
    });

    Ok(groups)
}
