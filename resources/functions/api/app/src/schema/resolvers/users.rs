use crate::{clients::ct::CtService, schema::types::user::User};
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};

pub async fn resolve_current_user(
    ct: &CtService,
    auth_context: &AuthContext,
) -> Result<Option<User>, AppError> {
    let response = ct.get_current_user(auth_context).await?;

    Ok(response.map(|ct_user| User {
        id: format!("{}", ct_user.data.id),
        first_name: ct_user.data.firstName,
        last_name: ct_user.data.lastName,
        image_url: ct_user
            .data
            .imageUrl
            .as_ref()
            .filter(|value| !value.is_empty())
            .cloned(),
    }))
}

pub async fn resolve_user(ct: &CtService, id: &str) -> Result<Option<User>, AppError> {
    let response = ct.get_user_by_id(id).await?;

    let user = response.and_then(|ct_user| {
        ct_user.data.map(|data| User {
            id: format!("{}", data.id),
            first_name: data.firstName,
            last_name: data.lastName,
            image_url: data.imageUrl.filter(|x| !x.is_empty()),
        })
    });

    Ok(user)
}
