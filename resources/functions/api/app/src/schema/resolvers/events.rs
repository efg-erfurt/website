use crate::{
    clients::ct::CtService,
    schema::types::{event::Event, event_category::EventCategory},
    types::ct::CtEvent,
};
use efg_lambda_base::errors::AppError;
use std::{cmp::Ordering, collections::HashMap};
use time::{
    format_description::{self},
    Duration, OffsetDateTime, Time,
};

const REPEAT_NONE: &str = "0";
const REPEAT_DAILY: &str = "1";
const REPEAT_WEEKLY: &str = "7";
const REPEAT_SAME_DAY_MONTHLY: &str = "32";
const REPEAT_UNDEFINED: &str = "999";

pub async fn resolve_events(
    ct: &CtService,
    start: &OffsetDateTime,
    end: &Option<OffsetDateTime>,
    // TODO consider use pagination
    max_entries: usize,
    category_id: &Option<String>,
) -> Result<Vec<Event>, AppError> {
    let category_ids = if category_id.is_some() {
        vec![category_id.clone().unwrap()]
    } else {
        fetch_all_category_ids(ct).await?
    };

    let response = ct.get_calendars_by_category_ids(&category_ids).await?;

    let mut events = Vec::new();

    // map to event type
    for category_id in category_ids {
        let found_events = response
            .data
            .get(&category_id)
            .map(|values_map| match values_map {
                crate::types::ct::EitherReponse::Mapped(hash_map) => {
                    hash_map.values().cloned().collect::<Vec<_>>()
                }
                crate::types::ct::EitherReponse::List(vec) => vec.clone(),
            })
            .unwrap_or_else(Vec::new);

        for ev in found_events {
            let mut repeated_events =
                map_ct_event_to_repeated_events(&ev, &category_id, start, end)?;
            events.append(&mut repeated_events);
        }
    }

    // filter by start and optional end
    events.retain(|ev| {
        start.cmp(&ev.date).is_le() && end.map_or(true, |end| end.cmp(&ev.date).is_gt())
    });

    // filter out double entries from CT
    let mut keyed_events = HashMap::new();
    for ev in events {
        let key = format!("{}-{}", ev.name, ev.date);
        keyed_events.insert(key, ev.clone());
    }
    let mut unique_events = keyed_events.values().cloned().collect::<Vec<_>>();

    // sort by date and id
    unique_events.sort_by(|a, b| match a.date.cmp(&b.date) {
        Ordering::Equal => a.id.cmp(&b.id),
        val => val,
    });

    // limit result size
    let res = unique_events
        .iter()
        .take(max_entries)
        .cloned()
        .collect::<Vec<_>>();

    Ok(res)
}

pub async fn resolve_event_categories(ct: &CtService) -> Result<Vec<EventCategory>, AppError> {
    let categories_response = ct.get_categories().await?;

    let mut categories = categories_response
        .data
        .unwrap()
        .category
        .values()
        .map(|category| EventCategory {
            id: category.id.to_string(),
            name: category.bezeichnung.to_string(),
            background_color: category.color.clone(),
            text_color: category.textColor.clone(),
        })
        .collect::<Vec<_>>();

    categories.sort_by(|a, b| a.id.cmp(&b.id));

    Ok(categories)
}

pub async fn resolve_event_category(
    ct: &CtService,
    id: &str,
) -> Result<Option<EventCategory>, AppError> {
    let categories_response = ct.get_categories().await?;

    let category = categories_response
        .data
        .unwrap()
        .category
        .get(id)
        .map(|category| EventCategory {
            id: category.id.to_string(),
            name: category.bezeichnung.to_string(),
            background_color: category.color.clone(),
            text_color: category.textColor.clone(),
        });

    Ok(category)
}

async fn fetch_all_category_ids(ct: &CtService) -> Result<Vec<String>, AppError> {
    let categories_response = ct.get_categories().await?;

    Ok(categories_response
        .data
        .unwrap()
        .category
        .keys()
        .cloned()
        .collect::<Vec<_>>())
}

fn map_ct_event_to_repeated_events(
    event: &CtEvent,
    category_id: &str,
    start: &OffsetDateTime,
    end: &Option<OffsetDateTime>,
) -> Result<Vec<Event>, AppError> {
    let start_date = parse_ct_date(&event.startdate);

    // determine repeated events
    let repeat_end_date = event
        .repeat_until
        .clone()
        .filter(|date| !"0000-00-00 00:00:00".eq(date))
        .map(|d| parse_ct_date(&d));
    let frequence = event
        .repeat_frequence
        .clone()
        .map(|f| f.parse::<i64>().unwrap());
    let repeated_dates = match event.repeat_id.as_str() {
        REPEAT_NONE | REPEAT_UNDEFINED => vec![Event {
            id: event.id.clone(),
            date: start_date,
            category_id: category_id.to_string(),
            description: event.notizen.clone(),
            name: event.bezeichnung.clone(),
            place: event.ort.clone(),
        }],
        REPEAT_DAILY => transform_to_repeated_events(
            start_date,
            repeat_end_date,
            event,
            category_id,
            Duration::days(frequence.expect("missing frequence")),
        ),
        REPEAT_WEEKLY => transform_to_repeated_events(
            start_date,
            repeat_end_date,
            event,
            category_id,
            Duration::weeks(frequence.expect("missing frequence")),
        ),
        REPEAT_SAME_DAY_MONTHLY => {
            transform_to_monthly_same_day_events(start_date, repeat_end_date, event, category_id)
        }
        repeat_id => unimplemented!("missing implementation for: {}", repeat_id),
    };

    // filter exception time frames
    let mut filtered_dates = repeated_dates
        .iter()
        .filter(|d| !is_part_of_exception(d, event))
        .cloned()
        .collect::<Vec<_>>();

    // additional custom events
    if let Some(e) = event.csevents.as_ref() {
        let mut custom_events = e
            .values()
            .map(|custom_event| {
                let date = parse_ct_date(&custom_event.startdate);

                Event {
                    id: format!("{}-c{}", &event.id, &custom_event.id),
                    date,
                    category_id: category_id.to_string(),
                    description: event.notizen.clone(),
                    name: event.bezeichnung.clone(),
                    place: event.ort.clone(),
                }
            })
            .collect::<Vec<_>>();
        filtered_dates.append(&mut custom_events);
    }
    // add also addition dates
    if let Some(e) = event.additions.as_ref() {
        let mut additions = e
            .values()
            .map(|addition| {
                let mut date = parse_ct_date(&addition.add_date);

                date = date.replace_time(start_date.time());

                Event {
                    id: format!("{}-a{}", &event.id, &addition.id),
                    date,
                    category_id: category_id.to_string(),
                    description: event.notizen.clone(),
                    name: event.bezeichnung.clone(),
                    place: event.ort.clone(),
                }
            })
            .collect::<Vec<_>>();
        filtered_dates.append(&mut additions);
    }

    // filter by start and optional end
    filtered_dates.retain(|ev| {
        start.cmp(&ev.date).is_le() && end.map_or(true, |end| end.cmp(&ev.date).is_ge())
    });

    Ok(filtered_dates)
}

fn is_part_of_exception(event: &Event, base_event: &CtEvent) -> bool {
    base_event.exceptions.as_ref().map_or(false, |e| {
        e.values()
            // matches specific exception
            .map(|exception| {
                let start = parse_ct_date(&exception.except_date_start);
                let end = parse_ct_date(&exception.except_date_end)
                    .replace_time(Time::from_hms(23, 59, 0).unwrap());
                event.date.cmp(&start).is_ge() && event.date.cmp(&end).is_le()
            })
            // matches any exception
            .reduce(|a, b| a || b)
            .unwrap_or(false)
    })
}

fn transform_to_monthly_same_day_events(
    start_date: OffsetDateTime,
    repeat_end_date: Option<OffsetDateTime>,
    event: &CtEvent,
    category_id: &str,
) -> Vec<Event> {
    let mut events = Vec::new();
    let mut date = start_date;
    let until_date = repeat_end_date.expect("missing repeat until date");

    let week_in_month = get_week_in_month(&date);
    let mut repeat_id: u16 = 0;
    while date.cmp(&until_date) != Ordering::Greater {
        if is_same_week_in_month(week_in_month, &date) {
            events.push(Event {
                id: format!("{}-r{}", &event.id, repeat_id),
                date,
                category_id: category_id.to_string(),
                description: event.notizen.clone(),
                name: event.bezeichnung.clone(),
                place: event.ort.clone(),
            });
            repeat_id += 1;
        }
        date += Duration::weeks(1); // go to next week
    }

    events
}

fn is_same_week_in_month(week_in_month: u8, current_date: &OffsetDateTime) -> bool {
    week_in_month == get_week_in_month(current_date)
}

/// Calculates the week index during a month.
/// The value can diverge from 0 to 4 based on maximum number of five week days per month.
fn get_week_in_month(date: &OffsetDateTime) -> u8 {
    let day_in_month = date.day();
    (day_in_month - (day_in_month % 7)) / 7
}

/// Returns repeated events based on a defined duration. Please note, this method doesn't support custom repeat frequences yet.
fn transform_to_repeated_events(
    start_date: OffsetDateTime,
    repeat_end_date: Option<OffsetDateTime>,
    event: &CtEvent,
    category_id: &str,
    duration: Duration,
) -> Vec<Event> {
    let mut events = Vec::new();
    let mut date = start_date;
    let until_date = repeat_end_date.expect("missing repeat until date");
    let mut repeat_id: u16 = 0;
    while date.cmp(&until_date) != Ordering::Greater {
        events.push(Event {
            id: format!("{}-r{}", &event.id, repeat_id),
            date,
            category_id: category_id.to_string(),
            description: event.notizen.clone(),
            name: event.bezeichnung.clone(),
            place: event.ort.clone(),
        });
        date += duration;
        repeat_id += 1;
    }
    events
}

fn parse_ct_date(date_string: &str) -> OffsetDateTime {
    let format = format_description::parse(
        "[year]-[month]-[day] [hour]:[minute]:[second] [offset_hour sign:mandatory]:[offset_minute]:[offset_second]",
    ).expect("creating format pattern failed");

    let extended_date = format!("{date_string} +00:00:00");
    OffsetDateTime::parse(&extended_date, &format)
        .unwrap_or_else(|_| panic!("parsing date failed: {}", &extended_date))
}

#[cfg(test)]
mod tests {
    use super::get_week_in_month;
    use crate::{
        clients::ct::{CtClient, CtService},
        create_ct_rest_mock,
        schema::{resolvers::events::resolve_events, types::event::Event},
        types::ct::{CtEventsOverviewResponse, CtEventsResponse},
    };
    use async_trait::async_trait;
    use efg_lambda_base::errors::AppError;
    use std::{sync::Arc, vec};
    use time::macros::datetime;

    /// Macro for generating mock ct rest client implementation with only a custom categories file.
    #[macro_export]
    macro_rules! create_ct_rest_mock_with_categories {
        ($s:expr) => {{
            create_ct_rest_mock!(
                async fn get_categories(&self) -> Result<CtEventsOverviewResponse, AppError> {
                    Ok(serde_json::from_str(
                        &include_str!("../../../tests/query/responses/ct/getMasterData.json")
                            .to_string(),
                    )
                    .expect("reading mock data failed"))
                },
                async fn get_calendars_by_category_ids(
                    &self,
                    _category_ids: &[String],
                ) -> Result<CtEventsResponse, AppError> {
                    Ok(serde_json::from_str(&$s).expect("reading mock data failed"))
                }
            )
        }};
    }

    /// Macro for generating mock ct rest client implementation with required category id.
    #[macro_export]
    macro_rules! create_ct_rest_mock_with_specific_category {
        ($s:expr) => {{
            create_ct_rest_mock!(
                async fn get_calendars_by_category_ids(
                    &self,
                    _category_ids: &[String],
                ) -> Result<CtEventsResponse, AppError> {
                    Ok(serde_json::from_str(&$s).expect("reading mock data failed"))
                }
            )
        }};
    }

    #[tokio::test]
    async fn test_should_resolve_events() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock_with_categories!(
            include_str!("../../../tests/query/responses/ct/getCalPerCategory.json").to_string()
        );

        // when
        let events = resolve_events_with_no_filters(ct_rest_mock).await;

        // then
        assert_eq!(610, events.len(), "unexpected total events count");
        let unique_ids = events.iter().map(|ev| ev.id.clone()).len();
        ct_rest_mock_collector.collect_calls();
        assert_eq!(events.len(), unique_ids, "event ids are not unique");
    }

    #[tokio::test]
    async fn test_should_resolve_events_with_start_filter() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock_with_categories!(
            include_str!("../../../tests/query/responses/ct/getCalPerCategory_daily.json")
                .to_string()
        );
        let ct_service: CtService = Arc::new(ct_rest_mock);

        // when
        let events = (resolve_events(
            &ct_service,
            &datetime!(2080-12-13 19:30 UTC),
            &None,
            99_999,
            &None,
        )
        .await)
            .expect("should resolve");

        // then
        ct_rest_mock_collector.collect_calls();
        assert!(
            events.is_empty(),
            "events should be empty because of start filter"
        );
    }

    #[tokio::test]
    async fn test_should_resolve_events_with_end_filter() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock_with_categories!(
            include_str!("../../../tests/query/responses/ct/getCalPerCategory_daily.json")
                .to_string()
        );
        let ct_service: CtService = Arc::new(ct_rest_mock);

        // when
        let events = (resolve_events(
            &ct_service,
            &datetime!(2000-12-13 19:30 UTC),
            &Some(datetime!(2010-12-13 19:30 UTC)),
            99_999,
            &None,
        )
        .await)
            .expect("should resolve");

        // then
        ct_rest_mock_collector.collect_calls();
        assert!(
            events.is_empty(),
            "events should be empty because of end filter"
        );
    }

    #[tokio::test]
    async fn test_should_resolve_events_with_unknown_category_filter() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock_with_categories!(
            include_str!("../../../tests/query/responses/ct/getCalPerCategory_daily.json")
                .to_string()
        );
        let ct_service: CtService = Arc::new(ct_rest_mock);

        // when
        let events = (resolve_events(
            &ct_service,
            &datetime!(2000-12-13 19:30 UTC),
            &Some(datetime!(2080-12-13 19:30 UTC)),
            99_999,
            &Some("12345".to_string()),
        )
        .await)
            .expect("should resolve");

        // then
        ct_rest_mock_collector.collect_calls();
        assert!(
            events.is_empty(),
            "events should be empty because of category filter"
        );
    }

    #[tokio::test]
    async fn test_should_resolve_events_with_category_filter() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock_with_specific_category!(
            include_str!("../../../tests/query/responses/ct/getCalPerCategory_with_category.json")
                .to_string()
        );
        let ct_service: CtService = Arc::new(ct_rest_mock);

        // when
        let events = (resolve_events(
            &ct_service,
            &datetime!(2000-12-13 19:30 UTC),
            &None,
            99_999,
            &Some("123456".to_string()),
        )
        .await)
            .expect("should resolve");

        // then
        ct_rest_mock_collector.collect_calls();
        assert_eq!(5, events.len());
    }

    #[tokio::test]
    async fn test_should_resolve_events_with_max_entries() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock_with_categories!(
            include_str!("../../../tests/query/responses/ct/getCalPerCategory.json").to_string()
        );

        // when
        let events = resolve_events_with_max_entries(ct_rest_mock, 10).await;

        // then
        ct_rest_mock_collector.collect_calls();
        assert_eq!(10, events.len(), "should have limited entries count");
    }

    #[tokio::test]
    async fn test_should_resolve_events_for_daily_events() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock_with_categories!(
            include_str!("../../../tests/query/responses/ct/getCalPerCategory_daily.json")
                .to_string()
        );

        // when
        let events = resolve_events_with_no_filters(ct_rest_mock).await;

        // then
        ct_rest_mock_collector.collect_calls();
        assert_eq!(5, events.len());
    }

    #[tokio::test]
    async fn test_should_resolve_events_for_weekly_events() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock_with_categories!(
            include_str!("../../../tests/query/responses/ct/getCalPerCategory_weekly.json")
                .to_string()
        );

        // when
        let events = resolve_events_with_no_filters(ct_rest_mock).await;

        // then
        ct_rest_mock_collector.collect_calls();
        assert_eq!(24, events.len());
    }

    #[tokio::test]
    async fn test_should_resolve_events_for_monthly_same_day_events() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) =
            create_ct_rest_mock_with_categories!(include_str!(
                "../../../tests/query/responses/ct/getCalPerCategory_monthly_same_day.json"
            )
            .to_string());

        // when
        let events = resolve_events_with_no_filters(ct_rest_mock).await;

        // then
        ct_rest_mock_collector.collect_calls();
        assert_eq!(5, events.len());
    }

    #[test]
    fn test_should_get_week_in_month() {
        // given
        let dates = vec![
            datetime!(2022-03-27 10:30 UTC),
            datetime!(2022-03-01 10:30 UTC),
            datetime!(2022-03-31 10:30 UTC),
        ];

        // when
        let weekdays = dates.iter().map(get_week_in_month).collect::<Vec<_>>();

        // then
        assert_eq!(vec![3, 0, 4], weekdays);
    }

    async fn resolve_events_with_no_filters<C>(ct_rest_mock: C) -> Vec<Event>
    where
        C: CtClient + 'static,
    {
        let ct_service: CtService = Arc::new(ct_rest_mock);
        (resolve_events(
            &ct_service,
            &datetime!(2020-12-13 19:30 UTC),
            &None,
            99_999,
            &None,
        )
        .await)
            .expect("should resolve")
    }

    async fn resolve_events_with_max_entries<C>(ct_rest_mock: C, max_entries: usize) -> Vec<Event>
    where
        C: CtClient + 'static,
    {
        let ct_service: CtService = Arc::new(ct_rest_mock);
        (resolve_events(
            &ct_service,
            &datetime!(2020-12-13 19:30 UTC),
            &None,
            max_entries,
            &None,
        )
        .await)
            .expect("should resolve")
    }
}
