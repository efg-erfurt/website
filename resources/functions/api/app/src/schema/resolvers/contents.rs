use crate::schema::{mutation::Language, types::content::Content};
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use time::OffsetDateTime;

pub async fn resolve_contents(
    content_db: &DbService<Content>,
    keys: &[String],
    language: &Language,
) -> Result<Vec<Content>, AppError> {
    let db_keys = keys
        .iter()
        .map(|key| Content::build_id(key, &language.to_str()))
        .collect::<Vec<_>>();
    let results = content_db.batch_get_values_by_ids(&db_keys).await?;

    Ok(results.values().cloned().collect::<Vec<_>>())
}

pub async fn resolve_content(
    content_db: &DbService<Content>,
    key: &str,
    language: &Language,
) -> Result<Option<Content>, AppError> {
    let result = content_db
        .get_value_by_id(&Content::build_id(key, &language.to_str()))
        .await?;

    Ok(result)
}

pub async fn update_content(
    content_db: &DbService<Content>,
    key: &str,
    content: &str,
    language: &Language,
) -> Result<(), AppError> {
    let id = Content::build_id(key, &language.to_str());
    let _existing_content = content_db
        .get_value_by_id(&id)
        .await?
        .ok_or_else(|| AppError::new("content missing"))?;

    let new_content = Content {
        key: key.to_string(),
        content: content.to_string(),
        date: OffsetDateTime::now_utc(),
        language: language.to_str(),
    };

    content_db.put_item(new_content).await?;

    Ok(())
}
