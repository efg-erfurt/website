use super::events::resolve_events;
use crate::{
    clients::{ct::CtService, http::HttpService},
    schema::{
        mutation::Language,
        types::{
            event::Event,
            post::Post,
            search_result::{PageResult, SearchResult},
            sermon::Sermon,
        },
    },
};
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use efg_search::crawler::Crawler;
use futures::future::join_all;
use http::Request;
use std::cmp::Ordering;
use time::OffsetDateTime;
use tokio::join;
use tracing::error;
use uuid::Uuid;

const MAX_RESULTS: usize = 10;

pub async fn search(
    query: &str,
    language: &Language,
    start_date: &OffsetDateTime,
    http_service: &HttpService,
    post_db: &DbService<Post>,
    sermon_db: &DbService<Sermon>,
    ct_service: &CtService,
) -> Result<SearchResult, AppError> {
    let normalized_query = query.to_lowercase();

    let (pages, events, posts, sermons) = join!(
        determine_page_results(&normalized_query, language, http_service),
        determine_events(&normalized_query, start_date, ct_service),
        determine_posts(&normalized_query, post_db),
        determine_sermons(&normalized_query, sermon_db)
    );

    Ok(SearchResult {
        id: Uuid::new_v4().to_string(),
        language: language.to_str(),
        query: query.to_string(),
        pages: pages?,
        events: events?,
        posts: posts?,
        sermons: sermons?,
    })
}

async fn determine_events(
    query: &str,
    start_date: &OffsetDateTime,
    ct_service: &CtService,
) -> Result<Vec<Event>, AppError> {
    let all_events = resolve_events(ct_service, start_date, &None, 1000, &None).await?;

    let filtered_events = all_events
        .iter()
        .filter(|e| {
            e.name.to_lowercase().contains(query)
                || e.description
                    .as_ref()
                    .filter(|d| d.to_lowercase().contains(query))
                    .is_some()
        })
        .take(MAX_RESULTS)
        .cloned()
        .collect::<Vec<_>>();

    Ok(filtered_events)
}

async fn determine_posts(query: &str, post_db: &DbService<Post>) -> Result<Vec<Post>, AppError> {
    let all_posts = post_db.get_all_values("id", 0, 99999).await?;

    let filtered_posts = all_posts
        .iter()
        .filter(|s| {
            s.title.to_lowercase().contains(query) || s.content.to_lowercase().contains(query)
        })
        .take(MAX_RESULTS)
        .cloned()
        .collect::<Vec<_>>();

    Ok(filtered_posts)
}

async fn determine_sermons(
    query: &str,
    sermon_db: &DbService<Sermon>,
) -> Result<Vec<Sermon>, AppError> {
    let all_sermons = sermon_db.get_all_values("id", 0, 99999).await?;

    let filtered_sermons = all_sermons
        .iter()
        .filter(|s| s.title.to_lowercase().contains(query))
        .take(MAX_RESULTS)
        .cloned()
        .collect::<Vec<_>>();

    Ok(filtered_sermons)
}

async fn determine_page_results(
    query: &str,
    language: &Language,
    http_service: &HttpService,
) -> Result<Vec<PageResult>, AppError> {
    let language = language.to_str();
    let uris = [
        format!("https://www.efg-erfurt.de/{language}/news"),
        format!("https://www.efg-erfurt.de/{language}/about-our-believe"),
        format!("https://www.efg-erfurt.de/{language}/about-us"),
        format!("https://www.efg-erfurt.de/{language}/sermons"),
        format!("https://www.efg-erfurt.de/{language}/events"),
        format!("https://www.efg-erfurt.de/{language}/donate"),
        format!("https://www.efg-erfurt.de/{language}/contribute"),
        format!("https://www.efg-erfurt.de/{language}/projects"),
        format!("https://www.efg-erfurt.de/{language}/groups"),
        format!("https://www.efg-erfurt.de/{language}/contact"),
        format!("https://www.efg-erfurt.de/{language}/cafe"),
        format!("https://www.efg-erfurt.de/{language}/imprint"),
        format!("https://www.efg-erfurt.de/{language}/event?id=53"),
    ];

    // send crawl requests
    let pages = uris
        .iter()
        .map(|uri| crawl_page(uri, query, http_service))
        .collect::<Vec<_>>();

    // wait for results
    let mut results = join_all(pages)
        .await
        .into_iter()
        .collect::<Result<Vec<_>, _>>()?
        .iter()
        .flatten()
        .cloned()
        .collect::<Vec<_>>();

    // sort by score
    results.sort_by(|a, b| {
        if b.score - a.score > 0.0 {
            Ordering::Greater
        } else if b.score - a.score < 0.0 {
            Ordering::Less
        } else {
            Ordering::Equal
        }
    });

    // take best results
    Ok(results
        .iter()
        .take(MAX_RESULTS)
        .cloned()
        .collect::<Vec<_>>())
}

async fn crawl_page(
    path: &str,
    query: &str,
    http_service: &HttpService,
) -> Result<Option<PageResult>, AppError> {
    // fetch page
    let request = Request::builder()
        .uri(path)
        .body(None)
        .map_err(|err| AppError::from_error(&err, "building request failed"))?;
    let response = http_service.send_request(request).await?;

    if !response.status().is_success() {
        error!("crawling {} failed with status {}", path, response.status());
        return Err(AppError::new("unexpected response"));
    }

    let page = response.body();

    parse_page(page, path, query)
}

fn parse_page(page: &str, path: &str, query: &str) -> Result<Option<PageResult>, AppError> {
    let crawler = Crawler;
    let page = crawler.parse_page(page);
    let score = crawler.calculate_score(&page, query);

    Ok(if score > 0.0 {
        Some(PageResult {
            title: page
                .get("title")
                .filter(|titles| !titles.is_empty())
                .map(|titles| titles.first().unwrap().clone())
                .unwrap_or_else(|| "Unbekannte Seite".to_string()),
            path: path.to_string(),
            score,
        })
    } else {
        None
    })
}
