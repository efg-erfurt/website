use super::build_paginated_list;
use crate::schema::{
    mutation::Language,
    types::{
        create_sermon_input::CreateSermonInput, pagination::Pagination,
        pagination_input::PaginationInput, sermon::Sermon, update_sermon_input::UpdateSermonInput,
    },
};
use efg_db_client::db::DbService;
use efg_lambda_base::errors::AppError;
use time::OffsetDateTime;
use uuid::Uuid;

pub async fn resolve_sermons(
    sermon_db: &DbService<Sermon>,
    language: &Language,
    speaker: &Option<String>,
    pagination: &PaginationInput,
) -> Result<Pagination<Sermon>, AppError> {
    let all_sermons = sermon_db.get_all_values("publishDate", 0, 999999).await?;

    let mut filtered_sermons = all_sermons
        .iter()
        .filter(|sermon| sermon.language.eq(&language.to_str()))
        .filter(|sermon| {
            speaker
                .as_ref()
                .map(|s| s.eq(&sermon.speaker))
                .unwrap_or(true)
        })
        .cloned()
        .collect::<Vec<_>>();

    // sort by talk date descending
    filtered_sermons.sort_by(|a, b| b.talk_date.cmp(&a.talk_date));

    Ok(build_paginated_list(&filtered_sermons, pagination))
}

pub async fn resolve_latest_sermons(
    sermon_db: &DbService<Sermon>,
    language: &Language,
    pagination: &PaginationInput,
) -> Result<Pagination<Sermon>, AppError> {
    let all_sermons = sermon_db.get_all_values("publishDate", 0, 999999).await?;

    // filter by language
    let mut filtered_sermons = all_sermons
        .iter()
        .filter(|sermon| sermon.language.eq(&language.to_str()))
        .cloned()
        .collect::<Vec<_>>();

    // sort by talk date descending
    filtered_sermons.sort_by(|a, b| b.talk_date.cmp(&a.talk_date));

    Ok(build_paginated_list(&filtered_sermons, pagination))
}

pub async fn create_sermon(
    sermon_db: &DbService<Sermon>,
    input: CreateSermonInput,
) -> Result<Sermon, AppError> {
    let sermon = Sermon {
        id: Uuid::new_v4().to_string(),
        title: input.title,
        comment: input.comment,
        language: input.language,
        publish_date: OffsetDateTime::now_utc(),
        speaker: input.speaker,
        tags: input.tags,
        talk_date: input.talk_date,
        text_url: input.text_url,
        translation_url: input.translation_url,
        audio_url: input.audio_url,
        video_url: input.video_url,
    };

    sermon_db.put_item(sermon.clone()).await?;

    Ok(sermon)
}

pub async fn update_sermon(
    sermon_db: &DbService<Sermon>,
    input: UpdateSermonInput,
) -> Result<Sermon, AppError> {
    match sermon_db.get_value_by_id(&input.id).await? {
        Some(old_sermon) => {
            let sermon = Sermon {
                id: input.id,
                title: input.title,
                comment: input.comment,
                language: input.language,
                publish_date: old_sermon.publish_date,
                speaker: input.speaker,
                tags: input.tags,
                talk_date: input.talk_date,
                text_url: input.text_url,
                translation_url: input.translation_url,
                audio_url: input.audio_url,
                video_url: input.video_url,
            };

            sermon_db.put_item(sermon.clone()).await?;

            Ok(sermon)
        }
        None => Err(AppError::new("unknown sermon")),
    }
}

pub async fn delete_sermon(sermon_db: &DbService<Sermon>, id: &str) -> Result<(), AppError> {
    sermon_db.delete_item(id).await
}
