use crate::{clients::ct::CtService, schema::types::group::Group, types::ct::CtGroup};
use efg_lambda_base::errors::AppError;

pub async fn resolve_groups(group_id: &str, ct: &CtService) -> Result<Vec<Group>, AppError> {
    let response = ct.get_group_by_id(group_id).await?;

    Ok(response
        .data
        .map(|d| {
            d.groups
                .iter()
                .map(|group| map_ct_group_to_group(group, group_id))
                .collect()
        })
        .unwrap_or_default())
}

fn map_ct_group_to_group(ct_group: &CtGroup, id: &str) -> Group {
    Group {
        id: format!("{}-{}", id, ct_group.id),
        name: ct_group.name.clone(),
        time: ct_group.information.meetingTime.clone(),
        day: ct_group
            .information
            .weekday
            .as_ref()
            .map(|n| n.nameTranslated.clone()),
        place: ct_group
            .information
            .groupPlaces
            .iter()
            .map(|g| format!("{}, {} {}", g.name, g.postalcode, g.city))
            .reduce(|a, b| format!("{a} | {b}")),
        note: ct_group.information.note.clone(),
        image_url: ct_group.information.imageUrl.clone(),
        ages: ct_group
            .information
            .targetGroup
            .as_ref()
            .map(|t| t.nameTranslated.clone()),
        contact_url: ct_group
            .signUpConditions
            .as_ref()
            .filter(|s| s.canContactLeader)
            .map(|_| {
                format!(
                    "https://efg-erfurt.church.tools/publicgroup/{}",
                    ct_group.id
                )
            }),
        // deprecated
        can_be_contacted: false,
        leader_ids: ct_group
            .information
            .leader
            .iter()
            .map(|l| l.domainIdentifier.to_string())
            .collect(),
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        clients::ct::CtService, create_ct_rest_mock, schema::resolvers::groups::resolve_groups,
        types::ct::CtGroupsResponse,
    };
    use async_trait::async_trait;
    use efg_lambda_base::errors::AppError;
    use std::sync::Arc;

    #[tokio::test]
    async fn test_should_resolve_groups() {
        // given
        let (ct_rest_mock, mut ct_rest_mock_collector) = create_ct_rest_mock!(
            async fn get_group_by_id(&self, _group_id: &str) -> Result<CtGroupsResponse, AppError> {
                Ok(serde_json::from_str(
                    &include_str!("../../../tests/query/responses/ct/getGroups.json").to_string(),
                )
                .expect("reading mock data failed"))
            }
        );
        let ct_service: CtService = Arc::new(ct_rest_mock);

        // when
        let groups = (resolve_groups("ibAPusntnxi3hqTBHogr3wUhEqqoaRhP", &ct_service).await)
            .expect("should resolve");

        // then
        ct_rest_mock_collector.collect_calls();
        assert_eq!(5, groups.len());
    }
}
