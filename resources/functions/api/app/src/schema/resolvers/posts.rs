use super::build_paginated_list;
use crate::{
    clients::ct::CtService,
    schema::{
        mutation::Language,
        types::{
            create_post_input::CreatePostInput, pagination::Pagination,
            pagination_input::PaginationInput, post::Post, update_post_input::UpdatePostInput,
        },
    },
};
use efg_db_client::db::DbService;
use efg_lambda_base::{auth_context::AuthContext, errors::AppError};
use time::OffsetDateTime;
use uuid::Uuid;

pub async fn resolve_latest_posts(
    post_db: &DbService<Post>,
    tag: &Option<String>,
    language: &Language,
    pagination: &PaginationInput,
) -> Result<Pagination<Post>, AppError> {
    let all_posts = post_db.get_all_values("publishDate", 0, 99999).await?;

    // filter by language
    let mut filtered_posts = all_posts
        .iter()
        .filter(|post| post.language.eq(&language.to_str()))
        .filter(|post| tag.as_ref().map(|t| post.tags.contains(t)).unwrap_or(true))
        .cloned()
        .collect::<Vec<_>>();

    // sort by talk date descending
    filtered_posts.sort_by(|a, b| b.publish_date.cmp(&a.publish_date));

    Ok(build_paginated_list(&filtered_posts, pagination))
}

pub async fn resolve_post(post_db: &DbService<Post>, id: &str) -> Result<Option<Post>, AppError> {
    let result = post_db.get_value_by_id(id).await?;

    Ok(result)
}

pub async fn create_post(
    post_db: &DbService<Post>,
    ct_service: &CtService,
    auth_context: &AuthContext,
    input: CreatePostInput,
) -> Result<Post, AppError> {
    let user = ct_service
        .get_current_user(auth_context)
        .await?
        .expect("user should be existing");

    let new_id = Uuid::new_v4().to_string();
    let post = Post {
        id: new_id,
        date: OffsetDateTime::now_utc(),
        publish_date: OffsetDateTime::now_utc(),
        title: input.title,
        image_url: input.image_url,
        content: input.content,
        summary: None,
        tags: input.tags,
        language: input.language,
        user_id: user.data.id.to_string(),
    };

    post_db.put_item(post.clone()).await?;

    Ok(post)
}

pub async fn update_post(
    post_db: &DbService<Post>,
    ct_service: &CtService,
    auth_context: &AuthContext,
    input: UpdatePostInput,
) -> Result<Post, AppError> {
    let existing_post = post_db
        .get_value_by_id(&input.id)
        .await?
        .ok_or_else(|| AppError::new("no post found"))?;

    let user = ct_service
        .get_current_user(auth_context)
        .await?
        .expect("user should be existing");

    let post = Post {
        id: input.id,
        date: OffsetDateTime::now_utc(),
        publish_date: existing_post.publish_date,
        title: input.title,
        image_url: input.image_url,
        content: input.content,
        summary: existing_post.summary,
        tags: input.tags,
        language: existing_post.language,
        user_id: user.data.id.to_string(),
    };

    post_db.put_item(post.clone()).await?;

    Ok(post)
}

pub async fn delete_post(post_db: &DbService<Post>, id: &str) -> Result<(), AppError> {
    post_db.delete_item(id).await
}
