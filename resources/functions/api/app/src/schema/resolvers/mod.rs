use super::types::{pagination::Pagination, pagination_input::PaginationInput};
use async_graphql::OutputType;

pub mod contents;
pub mod events;
pub mod groups;
pub mod posts;
pub mod search;
pub mod sermons;
pub mod signups;
pub mod users;

pub fn build_paginated_list<T: OutputType + Clone>(
    all: &[T],
    pagination: &PaginationInput,
) -> Pagination<T> {
    let page = pagination.page;
    let size = pagination.size;

    // filter page from complete result set
    let content = all
        .iter()
        .skip(page * size)
        .take(size)
        .cloned()
        .collect::<Vec<_>>();
    let count = content.len();
    let total_count = all.len();

    Pagination {
        content,
        page,
        count,
        total_count,
        total_pages: (total_count / size) + usize::from(total_count % size != 0),
    }
}

#[cfg(test)]
mod tests {
    use super::build_paginated_list;
    use crate::schema::types::pagination_input::PaginationInput;

    #[test]
    fn test_should_return_first_page() {
        // given
        let all = (0..100).collect::<Vec<_>>();

        // when
        let page = build_paginated_list(&all, &PaginationInput { page: 0, size: 5 });

        // then
        assert_eq!(5, page.count);
        assert_eq!(5, page.content.len());
        assert_eq!(0, page.page);
        assert_eq!(100, page.total_count);
        assert_eq!(20, page.total_pages);
    }

    #[test]
    fn test_should_return_third_page() {
        // given
        let all = (0..100).collect::<Vec<_>>();

        // when
        let page = build_paginated_list(&all, &PaginationInput { page: 2, size: 5 });

        // then
        assert_eq!(5, page.count);
        assert_eq!(5, page.content.len());
        assert_eq!(2, page.page);
        assert_eq!(100, page.total_count);
        assert_eq!(20, page.total_pages);
    }

    #[test]
    fn test_should_return_page_with_100_items() {
        // given
        let all = (0..100).collect::<Vec<_>>();

        // when
        let page = build_paginated_list(&all, &PaginationInput { page: 0, size: 100 });

        // then
        assert_eq!(100, page.count);
        assert_eq!(100, page.content.len());
        assert_eq!(0, page.page);
        assert_eq!(100, page.total_count);
        assert_eq!(1, page.total_pages);
    }

    #[test]
    fn test_should_return_last_page() {
        // given
        let all = (0..7).collect::<Vec<_>>();

        // when
        let page = build_paginated_list(&all, &PaginationInput { page: 1, size: 5 });

        // then
        assert_eq!(2, page.count);
        assert_eq!(2, page.content.len());
        assert_eq!(1, page.page);
        assert_eq!(7, page.total_count);
        assert_eq!(2, page.total_pages);
    }

    #[test]
    fn test_should_return_empty_page() {
        // given

        // when
        let page = build_paginated_list(&Vec::<i32>::new(), &PaginationInput { page: 0, size: 5 });

        // then
        assert_eq!(0, page.count);
        assert_eq!(0, page.content.len());
        assert_eq!(0, page.page);
        assert_eq!(0, page.total_count);
        assert_eq!(0, page.total_pages);
    }
}
