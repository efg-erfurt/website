use crate::{
    clients::{
        auth::{client::AuthClientImpl, AuthService},
        cache::{CacheClientImpl, CacheService},
        ct::{client::CtClientImpl, CtService},
        http::{HttpClientImpl, HttpService},
        mail::{MailClientImpl, MailService},
    },
    config::AppConfig,
    schema::{
        mutation::Mutation,
        query::QueryRoot,
        types::{content::Content, post::Post, sermon::Sermon},
    },
};
use async_graphql::{EmptySubscription, Schema};
use efg_db_client::{db::DbService, dynamo_db::DynamoDbClient};
use efg_object_client::{db::ObjectService, s3_client::S3Client};
use std::{sync::Arc, time::Duration};

pub mod mutation;
pub mod query;
mod resolvers;
pub mod types;

// Cache entries for one hour
const CACHE_DURATION: Duration = Duration::from_secs(60 * 60);

pub async fn init_schema(
    app_config: &AppConfig,
    aws_config: &aws_config::SdkConfig,
) -> Schema<QueryRoot, Mutation, EmptySubscription> {
    let (content_client, post_client, sermon_client, cache_client) = (
        DynamoDbClient::new(app_config.db().content_table(), aws_config, None),
        DynamoDbClient::new(app_config.db().post_table(), aws_config, None),
        DynamoDbClient::new(app_config.db().sermon_table(), aws_config, None),
        DynamoDbClient::new(
            app_config.db().cache_table(),
            aws_config,
            Some(CACHE_DURATION),
        ),
    );
    let s3_client = S3Client::new(app_config.objects().public_bucket(), aws_config);
    let ct_service = Arc::new(CtClientImpl::new(
        app_config.ct().user(),
        app_config.ct().password(),
        Arc::new(CacheClientImpl::new(cache_client.clone())),
        Arc::new(HttpClientImpl::new()),
    ));
    let auth_client = AuthClientImpl::new(ct_service.clone());

    Schema::build(QueryRoot, Mutation, EmptySubscription)
        .data::<DbService<Content>>(Box::new(content_client))
        .data::<DbService<Post>>(Box::new(post_client))
        .data::<DbService<Sermon>>(Box::new(sermon_client))
        .data::<AuthService>(Box::new(auth_client))
        .data::<ObjectService>(Box::new(s3_client))
        .data::<CtService>(ct_service)
        .data::<HttpService>(Arc::new(HttpClientImpl::new()))
        .data::<CacheService>(Box::new(CacheClientImpl::new(cache_client)))
        .data::<MailService>(Box::new(MailClientImpl::new(app_config.smtp())))
        // .extension(Tracing)
        .finish()
}

#[cfg(test)]
mod tests {
    use aws_config::BehaviorVersion;
    use secrecy::ExposeSecret;

    use super::init_schema;
    use crate::config::AppConfig;

    #[tokio::test]
    async fn test_should_build_schema() {
        // given
        std::env::set_var("CT_USER", "user");
        std::env::set_var("CT_PW", "pw");
        std::env::set_var("TABLE_CONTENT", "content");
        std::env::set_var("TABLE_POST", "post");
        std::env::set_var("TABLE_SERMON", "sermon");
        std::env::set_var("TABLE_CACHE", "cache");
        std::env::set_var("SMTP_HOST", "host");
        std::env::set_var("SMTP_USER", "user");
        std::env::set_var("SMTP_MAIL", "mail");
        std::env::set_var("SMTP_PW", "pw");
        std::env::set_var("BUCKET_PUBLIC", "public-assets");

        // when
        let app_config = AppConfig::new();
        let aws_config = aws_config::load_defaults(BehaviorVersion::latest()).await;
        let schema = init_schema(&app_config, &aws_config).await;

        // then
        assert!(!schema.names().is_empty());
        assert_eq!("user", app_config.ct().user());
        assert_eq!("pw", app_config.ct().password().expose_secret());
        assert_eq!("content", app_config.db().content_table());
        assert_eq!("post", app_config.db().post_table());
        assert_eq!("sermon", app_config.db().sermon_table());
        assert_eq!("cache", app_config.db().cache_table());
        assert_eq!("host", app_config.smtp().host());
        assert_eq!("user", app_config.smtp().user());
        assert_eq!("mail", app_config.smtp().mail());
        assert_eq!("pw", app_config.smtp().password().expose_secret());
        assert_eq!("public-assets", app_config.objects().public_bucket());
    }
}
