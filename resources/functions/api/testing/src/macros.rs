/// Macro for generating mock db client implementation. Each method is separated with commas. Unmocked methods use unimplemented!().
#[macro_export]
macro_rules! create_db_mock {
    ($type:ident, $name:ident, $($i:item), *) => {{
        use async_trait::async_trait;
        use efg_db_client::db::DbClient;
        use efg_testing::call_collector::CallCollector;
        use efg_macros::track_mock_calls;

        #[derive(Clone)]
        struct $name {
            call_sender: std::sync::mpsc::SyncSender<Option<String>>,
        }

        #[async_trait]
        impl DbClient<$type> for $name {
            $(
                #[track_mock_calls]
                $i
            )*
        }

        let (call_sender, mut call_receiver) = std::sync::mpsc::sync_channel(1024);

        let mock_instance = $name {
            call_sender: call_sender.clone()
        };

        (mock_instance, CallCollector::new(call_receiver, call_sender))
    }};
}
