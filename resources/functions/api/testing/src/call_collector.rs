use std::collections::HashMap;

pub struct CallCollector {
    calls: HashMap<String, u32>,
    call_receiver: std::sync::mpsc::Receiver<Option<String>>,
    close_sender: std::sync::mpsc::SyncSender<Option<String>>,
}

impl CallCollector {
    pub fn new(
        call_receiver: std::sync::mpsc::Receiver<Option<String>>,
        close_sender: std::sync::mpsc::SyncSender<Option<String>>,
    ) -> Self {
        CallCollector {
            calls: HashMap::new(),
            call_receiver,
            close_sender,
        }
    }

    pub fn collect_calls(&mut self) {
        self.close_sender.send(None).unwrap();
        while let Ok(call) = self.call_receiver.recv() {
            match call {
                Some(call) => {
                    let new_count = self
                        .calls
                        .get(&call)
                        .map(|count| count + 1)
                        .unwrap_or_else(|| 1);
                    self.calls.insert(call.to_string(), new_count);
                }
                None => {
                    break;
                }
            }
        }
    }

    pub fn total_calls(&self) -> u32 {
        self.calls
            .values()
            .copied()
            .reduce(|a, b| a + b)
            .unwrap_or(0)
    }

    pub fn get_calls_by_method(&self, method: &str) -> u32 {
        *self.calls.get(method).unwrap_or(&0)
    }
}
