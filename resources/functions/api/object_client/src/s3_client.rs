use crate::db::ObjectClient;
use async_trait::async_trait;
use aws_config::SdkConfig;
use aws_sdk_s3::{primitives::ByteStream, types::StorageClass, Client};
use efg_lambda_base::errors::AppError;
use tracing::{debug_span, info, Instrument};

pub struct S3Client {
    client: Client,
    bucket_name: String,
}

impl S3Client {
    pub fn new(bucket_name: &str, aws_config: &SdkConfig) -> Self {
        let client = Client::new(aws_config);

        Self {
            bucket_name: bucket_name.to_string(),
            client,
        }
    }
}

#[async_trait]
impl ObjectClient for S3Client {
    /// Uploads a new object to the object storage.
    async fn upload(&self, key: &str, data: Vec<u8>, mime: &str) -> Result<(), AppError> {
        async {
            // make sure to get correct S3 paths
            if key.starts_with('/') {
                return Err(AppError::new("expected key without leading slash"));
            }

            let _result = self
                .client
                .put_object()
                .bucket(&self.bucket_name)
                .body(ByteStream::from(data))
                .key(key)
                .content_type(mime)
                // assets are cached via CloudFront so Standard-IA is enough
                .storage_class(StorageClass::StandardIa)
                // cache assets for one year via cloudfront and client
                .cache_control("public,max-age=31536000,immutable")
                .send()
                .await
                .map_err(|err| AppError::from_error(&err, "uploading file failed"))?;

            info!("uploaded item {} to S3 successfully", key);

            Ok(())
        }
        .instrument(debug_span!("upload", key = key))
        .await
    }

    /// Downloads a file by key.
    async fn download(&self, key: &str) -> Result<Vec<u8>, AppError> {
        async {
            let result = self
                .client
                .get_object()
                .bucket(&self.bucket_name)
                .key(key)
                .send()
                .await
                .map_err(|err| AppError::from_error(&err, "downloading file failed"))?;

            let bytes = result
                .body
                .collect()
                .await
                .map(|x| x.into_bytes().to_vec())
                .map_err(|err| AppError::from_error(&err, "reading file failed"))?;

            Ok(bytes)
        }
        .instrument(debug_span!("download", key = key))
        .await
    }

    /// Deletes an object by key.
    async fn delete(&self, key: &str) -> Result<(), AppError> {
        async {
            let _result = self
                .client
                .delete_object()
                .bucket(&self.bucket_name)
                .key(key)
                .send()
                .await
                .map_err(|err| AppError::from_error(&err, "downloading file failed"))?;

            info!("deleted item {} from S3 successfully", key);

            Ok(())
        }
        .instrument(debug_span!("delete", key = key))
        .await
    }
}
