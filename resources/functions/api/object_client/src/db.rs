use async_trait::async_trait;
use efg_lambda_base::errors::AppError;
pub type ObjectService = Box<dyn ObjectClient + Send + Sync>;

#[async_trait]
pub trait ObjectClient: Send + Sync {
    /// Uploads a new object to the object storage.
    async fn upload(&self, _key: &str, _data: Vec<u8>, _mime: &str) -> Result<(), AppError> {
        unimplemented!("upload")
    }

    /// Uploads a new object to the object storage.
    async fn download(&self, _key: &str) -> Result<Vec<u8>, AppError> {
        unimplemented!("download")
    }

    /// Deletes an object by key from the object storage.
    async fn delete(&self, _key: &str) -> Result<(), AppError> {
        unimplemented!("delete")
    }
}
