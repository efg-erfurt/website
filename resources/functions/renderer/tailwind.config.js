const { colors } = require("tailwindcss/defaultTheme");

const config = {
  content: ["./src/**/*.tsx"],
  safelist: [
    "focus:bg-efg-blue-bright",
    "hover:bg-efg-blue-bright",
    "group-hover:bg-efg-blue-bright",
    "focus:bg-efg-orange",
    "hover:bg-efg-orange",
    "group-hover:bg-efg-orange",
    "focus:bg-efg-yellow",
    "hover:bg-efg-yellow",
    "group-hover:bg-efg-yellow"
  ],
  theme: {
    extend: {
      colors: {
        "efg-yellow": "#fecb2f",
        "efg-blue-bright": "#6da9c0",
        "efg-blue-dark": "#1d5778",
        "efg-orange": "#ed6500",
        gray: {
          50: "#FAFAFA",
          100: "#F5F5F5",
          200: "#E6E6E6",
          300: "#D6D6D6",
          400: "#B8B8B8",
          500: "#999999",
          600: "#8A8A8A",
          700: "#5C5C5C",
          800: "#454545",
          900: "#2E2E2E"
        },
        blue: {
          ...colors.blue,
          50: "#e1e4e9"
        }
      },
      spacing: {
        48: "12rem",
        72: "18rem",
        84: "21rem",
        96: "24rem",
        144: "36rem",
        192: "48rem"
      }
    },
    fontFamily: {
      sans: ["OpenSans", "sans-serif"],
      serif: ["Vollkorn", "serif"]
    }
  },
  plugins: []
};

export default config;
