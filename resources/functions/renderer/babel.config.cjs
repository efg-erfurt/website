module.exports = {
  plugins: ["@loadable/babel-plugin"],
  presets: [
    [
      "@babel/preset-env",
      {
        useBuiltIns: undefined,
        corejs: false,
        targets: { node: "20" }
      }
    ],
    [
      "@babel/preset-react",
      {
        development: true
      }
    ],
    "@babel/preset-typescript"
  ]
};
