import path from "path";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import CssMinimizerPlugin from "css-minimizer-webpack-plugin";
import LoadablePlugin from "@loadable/webpack-plugin";
import TerserPlugin from "terser-webpack-plugin";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import webpack from "webpack";
import tailwind from "tailwindcss";
import nodeExternals from "webpack-node-externals";
import * as url from "url";

const __dirname = url.fileURLToPath(new URL(".", import.meta.url));

const production = process.env.NODE_ENV === "production";

console.log(
  `Building ${
    process.env.NODE_ENV === "production" ? "Production" : "Development"
  } build...`
);

const srcFolder = "src";
const distFolder = "dist";
const libFolder = "lib";

const createConfig = options => {
  const { target, source } = options;
  const hasNodeTarget = target === "node";

  return {
    name: target,
    target,
    mode: production ? "production" : "development",
    entry: path.resolve(__dirname, srcFolder, source),
    devtool: hasNodeTarget ? undefined : "source-map",
    externalsPresets: { node: true },
    externals: hasNodeTarget ? [nodeExternals()] : [],
    output: {
      path: hasNodeTarget
        ? path.resolve(__dirname, libFolder)
        : path.resolve(__dirname, distFolder),
      publicPath: production ? "/public/" : "http://localhost:3001/public/",
      filename: hasNodeTarget ? "app.js" : "app-[chunkhash:12].js",
      chunkFilename: "[chunkhash:12].js",
      libraryTarget: hasNodeTarget ? "commonjs2" : undefined
    },
    node: {
      __dirname: false
    },
    resolve: {
      extensions: [
        ".md",
        ".css",
        ".mjs",
        ".ts",
        ".tsx",
        ".js",
        ".json",
        ".woff2",
        ".png"
      ]
    },
    module: {
      rules: [
        {
          test: /\.md$/,
          type: "asset/source"
        },
        {
          test: /\.(ttf|eot|woff|woff2)$/,
          type: "asset/resource",
          generator: {
            filename: "fonts/[name].[ext]"
          }
        },
        {
          test: /\.png$/,
          type: "asset/resource",
          generator: {
            filename: "images/[name].[ext]"
          }
        },
        ...(hasNodeTarget
          ? [
              // ignore stylings and just export them as files
              {
                test: /(\.css|\.scss|\.sass)$/,
                type: "asset/resource"
              }
            ]
          : [
              {
                test: /(\.css|\.scss|\.sass)$/,
                use: [
                  {
                    loader: MiniCssExtractPlugin.loader
                  },
                  {
                    loader: "css-loader",
                    options: {
                      url: false,
                      importLoaders: 2
                    }
                  },
                  {
                    loader: "postcss-loader",
                    options: {
                      postcssOptions: {
                        ident: "postcss",
                        plugins: [
                          ["postcss-preset-env", { browsers: ">1.0% in DE" }],
                          tailwind("./tailwind.config.js")
                        ]
                      }
                    }
                  },
                  {
                    loader: "sass-loader",
                    options: {}
                  }
                ]
              }
            ]),
        {
          test: /\.ts(x?)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: "babel-loader?optional=runtime&cacheDirectory",
              options: {
                caller: { target },
                plugins: ["@loadable/babel-plugin"],
                presets: [
                  [
                    "@babel/preset-env",
                    {
                      useBuiltIns: hasNodeTarget ? undefined : "entry",
                      corejs: hasNodeTarget
                        ? false
                        : { version: 3, proposals: true },
                      targets: hasNodeTarget ? { node: "20" } : ">1.0% in DE"
                    }
                  ],
                  [
                    "@babel/preset-react",
                    {
                      development: !production
                    }
                  ]
                ]
              }
            },
            {
              loader: "ts-loader",
              options: {
                transpileOnly: true
              }
            }
          ]
        }
      ]
    },
    optimization: {
      minimize: production && !hasNodeTarget,
      minimizer: [new TerserPlugin(), new CssMinimizerPlugin()]
    },
    plugins: [
      new webpack.DefinePlugin({
        "process.env.WP_TARGET": `"${target}"`
      }),
      ...(hasNodeTarget
        ? [new ForkTsCheckerWebpackPlugin()]
        : [
            new MiniCssExtractPlugin({
              filename: "[contenthash].css",
              chunkFilename: "[contenthash].css"
            }),
            new LoadablePlugin()
          ])
    ]
  };
};

const config = [
  createConfig({ target: "web", source: "client.tsx" }),
  createConfig({ target: "node", source: "server.ts" })
];

export default config;
