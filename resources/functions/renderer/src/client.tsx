import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject
} from "@apollo/client";
import { ApolloLink, concat } from "@apollo/client/link/core";
import { BatchHttpLink } from "@apollo/client/link/batch-http";
import { loadableReady } from "@loadable/component";
import { BrowserRouter } from "react-router-dom";
import React, { useEffect } from "react";
import { hydrateRoot } from "react-dom/client";
import { HelmetProvider } from "react-helmet-async";
import { getToken } from "./client/components/auth";
import { App } from "./client/pages/App";

const createGQLClient = () => {
  const httpLink = new BatchHttpLink({
    uri:
      (window.location.hostname === "localhost"
        ? "http://localhost:3002"
        : `${window.location.protocol}//${window.location.host}`) + "/api/gql",
    batchInterval: 25,
    credentials: "same-origin"
  });
  const authLink = new ApolloLink((operation, forward) => {
    operation.setContext({
      headers: {
        authorization: `Bearer ${getToken()}`
      }
    });
    return forward(operation);
  });

  return new ApolloClient({
    cache: new InMemoryCache({
      resultCaching: true
    }).restore((window as any).__APOLLO_STATE__),
    link: concat(authLink, httpLink),
    assumeImmutableResults: true
  });
};

const AppWithCallbackAfterRender = ({
  client
}: {
  client: ApolloClient<NormalizedCacheObject>;
}) => {
  useEffect(() => {
    new Promise(async () => {
      if (getToken()) {
        await client.clearStore();
        await client.reFetchObservableQueries(true);
      }
    });
  });

  return (
    <HelmetProvider>
      <BrowserRouter>
        <App client={client} />
      </BrowserRouter>
    </HelmetProvider>
  );
};

try {
  loadableReady(async () => {
    const client = createGQLClient();
    hydrateRoot(
      document.getElementById("root")!,
      <AppWithCallbackAfterRender client={client} />
    );
  });
} catch (err) {
  console.error("SSR-Hydration has failed", err);
}
