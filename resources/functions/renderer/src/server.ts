import { logger } from "./server/logger";
import {
  APIGatewayProxyStructuredResultV2,
  APIGatewayProxyEventV2
} from "aws-lambda";
import { handleRouting } from "./server/router";

if (typeof window === "undefined") {
  (global as any).window = {};
}

export const handler = async (
  event: APIGatewayProxyEventV2
): Promise<APIGatewayProxyStructuredResultV2> => {
  logger.info(
    `received incoming event: ${JSON.stringify({
      method: event.requestContext.http.method,
      path: event.rawPath,
      queryParams: event.rawQueryString
    })}`
  );

  return await handleRouting(event);
};
