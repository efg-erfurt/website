import { beforeEach, describe, expect, it } from "@jest/globals";
import { handler } from "./server";
import { buildApiGwEvent } from "./utils/testing";

describe("Server", () => {
  beforeEach(() => {
    process.env.ORIGIN = "localhost";
  });

  [
    { given: { path: "/" }, expected: { statusCode: 301 } },
    {
      given: { path: "/cafe.htm" },
      expected: { statusCode: 301 }
    },
    {
      given: { path: "/de/cafe" },
      expected: { statusCode: 301 }
    },
    {
      given: { path: "/en/cafe" },
      expected: { statusCode: 301 }
    },
    {
      given: { path: "/de/prayers" },
      expected: { statusCode: 301 }
    },
    {
      given: { path: "/en/prayers" },
      expected: { statusCode: 301 }
    },
    {
      given: { path: "/de/posts" },
      expected: { statusCode: 301 }
    },
    {
      given: { path: "/de/posts/" },
      expected: { statusCode: 301 }
    },
    {
      given: { path: "/de/weihnachten-23" },
      expected: { statusCode: 303 }
    },
    {
      given: { path: "/de/anmeldung-gebet-23" },
      expected: { statusCode: 303 }
    },
    {
      given: { path: "/de/agw-24" },
      expected: { statusCode: 303 }
    }
  ].map(({ given, expected }) => {
    it(`should handle request to GET ${given.path} with redirect`, async () => {
      // given
      const event = buildApiGwEvent(given.path);

      // when
      const result = await handler(event);

      // then
      expect(result.statusCode).toBe(expected.statusCode);
      expect(result.body).toBe("");
      expect(result.headers).toMatchSnapshot();
    });
  });

  [
    {
      given: { path: "/de/not-found" },
      expected: { statusCode: 404 }
    },
    {
      given: { path: "/de/unknown-route" },
      expected: { statusCode: 404 }
    },
    {
      given: { path: "/abc" },
      expected: { statusCode: 404 }
    },
    {
      given: { path: "/de/" },
      expected: { statusCode: 200 }
    }
  ].map(({ given, expected }) => {
    it(`should handle request to GET ${given.path} with render`, async () => {
      // given
      const event = buildApiGwEvent(given.path);

      // when
      const result = await handler(event);

      // then
      expect(result.statusCode).toBe(expected.statusCode);
      expect(result.body).not.toBe("");
      expect(result.headers).toMatchSnapshot();
    });
  });
});
