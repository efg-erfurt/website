export enum RobotsValue {
  INDEX_FOLLOW = "index,follow",
  INDEX_NO_FOLLOW = "index,nofollow",
  NO_INDEX_NO_FOLLOW = "noindex,nofollow",
  NO_INDEX_FOLLOW = "noindex,follow"
}
