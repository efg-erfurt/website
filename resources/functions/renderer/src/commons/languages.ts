export const Languages: LanguageMap = {
  DE: {
    code: "de",
    label: "Deutsch",
    published: true
  },
  EN: {
    code: "en",
    label: "English",
    published: true
  }
};

export interface Language {
  code: string;
  label: string;
  published: boolean;
}

interface LanguageMap {
  [name: string]: Language;
}

export const languages = Object.values(Languages);
export const defaultLanguage = languages[0];
