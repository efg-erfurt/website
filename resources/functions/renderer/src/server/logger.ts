import pino from "pino";

const asyncDestination = pino.destination({
  sync: false
});

export const logger: pino.BaseLogger = pino(
  { level: "info" },
  asyncDestination
);

export const logError = (err: Error | null) => {
  if (!err) {
    logger.error("Unknown error happened");
  } else {
    logger.error(`${err.name}: ${err.message}`, err.stack);
  }
};
