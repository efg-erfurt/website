import { createContext } from "react";

export class RenderResult {
  notFound: boolean = false;

  public markNotFound(): void {
    this.notFound = true;
  }

  public isNotFound(): boolean {
    return this.notFound;
  }
}

export type RenderResultContext = RenderResult | null;

export const ResultContext = createContext<RenderResultContext>(null);
