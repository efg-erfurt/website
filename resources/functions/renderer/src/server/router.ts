import {
  APIGatewayProxyEventV2,
  APIGatewayProxyStructuredResultV2
} from "aws-lambda";
import { handleSpaRequest } from "./handlers/spa";

export const handleRouting = async (
  event: APIGatewayProxyEventV2
): Promise<APIGatewayProxyStructuredResultV2> => {
  // remove trailing slashes
  let resolvedPath = event.requestContext.http.path;
  if (resolvedPath.endsWith("/")) {
    resolvedPath = resolvedPath.slice(0, -1);
  }

  switch (resolvedPath) {
    case "":
      return redirectTo(
        301,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/de/`
      );
    case "/cafe.htm":
      return redirectTo(
        301,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/de/cafe`
      );
    case "/de/cafe":
      return redirectTo(
        301,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/de/event?id=36`
      );
    case "/en/cafe":
      return redirectTo(
        301,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/en/event?id=36`
      );
    case "/de/prayers":
      return redirectTo(
        301,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/de/event?id=53`
      );
    case "/en/prayers":
      return redirectTo(
        301,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/en/event?id=53`
      );
    case "/de/posts":
      return redirectTo(
        301,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/de/news`
      );
    case "/en/posts":
      return redirectTo(
        301,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/en/news`
      );
    case "/de/weihnachten-23":
      return redirectTo(
        302,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/public/flyers/weihnachten-23.pdf`
      );
    case "/de/anmeldung-gebet-23":
      return redirectTo(
        302,
        `https://www.gebetshaus-erfurt.de/gebetswoche2024`
      );
    case "/zeitung-82":
      return redirectTo(
        302,
        `https://efg-erfurt.church.tools/?q=public/filedownload&id=21816&filename=3cfa77501d6a5c8606a411151b2796697ee6bf8a6d8729805140e7a2369ede09`
      );
    case "/de/agw-24":
      return redirectTo(
        302,
        `${process.env.PROTOCOL}//${process.env.ORIGIN}/de/post?id=e082b991-85d6-4d6c-955f-89636efd4a2c`
      );
  }

  return handleSpaRequest(event);
};

const redirectTo = (
  status: number,
  url: string
): APIGatewayProxyStructuredResultV2 => ({
  statusCode: status,
  headers: {
    Location: url
  },
  body: ""
});
