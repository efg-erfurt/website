import { ChunkExtractor } from "@loadable/server";
import { ApolloClient } from "@apollo/client";

export const buildHeader = (
  helmetContext: any,
  chunkExtractor: ChunkExtractor,
  client: ApolloClient<Record<string, unknown>>,
  includeRefresh: boolean
) => {
  const { helmet } = helmetContext;

  return `<!DOCTYPE html>
<html ${helmet.htmlAttributes.toString()}>
<head>
${helmet.title.toString()}
${helmet.meta.toString()}<meta name="theme-color" content="#3367D6" />${
    includeRefresh ? `<meta http-equiv="refresh" content="3600" />` : ""
  }
${chunkExtractor.getLinkTags()}
<link rel="manifest" href="/public/manifest.json" />
<link rel="apple-touch-icon" sizes="180x180" href="/public/images/apple-touch-icon.png" />
<link rel="icon" type="image/png" sizes="32x32" href="/public/images/favicon-32x32.png" />
<link rel="icon" type="image/png" sizes="16x16" href="/public/images/favicon-16x16.png" />
<link rel="preload" type="font/woff2" href="/public/fonts/Vollkorn-SemiBold.woff2" as="font" crossOrigin />
<link rel="preload" type="font/woff2" href="/public/fonts/OpenSans-Regular.woff2" as="font" crossOrigin />
<link rel="preload" type="font/woff2" href="/public/fonts/OpenSans-Bold.woff2" as="font" crossOrigin />
${helmet.script.toString()}
${chunkExtractor.getStyleTags()}
<script>
window.__APOLLO_STATE__ = ${JSON.stringify(client.extract())};
</script>
</head>
<body>
<div id="root">`;
};

export const buildFooter = (chunkExtractor: ChunkExtractor) =>
  `</div>
  ${chunkExtractor.getScriptTags()}
</body>
</html>`;

export const generateErrorHtml = (
  errorCode: number
) => `<html><head><title>Unexpected Error occured</title></head>
<body>
<h1>We're really sorry!</h1>
<h3>An unexpected error occured ${errorCode}.</h3>
<p>If this error persists, please be so kind and contact somebody who is responsible for this website. Thanks a lot!</p>
</body></html>`;
