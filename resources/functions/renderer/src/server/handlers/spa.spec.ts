import { beforeEach, describe, expect, it } from "@jest/globals";
import { handleSpaRequest } from "./spa";
import { buildApiGwEvent } from "../../utils/testing";

describe("SPA", () => {
  beforeEach(() => {
    process.env.ORIGIN = "localhost";
  });

  it(`should render page for /de/ locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/events locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/events");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/sermons locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/sermons");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/contact locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/contact");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/groups locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/groups");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/event?id=10 locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/event", { queryParams: "id=10" });

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/posts locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/posts");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/news locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/news");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/search?q=test locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/search", { queryParams: "q=test" });

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/post?id=c0771357-8e30-43e9-b3e5-2357df862f08 locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/post", {
      queryParams: "id=c0771357-8e30-43e9-b3e5-2357df862f08"
    });

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/support locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/support");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/about-us locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/about-us");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/about-our-believe locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/about-our-believe");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/donate locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/donate");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/contribute locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/contribute");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });

  it(`should render page for /de/projects locally`, async () => {
    // given
    const event = buildApiGwEvent("/de/projects");

    // when
    const result = await handleSpaRequest(event);

    // then
    expect(result.statusCode).toBe(200);
    expect(result.body).toMatchSnapshot();
  });
});
