import React from "react";
import { resolve } from "path";
import { readFileSync } from "fs";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import { BatchHttpLink } from "@apollo/client/link/batch-http";
import { renderToStringWithData } from "@apollo/client/react/ssr";
import { ChunkExtractor } from "@loadable/server";
import { StaticRouter } from "react-router-dom/server";
import { HelmetProvider } from "react-helmet-async";
import { App } from "../../client/pages/App";
import { buildFooter, buildHeader } from "../html";
import fetch from "cross-fetch";
import { logger } from "../logger";
import { RenderResult, ResultContext } from "../renderresult";
import {
  APIGatewayProxyEventV2,
  APIGatewayProxyStructuredResultV2
} from "aws-lambda";
import { isJestTest } from "../../utils/testing";

const statsFile = isJestTest()
  ? resolve(__dirname, "../../utils/loadable-stats-test.json")
  : process.env.STAGE === "local"
  ? resolve(__dirname, "..", "dist", "loadable-stats.json")
  : resolve(__dirname, "loadable-stats.json");
const stats = JSON.parse(readFileSync(statsFile).toString("utf8"));

const getStatusCode = (url: string, renderResult: RenderResult) =>
  url.match(/(\/[a-z]{2}(\/)(not-found))$/g) || renderResult.isNotFound()
    ? 404
    : 200;

const renderJsxAsString = async (
  event: APIGatewayProxyEventV2
): Promise<{
  helmetContext: unknown;
  chunkExtractor: ChunkExtractor;
  html: string;
  client: ApolloClient<Record<string, unknown>>;
  renderResult: RenderResult;
}> => {
  const client = new ApolloClient({
    ssrMode: true,
    connectToDevTools: false,
    assumeImmutableResults: true,
    cache: new InMemoryCache(),
    link: new BatchHttpLink({
      uri:
        (process.env.STAGE === "local"
          ? "http://localhost:3002"
          : `${process.env.PROTOCOL}//${process.env.ORIGIN}`) + "/api/gql",
      batchInterval: 15,
      batchMax: 50,
      credentials: "same-origin",
      fetch
    })
  });
  const chunkExtractor = new ChunkExtractor({
    stats,
    publicPath:
      process.env.STAGE === "local" ? "http://localhost:3001/public" : undefined
  });

  const helmetContext = {};
  const renderResult = new RenderResult();
  const jsx = chunkExtractor.collectChunks(
    <ResultContext.Provider value={renderResult}>
      <HelmetProvider context={helmetContext}>
        <StaticRouter
          location={`${event.rawPath}?${event.rawQueryString}` || ""}
        >
          <App client={client} />
        </StaticRouter>
      </HelmetProvider>
    </ResultContext.Provider>
  );

  const html = await renderToStringWithData(jsx);

  logger.info("rendered static page successfully");

  return {
    helmetContext,
    chunkExtractor,
    client,
    html,
    renderResult
  };
};

export const handleSpaRequest = async (
  event: APIGatewayProxyEventV2
): Promise<APIGatewayProxyStructuredResultV2> => {
  try {
    const path = event.requestContext.http.path;

    const { helmetContext, html, chunkExtractor, client, renderResult } =
      await renderJsxAsString(event);

    const headers = {
      "WWW-Authenticate": "Basic",
      "Content-Type": "text/html; charset=utf-8",
      "cache-control": "max-age=1800",
      "Content-Security-Policy": `default-src 'self' 'unsafe-inline' 'unsafe-eval' ${
        process.env.STAGE === "local"
          ? "http://localhost:3001 http://localhost:3002"
          : ""
      } ${process?.env.PROTOCOL}//${
        process?.env.ORIGIN
      } https://efg-erfurt.church.tools data: wss: ws:;`,
      "X-Frame-Options": "SAMEORIGIN",
      "X-Content-Type-Options": "nosniff",
      "Referrer-Policy": "strict-origin",
      "Feature-Policy": "sync-xhr 'self';",
      "Strict-Transport-Security":
        "max-age=31536000; includeSubDomains; preload",
      "X-Robots-Tag": "noimageindex"
    };
    const statusCode = getStatusCode(path || "", renderResult);

    let body = buildHeader(
      helmetContext,
      chunkExtractor,
      client,
      path.includes("/info-screen")
    );
    body += html;
    body += buildFooter(chunkExtractor);

    return {
      headers,
      body,
      statusCode
    };
  } catch (error) {
    logger.error(`rendering failed: ${JSON.stringify(error)}`);
    throw error;
  }
};
