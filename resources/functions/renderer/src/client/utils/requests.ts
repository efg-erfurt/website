import { ApolloClient } from "@apollo/client";

export const fetchWithReload = async <T>({
  url,
  method,
  body,
  apolloClient,
  onError,
  onSuccessful
}: {
  url: string;
  method?: "POST" | "DELETE" | "PUT";
  body?: Record<string, unknown>;
  apolloClient: ApolloClient<Record<string, unknown>> | null;
  onSuccessful?: (response: T) => Promise<void>;
  onError?: (error: Error) => void;
}) => {
  try {
    const res = await fetch(url, {
      method: method || "POST",
      body: body ? JSON.stringify(body) : undefined
    });
    if (res.ok && onSuccessful) {
      const response = await res.json();
      await onSuccessful(response);
      if (apolloClient) {
        await apolloClient.reFetchObservableQueries();
      }
    } else {
      if (onError) {
        onError(new Error("No positive answer"));
      }
    }
  } catch (err) {
    if (onError) {
      console.error(err);
      onError(err as Error);
    }
  }
};
