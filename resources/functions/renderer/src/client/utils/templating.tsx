import React from "react";
import { render as renderTemplate } from "squirrelly";

export const render = <T extends Record<string, unknown>>(
  template: string,
  data: T
): JSX.Element => {
  try {
    return <>{renderTemplate(template, data)}</>;
  } catch (err) {
    return <>Fehler: {template}</>;
  }
};
