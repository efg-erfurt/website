import React, { useState } from "react";
import type { FieldRenderProps } from "react-final-form";
import { Form } from "react-bulma-components";
import ReactMde, { Command, ExecuteOptions } from "react-mde";
import { ContentQuery, useContentsQuery } from "../i18n/ContentQuery";
import { Dialog } from "../layout/dialog/Dialog";
import { Icon } from "../media/Icon";
import { Markdown } from "../text/Markdown";
import { FileInputDialog } from "./FileInputDialog";

if (process.env.WP_TARGET === "web" || process.env.WP_TARGET === `"web"`) {
  require("./markdown-input.scss");
}

export interface MarkdownInputFieldProps extends FieldRenderProps<string> {
  label?: string;
}

export const MarkdownInputAdapter = ({
  input: { value, onChange },
  meta,
  label,
  required = false,
  id
}: MarkdownInputFieldProps) => (
  <Form.Field>
    {label && (
      <Form.Label htmlFor={id}>
        {label}
        {required ? "*" : ""}
      </Form.Label>
    )}
    <Form.Control>
      <MarkdownInput value={value} onChange={onChange} id={id} />
      {meta.error && meta.touched && (
        <Form.Help color="danger">{meta.error}</Form.Help>
      )}
    </Form.Control>
  </Form.Field>
);

interface MarkdownInputProps {
  id?: string;
  value: string;
  onChange: (content: string) => void;
}

const AdaptedTextarea = React.forwardRef<"textarea">((props, ref) =>
  !ref ? null : <Form.Textarea {...props} domRef={ref as any} />
);

export const MarkdownInput = ({ value, id, onChange }: MarkdownInputProps) => {
  const [selectedTab, setSelectedTab] = useState<"write" | "preview">("write");
  const [showHelp, setShowHelp] = useState(false);
  const [showUpload, setShowUpload] = useState<ExecuteOptions | null>(null);
  const {
    "components.editing.preview": preview,
    "components.editing.edit": write
  } = useContentsQuery([
    "components.editing.preview",
    "components.editing.edit"
  ]);
  const helpCommand: Command = {
    icon: () => <Icon ariaLabel="Hilfe" icon="question-circle" />,
    execute: () => setShowHelp(true)
  };
  const imageCommand: Command = {
    icon: () => <Icon ariaLabel="Bild hochladen" icon="image" />,
    execute: setShowUpload
  };

  return (
    <div>
      <Dialog
        header="components.editing.markdown.title"
        visible={showHelp}
        onClose={() => setShowHelp(false)}
      >
        <ContentQuery markdown property="components.editing.markdown.help" />
      </Dialog>
      <FileInputDialog
        visible={showUpload !== null}
        onClose={() => setShowUpload(null)}
        onSuccess={path => {
          showUpload?.textApi.replaceSelection(
            `![${showUpload.textApi.getState().selectedText}](${path})`
          );
          setShowUpload(null);
        }}
      />
      <ReactMde
        classes={{ textArea: `textarea-${id}` }}
        value={value}
        onChange={onChange}
        selectedTab={selectedTab}
        onTabChange={setSelectedTab}
        generateMarkdownPreview={async markdown => (
          <Markdown content={markdown} />
        )}
        l18n={{
          preview,
          write,
          uploadingImage: "Bild hochladen",
          pasteDropSelect: "Datei hochladen"
        }}
        minEditorHeight={400}
        maxEditorHeight={800}
        commands={{
          help: helpCommand,
          image: imageCommand
        }}
        toolbarCommands={[
          ["bold", "italic", "strikethrough"],
          ["header", "link", "quote", "image"],
          ["ordered-list", "unordered-list"],
          ["help"]
        ]}
        textAreaComponent={AdaptedTextarea}
      />
    </div>
  );
};
