import dayjs from "dayjs";
import React, { ChangeEvent } from "react";
import { Form } from "react-bulma-components";
import type { FieldRenderProps } from "react-final-form";

export type RawDate = {
  value: Date | null;
  raw: { day?: string; month?: string; year?: string };
};

const parseNumber = (value?: string) => {
  if (!value) {
    return null;
  }

  const parsed = Number.parseInt(value);
  if (Number.isNaN(parsed) || !Number.isSafeInteger(parsed)) {
    return null;
  }

  return parsed;
};

const tryParseRawDate = (date: RawDate): RawDate => {
  try {
    const parsedDay = parseNumber(date.raw.day);
    const parsedMonth = parseNumber(date.raw.month);
    const parsedYear = parseNumber(date.raw.year);
    if (!parsedDay || !parsedMonth || !parsedYear) {
      return { ...date, value: null };
    }

    return { ...date, value: new Date(parsedYear, parsedMonth - 1, parsedDay) };
  } catch (err) {
    return { ...date, value: null };
  }
};

export const DateInputAdapter = ({
  input: { value, onChange, ...otherProps },
  meta,
  label,
  required = false,
  id
}: FieldRenderProps<RawDate>) => {
  return (
    <Form.Field>
      {label && (
        <Form.Label htmlFor={id}>
          {label}
          {required ? "*" : ""}
        </Form.Label>
      )}
      <Form.Control>
        <div className="flex justify-between items-center">
          <div className="grid grid-flow-col gap-2 items-center">
            <Form.Input
              id={`${id}-day`}
              className="w-10"
              value={value.raw.day}
              color={meta.error && meta.touched ? "danger" : undefined}
              onChange={(ev: ChangeEvent<HTMLInputElement>) => {
                onChange(
                  tryParseRawDate({
                    value: value.value,
                    raw: { ...value.raw, day: ev.target.value }
                  })
                );
              }}
              {...otherProps}
            />
            <div>.</div>
            <Form.Input
              id={`${id}-month`}
              className="w-10"
              value={value.raw.month}
              color={meta.error && meta.touched ? "danger" : undefined}
              onChange={(ev: ChangeEvent<HTMLInputElement>) => {
                onChange(
                  tryParseRawDate({
                    value: value.value,
                    raw: { ...value.raw, month: ev.target.value }
                  })
                );
              }}
              {...otherProps}
            />
            <div>.</div>
            <Form.Input
              id={`${id}-year`}
              className="w-16"
              value={value.raw.year}
              color={meta.error && meta.touched ? "danger" : undefined}
              onChange={(ev: ChangeEvent<HTMLInputElement>) => {
                onChange(
                  tryParseRawDate({
                    value: value.value,
                    raw: { ...value.raw, year: ev.target.value }
                  })
                );
              }}
              {...otherProps}
            />
          </div>
          <div>{value.value ? dayjs(value.value).format("dddd, LL") : "-"}</div>
        </div>
        {meta.error && meta.touched && (
          <Form.Help color="danger">{meta.error}</Form.Help>
        )}
      </Form.Control>
    </Form.Field>
  );
};
