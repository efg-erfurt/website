import { NavigateFunction, useNavigate } from "react-router-dom";
import React, { useContext } from "react";
import { Field, FieldRenderProps, Form } from "react-final-form";
import { useContentQuery } from "../i18n/ContentQuery";
import { Language } from "../i18n/languages";
import { PageContext } from "../page/PageContext";
import { Form as BulmaForm } from "react-bulma-components";

interface SearchForm {
  q: string;
}

const onSubmit: (
  language: Language,
  navigate: NavigateFunction
) => (values: SearchForm) => Promise<void> =
  (language, navigate) => async values => {
    const destination = `/${language.code}/search?q=${encodeURIComponent(
      values.q
    )}`;
    navigate(destination);
  };

const ExtractedInputAdapter = ({
  input: { type: _type, onChange, ...otherInputProps },
  meta: _meta,
  ...rest
}: FieldRenderProps<string>) => (
  <BulmaForm.Input
    {...otherInputProps}
    {...rest}
    onChange={ev => onChange(ev.target.value)}
  />
);

export const Search = () => {
  const searchPlaceholder = useContentQuery("components.search.title");
  const { language } = useContext(PageContext);
  const navigate = useNavigate();
  return (
    <Form
      onSubmit={onSubmit(language, navigate)}
      render={({ handleSubmit }) => (
        <form
          className="inline"
          onSubmit={handleSubmit}
          action={`/${language.code}/search`}
        >
          <Field<string>
            id="search"
            name="q"
            title={searchPlaceholder}
            component={ExtractedInputAdapter}
            type="text"
            placeholder={searchPlaceholder}
          />
        </form>
      )}
    />
  );
};
