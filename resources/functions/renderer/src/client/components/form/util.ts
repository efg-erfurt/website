export type FormErrorsMap<T> = Partial<{ [P in keyof T]?: string }>;
