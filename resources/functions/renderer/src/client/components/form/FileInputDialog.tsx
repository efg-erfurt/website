import React, { useState } from "react";
import { Field, Form } from "react-final-form";
import { getToken } from "../auth";
import { Dialog } from "../layout/dialog/Dialog";
import { Spinner } from "../media/Spinner";
import { Button } from "./Button";
import { InputAdapter } from "./Input";
import type { FormErrorsMap } from "./util";
import { Message } from "../errors/Message";
import { FileInput } from "./FileInput";

if (process.env.WP_TARGET === "web" || process.env.WP_TARGET === `"web"`) {
  require("./markdown-input.scss");
}

type ImageUrlForm = {
  imageUrl: string;
};

type UploadResult = { path?: string; error?: string; message?: string };

const uploadFile = async (file: File | null): Promise<UploadResult> => {
  if (!file) {
    return {
      error: "Fehler",
      message: "Es wurde keine Datei ausgewählt"
    };
  }

  const body = new FormData();
  body.append("data", file);

  try {
    const headers = new Headers({
      authorization: `Basic ${getToken()}`
    });
    const res = await fetch("/api/files", {
      mode: "same-origin",
      credentials: "same-origin",
      method: "POST",
      body,
      headers
    });
    if (res.ok) {
      const result = await res.json();
      return {
        path: result.path
      };
    } else {
      return await res.json();
    }
  } catch (err) {
    console.error(err);
    return {
      error: "Fehler",
      message: "Es ist ein unbekannter Fehler aufgetreten"
    };
  }
};

export const FileInputDialog = ({
  initialValue,
  visible,
  onClose,
  onSuccess
}: {
  initialValue?: string;
  visible: boolean;
  onSuccess: (path: string) => void;
  onClose: () => void;
}) => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [uploadInProgress, setUploadInProgress] = useState(false);
  const [uploadResult, setUploadResult] = useState<UploadResult>({});
  const handleUpload = async () => {
    setUploadInProgress(true);
    const result = await uploadFile(selectedFile);
    setUploadResult(result);
    setUploadInProgress(false);
    if (result.path) {
      onSuccess(result.path);
    }
  };
  const onImageUrlSubmit: (form: { imageUrl: string }) => void = ({
    imageUrl
  }) => onSuccess(imageUrl);

  return (
    <Dialog
      header="components.editing.image.title"
      visible={visible}
      onClose={onClose}
    >
      <div className="flex justify-evenly">
        <Form<{ imageUrl: string }>
          onSubmit={onImageUrlSubmit}
          initialValues={{ imageUrl: initialValue || "" }}
          validate={values => {
            const errors: FormErrorsMap<ImageUrlForm> = {};
            if (!values.imageUrl) {
              errors.imageUrl = "Keine URL angegeben";
            }
            return errors;
          }}
          render={({ handleSubmit, submitting, pristine }) => (
            <div className="inline-grid grid-flow-row gap-2">
              <Field label="URL" name="imageUrl" component={InputAdapter} />
              <div>
                <Button
                  onClick={() => {
                    handleSubmit();
                  }}
                  color="primary"
                  loading={submitting}
                  disabled={pristine}
                >
                  Einfügen
                </Button>
              </div>
            </div>
          )}
        />
        <div className="self-center">oder</div>
        <div className="inline-grid grid-flow-row gap-4">
          {!uploadInProgress && !!uploadResult.error && (
            <Message type="error" title={uploadResult.error}>
              {uploadResult.message}
            </Message>
          )}
          {uploadInProgress ? (
            <Spinner />
          ) : (
            <FileInput
              label="Bild auswählen"
              accept="image/png, image/jpeg"
              onChange={setSelectedFile}
            />
          )}
          <div>
            <Button color="primary" onClick={() => handleUpload()}>
              Hochladen
            </Button>
          </div>
        </div>
      </div>
    </Dialog>
  );
};
