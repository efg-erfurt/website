import React from "react";
import { Form } from "react-bulma-components";
import { Icon } from "../media/Icon";

type FileInputFieldProps = Partial<HTMLInputElement> & {
  label?: string;
  onChange: (file: File | null) => void;
};

export const FileInput = ({
  label,
  id,
  onChange,
  required = false
}: FileInputFieldProps) => (
  <Form.Field>
    {label && (
      <Form.Label htmlFor={id}>
        {label}
        {required ? "*" : ""}
      </Form.Label>
    )}
    <Form.Control>
      <Form.InputFile
        id={id}
        icon={<Icon ariaLabel="Hochladen" icon="upload" />}
        boxed
        onChange={ev => {
          onChange(ev.target.files?.item(0) || null);
        }}
      />
    </Form.Control>
  </Form.Field>
);
