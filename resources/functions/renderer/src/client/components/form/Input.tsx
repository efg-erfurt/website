import type { IconProp } from "@fortawesome/fontawesome-svg-core";
import React, { InputHTMLAttributes } from "react";
import { Form, Icon as BulmaIcon } from "react-bulma-components";
import type { FieldMetaState, FieldRenderProps } from "react-final-form";
import { Icon } from "../media/Icon";

interface InputProps<E extends HTMLInputElement = HTMLInputElement>
  extends FieldMetaState<string>,
    InputHTMLAttributes<E> {
  fullWidth?: boolean;
}

export interface InputFieldProps extends FieldRenderProps<string>, InputProps {
  label?: string;
  icon?: IconProp;
}

export const InputAdapter = ({
  input: { type, onChange, ...otherInputProps },
  meta,
  fullWidth: _fullWidth,
  label,
  icon,
  required = false,
  id
}: InputFieldProps) => (
  <Form.Field>
    {label && (
      <Form.Label htmlFor={id}>
        {label}
        {required ? "*" : ""}
      </Form.Label>
    )}
    <Form.Control>
      <Form.Input
        id={id}
        type={type}
        color={meta.error && meta.touched ? "danger" : undefined}
        {...otherInputProps}
        onChange={ev => onChange(ev.target.value)}
      />
      {meta.error && meta.touched && (
        <Form.Help color="danger">{meta.error}</Form.Help>
      )}
      {icon && (
        <BulmaIcon align="left">
          <Icon icon={icon} />
        </BulmaIcon>
      )}
    </Form.Control>
  </Form.Field>
);
