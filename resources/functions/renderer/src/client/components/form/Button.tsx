import React, {
  ButtonHTMLAttributes,
  useCallback,
  useState,
  MouseEvent,
  PropsWithChildren
} from "react";
import { Spinner } from "../media/Spinner";
import { Button as BulmaButton } from "react-bulma-components";

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  onClick?: (ev: MouseEvent<HTMLButtonElement>) => void | Promise<void>;
  loading?: boolean;
  background?: "transparent" | string;
}

export const Button = (props: ButtonProps) => {
  const [isLoading, setLoading] = useState(false);
  const {
    onClick,
    children,
    loading,
    disabled,
    inputMode: _inputMode,
    unselectable: _unselectable,
    background,
    type,
    ...otherProps
  } = props;

  const extendedOnClick = useCallback(
    (ev: any) => {
      const loadAsync = async () => {
        if (!onClick) {
          return;
        }

        setLoading(true);
        await onClick(ev);
        setLoading(false);
      };

      loadAsync();
    },
    [onClick]
  );

  return isLoading || loading ? (
    <BulmaButton {...otherProps} disabled>
      <Spinner />
    </BulmaButton>
  ) : (
    <BulmaButton
      type={type || "button"}
      onClick={props.onClick ? extendedOnClick : undefined}
      disabled={disabled}
      outlined={background === "transparent"}
      rounded={background === "transparent"}
      {...otherProps}
    >
      {children}
    </BulmaButton>
  );
};

export const ButtonGroup = ({ children }: PropsWithChildren<unknown>) => (
  <BulmaButton.Group hasAddons>{children}</BulmaButton.Group>
);

export const ActionsToolbar = ({ children }: PropsWithChildren<unknown>) => (
  <div className="flex justify-between items-start mb-8">{children}</div>
);
