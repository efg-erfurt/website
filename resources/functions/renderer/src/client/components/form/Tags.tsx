import React, { ChangeEvent, useState } from "react";
import { Form } from "react-bulma-components";
import type { FieldRenderProps } from "react-final-form";
import { Icon } from "../media/Icon";
import { Tag } from "../text/Tag";

export interface TagsFieldProps extends FieldRenderProps<Array<string>> {
  label?: string;
}

export const TagsInputAdapter = ({
  input: { value, onChange },
  label,
  required = false,
  id
}: TagsFieldProps) => (
  <Form.Field>
    {label && (
      <Form.Label htmlFor={id}>
        {label}
        {required ? "*" : ""}
      </Form.Label>
    )}
    <Form.Control>
      <EditableTags
        currentItems={value || []}
        onChange={items => onChange(items)}
      />
    </Form.Control>
  </Form.Field>
);

const EditableTags = ({
  currentItems,
  onChange
}: {
  currentItems: Array<string>;
  onChange: (items: Array<string>) => void;
}) => {
  const [newTag, setNewTag] = useState("");
  return (
    <div className="inline-grid grid-flow-col items-center gap-1">
      {currentItems.map((i, tIndex) => (
        <Tag key={`t-${tIndex}`}>
          {i}
          {onChange && (
            <>
              {" "}
              <Icon
                ariaLabel="Labels"
                icon="times"
                onClick={() => onChange(currentItems.filter(n => n !== i))}
              />
            </>
          )}
        </Tag>
      ))}
      {onChange && (
        <>
          <Form.Input
            id="tag-input"
            style={{ width: "4rem" }}
            className="is-small"
            value={newTag}
            onChange={(ev: ChangeEvent<HTMLInputElement>) =>
              setNewTag(ev.target.value)
            }
          />
          <Icon
            ariaLabel="Label hinzufügen"
            id="add-tag"
            icon="plus"
            onClick={() => {
              onChange(
                currentItems.findIndex(n => newTag === n) > -1
                  ? currentItems
                  : [...currentItems, newTag]
              );
              setNewTag("");
            }}
          />
        </>
      )}
    </div>
  );
};
