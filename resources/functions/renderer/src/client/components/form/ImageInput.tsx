import React, { useState } from "react";
import { Form } from "react-bulma-components";
import type { FieldRenderProps } from "react-final-form";
import { Button } from "./Button";
import { FileInputDialog } from "./FileInputDialog";

export const ImageInputAdapter = ({
  input: { value, onChange },
  meta,
  label,
  id
}: FieldRenderProps<string>) => {
  const [showFileDialog, setShowFileDialog] = useState(false);
  return (
    <Form.Field>
      {label && <Form.Label htmlFor={id}>{label}</Form.Label>}
      <Form.Control>
        <div className="flex flex-row items-center">
          <div className="w-20 rounded mr-2">
            {!value ? (
              <div className="bg-gray-700 rounded h-full w-full" />
            ) : (
              <img
                className="rounded h-full w-full shadow-xs"
                src={value}
                alt="Vorschau"
              />
            )}
          </div>
          <FileInputDialog
            initialValue={value}
            visible={showFileDialog}
            onSuccess={path => {
              onChange(path);
              setShowFileDialog(false);
            }}
            onClose={() => setShowFileDialog(false)}
          />
          <Button onClick={() => setShowFileDialog(true)}>Ändern</Button>
        </div>
        {meta.error && meta.touched && (
          <Form.Help color="danger">{meta.error}</Form.Help>
        )}
      </Form.Control>
    </Form.Field>
  );
};
