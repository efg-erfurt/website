import type { IconProp } from "@fortawesome/fontawesome-svg-core";
import React, { InputHTMLAttributes } from "react";
import { Form, Icon as BulmaIcon } from "react-bulma-components";
import type { FieldMetaState, FieldRenderProps } from "react-final-form";
import { Icon } from "../media/Icon";

interface TextareaProps<E extends HTMLTextAreaElement = HTMLTextAreaElement>
  extends FieldMetaState<string>,
    InputHTMLAttributes<E> {
  fullWidth?: boolean;
}

export interface TextareaAdapterFieldProps
  extends FieldRenderProps<string>,
    TextareaProps {
  label?: string;
  icon?: IconProp;
}

export const TextareaAdapter = ({
  input: { onChange, ...otherInputProps },
  meta,
  fullWidth: _fullWidth,
  label,
  icon,
  required = false,
  id
}: TextareaAdapterFieldProps) => (
  <Form.Field>
    {label && (
      <Form.Label htmlFor={id}>
        {label}
        {required ? "*" : ""}
      </Form.Label>
    )}
    <Form.Control>
      <Form.Textarea
        id={id}
        color={meta.error && meta.touched ? "danger" : undefined}
        {...otherInputProps}
        onChange={ev => onChange(ev.target.value)}
      />
      {meta.error && meta.touched && (
        <Form.Help color="danger">{meta.error}</Form.Help>
      )}
      {icon && (
        <BulmaIcon align="left">
          <Icon icon={icon} />
        </BulmaIcon>
      )}
    </Form.Control>
  </Form.Field>
);
