import React from "react";
import { Field, Form } from "react-final-form";
import { Button, ButtonGroup } from "../form/Button";
import { InputAdapter } from "../form/Input";
import { EditorProps, PropertyEditorProps } from "./GenericPropertyEditor";
import { Icon } from "../media/Icon";
import { useContentQuery } from "../i18n/ContentQuery";
import { ImageInputAdapter } from "../form/ImageInput";

interface ValueForm {
  content: string;
}

type PropertyPlainEditorProps = Omit<EditorProps, "initialValue"> &
  PropertyEditorProps;

export const PropertyPlainEditor = ({
  property,
  discardHandler,
  saveHandler
}: PropertyPlainEditorProps) => {
  const onSubmit: (form: ValueForm) => void = ({ content }) =>
    saveHandler(content);
  const initialValue = useContentQuery(property);
  return (
    <Form<ValueForm>
      onSubmit={onSubmit}
      initialValues={{ content: initialValue }}
      render={({ handleSubmit, submitting, pristine }) => (
        <form onSubmit={handleSubmit}>
          <div className="inline-grid grid-flow-col gap-2">
            <div>
              {property.endsWith("imgUrl") ? (
                <Field<string>
                  name="content"
                  component={ImageInputAdapter}
                  fullWidth
                />
              ) : (
                <Field name="content" component={InputAdapter} />
              )}
            </div>
            <ButtonGroup>
              {!submitting && (
                <Button onClick={discardHandler}>
                  <Icon ariaLabel="Abbrechen" icon="times" />
                </Button>
              )}
              <Button
                color="primary"
                type="submit"
                loading={submitting}
                disabled={pristine}
              >
                <Icon ariaLabel="Speichern" icon="save" />
              </Button>
            </ButtonGroup>
          </div>
        </form>
      )}
    />
  );
};
