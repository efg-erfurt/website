import { gql, useMutation } from "@apollo/client";
import React from "react";
import { getContentFn } from "../i18n/ContentQuery";
import { Language } from "../i18n/languages";
import { PropertyPlainEditor } from "./PropertyPlainEditor";
import { PropertyMarkdownEditor } from "./PropertyMarkdownEditor";
import {
  GetContentQueryVariables,
  Language as GQLLanguage,
  UpdateContentMutation,
  UpdateContentMutationVariables
} from "../../../types/generated";

const updateContentFn = gql`
  mutation updateContent(
    $key: String!
    $content: String!
    $language: Language!
  ) {
    updateContent(key: $key, content: $content, language: $language)
  }
`;

export interface EditorProps {
  initialValue?: string;
  discardHandler: () => void;
  saveHandler: (newContent: string) => void;
}

export interface PropertyEditorProps {
  language: Language;
  property: string;
}

type GenericPropertyEditorProps = PropertyEditorProps & {
  markdown: boolean;
  setInEditmode: (newValue: boolean) => void;
};

const GenericPropertyEditor = ({
  language,
  property,
  setInEditmode,
  markdown
}: GenericPropertyEditorProps) => {
  const [updateContent] = useMutation<
    UpdateContentMutation,
    UpdateContentMutationVariables
  >(updateContentFn, {
    awaitRefetchQueries: true,
    refetchQueries: [
      {
        query: getContentFn,
        variables: {
          key: property,
          language:
            language.code === "de" ? GQLLanguage.German : GQLLanguage.English
        } as GetContentQueryVariables
      }
    ]
  });

  const handleSave = async (content: string) => {
    await updateContent({
      variables: {
        content,
        key: property,
        language:
          language.code === "de" ? GQLLanguage.German : GQLLanguage.English
      }
    });
    setInEditmode(false);
  };

  const handleDiscard = () => setInEditmode(false);

  return markdown ? (
    <PropertyMarkdownEditor
      property={property}
      language={language}
      saveHandler={handleSave}
      discardHandler={handleDiscard}
    />
  ) : (
    <PropertyPlainEditor
      property={property}
      language={language}
      saveHandler={handleSave}
      discardHandler={handleDiscard}
    />
  );
};

export default GenericPropertyEditor;
