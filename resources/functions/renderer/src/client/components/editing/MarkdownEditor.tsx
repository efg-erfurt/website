import React, { useState } from "react";
import { EditorProps } from "./GenericPropertyEditor";
import { ContentQuery, useContentsQuery } from "../i18n/ContentQuery";
import { Button, ButtonGroup, ActionsToolbar } from "../form/Button";
import { MarkdownInput } from "../form/MarkdownInput";

type MarkdownEditorProps = EditorProps & {
  customActions?: Array<{
    property: string;
    onClick: (value: string) => void | Promise<void>;
  }>;
};

export const MarkdownEditor = ({
  customActions = [],
  initialValue,
  saveHandler,
  discardHandler
}: MarkdownEditorProps) => {
  const [value, setValue] = useState(initialValue);
  const {
    "components.editing.save": saveAction,
    "components.editing.discard": discardAction
  } = useContentsQuery([
    "components.editing.save",
    "components.editing.discard"
  ]);
  return (
    <div className="grid gap-2">
      <MarkdownInput onChange={setValue} value={value || ""} />
      <ActionsToolbar>
        <ButtonGroup>
          {customActions.map(({ property, onClick }, i) => (
            <Button
              key={`mde-action-${i}`}
              onClick={() => onClick(value || "")}
            >
              <ContentQuery readonly property={property} />
            </Button>
          ))}
        </ButtonGroup>
        <ButtonGroup>
          <Button onClick={() => discardHandler()}>{discardAction}</Button>
          <Button color="primary" onClick={() => saveHandler(value || "")}>
            {saveAction}
          </Button>
        </ButtonGroup>
      </ActionsToolbar>
    </div>
  );
};
