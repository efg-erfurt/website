import React from "react";
import { EditorProps, PropertyEditorProps } from "./GenericPropertyEditor";
import { MarkdownEditor } from "./MarkdownEditor";
import { useContentQuery } from "../i18n/ContentQuery";

type PropertyMarkdownEditorProps = Omit<EditorProps, "initialValue"> &
  PropertyEditorProps;

export const PropertyMarkdownEditor = ({
  property,
  saveHandler,
  discardHandler
}: PropertyMarkdownEditorProps) => {
  const initialValue = useContentQuery(property);
  return (
    <MarkdownEditor
      initialValue={initialValue}
      saveHandler={saveHandler}
      discardHandler={discardHandler}
    />
  );
};
