import dayjs from "dayjs";
import React, { PropsWithChildren } from "react";
import { Post } from "../../../types/generated";
import { Link } from "../text/Link";
import { MarkdownPreview } from "../text/Markdown";
import { Paragraph } from "../text/Paragraph";
import { Tags } from "../text/Tag";

export const PostPreview = ({
  post,
  showSummary = false
}: {
  post: Post;
  showSummary?: boolean;
}) => (
  <PostHeader post={post} isSinglePost={false}>
    {showSummary && (
      <div className="my-2">
        {post.summary ? (
          <Paragraph>{post.summary}</Paragraph>
        ) : (
          <MarkdownPreview content={post.content} />
        )}
      </div>
    )}
    {showSummary && <Tags items={post.tags} toFn={t => `/news?tag=${t}`} />}
  </PostHeader>
);

export const PostHeader = ({
  children,
  isSinglePost,
  post
}: PropsWithChildren<{ post: Post; isSinglePost: boolean }>) => (
  <div className="flex">
    <div className="w-20 h-20 rounded mr-4">
      <Link to={`/post?id=${post.id}`}>
        {!post.imageUrl ? (
          <div className="bg-gray-700 rounded h-full w-full" />
        ) : (
          <img
            className="rounded h-full w-full shadow-xs"
            src={post.imageUrl}
            alt="Beitragsbild"
          />
        )}
      </Link>
    </div>
    <div className="flex flex-col flex-1">
      <div className="font-bold text-sm"></div>
      {isSinglePost ? (
        <h1 className="font-serif font-bold text-2xl">{post.title}</h1>
      ) : (
        <div className="font-normal text-lg">
          <Link to={`/post?id=${post.id}`}>{post.title}</Link>
        </div>
      )}
      <div className="text-sm">
        veröffentlicht am {dayjs(post.publishDate).format("DD.MM.YY")}
      </div>
      {children}
    </div>
  </div>
);
