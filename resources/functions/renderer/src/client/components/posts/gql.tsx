import { gql } from "@apollo/client";

export const getLatestPostsFn = gql`
  query getLatestPosts(
    $language: Language!
    $tag: String
    $pagination: PaginationInput!
  ) {
    latestPosts(language: $language, tag: $tag, pagination: $pagination) {
      content {
        id
        title
        publishDate
        summary
        content
        imageUrl
        tags
        date
        language
        creator {
          id
          firstName
          lastName
          name
        }
      }
      totalPages
      totalCount
      page
      count
    }
  }
`;
