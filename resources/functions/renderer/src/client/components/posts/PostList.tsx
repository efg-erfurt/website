import { useQuery } from "@apollo/client";
import React, { useContext } from "react";
import {
  GetLatestPostsQuery,
  GetLatestPostsQueryVariables,
  Post,
  Language as GQLLanguage
} from "../../../types/generated";
import { Button } from "../form/Button";
import { ContentQuery } from "../i18n/ContentQuery";
import { Language } from "../i18n/languages";
import { Spinner } from "../media/Spinner";
import { PageContext } from "../page/PageContext";
import { Link } from "../text/Link";
import { getLatestPostsFn } from "./gql";
import { PostPreview } from "./PostPreview";

const LatestPostsContent = ({ language }: { language: Language }) => {
  const { data, loading, error } = useQuery<
    GetLatestPostsQuery,
    GetLatestPostsQueryVariables
  >(getLatestPostsFn, {
    variables: {
      language: language.code ? GQLLanguage.German : GQLLanguage.English,
      pagination: { page: 0, size: 5 }
    }
  });

  if (loading) {
    return <Spinner />;
  } else if (error || !data) {
    return null;
  } else if (data.latestPosts.totalCount === 0) {
    return <ContentQuery markdown property="components.posts.empty" />;
  }

  return (
    <div className="grid gap-4">
      {data.latestPosts.content.map(p => (
        <PostPreview key={p.id} post={p} />
      ))}
    </div>
  );
};

export interface PostListProps {
  posts: Post[];
}

export const LatestPosts = () => {
  const { language } = useContext(PageContext);
  return (
    <>
      <LatestPostsContent language={language} />
      <Link to="/news">
        <Button className="w-full">Weitere Beiträge</Button>
      </Link>
    </>
  );
};
