import React, { useState } from "react";
import { gql, useQuery } from "@apollo/client";
import type { IconProp } from "@fortawesome/fontawesome-svg-core";
import { Link } from "../text/Link";
import { Spinner } from "../media/Spinner";
import { Error } from "../errors/Error";
import { Icon } from "../media/Icon";
import { Card, CardGroup } from "../layout/Card";
import { Button } from "../form/Button";
import { ContactHeader } from "../contacts/ContactInfo";
import {
  GetGroupsQuery,
  GetGroupsQueryVariables,
  Group
} from "../../../types/generated";
import { Markdown } from "../text/Markdown";

const getGroupsFn = gql`
  query getGroups($groupsId: String!) {
    groups(id: $groupsId) {
      id
      name
      note
      imageUrl
      place
      ages
      time
      day
      contactUrl
      leaders {
        id
        firstName
        lastName
        name
        imageUrl
      }
    }
  }
`;

type GroupCardsProps = {
  groupsId: string;
};

const InfoLine = ({
  icon,
  text,
  label
}: {
  icon: IconProp;
  text: string;
  label: string;
}) => (
  <div className="flex items-center">
    <div className="flex w-4 m-1">
      <Icon ariaLabel={label} icon={icon} color="#BBB" />
    </div>
    <div className="flex-1 text-xs">{text}</div>
  </div>
);

export const GroupCards = ({ groupsId }: GroupCardsProps) => {
  const { data, error, loading } = useQuery<
    GetGroupsQuery,
    GetGroupsQueryVariables
  >(getGroupsFn, {
    variables: { groupsId }
  });
  if (loading) {
    return <Spinner />;
  } else if (!data || error) {
    return <Error error={error} />;
  }

  return (
    <CardGroup>
      {data.groups.map(g => (
        <GroupCard key={`g-${g.id}`} groupsId={groupsId} group={g} />
      ))}
    </CardGroup>
  );
};

const GroupCard = ({
  group: {
    id,
    name,
    note,
    imageUrl,
    place,
    ages,
    time,
    day,
    leaders,
    contactUrl
  }
}: {
  groupsId: string;
  group: Group;
}) => {
  const [isOpen, setOpen] = useState(false);
  return (
    <Card title={name} margin={false}>
      {!!imageUrl && (
        <img
          src={`${imageUrl}?w=400&h=220&q=85`}
          alt="Vorschau"
          className="w-full"
        />
      )}
      <div className="grid m-2 gap-2 p-1">
        {note.length <= 300 ? (
          <p className="text-sm">{note}</p>
        ) : (
          <p className="text-sm leading-6">
            {isOpen ? (
              <Markdown content={note} />
            ) : (
              <>
                <Markdown content={`${note.slice(0, 297)}...`} />
                <Icon
                  onClick={() => setOpen(!isOpen)}
                  icon={isOpen ? "chevron-down" : "chevron-right"}
                  size="1x"
                />
              </>
            )}
          </p>
        )}
        <div className="text-xs font-bold">Details</div>
        {time && !day && <InfoLine label="Zeit" icon="clock" text={time} />}
        {!time && day && <InfoLine label="Datum" icon="clock" text={day} />}
        {time && day && (
          <InfoLine
            label="Datum und Uhrzeit"
            icon="clock"
            text={`${day} | ${time}`}
          />
        )}
        {place && <InfoLine label="Ort" icon="map-marker-alt" text={place} />}
        {ages && <InfoLine label="Altersgruppe" icon="users" text={ages} />}
        {leaders.length > 0 && (
          <div className="mt-2">
            <ContactHeader />
            <div className="grid grid-cols-1 sm:grid-cols-2 gap-2 mt-2">
              {leaders.map(l => (
                <div
                  key={`g-${id}-${l.id}`}
                  className="text-xs flex flex-row items-center"
                >
                  <div className="font-bold bg-efg-blue-bright text-white flex items-center justify-center rounded-md shadow-sm w-12 h-12 mr-2">
                    {l.imageUrl ? (
                      <img
                        alt="Profilbild"
                        className="rounded-md"
                        src={`${l.imageUrl}?w=50&h=50&q=85`}
                      />
                    ) : (
                      <span>
                        {l.firstName?.slice(0, 1).toUpperCase()}
                        {l.lastName?.slice(0, 1).toUpperCase()}
                      </span>
                    )}
                  </div>
                  <div className="flex-1">
                    {l.firstName} {l.lastName}
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
        {contactUrl && (
          <Link showArrow={false} to={contactUrl}>
            <Button className="w-full">Kontaktieren</Button>
          </Link>
        )}
      </div>
    </Card>
  );
};
