import { gql, useApolloClient } from "@apollo/client";
import FullCalendar from "@fullcalendar/react";
import {
  EventInput,
  EventSourceFunc,
  EventSourceFuncArg
} from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import dayjs from "dayjs";
import React, { useContext, useState } from "react";
import { useCallbackRef } from "use-callback-ref";
import { getToday } from "../../../commons";
import { Icon } from "../media/Icon";
import { PageContext } from "../page/PageContext";
import { Heading } from "../text/Heading";
import {
  GetEventsWithMaxEntriesQuery,
  GetEventsWithMaxEntriesQueryVariables
} from "../../../types/generated";

export const getEventsFn = gql`
  query getEventsWithMaxEntries(
    $start: DateTime!
    $end: DateTime!
    $maxEntries: Int!
  ) {
    events(start: $start, end: $end, maxEntries: $maxEntries) {
      id
      name
      date
      place
      category {
        id
        name
        textColor
        backgroundColor
      }
    }
  }
`;

const getFullDayTimeframe = (day: Date) => {
  const start = new Date(day.getFullYear(), day.getMonth(), day.getDate());
  const end = new Date(day.getFullYear(), day.getMonth(), day.getDate() + 1);
  return { start, end };
};

const Calendar = ({
  timeframeSelected
}: {
  timeframeSelected?: (args: { start: Date; end: Date }) => void;
}) => {
  const {
    language: { code }
  } = useContext(PageContext);
  const [currentState, setState] = useState<{ date: Date }>({
    date: getToday()
  });
  const client = useApolloClient();
  const calendarRef = useCallbackRef<FullCalendar | null>(null, ref => {
    setState({ date: ref?.getApi().getDate() || getToday() });
  });
  const calendarApi =
    calendarRef && calendarRef.current ? calendarRef.current.getApi() : null;
  const eventsProvider: EventSourceFunc = async ({
    start,
    end
  }: EventSourceFuncArg) => {
    const { data, error, loading } = await client.query<
      GetEventsWithMaxEntriesQuery,
      GetEventsWithMaxEntriesQueryVariables
    >({
      query: getEventsFn,
      variables: {
        start: start.toISOString(),
        end: end.toISOString(),
        maxEntries: 100
      }
    });

    if (loading || error || !data) {
      return [];
    }

    return data.events.map<EventInput>(n => ({
      date: new Date(n.date),
      id: n.id,
      title: n.name,
      start: new Date(n.date),
      textColor: n.category?.textColor || "#000",
      backgroundColor: n.category?.backgroundColor || "#FFF",
      borderColor: n.category?.backgroundColor || "#FFF",
      groupId: n.category?.id || "unknown",
      url: n.category ? `/${code}/event?id=${n.category.id}` : undefined
    }));
  };

  return (
    <>
      <div className="grid grid-flow-col gap-2 justify-between items-center mb-4">
        <button
          className="px-2 rounded-sm bg-gray-200 hover:bg-gray-100 hover:text-black"
          onClick={() => {
            calendarApi?.prev();
            setState({ date: calendarApi?.getDate() || getToday() });
          }}
        >
          <Icon ariaLabel="Letzter Monat" id="prev-month" icon="chevron-left" />
        </button>
        <Heading size={4} id="current-month-label">
          {dayjs(currentState.date).format("MMMM YYYY")}
        </Heading>
        <button
          className="px-2 rounded-sm bg-gray-200 hover:bg-gray-100 hover:text-black"
          onClick={() => {
            calendarApi?.next();
            setState({ date: calendarApi?.getDate() || getToday() });
          }}
        >
          <Icon
            ariaLabel="Nächster Monat"
            id="next-month"
            icon="chevron-right"
          />
        </button>
      </div>
      <FullCalendar
        ref={calendarRef}
        weekNumbers
        locale={code}
        initialView="dayGridMonth"
        plugins={[dayGridPlugin]}
        events={eventsProvider}
        displayEventTime={false}
        navLinks={true}
        navLinkDayClick={day =>
          timeframeSelected ? timeframeSelected(getFullDayTimeframe(day)) : {}
        }
        navLinkWeekClick={weekStart =>
          timeframeSelected
            ? timeframeSelected({
                start: weekStart,
                end: dayjs(weekStart).add(1, "week").toDate()
              })
            : {}
        }
        selectable={true}
        selectAllow={() => true}
        headerToolbar={false}
      />
    </>
  );
};

export default Calendar;
