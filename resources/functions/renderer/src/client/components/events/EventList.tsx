import { gql, useQuery } from "@apollo/client";
import dayjs from "dayjs";
import React, { useState } from "react";
import { getToday } from "../../../commons";
import {
  Event,
  GetEventsQuery,
  GetEventsQueryVariables
} from "../../../types/generated";
import { Error } from "../errors/Error";
import { Button } from "../form/Button";
import { ContentQuery } from "../i18n/ContentQuery";
import { Icon } from "../media/Icon";
import { Spinner } from "../media/Spinner";
import { Link } from "../text/Link";
import { Markdown } from "../text/Markdown";

export const getEventsFn = gql`
  query getEvents($start: DateTime!, $end: DateTime) {
    events(start: $start, end: $end) {
      id
      name
      date
      place
      description
      category {
        id
        name
        backgroundColor
        textColor
      }
    }
  }
`;

const getEventsGroupyByDate = (events: Array<Event>) => {
  const eventsMap: Map<number, Array<Event>> = new Map();
  for (const e of events) {
    const time = dayjs(e.date)
      .hour(0)
      .minute(0)
      .second(0)
      .millisecond(0)
      .unix();
    if (!eventsMap.has(time)) {
      eventsMap.set(time, [e]);
    } else {
      eventsMap.set(time, [...eventsMap.get(time)!, e]);
    }
  }
  return eventsMap;
};

const EventDayItem = ({
  events,
  day
}: {
  events: Array<Event>;
  day: dayjs.Dayjs;
}) => (
  <div>
    <div className="font-bold text-sm">{dayjs(day).format("dddd, DD.MM.")}</div>
    <div className="grid gap-2 mt-1">
      {events.map(ev => (
        <EventItem key={`ev-${ev.id}`} event={ev} />
      ))}
    </div>
  </div>
);

const EventItem = ({
  event: { place, date, name, category, description }
}: {
  event: Event;
}) => {
  const [isOpen, setOpen] = useState(false);
  return (
    <div className="flex flex-row">
      {category && (
        <div className="w-1/12 flex justify-center pt-4 pr-2">
          {category.id === "10" ? (
            <Icon ariaLabel="Gottesdienst" icon="church" size="1x" />
          ) : category.id === "27" ? (
            <Icon ariaLabel="Junge Gemeinde" icon="child" size="1x" />
          ) : category.id === "51" || category.id === "33" ? (
            <Icon ariaLabel="Bibel" icon="bible" size="1x" />
          ) : category.id === "36" ? (
            <Icon ariaLabel="Café" icon="coffee" size="1x" />
          ) : category.id === "39" ? (
            <Icon ariaLabel="" icon="blind" size="1x" />
          ) : category.id === "53" ? (
            <Icon ariaLabel="Gebet" icon="praying-hands" size="1x" />
          ) : category.id === "61" ? (
            <Icon ariaLabel="Aktionen" icon="hand-holding-heart" size="1x" />
          ) : null}
        </div>
      )}
      <div className="flex-1">
        <span className="text-sm">
          {dayjs.utc(date).hour() === 0
            ? "ganztägig"
            : `${dayjs.utc(date).format("H:mm")} Uhr`}
        </span>
        <br />
        {category ? <Link to={`/event?id=${category.id}`}>{name}</Link> : name}
        {isOpen && (
          <div className="text-sm mt-2">
            {!!description && <Markdown content={description} />}
            <b>Ort:</b> {place}
          </div>
        )}
      </div>
      <div>
        <button
          className="p-2 hover:text-black"
          onClick={() => setOpen(!isOpen)}
        >
          <Icon
            ariaLabel="Details anzeigen"
            icon={isOpen ? "chevron-down" : "chevron-right"}
            size="1x"
            additionalClassName="px-2 py-1"
          />
        </button>
      </div>
    </div>
  );
};

export interface EventListProps {
  events: Event[];
}

export const EventList = ({ events }: EventListProps) => {
  const eventsMap = getEventsGroupyByDate(events);
  return (
    <div className="grid gap-4 max-w-xl">
      {Array.from(eventsMap.entries()).map(([time, events]) => (
        <EventDayItem key={time} events={events} day={dayjs.unix(time)} />
      ))}
    </div>
  );
};

export const EventsContent = ({ start, end }: { start?: Date; end?: Date }) => {
  const startOrNow = start || getToday();
  const endOrNextSevenDays =
    end || new Date(startOrNow.getTime() + 60 * 60 * 24 * 7 * 1000);
  const { data, loading, error } = useQuery<
    GetEventsQuery,
    GetEventsQueryVariables
  >(getEventsFn, {
    variables: {
      start: startOrNow.toISOString(),
      end: endOrNextSevenDays.toISOString()
    }
  });

  if (loading) {
    return <Spinner />;
  } else if (error) {
    return <Error error={error} />;
  } else if (!data || data.events.length === 0) {
    return <ContentQuery markdown property="components.events.empty" />;
  }

  return <EventList events={data.events} />;
};

export const UpcomingEvents = () => (
  <div>
    <EventsContent />
    <ContentQuery property="components.calendar.goto">
      {property => (
        <Link to="/events">
          <Button className="w-full mt-4">{property}</Button>
        </Link>
      )}
    </ContentQuery>
  </div>
);
