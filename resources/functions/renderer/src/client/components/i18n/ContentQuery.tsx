import { gql, useQuery } from "@apollo/client";
import loadable from "@loadable/component";
import React, { ConsumerProps, useContext, useState } from "react";
import {
  GetContentQuery,
  GetContentQueryVariables,
  GetContentsQuery,
  GetContentsQueryVariables,
  Language as GQLLanguage,
  User
} from "../../../types/generated";
import { Error } from "../errors/Error";
import { Icon } from "../media/Icon";
import { Spinner } from "../media/Spinner";
import { PageContext } from "../page/PageContext";
import { Markdown } from "../text/Markdown";
import { Language } from "./languages";

export const getContentFn = gql`
  query getContent($key: String!, $language: Language!) {
    content(language: $language, key: $key) {
      key
      content
    }
  }
`;

export const getContentsFn = gql`
  query getContents($keys: [String!]!, $language: Language!) {
    contents(language: $language, keys: $keys) {
      key
      content
    }
  }
`;

const GenericPropertyEditor = loadable(
  () => import("../editing/GenericPropertyEditor")
);

const EditableField = ({
  user,
  content,
  property,
  language,
  markdown,
  children
}: {
  user: User | null;
  markdown: boolean;
  language: Language;
  property: string;
  content: string;
} & ConsumerProps<string>) => {
  const [isInEditmode, setInEditmode] = useState(false);
  if (!user) {
    return (
      <>
        {markdown ? <Markdown content={content} /> : <>{children(content)}</>}
      </>
    );
  } else if (!isInEditmode) {
    return (
      <>
        {markdown ? (
          <div className="flex flex-row items-start border border-dashed rounded-lg p-2 hover:border-efg-orange">
            <div className="flex-1 max-w-full">
              <Markdown content={content} />
            </div>
            <div className="bg-gray-700 text-white flex p-2 rounded-md">
              <Icon icon="edit" onClick={() => setInEditmode(true)} />
            </div>
          </div>
        ) : (
          <div className="relative">
            <>{children(content)}</>
            <div className="absolute right-0 top-0 bg-gray-700 text-white flex p-2 rounded-md">
              <Icon icon="edit" onClick={() => setInEditmode(true)} />
            </div>
          </div>
        )}
      </>
    );
  }

  return (
    <GenericPropertyEditor
      setInEditmode={setInEditmode}
      language={language}
      markdown={markdown}
      property={property}
    />
  );
};

export interface ContentQueryProps extends Partial<ConsumerProps<string>> {
  property: string;
  markdown?: boolean;
  readonly?: boolean;
  forcedLanguage?: Language;
}

const ContentQueryWithLanguage = ({
  property,
  language,
  user,
  children = value => value,
  markdown = false,
  readonly = false
}: ContentQueryProps & {
  language: Language;
  user: User | null;
}) => {
  const { loading, error, data } = useQuery<
    GetContentQuery,
    GetContentQueryVariables
  >(getContentFn, {
    variables: {
      key: property,
      language:
        language.code === "de" ? GQLLanguage.German : GQLLanguage.English
    }
  });

  if (loading) {
    return <Spinner />;
  } else if (error || !data) {
    return <Error error={error} />;
  }

  if (readonly) {
    return <>{children(data.content?.content || "")}</>;
  }

  return (
    <EditableField
      markdown={markdown}
      language={language}
      property={property}
      content={data.content?.content || ""}
      user={user}
    >
      {children}
    </EditableField>
  );
};

export const ContentQuery = (props: ContentQueryProps) => {
  const { language, user } = useContext(PageContext);
  return (
    <ContentQueryWithLanguage
      {...{ user, language: props.forcedLanguage || language, ...props }}
    />
  );
};

export const ImageQuery = ({
  property,
  rounded = "full",
  alt = "Grafik"
}: {
  alt?: string;
  property: string;
  rounded?: "full" | "top";
}) => (
  <ContentQuery property={property}>
    {imgUrl =>
      imgUrl ? (
        <img
          src={imgUrl}
          alt={alt || "Illustration"}
          className={`${
            rounded === "full" ? "rounded" : "rounded-t"
          } h-full w-full`}
        />
      ) : (
        <div
          className={`${
            rounded === "full" ? "rounded" : "rounded-t"
          } h-full w-full bg-gray-700`}
        />
      )
    }
  </ContentQuery>
);

export const useContentQuery = (
  property: string,
  options?: { fallback?: string; forcedLanguage?: Language }
) => {
  const { language } = useContext(PageContext);

  const { loading, error, data } = useQuery<
    GetContentQuery,
    GetContentQueryVariables
  >(getContentFn, {
    variables: {
      key: property,
      language:
        (options?.forcedLanguage?.code || language.code) === "de"
          ? GQLLanguage.German
          : GQLLanguage.English
    }
  });

  if (loading || error || !data) {
    return options?.fallback || "";
  }

  return data.content?.content;
};

export const useContentsQuery = (properties: Array<string>) => {
  const { language } = useContext(PageContext);

  const { loading, error, data } = useQuery<
    GetContentsQuery,
    GetContentsQueryVariables
  >(getContentsFn, {
    variables: {
      keys: properties,
      language:
        language.code === "de" ? GQLLanguage.German : GQLLanguage.English
    }
  });

  const res: Record<string, string> = {};
  if (loading || error || !data) {
    properties.forEach(p => (res[p] = ""));
  } else {
    data.contents.forEach(c => (res[c.key] = c.content));
  }

  return res;
};
