import React, { ChangeEvent, useContext } from "react";
import { Form, Icon as BulmaIcon } from "react-bulma-components";
import { PageContext } from "../page/PageContext";
import { useContentQuery } from "./ContentQuery";
import { languages } from "./languages";
import { Icon } from "../media/Icon";
import { useNavigate, useSearchParams } from "react-router-dom";

export const LanguageSelect = () => {
  const { language, path, user } = useContext(PageContext);
  const langTitle = useContentQuery("components.i18n.selector", {
    fallback: "Language"
  });
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  return (
    <Form.Control>
      <BulmaIcon align="left">
        <Icon ariaLabel="Sprache" icon="globe-africa" />
      </BulmaIcon>
      <Form.Select
        id="language-select"
        title={langTitle}
        value={language.code}
        onChange={(ev: ChangeEvent<HTMLSelectElement>) => {
          navigate(
            `/${ev.target.value}/${path.substring(4)}${
              searchParams ? `?${searchParams.toString()}` : ""
            }`
          );
        }}
      >
        {languages
          .filter(n => (user === null ? n.published : true))
          .map(l => (
            <option key={l.code} value={l.code}>
              {l.label}
            </option>
          ))}
      </Form.Select>
    </Form.Control>
  );
};
