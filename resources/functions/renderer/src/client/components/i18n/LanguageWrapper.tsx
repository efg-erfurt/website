import { useQuery, gql } from "@apollo/client";
import dayjs from "dayjs";
import "dayjs/locale/de";
import React, { useContext } from "react";
import { Helmet } from "react-helmet-async";
import { PageContext } from "../page/PageContext";
import { getLanguage, Languages } from "./languages";
import {
  GetCurrentUserQuery,
  GetCurrentUserQueryVariables
} from "../../../types/generated";
import { Navigate, Outlet, useLocation, useParams } from "react-router-dom";
import { ResultContext } from "../../../server/renderresult";

export const getCurrentUserFn = gql`
  query getCurrentUser {
    currentUser {
      id
      firstName
      lastName
      name
      imageUrl
    }
  }
`;

export const LanguageWrapper = () => {
  const { lang } = useParams();
  const language = getLanguage(lang);
  const location = useLocation();
  const { data, error, loading } = useQuery<
    GetCurrentUserQuery,
    GetCurrentUserQueryVariables
  >(getCurrentUserFn);
  dayjs.locale(lang || Languages.DE.code);
  const result = useContext(ResultContext);

  const unknownLanguage =
    Object.values(Languages).find(({ code }) => code === lang) === undefined;
  if (unknownLanguage) {
    result?.markNotFound();
    return <Navigate to="/de/not-found" replace />;
  }

  return (
    <>
      <Helmet htmlAttributes={{ lang: language.code }}>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Helmet>
      <PageContext.Provider
        value={{
          origin: window?.location?.host || process?.env.ORIGIN || "",
          protocol:
            window?.location?.protocol || process?.env.PROTOCOL || "https:",
          language,
          path: location?.pathname || "",
          user: error || loading || !data ? null : data.currentUser || null
        }}
      >
        <Outlet />
      </PageContext.Provider>
    </>
  );
};
