import React, { PropsWithChildren } from "react";
import { Modal } from "react-bulma-components";
import { ContentQuery } from "../../i18n/ContentQuery";

export const Dialog = ({
  children,
  visible,
  onClose,
  header
}: PropsWithChildren<{
  visible: boolean;
  header: string;
  onClose: () => void;
}>) => {
  if (!visible) {
    return null;
  }

  return (
    <Modal show={visible} onClose={onClose}>
      <Modal.Content style={{ width: "90%" }}>
        <Modal.Card style={{ width: "100%" }}>
          <Modal.Card.Header onClose={onClose}>
            <Modal.Card.Title>
              <ContentQuery readonly property={header} />
            </Modal.Card.Title>
          </Modal.Card.Header>
          <Modal.Card.Body>{children}</Modal.Card.Body>
        </Modal.Card>
      </Modal.Content>
    </Modal>
  );
};
