import React, { useContext } from "react";
import { ContentQuery, useContentQuery } from "../i18n/ContentQuery";
import { PageContext } from "../page/PageContext";
import { Heading } from "../text/Heading";
import { NavigationTopElement } from "./Header";
import { Link } from "react-router-dom";

export const PageNavigation = ({
  element: { property, subElements }
}: {
  element: NavigationTopElement;
}) => {
  const pageName = useContentQuery(property);
  const { language } = useContext(PageContext);
  return (
    <div className="flex">
      <div className="w-1/3">
        <Heading>{pageName}</Heading>
        <div className="flex flex-col">
          {subElements.map(e => (
            <div key={`link-${e.property}`}>
              <ContentQuery property={e.property}>
                {val => (
                  <Link to={`/${language.code}${e.destination}`}>{val}</Link>
                )}
              </ContentQuery>
            </div>
          ))}
        </div>
      </div>
      <div className="w-2/3"></div>
    </div>
  );
};
