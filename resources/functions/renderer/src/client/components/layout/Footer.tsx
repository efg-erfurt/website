import { ApolloClient, useApolloClient } from "@apollo/client";
import React, { PropsWithChildren, useContext } from "react";
import { render } from "../../utils/templating";
import { logoutAndRefetch } from "../auth";
import { ContentQuery, useContentsQuery } from "../i18n/ContentQuery";
import { Icon } from "../media/Icon";
import { PageContext } from "../page/PageContext";
import { Link } from "../text/Link";
import { LanguageSelect } from "../i18n/LanguageSelect";
import { Image } from "../media/Image";

const FooterTitle = ({ children }: PropsWithChildren<unknown>) => (
  <div className="font-serif uppercase font-bold mb-2">{children}</div>
);

export const Footer = () => {
  const apolloClient = useApolloClient() as ApolloClient<
    Record<string, unknown>
  >;
  const { user } = useContext(PageContext);
  const {
    "general.socialMedia.facebook": facebookUrl,
    "general.socialMedia.instagram": instagramUrl,
    "general.socialMedia.whatsapp": whatsappUrl,
    "general.socialMedia.youtube": youtubeUrl
  } = useContentsQuery([
    "general.socialMedia.facebook",
    "general.socialMedia.instagram",
    "general.socialMedia.whatsapp",
    "general.socialMedia.youtube"
  ]);

  return (
    <div className="flex justify-center bg-gray-100 pt-6">
      <div className="container p-8">
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8">
          <div>
            <FooterTitle>
              <ContentQuery property="footer.columnA" />
            </FooterTitle>
            <div className="grid gap-2">
              <ContentQuery property="footer.address" markdown />
              <ContentQuery property="pages.journey.content" markdown />
            </div>
          </div>
          <div>
            <FooterTitle>
              <ContentQuery property="footer.columnB" />
            </FooterTitle>
            <div className="grid gap-4">
              <ContactItem
                name="Ralf-Detlef Ossa"
                imgSrc="ralf"
                mail="ossa@efg-erfurt.de"
                phone="0361/21844424"
                position="Pastor"
              />
              <ContactItem
                name="Lars Meininger"
                imgSrc="lars"
                mail="gemeindeleiter@efg-erfurt.de"
                phone="0361/5401548"
                position="Gemeindeleiter"
              />
              <ContactItem
                name="Mirjam Liedtke"
                imgSrc="mirjam"
                mail="buero@efg-erfurt.de"
                phone="0151/59151295"
                position="Gemeindesekretärin"
              />
            </div>
          </div>
          <div className="grid gap-4">
            <div>
              <FooterTitle>
                <ContentQuery property="footer.columnC" />
              </FooterTitle>
              <div className="grid gap-2">
                <ContentQuery property="pages.imprint.title">
                  {title => <Link to="/imprint">{title}</Link>}
                </ContentQuery>
                <ContentQuery property="pages.privacy.title">
                  {title => <Link to="/privacy">{title}</Link>}
                </ContentQuery>
                <ContentQuery property="pages.contact.title">
                  {title => <Link to="/contact">{title}</Link>}
                </ContentQuery>
                <Link to="https://efg-erfurt.church.tools/">ChurchTools</Link>
                {!!user && (
                  <ContentQuery property="pages.admin.title">
                    {title => <Link to="/admin">{title}</Link>}
                  </ContentQuery>
                )}
                <div>
                  {user ? (
                    <>
                      <ContentQuery property="footer.loginSuccess">
                        {value => (
                          <span className="text-sm">
                            {render(value, {
                              name: `${user.firstName} ${user.lastName}`
                            })}{" "}
                            <Icon
                              ariaLabel="Ausloggen"
                              size="1x"
                              onClick={() => logoutAndRefetch(apolloClient)}
                              icon="sign-out-alt"
                            />
                          </span>
                        )}
                      </ContentQuery>
                    </>
                  ) : (
                    <Link to="/login">Login</Link>
                  )}
                </div>
                <div>
                  {!!facebookUrl && (
                    <Link
                      showArrow={false}
                      to={facebookUrl}
                      aria-label="Facebook"
                      className="pr-2"
                    >
                      <Icon
                        ariaLabel="Facebook"
                        size="lg"
                        icon={["fab", "facebook"]}
                      />
                    </Link>
                  )}
                  {!!whatsappUrl && (
                    <Link
                      showArrow={false}
                      to={whatsappUrl}
                      aria-label="Whatsapp"
                      className="pr-2"
                    >
                      <Icon
                        ariaLabel="Whatsapp"
                        size="lg"
                        icon={["fab", "whatsapp"]}
                      />
                    </Link>
                  )}
                  {!!instagramUrl && (
                    <Link
                      showArrow={false}
                      to={instagramUrl}
                      aria-label="Instagram"
                      className="pr-2"
                    >
                      <Icon
                        ariaLabel="Instagram"
                        size="lg"
                        icon={["fab", "instagram"]}
                      />
                    </Link>
                  )}
                  {!!youtubeUrl && (
                    <Link
                      showArrow={false}
                      to={youtubeUrl}
                      aria-label="YouTube"
                      className="pr-2"
                    >
                      <Icon
                        ariaLabel="YouTube"
                        size="lg"
                        icon={["fab", "youtube"]}
                      />
                    </Link>
                  )}
                </div>
                <div className="hidden md:block">
                  <LanguageSelect />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-row justify-evenly items-center mt-4 text-sm">
          <p className="text-xs m-4">
            Icons made by{" "}
            <Link to="https://www.flaticon.com/authors/freepik" title="Freepik">
              Freepik
            </Link>{" "}
            from{" "}
            <Link to="https://www.flaticon.com/" title="Flaticon">
              www.flaticon.com
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

const ContactItem = ({
  name,
  position,
  phone,
  mail,
  imgSrc
}: {
  name: string;
  imgSrc: string;
  position: string;
  phone: string;
  mail: string;
}) => (
  <div className="flex">
    <Image srcType="jpg" src={imgSrc} className="w-12 rounded-md shadow-xs" />
    <div className="flex-1 text-xs ml-4">
      <span className="text-sm">{name}</span>
      <br />
      {position}
      <br />
      <Icon ariaLabel="Telefon" icon="phone" /> {phone}
      <br />
      <Link showArrow={false} to={`mailto:${mail}`}>
        <Icon ariaLabel="Email" icon="envelope" /> Kontakt
      </Link>
    </div>
  </div>
);
