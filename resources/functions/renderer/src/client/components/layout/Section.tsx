import React, { PropsWithChildren } from "react";

export const Section = ({ children }: PropsWithChildren<unknown>) => (
  <div className="p-2">{children}</div>
);
