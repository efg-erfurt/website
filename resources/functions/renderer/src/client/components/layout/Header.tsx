import React, { useState } from "react";
import { Search } from "../form/Search";
import { ContentQuery, useContentsQuery } from "../i18n/ContentQuery";
import { LanguageSelect } from "../i18n/LanguageSelect";
import { Icon } from "../media/Icon";
import { Logo } from "../media/Image";
import { Link } from "../text/Link";
import { AsyncBackground } from "./AsyncBackground";
import { CenteredContainer } from "./CenteredContent";

export const navigationStructure: NavigationStructure = {
  topElements: [
    {
      path: "/inform",
      color: "efg-orange",
      property: "navigation.inform",
      subElements: [
        {
          property: "pages.inform.mission.title",
          destination: "/about-us"
        },
        {
          property: "pages.inform.believe.title",
          destination: "/about-our-believe"
        },
        {
          property: "pages.news.title",
          destination: "/news"
        }
      ]
    },
    {
      path: "/participate",
      color: "efg-yellow",
      property: "navigation.participate",
      subElements: [
        {
          property: "pages.events.title",
          destination: "/events"
        },
        {
          property: "pages.events.gottesdienst.title",
          destination: "/event?id=10"
        },
        {
          property: "pages.participate.groups.title",
          destination: "/groups"
        },
        {
          property: "pages.participate.cafe.title",
          destination: "/cafe"
        }
      ]
    },
    {
      path: "/support",
      color: "efg-blue-bright",
      property: "navigation.support",
      subElements: [
        {
          property: "pages.support.donate.title",
          destination: "/donate"
        },
        {
          property: "pages.support.contribute.title",
          destination: "/contribute"
        },
        {
          property: "pages.support.prayer.title",
          destination: "/event?id=53"
        },
        {
          property: "pages.support.projects.title",
          destination: "/projects"
        }
      ]
    }
  ]
};

export interface NavigationSubElement {
  property: string;
  destination: string;
}

export interface NavigationTopElement {
  path: string;
  color: string;
  property: string;
  subElements: Array<NavigationSubElement>;
}

export interface NavigationStructure {
  topElements: Array<NavigationTopElement>;
}

export const TopHeader = () => {
  const { topElements } = navigationStructure;
  const [isMenuOpen, setMenuOpen] = useState(false);
  const { "general.title": title, "general.subTitle": subTitle } =
    useContentsQuery(["general.title", "general.subTitle"]);
  return (
    <CenteredContainer>
      <div className="flex justify-between py-3 px-1">
        <div className="grid grid-flow-col items-center gap-2">
          <Link to="/">
            <Logo />
          </Link>
          <div className="flex flex-col">
            <div className="font-serif font-bold text-xl leading-none">
              <Link to="/">{title}</Link>
            </div>
            <div className="inline text-sm">{subTitle}</div>
          </div>
        </div>
        <div className="hidden md:flex items-center">
          {topElements.map(({ property, subElements, color, path }, tIndex) => (
            <div className="group my-1" key={`nav-${tIndex}`}>
              <Link
                className={`group-hover:shadow-xs group-hover:bg-${color} focus:bg-${color} block px-3 py-2 font-bold`}
                to={path}
              >
                <ContentQuery readonly property={property} />
              </Link>
              <div className="hidden group-focus:flex group-hover:flex absolute z-50">
                <AsyncBackground top="none" size="xs">
                  <div className="grid gap-2 justify-center px-2 pt-2">
                    {subElements.map(
                      ({ property: subProperty, destination }, sIndex) => (
                        <Link
                          key={`nav-${tIndex}-${sIndex}`}
                          className="text-sm"
                          to={destination}
                        >
                          <ContentQuery readonly property={subProperty} />
                        </Link>
                      )
                    )}
                  </div>
                </AsyncBackground>
              </div>
            </div>
          ))}
          <div className="w-20 ml-1 transition-all duration-300 ease-in-out focus-within:w-48">
            <Search />
          </div>
        </div>
        <div className="m-2 md:hidden hover:text-black">
          <button
            aria-label="Menü"
            className="border-none"
            onClick={() => setMenuOpen(!isMenuOpen)}
          >
            <Icon
              ariaLabel={isMenuOpen ? "Menü schließen" : "Menü öffnen"}
              size="lg"
              icon={isMenuOpen ? "times" : "bars"}
            />
          </button>
        </div>
      </div>
      {isMenuOpen && (
        <div className="flex flex-col md:hidden">
          {topElements.map(({ property, subElements, color, path }, tIndex) => (
            <div
              key={`nav-${tIndex}`}
              className={`hover:shadow-xs px-4 py-2 hover:bg-${color} focus:bg-${color}`}
            >
              <div className="font-bold">
                <Link to={path}>
                  <ContentQuery readonly property={property} />
                </Link>
              </div>
              <div>
                <div className="inline-grid grid-flow-col gap-2">
                  {subElements.map(
                    ({ property: subProperty, destination }, sIndex) => (
                      <Link
                        key={`nav-${tIndex}-${sIndex}`}
                        className="text-sm"
                        to={destination}
                      >
                        <ContentQuery readonly property={subProperty} />
                      </Link>
                    )
                  )}
                </div>
              </div>
            </div>
          ))}
          <div className="flex justify-between p-4">
            <div className="flex-1 w-full mr-4">
              <Search />
            </div>
            <LanguageSelect />
          </div>
        </div>
      )}
    </CenteredContainer>
  );
};
