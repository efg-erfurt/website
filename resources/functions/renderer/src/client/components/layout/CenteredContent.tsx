import React, { PropsWithChildren } from "react";

export const CenteredColumn = ({ children }: PropsWithChildren<unknown>) => (
  <div className="flex flex-col justify-center items-center">{children}</div>
);

export const CenteredRow = ({ children }: PropsWithChildren<unknown>) => (
  <div className="flex flex-row justify-evenly items-center">{children}</div>
);

export const CenteredContainer = ({ children }: PropsWithChildren<unknown>) => (
  <div className="flex justify-center mx-1">
    <div className="container">{children}</div>
  </div>
);
