import React, { PropsWithChildren } from "react";

type EdgeDirection = "up" | "down" | "none";
type Size = "xs" | "md" | "lg";

type AsyncBackgroundProps = {
  top?: EdgeDirection;
  bottom?: EdgeDirection;
  overlap?: boolean;
  size?: Size;
};

export const AsyncBackground = ({
  children,
  top = "down",
  bottom = "down",
  overlap = false,
  size = "md"
}: PropsWithChildren<AsyncBackgroundProps>) => (
  <div className={`bg-blue-50 relative ${overlap ? "my-4" : ""}`}>
    {top !== "none" && (
      <svg
        style={{
          top: calculateOffset(size)
        }}
        className={`absolute left-0 w-full ${calculateSize(size)} z-0`}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 100 100"
        preserveAspectRatio="none"
      >
        {top == "up" ? (
          <polygon fill="#e1e4e9" points="0,100 100,100 100,0 0,50" />
        ) : (
          <polygon fill="#e1e4e9" points="0,0 100,50 100,100 0,100" />
        )}
      </svg>
    )}
    {children}
    {bottom !== "none" && (
      <svg
        style={{
          bottom: calculateOffset(size)
        }}
        className={`absolute left-0 w-full  ${calculateSize(size)} z-30`}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 100 100"
        preserveAspectRatio="none"
      >
        {bottom == "down" ? (
          <polygon fill="#e1e4e9" points="0,0 100,0 100,100 0,50" />
        ) : (
          <polygon fill="#e1e4e9" points="0,0 100,0 100,50 0,100" />
        )}
      </svg>
    )}
  </div>
);

function calculateOffset(size: string) {
  return size === "md" ? "-3rem" : size === "lg" ? "-5rem" : "-1rem";
}

function calculateSize(size: string) {
  return size === "md" ? "h-12" : size === "lg" ? "h-20" : "h-4";
}
