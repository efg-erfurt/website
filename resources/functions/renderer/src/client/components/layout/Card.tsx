import React, { PropsWithChildren } from "react";
import { AsyncBackground } from "./AsyncBackground";

type CardProps = { title?: string; margin?: boolean };

export const Card = ({
  children,
  title,
  margin = true
}: PropsWithChildren<CardProps>) => (
  <div className="bg-gray-100 shadow-sm self-start rounded">
    {!!title && (
      <div className="font-bold text-lg font-serif py-2 px-3 bg-gray-700 text-white rounded-t">
        {title}
      </div>
    )}
    <div className={`grid ${margin ? "gap-2 md:gap-4 p-2 md:p-4" : ""}`}>
      {children}
    </div>
  </div>
);

type CardGroupProps = { direction?: "up" | "down" };

export const CardGroup = ({
  children,
  direction = "up"
}: PropsWithChildren<CardGroupProps>) => (
  <AsyncBackground overlap top={direction} bottom={direction}>
    <PlainCardGroup>{children}</PlainCardGroup>
  </AsyncBackground>
);

export const PlainCardGroup = ({ children }: PropsWithChildren<unknown>) => (
  <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-2 px-2 sm:gap-4 sm:px-4 py-0">
    {children}
  </div>
);
