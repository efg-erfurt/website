import React, { Fragment, useContext } from "react";
import { Helmet } from "react-helmet-async";
import { useContentQuery } from "../i18n/ContentQuery";
import { PageContext } from "../page/PageContext";
import { Link } from "../text/Link";

export interface BreadcrumbsProps {
  items: Array<{ property?: string; text?: string; to: string | null }>;
}

export const Breadcrumbs = ({ items }: BreadcrumbsProps) => {
  const path = items.map(({ property, text, to }) => ({
    value: property ? useContentQuery(property) : text,
    to
  }));
  const { origin, language, protocol } = useContext(PageContext);
  return (
    <>
      <Helmet>
        <script type="application/ld+json">
          {`{
        "@context": "https://schema.org",
        "@type": "BreadcrumbList",
          "itemListElement": 
            ${JSON.stringify(
              path.map((item, i) => ({
                "@type": "ListItem",
                position: i + 1,
                name: item.value,
                item: item.to
                  ? `${protocol}//${origin}/${language.code}${item.to}`
                  : undefined
              }))
            )}
        }`}
        </script>
      </Helmet>
      <div className="text-sm inline-grid grid-flow-col gap-2">
        {path.map(({ to, value }, i) => (
          <Fragment key={`bc-${i}`}>
            <div>{to ? <Link to={to}>{value}</Link> : value}</div>
            {i !== path.length - 1 && <div>/</div>}
          </Fragment>
        ))}
      </div>
    </>
  );
};
