import React from "react";

export const Spacer = ({ size = "large" }: { size?: "small" | "large" }) => (
  <div className={size === "large" ? "mt-16" : "mt-8"} />
);
