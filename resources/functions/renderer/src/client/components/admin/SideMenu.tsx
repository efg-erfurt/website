import React from "react";
import { Link } from "../text/Link";

export const SideMenu = () => (
  <div className="flex flex-col">
    <Link to="/admin">Allgemeines</Link>
  </div>
);
