import React from "react";
import { Link } from "../text/Link";
import {
  ContentQuery,
  ImageQuery,
  useContentQuery
} from "../i18n/ContentQuery";
import { Button } from "../form/Button";

export const ZeitungPreview = () => {
  const zeitungUrl = useContentQuery("components.zeitung.pdfUrl");
  return (
    <div className="flex flex-col">
      <ImageQuery property="components.zeitung.imgUrl" alt="Zeitung" />
      <ContentQuery markdown property="components.zeitung.preview" />
      <ContentQuery property="components.zeitung.read">
        {property => (
          <Link showArrow={false} to={zeitungUrl || ""}>
            <Button disabled={!zeitungUrl} className="w-full mt-2">
              {property}
            </Button>
          </Link>
        )}
      </ContentQuery>
    </div>
  );
};
