import { useMutation, gql } from "@apollo/client";
import React, { useContext, useState } from "react";
import loadable from "@loadable/component";
import { PageContext } from "../page/PageContext";
import { Card, CardGroup } from "../layout/Card";
import { getLatestSermonsFn, SermonPreview } from "./SermonList";
import { Dialog } from "../layout/dialog/Dialog";
import { Button } from "../form/Button";
import {
  DeleteSermonMutation,
  DeleteSermonMutationVariables,
  GetLatestSermonsQueryVariables,
  GetSermonsQuery,
  Language as GQLLanguage
} from "../../../types/generated";

const SermonCreateForm = loadable(
  () => import("../../pages/downloads/SermonCreateForm")
);

const deleteSermonFn = gql`
  mutation deleteSermon($id: String!) {
    deleteSermon(id: $id)
  }
`;

export const SermonCards = ({ sermons }: { sermons: GetSermonsQuery }) => {
  const {
    language: { code },
    user
  } = useContext(PageContext);
  const [newSermon, setNewSermon] = useState(false);
  const closeDialog = () => setNewSermon(false);
  const openDialog = () => setNewSermon(true);

  const [deleteSermon] = useMutation<
    DeleteSermonMutation,
    DeleteSermonMutationVariables
  >(deleteSermonFn, {
    awaitRefetchQueries: true,
    refetchQueries: [
      {
        query: getLatestSermonsFn,
        variables: {
          language: code === "de" ? GQLLanguage.German : GQLLanguage.English,
          pagination: { page: 0, size: 2 }
        } as GetLatestSermonsQueryVariables
      }
    ]
  });

  return (
    <>
      {user && (
        <Dialog
          visible={newSermon}
          header="components.editing.publish"
          onClose={closeDialog}
        >
          <SermonCreateForm onSuccess={closeDialog} />
        </Dialog>
      )}

      {sermons.sermons.totalCount !== 0 && (
        <CardGroup>
          {sermons.sermons.content.map(s => (
            <Card key={`sermon-${s.id}`}>
              <SermonPreview
                sermon={s}
                user={user}
                onDeleteSermon={async id => {
                  await deleteSermon({ variables: { id } });
                }}
              />
            </Card>
          ))}
          {!!user && (
            <Button id="create-sermon" color="primary" onClick={openDialog}>
              Neue Predigt veröffentlichen
            </Button>
          )}
        </CardGroup>
      )}
    </>
  );
};
