import React, { useContext, useState } from "react";
import { gql, useQuery } from "@apollo/client";
import type { IconProp } from "@fortawesome/fontawesome-svg-core";
import loadable from "@loadable/component";
import dayjs from "dayjs";
import { ContentQuery, useContentQuery } from "../i18n/ContentQuery";
import { Language } from "../i18n/languages";
import { Spinner } from "../media/Spinner";
import { PageContext } from "../page/PageContext";
import { Error } from "../errors/Error";
import { Link } from "../text/Link";
import { Button, ButtonGroup } from "../form/Button";
import { Icon } from "../media/Icon";
import { Tags } from "../text/Tag";
import { Dialog } from "../layout/dialog/Dialog";
import {
  User,
  Sermon,
  GetLatestSermonsQuery,
  GetLatestSermonsQueryVariables,
  Language as GQLLanguage
} from "../../../types/generated";

const SermonUpdateForm = loadable(
  () => import("../../pages/downloads/SermonUpdateForm")
);
export const getLatestSermonsFn = gql`
  query getLatestSermons($language: Language!, $pagination: PaginationInput!) {
    latestSermons(language: $language, pagination: $pagination) {
      content {
        id
        title
        publishDate
        talkDate
        speaker
        comment
        audioUrl
        videoUrl
        textUrl
        translationUrl
        language
        tags
      }
      totalPages
      totalCount
      page
      count
    }
  }
`;

const LatestSermonsContent = ({ language }: { language: Language }) => {
  const { data, loading, error } = useQuery<
    GetLatestSermonsQuery,
    GetLatestSermonsQueryVariables
  >(getLatestSermonsFn, {
    variables: {
      language: language.code ? GQLLanguage.German : GQLLanguage.English,
      pagination: { page: 0, size: 2 }
    }
  });

  if (loading) {
    return <Spinner />;
  } else if (error) {
    return <Error error={error} />;
  } else if (!data || data.latestSermons.totalCount === 0) {
    return <ContentQuery markdown property="pages.sermons.empty" />;
  }

  return (
    <div className="grid gap-4">
      {data.latestSermons.content.map(s => (
        <SermonPreview key={s.id} sermon={s} />
      ))}
    </div>
  );
};

export interface SermonListProps {
  sermons: Sermon[];
}

export const SermonPreview = ({
  sermon,
  user = null,
  onDeleteSermon
}: {
  sermon: Sermon;
  user?: User | null;
  onDeleteSermon?: (id: string) => Promise<void>;
}) => {
  const editAction = useContentQuery("components.editing.edit");
  const [editOpen, setEditOpen] = useState(false);
  const closeDialog = () => setEditOpen(false);
  const openDialog = () => setEditOpen(true);
  return (
    <div className="flex flex-col">
      <div className="flex justify-between font-bold text-sm">
        <div>{dayjs(sermon.talkDate).format("dddd, DD.MM.YY")}</div>
        {!!user && !!onDeleteSermon && (
          <>
            <ButtonGroup>
              <Button onClick={openDialog}>{editAction}</Button>
              <Button onClick={() => onDeleteSermon(sermon.id)} color="danger">
                <Icon ariaLabel="Löschen" icon="times" />
              </Button>
            </ButtonGroup>
            <Dialog
              header="components.editing.edit"
              visible={editOpen}
              onClose={closeDialog}
            >
              <SermonUpdateForm
                onSuccess={closeDialog}
                existingSermon={sermon}
              />
            </Dialog>
          </>
        )}
      </div>
      <div className="font-normal text-lg text-gray-900">{sermon.title}</div>
      <div className="text-sm mb-2">
        <Link to={`/sermons?speaker=${sermon.speaker}`}>{sermon.speaker}</Link>
      </div>
      <div className="text-sm mb-2">{sermon.comment}</div>
      <div className="flex justify-between">
        <Tags items={sermon.tags} />
        <div className="grid grid-flow-col gap-2">
          <DownloadLink
            url={sermon.videoUrl || null}
            icon={["fab", "youtube"]}
          />
          <DownloadLink url={sermon.audioUrl || null} icon="headphones-alt" />
          <DownloadLink url={sermon.textUrl || null} icon="file-pdf" />
          <DownloadLink url={sermon.translationUrl || null} icon="file-alt" />
        </div>
      </div>
    </div>
  );
};

const DownloadLink = ({
  url,
  icon
}: {
  url: string | null;
  icon: IconProp;
}) => {
  if (!url) {
    return (
      <Icon ariaLabel="Kein Download" color="gray" icon={icon} size="lg" />
    );
  }
  return (
    <Link to={url || ""} showArrow={false}>
      <Icon ariaLabel="Download" icon={icon} size="lg" />
    </Link>
  );
};

export const LatestSermons = () => {
  const { language } = useContext(PageContext);
  return (
    <div>
      <LatestSermonsContent language={language} />
      <ContentQuery property="components.sermons.goto">
        {property => (
          <Link to="/sermons">
            <Button className="w-full mt-4">{property}</Button>
          </Link>
        )}
      </ContentQuery>
    </div>
  );
};
