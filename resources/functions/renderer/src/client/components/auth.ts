import { ApolloClient } from "@apollo/client";

const ACCESS_TOKEN = "ACCESS_TOKEN";

export const logoutAndRefetch = async (
  client: ApolloClient<Record<string, unknown>>
) => {
  window.localStorage.removeItem(ACCESS_TOKEN);
  await client.resetStore();
};
export const loginAndRefetch = async (
  token: string,
  client: ApolloClient<Record<string, unknown>>
) => {
  window.localStorage.setItem(ACCESS_TOKEN, token);
  await client.resetStore();
};

export const getToken = () => window.localStorage.getItem(ACCESS_TOKEN) || null;
