import React from "react";
import { User } from "../../../types/generated";
import { Language, Languages } from "../i18n/languages";

interface PageContextProps {
  language: Language;
  path: string;
  origin: string;
  protocol: string;
  user: User | null;
}

export const PageContext = React.createContext<PageContextProps>({
  language: Languages.DE,
  path: "",
  origin: "",
  user: null,
  protocol: "https:"
});
