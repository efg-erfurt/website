import React, { PropsWithChildren } from "react";

export class ErrorHandler extends React.Component<
  PropsWithChildren<Record<string, unknown>>,
  { hasError: boolean }
> {
  public constructor(props: any) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  public render() {
    if (this.state.hasError) {
      return (
        <>
          <h1>Error</h1>
          <p>
            An unexpected error has happened. Please contact the admin of this
            page.
          </p>
        </>
      );
    }

    return this.props.children;
  }
}
