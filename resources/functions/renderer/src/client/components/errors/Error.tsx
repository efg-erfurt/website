import { ApolloError } from "@apollo/client/errors";
import React from "react";
import { Icon } from "../media/Icon";

export const Error = ({ error }: { error?: ApolloError }) => (
  <>
    <Icon ariaLabel="Fehler" color="red" icon="exclamation" />
    {error?.message}
  </>
);
