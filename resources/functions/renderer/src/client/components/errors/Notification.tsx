import React, { PropsWithChildren } from "react";

type NotificationProps = {
  type?: "info" | "warning" | "error" | "success";
};

export const Notification = ({
  children,
  type = "info"
}: PropsWithChildren<NotificationProps>) => (
  <div
    className={`${
      type === "info"
        ? "bg-efg-blue-bright"
        : type === "warning"
        ? "bg-efg-yellow"
        : type === "success"
        ? "bg-green-500"
        : "bg-red-700"
    }
           text-white font-bold p-2 my-4 rounded shadow-sm
          `}
  >
    {children}
  </div>
);
