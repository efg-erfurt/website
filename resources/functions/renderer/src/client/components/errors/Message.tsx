import React, { PropsWithChildren } from "react";

type MessageProps = {
  title: string;
  type?: "info" | "warning" | "error";
};

export const Message = ({
  children,
  title,
  type = "info"
}: PropsWithChildren<MessageProps>) => (
  <div className="shadow-sm rounded">
    <div
      className={`${
        type === "info"
          ? "bg-efg-blue-bright"
          : type === "warning"
          ? "bg-efg-yellow"
          : "bg-red-700"
      }
           text-white font-bold pb-1 pt-2 px-2 rounded-t
          `}
    >
      {title}
    </div>
    <div className="bg-gray-200 p-4 text-sm">{children}</div>
  </div>
);
