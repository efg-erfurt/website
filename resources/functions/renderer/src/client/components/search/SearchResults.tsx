import { gql, useQuery } from "@apollo/client";
import React, { Fragment, useContext } from "react";
import { Error } from "../errors/Error";
import { ContentQuery, useContentsQuery } from "../i18n/ContentQuery";
import { Spinner } from "../media/Spinner";
import { PageContext } from "../page/PageContext";
import { EventList } from "../events/EventList";
import { Heading } from "../text/Heading";
import { PostPreview } from "../posts/PostPreview";
import { SermonPreview } from "../downloads/SermonList";
import { Link } from "../text/Link";
import { Markdown } from "../text/Markdown";
import {
  Language,
  SearchQuery,
  SearchQueryVariables
} from "../../../types/generated";

const getSearchResultsFn = gql`
  query search($query: String!, $language: Language!) {
    search(query: $query, language: $language) {
      id
      query
      language
      posts {
        id
        publishDate
        summary
        content
        date
        title
        language
        creator {
          id
          firstName
          lastName
          name
        }
        tags
        imageUrl
      }
      pages {
        title
        path
        matches {
          content
          selection {
            from
            to
            highlight
          }
        }
      }
      events {
        id
        name
        place
        date
        category {
          id
          backgroundColor
          textColor
          name
        }
      }
      sermons {
        id
        title
        publishDate
        talkDate
        language
        speaker
        comment
        audioUrl
        videoUrl
        textUrl
        translationUrl
        tags
      }
    }
  }
`;

export const SearchResults = ({ query }: { query: string }) => {
  const { language } = useContext(PageContext);
  const {
    "pages.posts.detail.title": postsTitle,
    "pages.sermons.title": sermonsTitle,
    "pages.events.title": eventsTitle,
    "pages.search.empty": emptyResults
  } = useContentsQuery([
    "pages.posts.detail.title",
    "pages.events.title",
    "pages.sermons.title",
    "pages.search.empty"
  ]);
  const { data, error, loading } = useQuery<SearchQuery, SearchQueryVariables>(
    getSearchResultsFn,
    {
      variables: {
        query,
        language: language.code === "de" ? Language.German : Language.English
      }
    }
  );
  if (loading) {
    return <Spinner />;
  } else if (!data || error) {
    return <Error error={error} />;
  }
  const {
    search: { events, posts, pages, sermons }
  } = data;
  return (
    <div className="grid gap-8">
      <ContentQuery property="pages.search.results">
        {resultStr => (
          <Heading>
            {`${resultStr}: `}
            <q>{query}</q>
          </Heading>
        )}
      </ContentQuery>
      <div className="grid gap-2 mb-4">
        <Heading size={3}>{postsTitle}</Heading>
        {posts.length > 0 && (
          <div className="grid gap-8">
            {posts.map(p => (
              <PostPreview key={`post-${p.id}`} post={p} showSummary />
            ))}
          </div>
        )}
        {posts.length === 0 && <Markdown content={emptyResults} />}
      </div>
      <div className="grid gap-2 mb-4">
        <Heading size={3}>{eventsTitle}</Heading>
        {events.length > 0 && <EventList events={events} />}
        {events.length === 0 && <Markdown content={emptyResults} />}
      </div>
      <div className="grid gap-2 mb-4">
        <Heading size={3}>Seiten</Heading>
        {pages.length > 0 && (
          <div className="grid gap-8">
            {pages.map((n, pI) => (
              <div key={`p-${pI}`}>
                <div className="text-lg mb-2">
                  <Link to={n.path}>{n.title}</Link>
                </div>
                <div className="grid gap-2">
                  {n.matches.map(({ content, selection }, mI) => (
                    <div key={`m-${mI}`} className="lg:col-span-3 text-xs">
                      {selection.flatMap(({ from, to, highlight }, sI) => {
                        const k = `${pI}-${mI}-${sI}`;
                        return highlight ? (
                          <b key={k}>{content.slice(from, to)}</b>
                        ) : (
                          <Fragment key={k}>{content.slice(from, to)}</Fragment>
                        );
                      })}
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
        )}
        {pages.length === 0 && <Markdown content={emptyResults} />}
      </div>
      <div className="grid gap-2 mb-4">
        <Heading size={3}>{sermonsTitle}</Heading>
        {sermons.length > 0 && (
          <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
            {sermons.map(s => (
              <SermonPreview key={s.id} sermon={s} />
            ))}
          </div>
        )}
        {sermons.length === 0 && <Markdown content={emptyResults} />}
      </div>
    </div>
  );
};
