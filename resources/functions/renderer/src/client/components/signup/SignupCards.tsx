import React, { useState } from "react";
import { gql, useQuery } from "@apollo/client";
import type { IconProp } from "@fortawesome/fontawesome-svg-core";
import { Link } from "../text/Link";
import { Spinner } from "../media/Spinner";
import { Error } from "../errors/Error";
import { Icon } from "../media/Icon";
import { Card } from "../layout/Card";
import { Button } from "../form/Button";
import {
  GetSignupsQuery,
  GetSignupsQueryVariables,
  Signup
} from "../../../types/generated";

const getSignupsFn = gql`
  query getSignups($groupsId: String!) {
    signups(groupsId: $groupsId) {
      id
      name
      note
      imageUrl
      time
      day
      slotsLeft
      slotsTotal
      slotsUsed
    }
  }
`;

type SignupCardsProps = {
  groupsId: string;
};

const InfoLine = ({
  icon,
  text,
  label
}: {
  icon: IconProp;
  text: string;
  label: string;
}) => (
  <div className="flex items-center">
    <div className="flex w-4 m-1">
      <Icon ariaLabel={label} icon={icon} color="#BBB" />
    </div>
    <div className="flex-1 text-xs">{text}</div>
  </div>
);

export const SignupCards = ({ groupsId }: SignupCardsProps) => {
  const { data, error, loading } = useQuery<
    GetSignupsQuery,
    GetSignupsQueryVariables
  >(getSignupsFn, {
    variables: { groupsId }
  });
  if (loading) {
    return <Spinner />;
  } else if (!data || error) {
    return <Error error={error} />;
  }
  return (
    <div className="mx-2 sm:mx-4 mb-2 sm:mb-4">
      <Card title="Anmeldung zu Gottesdiensten">
        <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-2 sm:gap-4">
          {data.signups.map(s => (
            <SignupCard key={`s-${s.id}`} groupsId={groupsId} signup={s} />
          ))}
        </div>
      </Card>
    </div>
  );
};

const SignupCard = ({
  groupsId,
  signup: { id, name, note, imageUrl, time, day, slotsLeft, slotsTotal }
}: {
  groupsId: string;
  signup: Signup;
}) => {
  const [isOpen, setOpen] = useState(false);
  return (
    <div className="grid">
      {!!imageUrl && (
        <img
          className="rounded w-full shadow-xs"
          src={`${imageUrl}?w=400&h=180&q=85`}
          alt="Vorschau"
        />
      )}
      <div className="grid gap-2 mt-1">
        <div className="font-bold text-sm">{name}</div>
        {note.length <= 300 ? (
          <p className="text-sm">{note}</p>
        ) : (
          <p className="text-sm">
            {isOpen ? (
              note
            ) : (
              <>
                {note.slice(0, 297)}...
                <Icon
                  onClick={() => setOpen(!isOpen)}
                  icon={isOpen ? "chevron-down" : "chevron-right"}
                  size="1x"
                />
              </>
            )}
          </p>
        )}
        {time && !day && <InfoLine label="Zeit" icon="clock" text={time} />}
        {!time && day && <InfoLine label="Datum" icon="clock" text={day} />}
        {time && day && (
          <InfoLine
            label="Datum und Uhrzeit"
            icon="clock"
            text={`${day} | ${time}`}
          />
        )}
        {slotsLeft !== undefined && slotsLeft !== null ? (
          <InfoLine
            label="Freie Plätze"
            icon="users"
            text={
              slotsLeft > 0
                ? `${slotsLeft} von ${slotsTotal} frei`
                : "Keine Plätze frei"
            }
          />
        ) : null}
        <Link
          showArrow={false}
          to={`https://efg-erfurt.church.tools/publicgroup/${id}?hash=${groupsId}#/`}
        >
          <Button
            disabled={
              slotsLeft !== null && slotsLeft !== undefined && slotsLeft <= 0
            }
            className="w-full"
          >
            Anmelden
          </Button>
        </Link>
      </div>
    </div>
  );
};
