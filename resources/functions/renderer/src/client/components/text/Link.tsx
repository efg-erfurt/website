import {
  Link as RouterLink,
  LinkProps as RouterLinkProps,
  To
} from "react-router-dom";
import React, { useContext } from "react";
import { Icon } from "../media/Icon";
import { PageContext } from "../page/PageContext";

const getLocalizedTo = (
  to: To,
  origin: string,
  protocol: string,
  languageCode: string
) => {
  const localizedTo = `/${languageCode}${(to as string).replace(
    `${protocol}//${origin}/de`,
    ""
  )}`;
  return localizedTo;
};

type LinkProps = {
  showArrow?: boolean;
} & RouterLinkProps;

export const Link = (props: LinkProps) => <LocalizedLink {...props} />;

const isEmail = (email: string) => {
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const LocalizedLink = (props: LinkProps) => {
  const { showArrow = true, to, className, children, ...otherProps } = props;
  const {
    language: { code: languageCode },
    protocol,
    origin
  } = useContext(PageContext);

  const classes = `no-underline focus:underline hover:underline text-sky-800 ${
    className || ""
  }`;
  if (isEmail(to as string)) {
    return (
      <a className={classes} href={`mailto:${to}`} aria-label="Email senden">
        {children} <Icon ariaLabel="Email" icon="envelope" />
      </a>
    );
  }

  const external =
    typeof to === "string" &&
    to !== "#" &&
    !to.replace(`${protocol}//${origin}/${languageCode}`, "").startsWith("/");
  if (external) {
    return (
      <a
        className={classes}
        href={to}
        target="_href"
        aria-label={otherProps["aria-label"]}
      >
        {children}{" "}
        {showArrow && (
          <Icon ariaLabel="Externer Link" icon="external-link-alt" />
        )}
      </a>
    );
  }

  const localizedTo = getLocalizedTo(to, origin, protocol, languageCode);
  return (
    <RouterLink
      className={classes}
      to={!!to ? localizedTo : "#"}
      {...otherProps}
    >
      {children}
    </RouterLink>
  );
};
