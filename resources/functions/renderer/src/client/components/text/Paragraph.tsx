import React, { PropsWithChildren } from "react";

export const Paragraph = ({ children }: PropsWithChildren<unknown>) => (
  <p className="max-w-3xl text-sm leading-6">{children}</p>
);
