import React, { PropsWithChildren } from "react";
import ReactMarkdown from "react-markdown";
import newLine from "remark-breaks";
import { Link } from "./Link";
import { Heading } from "./Heading";
import { Paragraph } from "./Paragraph";

const ParagraphRenderer = ({ children }: PropsWithChildren<unknown>) => (
  <Paragraph>{children}</Paragraph>
);
const ImageRenderer = ({ alt, src }: { alt?: string; src?: string }) => (
  <div className="flex flex-col items-center max-w-3xl">
    <img className="rounded shadow-xs" src={src} alt={alt} />
  </div>
);
const LinkRenderer = ({
  children,
  href
}: PropsWithChildren<{ href?: string }>) => (
  <Link to={href || "#"}>{children}</Link>
);
const HeadingRender = ({
  children,
  level
}: PropsWithChildren<{ level: number }>) => (
  <Heading size={level}>{children}</Heading>
);
const ListRenderer = ({
  depth,
  ordered,
  children
}: PropsWithChildren<{ depth?: number; ordered?: boolean }>) => {
  if (depth === 0) {
    return (
      <Paragraph>
        {ordered ? (
          <ol className="list-decimal ml-4">{children}</ol>
        ) : (
          <ul className="list-disc ml-4">{children}</ul>
        )}
      </Paragraph>
    );
  } else if (ordered === true) {
    return <ol>{children}</ol>;
  } else if (ordered === false) {
    return <ul>{children}</ul>;
  }

  return <>{children}</>;
};
const ListItemRenderer = ({ children }: PropsWithChildren<unknown>) => (
  <li className="pb-2 pl-2">{children}</li>
);
const TableRenderer = ({ children }: PropsWithChildren<unknown>) => (
  <Paragraph>
    <table className="table-auto">{children}</table>
  </Paragraph>
);
const TableHeaderRenderer = ({ children }: PropsWithChildren<unknown>) => (
  <thead className="border-b border-efg-blue-dark font-bold">{children}</thead>
);
const TableBodyRenderer = ({ children }: PropsWithChildren<unknown>) => (
  <tbody className="divide-y divide-gray-400">{children}</tbody>
);
const TableCellRenderer = ({ children }: PropsWithChildren<unknown>) => (
  <td className="p-2">{children}</td>
);
const RootRenderer = ({ children }: PropsWithChildren<unknown>) => (
  <div className="grid gap-1">{children}</div>
);
const TableRowRenderer = ({ children }: PropsWithChildren<unknown>) => (
  <tr className="">{children}</tr>
);
const SimpleTextRenderer = ({ children }: PropsWithChildren<unknown>) => (
  <>{children}</>
);

const customRenderers = {
  root: RootRenderer,
  p: ParagraphRenderer,
  h1: HeadingRender,
  h2: HeadingRender,
  h3: HeadingRender,
  h4: HeadingRender,
  h5: HeadingRender,
  img: ImageRenderer,
  a: LinkRenderer,
  ul: ListRenderer,
  ol: ListRenderer,
  li: ListItemRenderer,
  table: TableRenderer,
  thead: TableHeaderRenderer,
  tbody: TableBodyRenderer,
  tc: TableCellRenderer,
  tr: TableRowRenderer,
  br: () => <br />
};

export interface MarkdownProps {
  content: string;
}

export const Markdown = ({ content }: MarkdownProps) => (
  <ReactMarkdown
    remarkPlugins={[newLine]}
    className="grid gap-4"
    components={customRenderers}
  >
    {content}
  </ReactMarkdown>
);

export const MarkdownPreview = ({ content }: MarkdownProps) => (
  <ReactMarkdown
    remarkPlugins={[newLine]}
    allowedElements={[
      "p",
      "a",
      "h1",
      "h2",
      "h3",
      "h4",
      "h5",
      "strong",
      "em",
      "br"
    ]}
    components={{ ...customRenderers, strong: SimpleTextRenderer }}
  >
    {content}
  </ReactMarkdown>
);
