import React, { PropsWithChildren } from "react";
import { Link } from "./Link";
import { LinkProps } from "react-router-dom";

const StyledTag = ({ children }: PropsWithChildren<unknown>) => (
  <span className="text-gray-600 text-xs rounded border px-1 truncate">
    {children}
  </span>
);

export const Tag = ({
  children,
  to,
  ...otherProps
}: PropsWithChildren<Omit<LinkProps, "to"> & { to?: string }>) =>
  !!to ? (
    <Link to={to} {...otherProps}>
      <StyledTag>{children}</StyledTag>
    </Link>
  ) : (
    <StyledTag>{children}</StyledTag>
  );

export const Tags = ({
  items,
  toFn
}: {
  items: Array<string>;
  toFn?: (tag: string) => string;
}) => (
  <div className="flex flex-wrap items-center gap-1">
    {items.map((t, i) => (
      <Tag key={`t-${i}`} to={toFn ? toFn(t) : undefined}>
        {t}
      </Tag>
    ))}
  </div>
);
