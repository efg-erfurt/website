import React from "react";
import type {
  DetailedHTMLProps,
  HTMLAttributes,
  PropsWithChildren
} from "react";

export const Heading = ({
  children,
  size = 1,
  className,
  ...otherProps
}: PropsWithChildren<
  { size?: number } & DetailedHTMLProps<
    HTMLAttributes<HTMLHeadingElement>,
    HTMLHeadingElement
  >
>) => {
  const otherClasses = `font-serif font-bold leading-tight ${className || ""}`;
  if (size === 1) {
    return (
      <h1 {...otherProps} className={`text-4xl ${otherClasses}`}>
        {children}
      </h1>
    );
  } else if (size === 2) {
    return (
      <h2 {...otherProps} className={`text-4xl ${otherClasses}`}>
        {children}
      </h2>
    );
  } else if (size === 3) {
    return (
      <h3 {...otherProps} className={`text-3xl ${otherClasses}`}>
        {children}
      </h3>
    );
  } else if (size === 4) {
    return (
      <h4 {...otherProps} className={`text-2xl ${otherClasses}`}>
        {children}
      </h4>
    );
  }
  return (
    <h5 {...otherProps} className={`text-xl ${otherClasses}`}>
      {children}
    </h5>
  );
};
