import { gql, useQuery } from "@apollo/client";
import React from "react";
import { Spinner } from "../media/Spinner";
import { Error } from "../errors/Error";
import { Link } from "../text/Link";
import { Icon } from "../media/Icon";
import { GetUserQuery, GetUserQueryVariables } from "../../../types/generated";

const getUserFn = gql`
  query getUser($id: String!) {
    user(id: $id) {
      id
      firstName
      lastName
      imageUrl
    }
  }
`;

export const ContactHeader = () => (
  <div className="text-xs font-bold">Ansprechpartner</div>
);

export const ContactInfo = ({
  id,
  mail,
  position
}: {
  id: string;
  mail: string;
  position?: string;
}) => {
  const { data, error, loading } = useQuery<
    GetUserQuery,
    GetUserQueryVariables
  >(getUserFn, {
    variables: {
      id
    }
  });
  if (loading) {
    return <Spinner />;
  } else if (!data || error) {
    return <Error error={error} />;
  }
  return (
    <div className="text-sm flex flex-row items-start">
      <div className="font-bold bg-efg-blue-bright text-white flex items-center justify-center rounded-md shadow-xs w-12 h-12">
        {data.user?.imageUrl ? (
          <img
            alt="Profilbild"
            className="rounded-md"
            src={`${data.user.imageUrl}?w=50&h=50&q=85`}
          />
        ) : (
          <span>
            {!data.user
              ? "-"
              : `${data.user.firstName
                  ?.slice(0, 1)
                  .toUpperCase()}${data.user.lastName
                  ?.slice(0, 1)
                  .toUpperCase()}`}
          </span>
        )}
      </div>
      <div className="flex-1 mx-4">
        {data.user ? `${data.user.firstName} ${data.user.lastName}` : "-"}
        {!!position && (
          <>
            <br />
            <span className="text-xs">{position}</span>
          </>
        )}
        <br />
        <Link showArrow={false} to={`mailto:${mail}`}>
          <Icon ariaLabel="Kontakt" icon="envelope" /> Kontakt
        </Link>
      </div>
    </div>
  );
};
