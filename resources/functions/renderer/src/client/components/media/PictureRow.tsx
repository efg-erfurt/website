import React from "react";
import { ContentQuery } from "../i18n/ContentQuery";
import { Image } from "./Image";

type Images = Array<{ srcType: "jpg" | "png"; name: string }>;

export const PictureRow = ({
  orientation,
  images = [],
  property
}: {
  orientation: "left" | "right";
  property: string;
  images?: Images;
}) => (
  <div className="grid sm:grid-cols-3 md:grid-cols-4 gap-4">
    {orientation === "right" && <ImageColumn images={images} />}
    <div className="sm:col-span-2 md:col-span-3 px-4">
      <ContentQuery markdown property={property} />
      <div className="block sm:hidden mt-4">
        <Images images={images} />
      </div>
    </div>
    {orientation === "left" && <ImageColumn images={images} />}
  </div>
);

const ImageColumn = ({ images }: { images: Images }) => (
  <div className="hidden flex-col sm:flex">
    <Images images={images} />
  </div>
);

const Images = ({ images }: { images: Images }) => (
  <>
    {images.map(({ name, srcType }) => (
      <div key={name} className="mb-8">
        <Image srcType={srcType} src={name} className="rounded shadow-xs" />
      </div>
    ))}
  </>
);
