import React from "react";
import { Icon } from "./Icon";

export const Spinner = () => <Icon ariaLabel="Lädt" icon="spinner" spin />;
