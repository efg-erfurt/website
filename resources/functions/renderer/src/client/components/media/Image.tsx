import React from "react";
import type { ImgHTMLAttributes, DetailedHTMLProps } from "react";

type ImageProps = {
  srcType: "jpg" | "png";
} & DetailedHTMLProps<ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>;

export const Image = ({ src, srcType, alt, ...otherProps }: ImageProps) => {
  const path = `/public/images/${src}`;
  return (
    <picture>
      <source type="image/webp" srcSet={`${path}.webp`} />
      {srcType === "jpg" ? (
        <source type="image/jpeg" srcSet={`${path}.jpg`} />
      ) : (
        <source type="image/png" srcSet={`${path}.png`} />
      )}
      <img
        src={`${path}.${srcType}`}
        alt={alt || "Illustration"}
        {...otherProps}
      />
    </picture>
  );
};

export const Logo = () => {
  const path = `/public/images/logo`;
  return (
    <picture>
      <source type="image/webp" srcSet={`${path}.webp`} />
      <source type="image/png" srcSet={`${path}.png`} />
      <img
        className="h-10 w-10 max-w-none inline"
        src={`${path}.png`}
        alt="Logo"
      />
    </picture>
  );
};
