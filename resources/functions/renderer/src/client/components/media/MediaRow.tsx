import React from "react";
import { ContentQuery } from "../i18n/ContentQuery";
import { Image } from "./Image";

export const MediaRow = ({
  imageSrc,
  imageAlt = "Illustration",
  property: content,
  imagePos = "left"
}: {
  imageSrc?: string;
  imageAlt?: string;
  imagePos?: "left" | "right";
  property: string;
  style?: any;
}) => (
  <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
    {imageSrc && imagePos === "left" && (
      <div className="hidden sm:flex justify-evenly h-full items-center">
        <Image
          srcType="png"
          className="m-4 w-16"
          alt={imageAlt}
          src={imageSrc}
        />
      </div>
    )}
    <div className="md:col-span-1 lg:col-span-2 xl:col-span-3">
      <ContentQuery markdown property={content} />
    </div>
    {imageSrc && imagePos === "right" && (
      <div className="hidden sm:flex justify-evenly h-full items-center">
        <Image
          srcType="png"
          className="m-4 w-16"
          alt={imageAlt}
          src={imageSrc}
        />
      </div>
    )}
    {imageSrc && (
      <div className="flex sm:hidden justify-evenly h-full items-center">
        <Image
          srcType="png"
          className="m-4 w-16"
          alt={imageAlt}
          src={imageSrc}
        />
      </div>
    )}
  </div>
);
