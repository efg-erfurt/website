import type { IconProp, SizeProp } from "@fortawesome/fontawesome-svg-core";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faGlobeAfrica,
  faAsterisk,
  faChevronLeft,
  faChevronRight,
  faChevronDown,
  faClock,
  faEdit,
  faEnvelope,
  faExclamation,
  faExternalLinkAlt,
  faHistory,
  faLock,
  faMapMarkerAlt,
  faQuestionCircle,
  faSave,
  faSearch,
  faSignOutAlt,
  faSpinner,
  faTags,
  faTimes,
  faUsers,
  faUser,
  faPlus,
  faWindowClose,
  faBars,
  faHeadphonesAlt,
  faFilePdf,
  faFileAlt,
  faLifeRing,
  faLightbulb,
  faCoffee,
  faChurch,
  faHandHoldingHeart,
  faPrayingHands,
  faBible,
  faChild,
  faBlind,
  faPhone,
  faUserFriends,
  faImage,
  faUpload
} from "@fortawesome/free-solid-svg-icons";
import {
  faFacebook,
  faWhatsapp,
  faInstagram,
  faYoutube
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

library.add(
  faGlobeAfrica,
  faPhone,
  faImage,
  faBlind,
  faChild,
  faPrayingHands,
  faCoffee,
  faBible,
  faChurch,
  faHandHoldingHeart,
  faLightbulb,
  faLifeRing,
  faHeadphonesAlt,
  faYoutube,
  faFilePdf,
  faFileAlt,
  faPlus,
  faEnvelope,
  faUser,
  faExclamation,
  faTimes,
  faSearch,
  faSignOutAlt,
  faEdit,
  faWindowClose,
  faSave,
  faSpinner,
  faMapMarkerAlt,
  faAsterisk,
  faHistory,
  faExternalLinkAlt,
  faChevronRight,
  faChevronLeft,
  faChevronDown,
  faTags,
  faClock,
  faUsers,
  faLock,
  faQuestionCircle,
  faWhatsapp,
  faFacebook,
  faInstagram,
  faUserFriends,
  faBars,
  faUpload
);

type IconSize = SizeProp;

export const Icon = ({
  id,
  icon,
  spin = false,
  size = "xs",
  ariaLabel = "",
  additionalClassName = "",
  color,
  onClick
}: {
  id?: string;
  icon: IconProp;
  spin?: boolean;
  size?: IconSize;
  color?: string;
  ariaLabel?: string;
  additionalClassName?: string;
  onClick?: () => any;
}) => (
  <FontAwesomeIcon
    id={id}
    className={
      !!onClick
        ? `hover:cursor-pointer hover:text-black ${additionalClassName}`
        : additionalClassName
    }
    icon={icon}
    size={size}
    color={color}
    spin={spin}
    aria-label={ariaLabel}
    onClick={onClick}
  />
);
