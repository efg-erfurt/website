import React from "react";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { LocalizedPage } from "../LocalizedPage";

const Zeitung67Page = () => (
  <LocalizedPage title="pages.zeitung.67.title">
    <CenteredContainer>
      <Section>
        <ContentQuery markdown property="pages.zeitung.67.content" />
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default Zeitung67Page;
