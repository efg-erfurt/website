import { gql, useQuery } from "@apollo/client";
import dayjs from "dayjs";
import { Helmet } from "react-helmet-async";
import React from "react";
import { getToday } from "../../../commons";
import { Error } from "../../components/errors/Error";
import { GroupCards } from "../../components/groups/GroupCards";
import {
  ContentQuery,
  useContentQuery
} from "../../components/i18n/ContentQuery";
import { MediaRow } from "../../components/media/MediaRow";
import { Spinner } from "../../components/media/Spinner";
import { LocalizedPage } from "../LocalizedPage";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Heading } from "../../components/text/Heading";
import { Section } from "../../components/layout/Section";
import { EventDetailContent } from "./EventDetailContent";
import {
  ContactHeader,
  ContactInfo
} from "../../components/contacts/ContactInfo";
import {
  GetEventCategoryQuery,
  GetEventCategoryQueryVariables
} from "../../../types/generated";
import { Navigate, useSearchParams } from "react-router-dom";

const getCategoryWithUpcomingFn = gql`
  query getEventCategory(
    $categoryId: String!
    $end: DateTime!
    $start: DateTime!
  ) {
    eventCategory(id: $categoryId) {
      id
      name
    }
    events(categoryId: $categoryId, start: $start, end: $end) {
      id
      name
      date
      place
      category {
        id
        name
        textColor
        backgroundColor
      }
    }
  }
`;

const EventDetailPageSection = () => {
  const [searchParams] = useSearchParams();
  const categoryId = searchParams.get("id") || "";
  const websiteTitle = useContentQuery("general.title");
  const { data, error, loading } = useQuery<
    GetEventCategoryQuery,
    GetEventCategoryQueryVariables
  >(getCategoryWithUpcomingFn, {
    variables: {
      categoryId,
      end: dayjs(getToday()).add(4, "week").toISOString(),
      start: dayjs(getToday())
    }
  });
  if (loading) {
    return <Spinner />;
  } else if (!data || error) {
    return <Error error={error} />;
  } else if (!data.eventCategory) {
    return <Navigate to="/de/not-found" replace />;
  }

  const {
    events,
    eventCategory: { name: title }
  } = data;

  return (
    <>
      <Helmet>
        <title>
          {websiteTitle} - {title}
        </title>
        <meta property="og:title" content={title} />
      </Helmet>
      <CenteredContainer>
        <Section>
          <Heading className="mb-4 mt-2">{title}</Heading>
          {categoryId === "10" ? (
            <EventDetailContent
              description="pages.events.gottesdienst.intro"
              cards={<GroupCards groupsId="AHiZMJ9JvKj1ey76ucDcn8GTNae8qOpR" />}
              events={events}
            >
              <div className="grid gap-8">
                <ContentQuery
                  markdown
                  property="pages.events.gottesdienst.intro"
                />
                <ContentQuery
                  markdown
                  property="pages.events.gottesdienst.intro2"
                />
                <MediaRow
                  property="pages.events.gottesdienst.pillarA"
                  imageSrc="up-arrow"
                />
                <MediaRow
                  property="pages.events.gottesdienst.pillarB"
                  imageSrc="spinning-arrows"
                  imagePos="right"
                />
                <MediaRow
                  property="pages.events.gottesdienst.pillarC"
                  imageSrc="right"
                />
              </div>
            </EventDetailContent>
          ) : categoryId === "27" ? (
            <EventDetailContent
              description="pages.participate.youth.content"
              cards={<GroupCards groupsId="ibAPusntnxi3hqTBHogr3wUhEqqoaRhP" />}
              events={events}
            >
              <ContentQuery
                markdown
                property="pages.participate.youth.content"
              />
            </EventDetailContent>
          ) : categoryId === "33" ? (
            <EventDetailContent
              description="pages.participate.biblestudies.content"
              cards={<GroupCards groupsId="VmggUjUrQz5S36hNVsJLDc9EMPRfcMuC" />}
              events={events}
            >
              <ContentQuery
                markdown
                property="pages.participate.biblestudies.content"
              />
            </EventDetailContent>
          ) : categoryId === "36" ? (
            <EventDetailContent
              description="pages.participate.cafe.content"
              cards={<GroupCards groupsId="wEUSyg01NvCjUSBHEaTxMez6JAyNnkbs" />}
              events={events}
            >
              <ContentQuery
                markdown
                property="pages.participate.cafe.content"
              />
            </EventDetailContent>
          ) : categoryId === "53" ? (
            <EventDetailContent
              events={events}
              description="pages.support.prayer.content"
            >
              <div className="grid gap-8">
                <ContentQuery
                  markdown
                  property="pages.support.prayer.content"
                />
                <MediaRow
                  property="pages.support.prayer.section.a"
                  imageSrc="pray"
                />
                <div className="grid gap-4">
                  <ContactHeader />
                  <ContactInfo id="13" mail="gebet@efg-erfurt.de" />
                </div>
              </div>
            </EventDetailContent>
          ) : (
            <EventDetailContent
              events={events}
              description="general.description"
            />
          )}
        </Section>
      </CenteredContainer>
    </>
  );
};

// TODO move title to sub pages because of different content (not Gottesdienst)
const EventDetailPage = () => (
  <LocalizedPage showHeader={false} title="pages.events.gottesdienst.title">
    <EventDetailPageSection />
  </LocalizedPage>
);

export default EventDetailPage;
