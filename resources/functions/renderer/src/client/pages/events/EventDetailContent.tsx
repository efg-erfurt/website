import React, { PropsWithChildren } from "react";
import { Event } from "../../../types/generated";
import { EventList } from "../../components/events/EventList";
import {
  ContentQuery,
  useContentQuery
} from "../../components/i18n/ContentQuery";
import { Spacer } from "../../components/layout/Spacer";
import { Heading } from "../../components/text/Heading";
import { Paragraph } from "../../components/text/Paragraph";
import { Helmet } from "react-helmet-async";

export const EventDetailContent = ({
  cards,
  events,
  description,
  children
}: PropsWithChildren<{
  cards?: JSX.Element;
  description: string;
  events: Array<Event>;
}>) => {
  const localizedDescription = useContentQuery(description);
  return (
    <>
      <Helmet>
        <meta
          name="description"
          content={localizedDescription?.slice(0, 155)}
        />
      </Helmet>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-8 lg:gap-12">
        <div className="col-span-1 md:col-span-2">
          {!!children ? (
            children
          ) : (
            <Paragraph>
              Zu dieser Veranstaltungskategorie existiert noch kein angepasster
              Beschreibungstext.
            </Paragraph>
          )}
        </div>
        <div className="mb-4">
          <Heading size={4} className="mb-2">
            <ContentQuery property="components.events.upcoming" />
          </Heading>
          <EventList events={events} />
        </div>
      </div>
      <Spacer />
      {cards}
    </>
  );
};
