import dayjs from "dayjs";
import React, { useState } from "react";
import loadable from "@loadable/component";
import { getToday } from "../../../commons";
import { EventsContent } from "../../components/events/EventList";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { Spinner } from "../../components/media/Spinner";
import { Heading } from "../../components/text/Heading";
import { render } from "../../utils/templating";
import { LocalizedPage } from "../LocalizedPage";

const Calendar = loadable(
  () => import(/* webpackPreload: true */ "../../components/events/Calendar"),
  {
    ssr: false,
    fallback: <Spinner />
  }
);

const EventsPage = () => {
  const today = getToday();
  const [currentState, setState] = useState<{
    timeframe: { start: Date; end: Date };
  }>({
    timeframe: {
      start: today,
      end: dayjs(today).add(1, "week").toDate()
    }
  });
  const { timeframe } = currentState;
  return (
    <LocalizedPage title="pages.events.title">
      <CenteredContainer>
        <Section>
          <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-2 gap-12 mb-8">
            <div>
              <div className="mb-4">
                {dayjs(timeframe.end).diff(timeframe.start, "day") > 1 ? (
                  <ContentQuery property="pages.events.eventsOfWeek">
                    {weekTemplate => (
                      <Heading size={4}>
                        {render(weekTemplate, {
                          week: dayjs(timeframe.start).week() + 1
                        })}
                      </Heading>
                    )}
                  </ContentQuery>
                ) : (
                  <ContentQuery property="pages.events.eventsOfDay">
                    {dayTemplate => (
                      <Heading size={4}>
                        {render(dayTemplate, {
                          day: dayjs(timeframe.start).format("LL")
                        })}
                      </Heading>
                    )}
                  </ContentQuery>
                )}
              </div>
              <EventsContent
                start={currentState.timeframe.start}
                end={currentState.timeframe.end}
              />
            </div>
            <div className="col-auto md:col-span-2 lg:col-auto">
              <Calendar
                timeframeSelected={timeframe => setState({ timeframe })}
              />
            </div>
          </div>
        </Section>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default EventsPage;
