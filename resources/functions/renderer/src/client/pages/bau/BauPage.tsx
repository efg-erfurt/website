import React from "react";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { LocalizedPage } from "../LocalizedPage";
import { Section } from "react-bulma-components";
import { ContentQuery } from "../../components/i18n/ContentQuery";

const BauPage = () => {
  return (
    <LocalizedPage title="pages.bau.title">
      <CenteredContainer>
        <Section>
          <ContentQuery markdown property="pages.bau.content" />
        </Section>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default BauPage;
