import React from "react";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { MediaRow } from "../../components/media/MediaRow";
import { LocalizedPage } from "../LocalizedPage";

const BelievePage = () => (
  <LocalizedPage
    title="pages.believe.title"
    description="pages.believe.section.a"
  >
    <CenteredContainer>
      <Section>
        <div className="grid justify-center gap-8 mb-8">
          <ContentQuery markdown property="pages.believe.content" />
          <MediaRow property="pages.believe.section.a" imageSrc="bible" />
          <MediaRow
            property="pages.believe.section.b"
            imageSrc="signpost"
            imagePos="right"
          />
          <MediaRow property="pages.believe.section.c" imageSrc="community" />
          <MediaRow
            property="pages.believe.section.d"
            imageSrc="helping-hand"
            imagePos="right"
          />
        </div>
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default BelievePage;
