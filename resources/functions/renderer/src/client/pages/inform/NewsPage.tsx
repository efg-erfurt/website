import { useQuery } from "@apollo/client";
import loadable from "@loadable/component";
import React, { useContext, useState } from "react";
import { UpcomingEvents } from "../../components/events/EventList";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { Error } from "../../components/errors/Error";
import { Spinner } from "../../components/media/Spinner";
import { PageContext } from "../../components/page/PageContext";
import { getLatestPostsFn } from "../../components/posts/gql";
import { LocalizedPage } from "../LocalizedPage";
import { Dialog } from "../../components/layout/dialog/Dialog";
import { Button } from "../../components/form/Button";
import { Paragraph } from "../../components/text/Paragraph";
import { Tag } from "../../components/text/Tag";
import { PostPreview } from "../../components/posts/PostPreview";
import { Link } from "../../components/text/Link";
import { Icon } from "../../components/media/Icon";
import {
  GetLatestPostsQuery,
  GetLatestPostsQueryVariables,
  Language as GQLLanguage
} from "../../../types/generated";
import { Spacer } from "../../components/layout/Spacer";
import { useSearchParams } from "react-router-dom";

const PostCreateForm = loadable(
  () => import("../../pages/posts/PostCreateForm")
);
const DetailedLatestPostsList = () => {
  const { user, language } = useContext(PageContext);
  const [showCreatePost, setShowCreatePost] = useState(false);
  const closeDialog = () => setShowCreatePost(false);
  const openDialog = () => setShowCreatePost(true);
  const [searchParams] = useSearchParams();
  const tag = searchParams.get("tag");
  const page = Number(searchParams.get("page")) || 0;
  const size = Number(searchParams.get("size")) || 5;
  const { data, error, loading } = useQuery<
    GetLatestPostsQuery,
    GetLatestPostsQueryVariables
  >(getLatestPostsFn, {
    variables: {
      tag,
      language: language.code ? GQLLanguage.German : GQLLanguage.English,
      pagination: { page, size }
    }
  });
  if (loading) {
    return <Spinner />;
  } else if (!data || error) {
    return <Error error={error} />;
  } else if (data.latestPosts.totalCount === 0) {
    return <ContentQuery markdown property="components.posts.empty" />;
  }

  return (
    <div className="grid gap-8">
      {!!user && (
        <>
          <Dialog
            header="pages.posts.create.title"
            onClose={closeDialog}
            visible={showCreatePost}
          >
            <div className="max-w-3xl">
              <PostCreateForm onSuccess={closeDialog} />
            </div>
          </Dialog>
          <Button id="create-post" color="primary" onClick={openDialog}>
            Neuen Beitrag veröffentlichen
          </Button>
        </>
      )}
      {tag && (
        <Paragraph>
          <ContentQuery property="pages.posts.list.tagFilter">
            {tagInfo => (
              <span>
                {`${tagInfo}:`} <Tag>{tag}</Tag>
              </span>
            )}
          </ContentQuery>
        </Paragraph>
      )}
      <div className="grid gap-8">
        {data.latestPosts.content.map(post => (
          <PostPreview key={`post-${post.id}`} post={post} showSummary />
        ))}
      </div>
      <div className="flex flex-row m-2 justify-center">
        {data.latestPosts.page !== 0 && (
          <Link
            to={`/news?page=${data.latestPosts.page - 1}&size=${size}${
              tag ? `&tag=${tag}` : ""
            }`}
            className="px-2 rounded-sm bg-gray-200 hover:bg-gray-100 hover:text-black"
          >
            <Icon ariaLabel="Vorherige Seite" icon="chevron-left" size="1x" />
          </Link>
        )}
        <div className="mx-4">Seite {data.latestPosts.page + 1}</div>
        {data.latestPosts.page + 1 < data.latestPosts.totalPages && (
          <Link
            to={`/news?page=${data.latestPosts.page + 1}&size=${size}${
              tag ? `&tag=${tag}` : ""
            }`}
            className="px-2 rounded-sm bg-gray-200 hover:bg-gray-100 hover:text-black"
          >
            <Icon ariaLabel="Nächste Seite" icon="chevron-right" size="1x" />
          </Link>
        )}
      </div>
    </div>
  );
};

const NewsPage = () => {
  return (
    <LocalizedPage title="pages.news.title" description="pages.news.content">
      <CenteredContainer>
        <Section>
          <div className="grid sm:grid-cols-1 lg:grid-cols-2 xl:grid-cols-3 gap-8 xl:gap-12">
            <div className="xl:col-span-2">
              <ContentQuery markdown property="pages.news.content" />
              <Spacer size="small" />
              <DetailedLatestPostsList />
            </div>
            <UpcomingEvents />
          </div>
        </Section>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default NewsPage;
