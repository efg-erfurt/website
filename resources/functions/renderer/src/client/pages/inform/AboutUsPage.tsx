import React from "react";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { MediaRow } from "../../components/media/MediaRow";
import { LocalizedPage } from "../LocalizedPage";

const AboutUsePage = () => (
  <LocalizedPage
    title="pages.aboutUs.title"
    description="pages.aboutUs.content"
  >
    <CenteredContainer>
      <Section>
        <div className="grid justify-center gap-8 mb-8">
          <ContentQuery markdown property="pages.aboutUs.content" />
          <MediaRow
            property="pages.aboutUs.section.a"
            imagePos="right"
            imageSrc="friend"
          />
          <MediaRow
            property="pages.aboutUs.section.b"
            imagePos="left"
            imageSrc="church"
          />
          <MediaRow
            property="pages.aboutUs.section.c"
            imagePos="right"
            imageSrc="united"
          />
        </div>
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default AboutUsePage;
