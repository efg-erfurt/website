import { ApolloProvider } from "@apollo/client/react";
import {
  Navigate,
  Route,
  Routes,
  useLocation,
  useParams
} from "react-router-dom";
import { ApolloClient } from "@apollo/client";
import loadable, { OptionsWithoutResolver } from "@loadable/component";
import dayjs from "dayjs";
import LocalizedFormat from "dayjs/plugin/localizedFormat";
import weekOfYear from "dayjs/plugin/weekOfYear";
import utc from "dayjs/plugin/utc";
import React, { useContext } from "react";
import { ErrorHandler } from "../components/errors/ErrorHandler";
import { Languages, getLanguage } from "../components/i18n/languages";
import { LanguageWrapper } from "../components/i18n/LanguageWrapper";
import NotFoundPage from "./errors/NotFoundPage";
import "core-js";
import "regenerator-runtime";
import LoadingPage from "./general/LoadingPage";
import StartPage from "./general/StartPage";
import { ResultContext } from "../../server/renderresult";

if (process.env.WP_TARGET === "web" || process.env.WP_TARGET === `"web"`) {
  require("./app.scss");
}

const LanguageForward = () => (
  <Navigate to={`/${getLanguage().code}`} replace />
);

const NotFoundForward = () => {
  // lang doesn't work for default path...
  const { lang = Languages.DE.code } = useParams();
  const location = useLocation();
  const result = useContext(ResultContext);
  result?.markNotFound();

  if (lang && !location.pathname) {
    return <Navigate to={`/${getLanguage(lang).code}/not-found`} replace />;
  }

  const splittedUri = location.pathname.split("/");

  return (
    <Navigate
      to={`/${
        getLanguage(splittedUri.length > 1 ? splittedUri[1] : undefined).code
      }/not-found`}
      replace
    />
  );
};

const opts: OptionsWithoutResolver<unknown> = {
  fallback: <LoadingPage />
};

const ImprintPage = loadable(() => import("./general/ImprintPage"), opts);
const ContactPage = loadable(() => import("./general/ContactPage"), opts);
const LoginPage = loadable(() => import("./general/LoginPage"), opts);
const SearchPage = loadable(() => import("./general/SearchPage"), opts);
const PostDetailPage = loadable(() => import("./posts/PostDetailPage"), opts);
const PostsPage = loadable(() => import("./posts/PostsPage"), opts);
const EventDetailPage = loadable(
  () => import("./events/EventDetailPage"),
  opts
);
const EventsPage = loadable(() => import("./events/EventsPage"), opts);
const SermonsPage = loadable(() => import("./downloads/SermonsPage"), opts);
const BauPage = loadable(() => import("./bau/BauPage"), opts);
const PrivacyPage = loadable(() => import("./general/PrivacyPage"), opts);
const CafePage = loadable(() => import("./life/CafePage"), opts);
const InternationalPage = loadable(
  () => import("./international/InternationalPage"),
  opts
);
const GroupsPage = loadable(() => import("./life/GroupsPage"), opts);
const AdminGeneralPage = loadable(() => import("./admin/GeneralPage"), opts);
const InformPage = loadable(() => import("./inform/InformPage"), opts);
const SupportPage = loadable(() => import("./support/SupportPage"), opts);
const PrayerPage = loadable(() => import("./support/PrayerPage"), opts);
const ParticipatePage = loadable(
  () => import("./participate/ParticipatePage"),
  opts
);
const AboutUsPage = loadable(() => import("./inform/AboutUsPage"), opts);
const BelievePage = loadable(() => import("./inform/BelievePage"), opts);
const NewsPage = loadable(() => import("./inform/NewsPage"), opts);
const ContributePage = loadable(() => import("./support/ContributePage"), opts);
const DonatePage = loadable(() => import("./support/DonatePage"), opts);
const ProjectsPage = loadable(() => import("./support/ProjectsPage"), opts);
const ScreenPage = loadable(() => import("./info/ScreenPage"), opts);
const Zeitung67Page = loadable(() => import("./zeitung/67"), opts);

dayjs.extend(LocalizedFormat);
dayjs.extend(weekOfYear);
dayjs.extend(utc);

export const App = ({
  client
}: {
  client: ApolloClient<Record<string, unknown>>;
}) => (
  <ApolloProvider client={client}>
    <ErrorHandler>
      <Routes>
        <Route path="/" element={<LanguageForward />} />
        <Route path=":lang" element={<LanguageWrapper />}>
          <Route index element={<StartPage />} />
          <Route path="inform" element={<InformPage />} />
          <Route path="support" element={<SupportPage />} />
          <Route path="prayers" element={<PrayerPage />} />
          <Route path="participate" element={<ParticipatePage />} />
          <Route path="about-us" element={<AboutUsPage />} />
          <Route path="about-our-believe" element={<BelievePage />} />
          <Route path="imprint" element={<ImprintPage />} />
          <Route path="contact" element={<ContactPage />} />
          <Route path="news" element={<NewsPage />} />
          <Route path="post" element={<PostDetailPage />} />
          <Route path="posts" element={<PostsPage />} />
          <Route path="events" element={<EventsPage />} />
          <Route path="event" element={<EventDetailPage />} />
          <Route path="login" element={<LoginPage />} />
          <Route path="search" element={<SearchPage />} />
          <Route path="sermons" element={<SermonsPage />} />
          <Route path="cafe-bau" element={<BauPage />} />
          <Route path="privacy" element={<PrivacyPage />} />
          <Route path="admin" element={<AdminGeneralPage />} />
          <Route path="cafe" element={<CafePage />} />
          <Route path="international" element={<InternationalPage />} />
          <Route path="groups" element={<GroupsPage />} />
          <Route path="contribute" element={<ContributePage />} />
          <Route path="donate" element={<DonatePage />} />
          <Route path="projects" element={<ProjectsPage />} />
          <Route path="not-found" element={<NotFoundPage />} />
          <Route path="info-screen" element={<ScreenPage />} />
          <Route path="zeitung/67" element={<Zeitung67Page />} />
          <Route path="*" element={<NotFoundForward />} />
        </Route>
      </Routes>
    </ErrorHandler>
  </ApolloProvider>
);
