import React from "react";
import { Navigate } from "react-router-dom";

const PrayerPage = () => <Navigate to="/de/event?id=53" replace />;

export default PrayerPage;
