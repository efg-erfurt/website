import React from "react";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { PictureRow } from "../../components/media/PictureRow";
import { LocalizedPage } from "../LocalizedPage";

const ProjectsPage = () => (
  <LocalizedPage title="pages.support.projects.title">
    <CenteredContainer>
      <div className="grid grid-cols-1 gap-8 p-2 pb-8">
        <ContentQuery markdown property="pages.support.projects.content" />
        <PictureRow
          property="pages.support.projects.diospi"
          orientation="right"
          images={[
            { name: "diospi-1", srcType: "jpg" },
            { name: "diospi-3", srcType: "jpg" }
          ]}
        />
        <PictureRow
          property="pages.support.projects.kongo"
          orientation="left"
          images={[
            { name: "yenge-1", srcType: "jpg" },
            { name: "yenge-2", srcType: "jpg" }
          ]}
        />
        <PictureRow
          property="pages.support.projects.barrio"
          orientation="right"
          images={[
            { name: "barrio-1", srcType: "jpg" },
            { name: "barrio-2", srcType: "jpg" }
          ]}
        />
      </div>
    </CenteredContainer>
  </LocalizedPage>
);

export default ProjectsPage;
