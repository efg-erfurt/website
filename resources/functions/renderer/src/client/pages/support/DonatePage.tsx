import React from "react";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { LocalizedPage } from "../LocalizedPage";
import { Link } from "../../components/text/Link";
import { Button } from "../../components/form/Button";
import {
  CenteredColumn,
  CenteredContainer
} from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { MediaRow } from "../../components/media/MediaRow";
import {
  ContactHeader,
  ContactInfo
} from "../../components/contacts/ContactInfo";

const DonatePage = () => (
  <LocalizedPage title="pages.support.donate.title">
    <CenteredContainer>
      <Section>
        <div className="grid justify-center gap-8 mb-8">
          <ContentQuery markdown property="pages.support.donate.content" />
          <MediaRow
            property="pages.support.donate.reasons"
            imageSrc="spende"
            imageAlt="Spende"
          />
          <ContentQuery markdown property="pages.support.donate.ways" />
          <CenteredColumn>
            <ContentQuery property="pages.support.donate.donateNow">
              {property => (
                <Link
                  showArrow={false}
                  to="https://www.paypal.com/donate/?hosted_button_id=FX33WX8F46Z8A"
                >
                  <Button color="primary">{property}</Button>
                </Link>
              )}
            </ContentQuery>
            <div className="text-xs my-4">(über PayPal)</div>
          </CenteredColumn>
          <div className="grid gap-4">
            <ContactHeader />
            <ContactInfo id="338" mail="finanzen@efg-erfurt.de" />
          </div>
        </div>
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default DonatePage;
