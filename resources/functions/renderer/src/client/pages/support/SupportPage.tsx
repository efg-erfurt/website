import React from "react";
import { LocalizedPage } from "../LocalizedPage";
import { navigationStructure } from "../../components/layout/Header";
import { PageNavigation } from "../../components/layout/PageNavigation";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { RobotsValue } from "../../../config";

const element = navigationStructure.topElements[2];

const SupportPage = () => (
  <LocalizedPage
    showHeader={false}
    title={element.property}
    robots={RobotsValue.NO_INDEX_FOLLOW}
  >
    <CenteredContainer>
      <Section>
        <PageNavigation element={element} />
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default SupportPage;
