import React from "react";
import {
  ContactHeader,
  ContactInfo
} from "../../components/contacts/ContactInfo";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { MediaRow } from "../../components/media/MediaRow";
import { LocalizedPage } from "../LocalizedPage";

const ContributePage = () => (
  <LocalizedPage
    title="pages.support.contribute.title"
    description="pages.support.contribute.section.c"
  >
    <CenteredContainer>
      <Section>
        <div className="grid justify-center gap-8 mb-8">
          <MediaRow property="pages.support.contribute.section.a" />
          <MediaRow
            property="pages.support.contribute.section.b"
            imagePos="left"
            imageSrc="participant"
          />
          <MediaRow property="pages.support.contribute.section.c" />
          <div className="grid gap-4">
            <ContactHeader />
            <div className="grid grid-cols-2 md:grid-cols-3 gap-4 md:gap-8">
              <ContactInfo
                id="41"
                position="Pastor (Gottesdienst)"
                mail="ossa@efg-erfurt.de"
              />
              <ContactInfo
                id="13"
                position="Älteste (Gebet)"
                mail="aelteste@efg-erfurt.de"
              />
              <ContactInfo
                id="241"
                position="Ältester"
                mail="aelteste@efg-erfurt.de"
              />
              <ContactInfo
                id="437"
                position="Gemeindeleiter"
                mail="gemeindeleiter@efg-erfurt.de"
              />
              <ContactInfo
                id="407"
                position="Stellvertretende Gemeindeleiterin"
                mail="gemeindeleiter@efg-erfurt.de"
              />
              <ContactInfo
                id="286"
                position="Internationale Gemeinde"
                mail="international@efg-erfurt.de"
              />
              <ContactInfo
                id="389"
                position="Gottesdienst"
                mail="gottesdienst@efg-erfurt.de"
              />
              <ContactInfo
                id="3070"
                position="Stadtteil & Café"
                mail="stadtteilarbeit@efg-erfurt.de"
              />
              <ContactInfo
                id="338"
                position="Finanzen"
                mail="finanzen@efg-erfurt.de"
              />
              <ContactInfo
                id="32"
                position="Verwaltung/Vermietung "
                mail="verwaltung@efg-erfurt.de"
              />
              <ContactInfo
                id="1168"
                position="Öffentlichkeitsarbeit"
                mail="oeffentlichkeitsarbeit@efg-erfurt.de"
              />
            </div>
          </div>
        </div>
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default ContributePage;
