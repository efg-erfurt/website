import React, { useContext } from "react";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { Link } from "../../components/text/Link";
import { LocalizedPage } from "../LocalizedPage";
import { ResultContext } from "../../../server/renderresult";

const NotFoundPage = () => {
  const result = useContext(ResultContext);
  result?.markNotFound();

  return (
    <LocalizedPage title="pages.error.404.title">
      <CenteredContainer>
        <Section>
          <ContentQuery markdown property="pages.error.404.content" />
          <Link to="/">Start</Link>
        </Section>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default NotFoundPage;
