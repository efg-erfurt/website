import React from "react";
import { Navigate } from "react-router-dom";

const CafePage = () => <Navigate to="/de/event?id=36" replace />;

export default CafePage;
