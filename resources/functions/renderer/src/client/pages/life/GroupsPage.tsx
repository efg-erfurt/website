import React from "react";
import { GroupCards } from "../../components/groups/GroupCards";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { Spacer } from "../../components/layout/Spacer";
import { Heading } from "../../components/text/Heading";
import { LocalizedPage } from "../LocalizedPage";

const GroupsPage = () => (
  <LocalizedPage title="pages.participate.groups.title">
    <CenteredContainer>
      <Section>
        <Heading size={3}>
          <ContentQuery property="pages.participate.homegroups.title" />
        </Heading>
        <ContentQuery
          markdown
          property="pages.participate.homegroups.content"
        />
        <Spacer />
        <GroupCards groupsId="ga0GuOSykYYjVzlrYxPhsJ68P8YWsR8s" />
      </Section>
      <Section>
        <Heading size={3} className="mt-16">
          <ContentQuery property="pages.participate.biblestudies.title" />
        </Heading>
        <ContentQuery
          markdown
          property="pages.participate.biblestudies.content"
        />
        <Spacer />
        <GroupCards groupsId="VmggUjUrQz5S36hNVsJLDc9EMPRfcMuC" />
      </Section>
      <Section>
        <Heading size={3} className="mt-16">
          <ContentQuery property="pages.participate.youth.title" />
        </Heading>
        <ContentQuery markdown property="pages.participate.youth.content" />
        <Spacer />
        <GroupCards groupsId="ibAPusntnxi3hqTBHogr3wUhEqqoaRhP" />
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default GroupsPage;
