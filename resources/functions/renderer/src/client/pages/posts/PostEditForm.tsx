import React from "react";
import { gql, useMutation } from "@apollo/client";
import { Language } from "../../components/i18n/languages";
import { getPostFn } from "./PostDetailPage";
import { PostUpdateForm } from "./PostUpdateForm";
import { getLatestPostsFn } from "../../components/posts/gql";
import {
  GetLatestPostsQueryVariables,
  GetPostQueryVariables,
  Post,
  UpdatePostMutation,
  UpdatePostMutationVariables,
  Language as GQLLanguage
} from "../../../types/generated";

export interface PostEditFormProps {
  post: Post;
  language: Language;
  onSuccess: () => void;
}

const updatePostFn = gql`
  mutation updatePost($post: UpdatePostInput!) {
    updatePost(post: $post) {
      id
    }
  }
`;

const PostEditForm = ({ post, language, onSuccess }: PostEditFormProps) => {
  const [updatePost] = useMutation<
    UpdatePostMutation,
    UpdatePostMutationVariables
  >(updatePostFn, {
    awaitRefetchQueries: true,
    refetchQueries: [
      {
        query: getPostFn,
        variables: {
          id: post.id
        } as GetPostQueryVariables
      },
      {
        query: getLatestPostsFn,
        variables: {
          language:
            language.code === "de" ? GQLLanguage.German : GQLLanguage.English,
          pagination: {}
        } as GetLatestPostsQueryVariables
      }
    ]
  });
  return (
    <PostUpdateForm
      post={post}
      onSubmit={async editedPost => {
        await updatePost({
          variables: {
            post: {
              id: post.id,
              language: language.code,
              content: editedPost.content,
              title: editedPost.header,
              imageUrl: editedPost.imageUrl,
              tags: editedPost.tags
            }
          }
        });
        onSuccess();
      }}
    />
  );
};

export default PostEditForm;
