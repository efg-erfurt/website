import React from "react";
import { Field, Form as FinalForm } from "react-final-form";
import { InputAdapter } from "../../components/form/Input";
import type { FormErrorsMap } from "../../components/form/util";
import { MarkdownInputAdapter } from "../../components/form/MarkdownInput";
import { Button, ActionsToolbar } from "../../components/form/Button";
import { useContentsQuery } from "../../components/i18n/ContentQuery";
import { Notification } from "../../components/errors/Notification";
import { TagsInputAdapter } from "../../components/form/Tags";
import { ImageInputAdapter } from "../../components/form/ImageInput";
import { Message } from "../../components/errors/Message";
import { Post } from "../../../types/generated";

interface UpdatePostForm {
  id?: string;
  header: string;
  content: string;
  imageUrl: string | null;
  tags: Array<string>;
}

export const PostUpdateForm = ({
  onSubmit,
  post
}: {
  post?: Post;
  onSubmit: (post: UpdatePostForm) => Promise<void>;
}) => {
  const {
    "pages.posts.create.emptyHeader": emptyHeader,
    "pages.posts.create.emptyContent": emptyContent,
    "pages.posts.create.header": header,
    "pages.posts.create.content": content,
    "pages.posts.create.success": success,
    "components.editing.publish": publish,
    "components.editing.save": save
  } = useContentsQuery([
    "pages.posts.create.emptyHeader",
    "pages.posts.create.emptyContent",
    "pages.posts.create.header",
    "pages.posts.create.content",
    "pages.posts.create.success",
    "components.editing.publish",
    "components.editing.save"
  ]);

  return (
    <FinalForm<UpdatePostForm>
      initialValues={{
        content: post?.content,
        tags: post?.tags,
        header: post?.title,
        imageUrl: post?.imageUrl
      }}
      onSubmit={onSubmit}
      validate={values => {
        const errors: FormErrorsMap<UpdatePostForm> = {};
        if (!values.header) {
          errors.header = emptyHeader;
        }
        if (!values.content) {
          errors.content = emptyContent;
        }
        return errors;
      }}
      render={({
        handleSubmit,
        submitting,
        pristine,
        hasValidationErrors,
        submitSucceeded
      }) => (
        <form onSubmit={handleSubmit}>
          {submitSucceeded && (
            <Notification type="success">{success}</Notification>
          )}
          <Field<string>
            id="header"
            name="header"
            component={InputAdapter}
            fullWidth
            label={header}
            required
          />
          <Field<string>
            id="content"
            name="content"
            component={MarkdownInputAdapter}
            label={content}
            required
          />
          <Field<string>
            id="imageUrl"
            name="imageUrl"
            component={ImageInputAdapter}
            fullWidth
            label="Vorschau"
          />
          <Message title="Bildeigenschaften">
            Es wird ein JPG-Bild mit einer Auflösung von 250x250 Pixeln
            empfohlen.
          </Message>
          <Field<Array<string>>
            id="tags"
            name="tags"
            component={TagsInputAdapter}
            label="Tags"
          />
          <ActionsToolbar>
            <div />
            <Button
              color="primary"
              type="submit"
              loading={submitting}
              disabled={pristine || hasValidationErrors}
            >
              {!!post ? save : publish}
            </Button>
          </ActionsToolbar>
        </form>
      )}
    />
  );
};
