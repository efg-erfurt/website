import React, { useContext } from "react";
import { gql, useMutation } from "@apollo/client";
import { PageContext } from "../../components/page/PageContext";
import { PostUpdateForm } from "./PostUpdateForm";
import { getLatestPostsFn } from "../../components/posts/gql";
import {
  CreatePostMutation,
  CreatePostMutationVariables,
  GetLatestPostsQueryVariables,
  Language as GQLLanguage
} from "../../../types/generated";

const createPostFn = gql`
  mutation createPost($post: CreatePostInput!) {
    createPost(post: $post) {
      id
    }
  }
`;

interface CreatePostForm {
  header: string;
  content: string;
  tags: Array<string>;
  imageUrl: string | null;
}

const PostCreateForm = ({ onSuccess }: { onSuccess: () => void }) => {
  const { language } = useContext(PageContext);
  const [createPost] = useMutation<
    CreatePostMutation,
    CreatePostMutationVariables
  >(createPostFn);
  const onSubmit: (arg: CreatePostForm) => Promise<void> = async ({
    content,
    header: title,
    imageUrl,
    tags
  }) => {
    await createPost({
      variables: {
        post: {
          language: language.code,
          content,
          title,
          imageUrl,
          tags: tags || []
        }
      },
      awaitRefetchQueries: true,
      refetchQueries: [
        {
          query: getLatestPostsFn,
          variables: {
            language:
              language.code === "de" ? GQLLanguage.German : GQLLanguage.English
          } as GetLatestPostsQueryVariables
        }
      ]
    });
    onSuccess();
  };

  return <PostUpdateForm onSubmit={onSubmit} />;
};

export default PostCreateForm;
