import { gql, useMutation, useQuery } from "@apollo/client";
import loadable from "@loadable/component";
import dayjs from "dayjs";
import React, { useContext, useState } from "react";
import { Helmet } from "react-helmet-async";
import { Error } from "../../components/errors/Error";
import { Button, ButtonGroup } from "../../components/form/Button";
import {
  ContentQuery,
  useContentsQuery
} from "../../components/i18n/ContentQuery";
import { Language } from "../../components/i18n/languages";
import { Breadcrumbs } from "../../components/layout/Breadcrumbs";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Icon } from "../../components/media/Icon";
import { Spinner } from "../../components/media/Spinner";
import { PageContext } from "../../components/page/PageContext";
import { Tags } from "../../components/text/Tag";
import { Heading } from "../../components/text/Heading";
import { Markdown } from "../../components/text/Markdown";
import { LocalizedPage } from "../LocalizedPage";
import { Dialog } from "../../components/layout/dialog/Dialog";
import { Spacer } from "../../components/layout/Spacer";
import { PostHeader } from "../../components/posts/PostPreview";
import { getLatestPostsFn } from "../../components/posts/gql";
import {
  DeletePostMutation,
  DeletePostMutationVariables,
  GetLatestPostsQueryVariables,
  GetPostQuery,
  GetPostQueryVariables,
  Language as GQLLanguage
} from "../../../types/generated";
import { useSearchParams } from "react-router-dom";
import { Paragraph } from "../../components/text/Paragraph";
import { ResultContext } from "../../../server/renderresult";

const PostEditForm = loadable(() => import("./PostEditForm"));

export const getPostFn = gql`
  query getPost($id: String!) {
    post(id: $id) {
      id
      title
      summary
      content
      publishDate
      date
      imageUrl
      language
      creator {
        id
        firstName
        lastName
        name
      }
      tags
    }
  }
`;

const deletePostFn = gql`
  mutation deletePost($id: String!) {
    deletePost(id: $id)
  }
`;

const PostDetailPage = () => {
  const [searchParams] = useSearchParams();
  const postId = searchParams.get("id");
  const { language } = useContext(PageContext);
  if (!postId) {
    return (
      <LocalizedPage title="pages.posts.detail.title">
        <CenteredContainer>
          <PostNotFound />
        </CenteredContainer>
      </LocalizedPage>
    );
  }

  return (
    <LocalizedPage showHeader={false} title="pages.posts.detail.title">
      <CenteredContainer>
        <PostDetailView language={language} postId={postId} />
      </CenteredContainer>
    </LocalizedPage>
  );
};

const PostDetailView = ({
  postId,
  language
}: {
  postId: string;
  language: Language;
}) => {
  const [isInEditMode, setInEditMode] = useState(false);
  const closeDialog = () => setInEditMode(false);
  const openDialog = () => setInEditMode(true);
  const { user, protocol, origin } = useContext(PageContext);
  const {
    "components.editing.edit": editAction,
    "general.title": websiteTitle
  } = useContentsQuery(["components.editing.edit", "general.title"]);
  const { data, error, loading } = useQuery<
    GetPostQuery,
    GetPostQueryVariables
  >(getPostFn, {
    variables: { id: postId }
  });
  const [deletePost] = useMutation<
    DeletePostMutation,
    DeletePostMutationVariables
  >(deletePostFn, {
    awaitRefetchQueries: true,
    refetchQueries: [
      {
        query: getLatestPostsFn,
        variables: {
          language:
            language.code === "de" ? GQLLanguage.German : GQLLanguage.English,
          pagination: {}
        } as GetLatestPostsQueryVariables
      },
      {
        query: getPostFn,
        variables: {
          id: postId
        } as GetPostQueryVariables
      }
    ]
  });
  if (loading) {
    return <Spinner />;
  } else if (!data || error) {
    return <Error error={error} />;
  } else if (!data.post) {
    return <PostNotFound />;
  }

  const { title, content, publishDate, date, tags, id, imageUrl } = data.post;

  const parsedPublishDate = dayjs(publishDate);
  const parsedModificationDate = dayjs(date);
  const languageCode = language.code.toLowerCase();

  return (
    <>
      <Helmet>
        <title>
          {websiteTitle} - {title}
        </title>
        <meta property="og:title" content={title} />
        <script type="application/ld+json">
          {`{
            "@context":"http://schema.org/",
            "@type":"BlogPosting",
            "url": "${protocol}//${origin}/${language.code}/post?id=${id}",
            "name":${JSON.stringify(title)},
            "headline":${JSON.stringify(title)},
            "articleBody":${JSON.stringify(content) || ""},
            "image": [${!!imageUrl ? JSON.stringify(imageUrl) : ""}],      
            "inLanguage":"${languageCode}-${languageCode}",
            "datePublished":"${parsedPublishDate.toISOString()}",
            "dateModified":"${parsedModificationDate.toISOString()}",
            "keywords":[${tags
              .map(t => JSON.stringify(t))
              .reduce((a, b) => (a ? `${a}, ${b}` : b), "")}]
          }`}
        </script>
      </Helmet>
      <Breadcrumbs
        items={[
          { property: "pages.posts.detail.title", to: "/news" },
          { text: title, to: null }
        ]}
      />
      <Spacer size="small" />
      <article lang={languageCode}>
        <div className="flex mb-8 justify-between items-start">
          <PostHeader post={data.post} isSinglePost>
            {!isInEditMode ? (
              <div className="my-4">
                <Markdown content={content} />
              </div>
            ) : (
              <Dialog
                header="components.editing.edit"
                visible={isInEditMode}
                onClose={closeDialog}
              >
                <PostEditForm
                  post={data.post}
                  language={language}
                  onSuccess={closeDialog}
                />
              </Dialog>
            )}
            {tags.length > 0 && (
              <Tags items={tags} toFn={t => `/news?tag=${t}`} />
            )}
          </PostHeader>
          {user && !isInEditMode ? (
            <ButtonGroup>
              <Button onClick={openDialog}>{editAction}</Button>
              <Button
                color="danger"
                onClick={async () => {
                  await deletePost({ variables: { id } });
                }}
              >
                <Icon ariaLabel="Schließen" icon="times" />
              </Button>
            </ButtonGroup>
          ) : null}
        </div>
      </article>
    </>
  );
};

const PostNotFound = () => {
  const result = useContext(ResultContext);
  result?.markNotFound();

  return (
    <>
      <Heading size={2}>
        <ContentQuery property="pages.posts.notFound.title" />
      </Heading>
      <Paragraph>
        <ContentQuery property="pages.posts.notFound.content" />
      </Paragraph>
      <Spacer />
    </>
  );
};

export default PostDetailPage;
