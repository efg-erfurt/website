import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import { PageContext } from "../../components/page/PageContext";

const PostsPage = () => {
  const { language } = useContext(PageContext);

  return <Navigate to={`/${language.code}/news`} replace />;
};

export default PostsPage;
