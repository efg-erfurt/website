import React from "react";
import { LocalizedPage } from "../LocalizedPage";
import { Section, Heading } from "react-bulma-components";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Card, CardGroup } from "../../components/layout/Card";
import { ContactInfo } from "../../components/contacts/ContactInfo";
import { Spacer } from "../../components/layout/Spacer";

const InternationalPage = () => (
  <LocalizedPage
    title="pages.international.title"
    description="pages.international.description"
    isRtl
  >
    <CenteredContainer>
      <Section>
        <Heading size={3}>
          <ContentQuery property="pages.international.intro.title" />
        </Heading>
        <ContentQuery markdown property="pages.international.intro.content" />
      </Section>
      <Spacer />
      <Section>
        <Heading size={3}>
          <ContentQuery property="pages.international.events.title" />
        </Heading>
        <ContentQuery markdown property="pages.international.events.content" />
        <CardGroup>
          <ContentQuery property="pages.international.events.cafe.title">
            {title => (
              <Card title={title}>
                <ContentQuery
                  markdown
                  property="pages.international.events.cafe.content"
                />
              </Card>
            )}
          </ContentQuery>
          <ContentQuery property="pages.international.events.service.title">
            {title => (
              <Card title={title}>
                <ContentQuery
                  markdown
                  property="pages.international.events.service.content"
                />
              </Card>
            )}
          </ContentQuery>
          <ContentQuery property="pages.international.events.studies.title">
            {title => (
              <Card title={title}>
                <ContentQuery
                  markdown
                  property="pages.international.events.studies.content"
                />
              </Card>
            )}
          </ContentQuery>
        </CardGroup>
      </Section>
      <Spacer />
      <Section>
        <div className="grid gap-4">
          <ContentQuery property="pages.international.contact.title">
            {title => <div className="text-xl font-bold">{title}</div>}
          </ContentQuery>
          <div className="grid grid-cols-2 md:grid-cols-3 gap-4 md:gap-8">
            <ContactInfo
              id="286"
              position="Internationale Gemeinde"
              mail="international@efg-erfurt.de"
            />
          </div>
          <ContentQuery property="pages.international.directions.title">
            {title => <div className="text-xl font-bold">{title}</div>}
          </ContentQuery>
          <ContentQuery property="pages.international.directions.content">
            {title => <div>{title}</div>}
          </ContentQuery>
        </div>
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default InternationalPage;
