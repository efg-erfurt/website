import { gql, useMutation } from "@apollo/client";
import React from "react";
import {
  ClearCacheMutation,
  ClearCacheMutationVariables,
  FillPropertiesMutation,
  FillPropertiesMutationVariables,
  Language,
  UpdateMonitorMutation,
  UpdateMonitorMutationVariables
} from "../../../types/generated";
import { Message } from "../../components/errors/Message";
import { Button } from "../../components/form/Button";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { Card, CardGroup } from "../../components/layout/Card";
import { navigationStructure } from "../../components/layout/Header";
import { Link } from "../../components/text/Link";
import { Paragraph } from "../../components/text/Paragraph";
import AdminPage from "./AdminPage";

const clearCacheFn = gql`
  mutation clearCache {
    clearCache
  }
`;

const fillPropertiesFn = gql`
  mutation fillProperties($language: Language!) {
    fillProperties(language: $language) {
      imported
      total
    }
  }
`;

const updateMonitorFn = gql`
  mutation updateMonitor($language: Language!) {
    updateMonitor(language: $language)
  }
`;

const PropertyRow = ({
  label,
  property,
  multiline = false,
  url = false
}: {
  label: string;
  property: string;
  multiline?: boolean;
  url?: boolean;
}) => (
  <div>
    <div>
      <b>{label}</b>
    </div>
    <div className={url ? "break-all" : ""}>
      <ContentQuery markdown={multiline} property={property} />
    </div>
  </div>
);

const GeneralPage = () => {
  const { topElements } = navigationStructure;
  const [clearCache] = useMutation<
    ClearCacheMutation,
    ClearCacheMutationVariables
  >(clearCacheFn);
  const [fillProperties] = useMutation<
    FillPropertiesMutation,
    FillPropertiesMutationVariables
  >(fillPropertiesFn);
  const [updateMonitor] = useMutation<
    UpdateMonitorMutation,
    UpdateMonitorMutationVariables
  >(updateMonitorFn);
  return (
    <AdminPage>
      <Message title="Beachtung der Lokalisierung">
        Bitte beachte bei der Anpassung der Properties, dass diese lokalisiert
        sind. Änderungen sollten daher für alle Sprachen gleichermaßen
        durchgeführt werden. Wenn eine Property in anderen Sprachen leer ist,
        wird der deutsche Wert angezeigt.
      </Message>
      <div className="mb-12" />
      <CardGroup>
        <Card title="Generelles">
          <PropertyRow label="Titel" property="general.title" />
          <PropertyRow label="Untertitel" property="general.subTitle" />
          <PropertyRow label="Beschreibung" property="general.description" />
        </Card>
        <Card title="Navigation">
          <ul className="list-disc list-outside pl-2 ml-2">
            {topElements.map(({ property, subElements }, tIndex) => (
              <li key={`nav-${tIndex}`}>
                <ContentQuery property={property} />
                <ul className="list-disc list-outside pl-2 ml-2">
                  {subElements.map(({ property }, sIndex) => (
                    <li key={`nav-${tIndex}-${sIndex}`}>
                      <ContentQuery property={property} />
                    </li>
                  ))}
                </ul>
              </li>
            ))}
          </ul>
        </Card>
        <Card title="Churchtools">
          <Paragraph>
            Alle Anfragen zu ChurchTools wie die Nutzerlogins sowie Inhalte wie
            Gruppen oder Veranstaltungen werden für maximal einen Tag in einem
            Cache zwischengespeichert, um die Anzahl von Anfragen zu reduzieren
            sowie die Geschwindigkeit der Webseite zu erhöhen.
          </Paragraph>
          <Button
            onClick={async () => {
              await clearCache({ optimisticResponse: { clearCache: true } });
            }}
          >
            <ContentQuery readonly property="pages.admin.clearCache" />
          </Button>
          <Button
            onClick={async () => {
              await fillProperties({
                variables: { language: Language.German }
              });
            }}
          >
            Übersetzung aktualisieren (Deutsch)
          </Button>
          <Button
            onClick={async () => {
              await fillProperties({
                variables: { language: Language.English }
              });
            }}
          >
            Übersetzung aktualisieren (Englisch)
          </Button>
        </Card>
        <Card title="Social Media">
          <PropertyRow
            label="Facebook"
            property="general.socialMedia.facebook"
          />
          <PropertyRow
            label="Instagram"
            property="general.socialMedia.instagram"
          />
          <PropertyRow
            label="Whatsapp"
            property="general.socialMedia.whatsapp"
          />
        </Card>
        <Card title="Login">
          <PropertyRow
            label="Information"
            property="pages.login.content"
            multiline
          />
        </Card>
        <Card title="Infoscreen">
          <PropertyRow
            label="Kalender URL"
            property="general.info.calendarUrl"
            multiline
            url
          />
          <PropertyRow
            label="Bilder Außen URL (ZIP)"
            property="general.info.imagesUrl"
            multiline
            url
          />
          <PropertyRow
            label="Bilder Innen URL (ZIP)"
            property="general.info.screensUrl"
            multiline
            url
          />
          <div className="flex">
            <div className="w-1/3">
              <b>Anzeige</b>
            </div>
            <div className="w-2/3">
              <p>
                <b>Außen</b>
                <br />
                <Link to="/info-screen">Bilder und Veranstaltungen</Link>
                <br />
                <Link to="/info-screen?mode=content">nur Bilder</Link>
                <br />
                <Link to="/info-screen?mode=events">nur Veranstaltungen</Link>
              </p>
              <p>
                <b>Innen</b>
                <br />
                <Link to="/info-screen?type=internal">
                  Bilder und Veranstaltungen
                </Link>
                <br />
                <Link to="/info-screen?mode=content&type=internal">
                  nur Bilder
                </Link>
                <br />
                <Link to="/info-screen?mode=events&type=internal">
                  nur Veranstaltungen
                </Link>
              </p>
            </div>
          </div>
          <Button
            onClick={async () => {
              // TODO EFG_15 use editor
              await updateMonitor({
                optimisticResponse: { updateMonitor: true },
                variables: { language: Language.German }
              });
            }}
          >
            <ContentQuery readonly property="pages.admin.updateMonitor" />
          </Button>
        </Card>
        <Card title="Zeitung">
          <PropertyRow
            label="Bild URL"
            property="components.zeitung.imgUrl"
            multiline
            url
          />
          <PropertyRow
            label="PDF URL"
            property="components.zeitung.pdfUrl"
            multiline
            url
          />
        </Card>
      </CardGroup>
    </AdminPage>
  );
};

export default GeneralPage;
