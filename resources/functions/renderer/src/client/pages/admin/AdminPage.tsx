import React, { useContext } from "react";
import { SideMenu } from "../../components/admin/SideMenu";
import { Message } from "../../components/errors/Message";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { PageContext } from "../../components/page/PageContext";
import { LocalizedPage, PageProps } from "../LocalizedPage";

const AdminPage = ({ children }: PageProps) => {
  const { user } = useContext(PageContext);
  if (!user) {
    return (
      <LocalizedPage title="pages.admin.title">
        <CenteredContainer>
          <Section>
            <Message type="error" title="Login notwendig">
              Diese Seite ist nur bei erfolgreichem Login sichtbar.
            </Message>
          </Section>
        </CenteredContainer>
      </LocalizedPage>
    );
  }

  return (
    <LocalizedPage title="pages.admin.title">
      <CenteredContainer>
        <Section>
          <div className="flex">
            <SideMenu />
            <div className="ml-4 flex-1">{children}</div>
          </div>
        </Section>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default AdminPage;
