import React, { PropsWithChildren, useContext } from "react";
import { Helmet } from "react-helmet-async";
import { useContentsQuery } from "../components/i18n/ContentQuery";
import { Footer } from "../components/layout/Footer";
import { TopHeader as Header } from "../components/layout/Header";
import { RobotsValue } from "../../config";
import { CenteredContainer } from "../components/layout/CenteredContent";
import { Heading } from "../components/text/Heading";
import { PageContext } from "../components/page/PageContext";
import { Languages } from "../components/i18n/languages";

export type PageProps = PropsWithChildren<unknown>;

export interface LocalizedPageProps extends PageProps {
  title: string;
  description?: string;
  showHeader?: boolean;
  robots?: RobotsValue;
  isRtl?: boolean;
}

export const LocalizedPage = ({
  title,
  description = "general.description",
  robots = RobotsValue.INDEX_FOLLOW,
  isRtl = false,
  showHeader = true,
  children
}: LocalizedPageProps) => {
  const {
    [title]: localizedTitle,
    [description]: localizedDescription,
    "general.title": websiteTitle
  } = useContentsQuery([title, "general.title", description]);
  const { origin, language, path, protocol } = useContext(PageContext);

  return (
    <>
      {!!localizedTitle && (
        <>
          <MetaHeader
            robots={
              language.code === Languages.DE.code
                ? robots
                : RobotsValue.NO_INDEX_FOLLOW
            }
            localizedTitle={localizedTitle}
            websiteTitle={websiteTitle}
          />
          <Helmet>
            <meta
              name="description"
              content={localizedDescription.slice(0, 155)}
            />
            <meta property="og:url" content={`${protocol}//${origin}${path}`} />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={localizedTitle} />
            <meta property="og:description" content={localizedDescription} />
            <meta property="og:locale" content={language.locale} />
            <meta
              property="og:image"
              content={`${protocol}//${origin}/public/images/logo.png`}
            />
            <html dir={isRtl ? "rtl" : "ltr"} />
          </Helmet>
        </>
      )}
      <Header />
      {showHeader && !!title ? (
        <>
          <CenteredContainer>
            <div className="p-2 mb-4 mt-2">
              <Heading>{localizedTitle}</Heading>
            </div>
          </CenteredContainer>
          {children}
        </>
      ) : (
        children
      )}
      <Footer />
    </>
  );
};

export const MinimalPage = ({
  title,
  robots = RobotsValue.INDEX_FOLLOW,
  children
}: Omit<LocalizedPageProps, "showHeader">) => {
  const { [title]: localizedTitle, "general.title": websiteTitle } =
    useContentsQuery([title, "general.title"]);
  return (
    <>
      <MetaHeader
        robots={robots}
        localizedTitle={localizedTitle}
        websiteTitle={websiteTitle}
      />
      <div>{children}</div>
    </>
  );
};

const MetaHeader = ({
  robots,
  localizedTitle,
  websiteTitle
}: {
  robots: RobotsValue;
  websiteTitle: string;
  localizedTitle: string;
}) => (
  <Helmet>
    <title>{`${websiteTitle} - ${localizedTitle}`}</title>
    <meta name="robots" content={robots} />
  </Helmet>
);
