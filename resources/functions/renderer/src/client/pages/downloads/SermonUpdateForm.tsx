import { gql, useMutation } from "@apollo/client";
import React, { useContext } from "react";
import {
  GetSermonsQueryVariables,
  Sermon,
  UpdateSermonMutation,
  UpdateSermonMutationVariables
} from "../../../types/generated";
import { PageContext } from "../../components/page/PageContext";
import { SermonEditForm, SermonForm } from "./SermonEditForm";
import { getSermonsFn } from "./PaginatedSermons";

const updateSermonFn = gql`
  mutation updateSermon($sermon: UpdateSermonInput!) {
    updateSermon(sermon: $sermon) {
      id
    }
  }
`;

const SermonUpdateForm = ({
  existingSermon,
  onSuccess
}: {
  existingSermon: Sermon;
  onSuccess: () => void;
}) => {
  const { language } = useContext(PageContext);
  const [updateSermon] = useMutation<
    UpdateSermonMutation,
    UpdateSermonMutationVariables
  >(updateSermonFn);
  const onSubmit: (arg: SermonForm) => Promise<void> = async ({
    id,
    title,
    speaker,
    comment,
    audioUrl,
    textUrl,
    translationUrl,
    videoUrl,
    talkDate,
    tags
  }) => {
    if (!id) {
      throw new Error("ID missing");
    }
    await updateSermon({
      variables: {
        sermon: {
          id,
          title,
          speaker,
          comment,
          audioUrl: audioUrl || null,
          videoUrl: videoUrl || null,
          textUrl: textUrl || null,
          translationUrl: translationUrl || null,
          talkDate: talkDate.value || new Date(),
          language: language.code,
          tags: tags || []
        }
      },
      awaitRefetchQueries: true,
      refetchQueries: [
        {
          query: getSermonsFn,
          variables: { language: language.code } as GetSermonsQueryVariables
        }
      ]
    });
    onSuccess();
  };

  return <SermonEditForm existingSermon={existingSermon} onSubmit={onSubmit} />;
};

export default SermonUpdateForm;
