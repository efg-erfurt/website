import React from "react";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { LocalizedPage } from "../LocalizedPage";
import { Section } from "react-bulma-components";
import { PaginatedSermons } from "./PaginatedSermons";

const PredigtenPage = () => {
  return (
    <LocalizedPage title="pages.sermons.title">
      <CenteredContainer>
        <Section>
          <PaginatedSermons />
        </Section>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default PredigtenPage;
