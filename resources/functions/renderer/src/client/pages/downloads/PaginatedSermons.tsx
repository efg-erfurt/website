import React, { useContext } from "react";
import { SermonCards } from "../../components/downloads/SermonCards";
import { Link } from "../../components/text/Link";
import { gql, useQuery } from "@apollo/client";
import { Spinner } from "../../components/media/Spinner";
import { Error } from "../../components/errors/Error";
import { useSearchParams } from "react-router-dom";
import {
  GetSermonsQuery,
  GetSermonsQueryVariables,
  Language as GQLLanguage
} from "../../../types/generated";
import { PageContext } from "../../components/page/PageContext";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { Spacer } from "../../components/layout/Spacer";
import { Paragraph } from "../../components/text/Paragraph";
import { Icon } from "../../components/media/Icon";

export const getSermonsFn = gql`
  query getSermons(
    $language: Language!
    $speaker: String
    $pagination: PaginationInput!
  ) {
    sermons(language: $language, speaker: $speaker, pagination: $pagination) {
      content {
        id
        title
        comment
        publishDate
        talkDate
        audioUrl
        textUrl
        translationUrl
        videoUrl
        tags
        speaker
        language
      }
      totalPages
      totalCount
      page
      count
    }
  }
`;

export const PaginatedSermons = () => {
  const {
    language: { code }
  } = useContext(PageContext);
  const [searchParams] = useSearchParams();
  const speaker = searchParams.get("speaker");
  const page = Number(searchParams.get("page") || 0);
  const size = Number(searchParams.get("size") || 18);

  const { data, error, loading } = useQuery<
    GetSermonsQuery,
    GetSermonsQueryVariables
  >(getSermonsFn, {
    variables: {
      language: code === "de" ? GQLLanguage.German : GQLLanguage.English,
      speaker,
      pagination: { page, size }
    }
  });

  if (loading) {
    return <Spinner />;
  } else if (!data || error) {
    return <Error error={error} />;
  }

  return (
    <>
      {speaker && (
        <>
          <Paragraph>
            <ContentQuery property="pages.sermons.list.speakerFilter">
              {tagInfo => (
                <span>
                  {`${tagInfo}:`} {speaker}
                </span>
              )}
            </ContentQuery>
          </Paragraph>
          <Spacer size="small" />
        </>
      )}

      {data.sermons.totalCount === 0 ? (
        <ContentQuery markdown property="pages.sermons.empty" />
      ) : (
        <>
          <SermonCards sermons={data} />

          <Spacer size="large" />

          <div className="flex flex-row m-2 justify-center mb-8">
            {data.sermons.page !== 0 && (
              <Link
                to={`/sermons?page=${data.sermons.page - 1}&size=${size}${
                  speaker ? `&speaker=${speaker}` : ""
                }`}
                className="px-2 rounded-sm bg-gray-200 hover:bg-gray-100 hover:text-black"
              >
                <Icon
                  ariaLabel="Vorherige Seite"
                  icon="chevron-left"
                  size="1x"
                />
              </Link>
            )}
            <div className="mx-4">Seite {data.sermons.page + 1}</div>
            {data.sermons.page + 1 < data.sermons.totalPages && (
              <Link
                to={`/sermons?page=${data.sermons.page + 1}&size=${size}${
                  speaker ? `&speaker=${speaker}` : ""
                }`}
                className="px-2 rounded-sm bg-gray-200 hover:bg-gray-100 hover:text-black"
              >
                <Icon
                  ariaLabel="Nächste Seite"
                  icon="chevron-right"
                  size="1x"
                />
              </Link>
            )}
          </div>
        </>
      )}
    </>
  );
};
