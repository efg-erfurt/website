import { gql, useMutation } from "@apollo/client";
import React, { useContext } from "react";
import {
  CreateSermonMutation,
  CreateSermonMutationVariables,
  GetLatestSermonsQueryVariables,
  GetSermonsQueryVariables,
  Language as GQLLanguage
} from "../../../types/generated";
import { getLatestSermonsFn } from "../../components/downloads/SermonList";
import { PageContext } from "../../components/page/PageContext";
import { SermonEditForm, SermonForm } from "./SermonEditForm";
import { getSermonsFn } from "./PaginatedSermons";

const createSermonFn = gql`
  mutation createSermon($sermon: CreateSermonInput!) {
    createSermon(sermon: $sermon) {
      id
    }
  }
`;

const SermonCreateForm = ({ onSuccess }: { onSuccess: () => void }) => {
  const { language } = useContext(PageContext);
  const [createSermon] = useMutation<
    CreateSermonMutation,
    CreateSermonMutationVariables
  >(createSermonFn);
  const onSubmit: (arg: SermonForm) => Promise<void> = async ({
    title,
    speaker,
    comment,
    audioUrl,
    textUrl,
    translationUrl,
    videoUrl,
    talkDate,
    tags
  }) => {
    await createSermon({
      variables: {
        sermon: {
          title,
          speaker,
          comment,
          audioUrl,
          videoUrl,
          textUrl,
          translationUrl,
          talkDate: talkDate.value || new Date(),
          language: language.code,
          tags: tags || []
        }
      },
      awaitRefetchQueries: true,
      refetchQueries: [
        {
          query: getSermonsFn,
          variables: {
            language:
              language.code === "de" ? GQLLanguage.German : GQLLanguage.English
          } as GetSermonsQueryVariables
        },
        {
          query: getLatestSermonsFn,
          variables: {
            language:
              language.code === "de" ? GQLLanguage.German : GQLLanguage.English
          } as GetLatestSermonsQueryVariables
        }
      ]
    });
    onSuccess();
  };

  return <SermonEditForm onSubmit={onSubmit} />;
};

export default SermonCreateForm;
