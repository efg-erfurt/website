import React from "react";
import { Field, Form as FinalForm } from "react-final-form";
import { ActionsToolbar, Button } from "../../components/form/Button";
import { InputAdapter } from "../../components/form/Input";
import { MarkdownInputAdapter } from "../../components/form/MarkdownInput";
import { TagsInputAdapter } from "../../components/form/Tags";
import type { FormErrorsMap } from "../../components/form/util";
import { Notification } from "../../components/errors/Notification";
import { useContentQuery } from "../../components/i18n/ContentQuery";
import { DateInputAdapter, RawDate } from "../../components/form/DateInput";
import dayjs from "dayjs";
import { Sermon } from "../../../types/generated";

export interface SermonForm {
  id?: string;
  title: string;
  comment: string;
  speaker: string;
  talkDate: RawDate;
  audioUrl: string | null;
  videoUrl: string | null;
  textUrl: string | null;
  translationUrl: string | null;
  tags: Array<string>;
}

export const SermonEditForm = ({
  existingSermon,
  onSubmit
}: {
  existingSermon?: Sermon;
  onSubmit: (arg: SermonForm) => Promise<void>;
}) => {
  const publish = existingSermon
    ? useContentQuery("components.editing.save")
    : useContentQuery("components.editing.publish");
  const existingTalkdate = !!existingSermon?.talkDate
    ? dayjs(existingSermon.talkDate).toDate()
    : null;

  return (
    <div className="max-w-4xl">
      <FinalForm<SermonForm>
        initialValues={{
          id: existingSermon?.id,
          title: existingSermon?.title,
          audioUrl: existingSermon?.audioUrl,
          textUrl: existingSermon?.textUrl,
          translationUrl: existingSermon?.translationUrl,
          videoUrl: existingSermon?.videoUrl,
          comment: existingSermon?.comment || undefined,
          tags: existingSermon?.tags,
          speaker: existingSermon?.speaker,
          talkDate: {
            value: existingTalkdate,
            raw: {
              day: existingTalkdate?.getDate().toString(),
              month: existingTalkdate
                ? `${existingTalkdate?.getMonth() + 1}`
                : undefined,
              year: existingTalkdate?.getFullYear().toString()
            }
          }
        }}
        onSubmit={onSubmit}
        validate={values => {
          const errors: FormErrorsMap<SermonForm> = {};
          if (!values.title) {
            errors.title = "Kein Titel angegeben";
          }
          if (!values.speaker) {
            errors.speaker = "Kein Autor angegeben";
          }
          if (!values.talkDate.raw.day) {
            errors.talkDate = "Kein Tag im Rededatum angegeben";
          } else if (!values.talkDate.raw.month) {
            errors.talkDate = "Kein Monat im Rededatum angegeben";
          } else if (!values.talkDate.raw.year) {
            errors.talkDate = "Kein Jahr im Rededatum angegeben";
          } else if (!values.talkDate.value) {
            errors.talkDate = "Kein gültiges Rededatum angegeben";
          }
          if (!values.comment) {
            errors.comment = "Keine Beschreibung angegeben";
          }
          return errors;
        }}
        render={({
          handleSubmit,
          submitting,
          pristine,
          hasValidationErrors,
          submitSucceeded
        }) => (
          <form onSubmit={handleSubmit}>
            {submitSucceeded && (
              <Notification type="success">
                Predigt erfolgreich veröffentlicht
              </Notification>
            )}
            <Field<string>
              id="title"
              name="title"
              component={InputAdapter}
              fullWidth
              label="Titel"
              required
            />
            <Field<string>
              id="speaker"
              name="speaker"
              component={InputAdapter}
              fullWidth
              label="Prediger"
              required
            />
            <Field<RawDate>
              id="talkDate"
              name="talkDate"
              component={DateInputAdapter}
              label="Rededatum"
              required
            />
            <Field<string>
              id="audioUrl"
              name="audioUrl"
              component={InputAdapter}
              fullWidth
              label="Audio-Link"
            />
            <Field<string>
              id="videoUrl"
              name="videoUrl"
              component={InputAdapter}
              fullWidth
              label="Video-Link"
            />
            <Field<string>
              id="textUrl"
              name="textUrl"
              component={InputAdapter}
              fullWidth
              label="Text-Link"
            />
            <Field<string>
              id="translationUrl"
              name="translationUrl"
              component={InputAdapter}
              fullWidth
              label="Übersetzung-Link"
            />
            <Field<string>
              id="comment"
              name="comment"
              component={MarkdownInputAdapter}
              label="Beschreibung"
              required
            />
            <Field<Array<string>>
              id="tags"
              name="tags"
              component={TagsInputAdapter}
              label="Tags"
            />
            <ActionsToolbar>
              <div />
              <Button
                color="primary"
                type="submit"
                loading={submitting}
                disabled={pristine || hasValidationErrors}
              >
                {publish}
              </Button>
            </ActionsToolbar>
          </form>
        )}
      />
    </div>
  );
};
