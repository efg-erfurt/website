import React from "react";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { LocalizedPage } from "../LocalizedPage";

const ImprintPage = () => (
  <LocalizedPage title="pages.imprint.title">
    <CenteredContainer>
      <Section>
        <ContentQuery markdown property="pages.imprint.content" />
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default ImprintPage;
