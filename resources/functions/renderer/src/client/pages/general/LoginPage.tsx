import { ApolloClient, useApolloClient } from "@apollo/client";
import { FORM_ERROR } from "final-form";
import React, { useContext } from "react";
import { Field, Form as FinalForm } from "react-final-form";
import { loginAndRefetch, logoutAndRefetch } from "../../components/auth";
import { Button } from "../../components/form/Button";
import { InputAdapter } from "../../components/form/Input";
import type { FormErrorsMap } from "../../components/form/util";
import { Notification } from "../../components/errors/Notification";
import {
  ContentQuery,
  useContentsQuery
} from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { PageContext } from "../../components/page/PageContext";
import { fetchWithReload } from "../../utils/requests";
import { render } from "../../utils/templating";
import { LocalizedPage } from "../LocalizedPage";
import { Spacer } from "../../components/layout/Spacer";
import { User } from "../../../types/generated";

type LoginFormValues = {
  user: string;
  pw: string;
};

const isEmailValid = (mail: string) =>
  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail);

const LoginForm = ({ user }: { user: User | null }) => {
  const apolloClient = useApolloClient() as ApolloClient<
    Record<string, unknown>
  >;
  const onSubmit = async (args: LoginFormValues) =>
    new Promise<{ [FORM_ERROR]: string } | void>(resolve =>
      fetchWithReload({
        url:
          window.location.hostname === "localhost"
            ? "http://localhost:3002/api/login"
            : "/api/login",
        body: args,
        apolloClient,
        onSuccessful: async (response: {
          status: string;
          token: string | null;
        }) => {
          if (response.token) {
            await loginAndRefetch(response.token, apolloClient);
          } else {
            await logoutAndRefetch(apolloClient);
          }
          resolve();
        },
        onError: () => resolve({ [FORM_ERROR]: "Login Failed" })
      })
    );

  const {
    "pages.login.failure": loginFailure,
    "pages.login.login": login,
    "pages.login.logout": logout,
    "pages.login.mail": mail,
    "pages.login.password": password,
    "pages.login.emptyPw": emptyPw,
    "pages.login.invalidEmail": invalidEmail
  } = useContentsQuery([
    "pages.login.failure",
    "pages.login.login",
    "pages.login.logout",
    "pages.login.mail",
    "pages.login.password",
    "pages.login.emptyPw",
    "pages.login.invalidEmail"
  ]);

  return !user ? (
    <FinalForm<LoginFormValues>
      onSubmit={onSubmit}
      validate={values => {
        const errors: FormErrorsMap<LoginFormValues> = {};
        if (!isEmailValid(values.user)) {
          errors.user = invalidEmail;
        }
        if (!values.pw) {
          errors.pw = emptyPw;
        }
        return errors;
      }}
      render={({
        handleSubmit,
        submitting,
        pristine,
        hasValidationErrors,
        hasSubmitErrors
      }) => (
        <div className="max-w-3xl">
          {hasSubmitErrors && (
            <>
              <Notification type="error">{loginFailure}</Notification>
              <Spacer size="small" />
            </>
          )}
          <ContentQuery markdown property="pages.login.content" />
          <Spacer size="small" />
          <form onSubmit={handleSubmit}>
            <Field<string>
              id="user"
              name="user"
              component={InputAdapter}
              label={mail}
              icon="envelope"
              fullWidth
              required
            />
            <Field<string>
              id="pw"
              name="pw"
              type="password"
              component={InputAdapter}
              fullWidth
              label={password}
              icon="lock"
              required
            />
            <Button
              color="primary"
              type="submit"
              loading={submitting}
              disabled={pristine || hasValidationErrors}
            >
              {login}
            </Button>
          </form>
        </div>
      )}
    />
  ) : (
    <>
      <p>
        <ContentQuery property="pages.login.success">
          {loggedInTemplate =>
            render(loggedInTemplate, {
              name: `${user.firstName} ${user.lastName}`
            })
          }
        </ContentQuery>
      </p>
      <p>
        <Button id="logout" onClick={() => logoutAndRefetch(apolloClient)}>
          {logout}
        </Button>
      </p>
    </>
  );
};

const LoginPage = () => {
  const { user } = useContext(PageContext);

  return (
    <LocalizedPage title="pages.login.title">
      <CenteredContainer>
        <Section>
          <div className="grid justify-center gap-8 mb-8">
            <LoginForm user={user} />
          </div>
        </Section>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default LoginPage;
