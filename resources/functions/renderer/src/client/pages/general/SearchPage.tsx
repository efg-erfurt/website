import React from "react";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { Spinner } from "../../components/media/Spinner";
import { SearchResults } from "../../components/search/SearchResults";
import { LocalizedPage } from "../LocalizedPage";
import { useSearchParams } from "react-router-dom";

const SearchPage = () => {
  const [searchParams] = useSearchParams();
  const query = searchParams.get("q");
  return (
    <LocalizedPage title="pages.search.title" showHeader={false}>
      <CenteredContainer>
        <Section>
          {query ? <SearchResults query={query} /> : <Spinner />}
        </Section>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default SearchPage;
