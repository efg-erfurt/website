import React from "react";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { LocalizedPage } from "../LocalizedPage";

const PrivacyPage = () => (
  <LocalizedPage title="pages.privacy.title">
    <CenteredContainer>
      <Section>
        <ContentQuery markdown property="pages.privacy.content" />
      </Section>
    </CenteredContainer>
  </LocalizedPage>
);

export default PrivacyPage;
