import React, { useContext } from "react";
import { ZeitungPreview } from "../../components/downloads/ZeitungPreview";
import { UpcomingEvents } from "../../components/events/EventList";
import { Button } from "../../components/form/Button";
import { ContentQuery, ImageQuery } from "../../components/i18n/ContentQuery";
import { Card, PlainCardGroup } from "../../components/layout/Card";
import {
  CenteredColumn,
  CenteredContainer
} from "../../components/layout/CenteredContent";
import { Icon } from "../../components/media/Icon";
import { LatestPosts } from "../../components/posts/PostList";
import { Link } from "../../components/text/Link";
import { LocalizedPage } from "../LocalizedPage";
import { AsyncBackground } from "../../components/layout/AsyncBackground";
import { LatestSermons } from "../../components/downloads/SermonList";
import { Helmet } from "react-helmet-async";
import { PageContext } from "../../components/page/PageContext";

const StartPage = () => {
  const { protocol, language, origin } = useContext(PageContext);

  return (
    <LocalizedPage showHeader={false} title="pages.welcome.title">
      <Helmet>
        <script type="application/ld+json">
          {`{
              "@context": "https://schema.org",
              "@type": "Organization",
              "url": "${protocol}//${origin}/${language.code}/",
              "logo": "${protocol}//${origin}/public/images/logo.png",
              "name": "EFG Erfurt",
              "description": "Evangelisch-Freikirchliche Gemeinde Erfurt",
              "address": {
                "@type": "PostalAddress",
                "streetAddress": "Magdeburger Allee 10",
                "addressLocality": "Erfurt",
                "addressCountry": "DE",
                "postalCode": "99085"
              },
              "contactPoint": {
                "@type": "ContactPoint",
                "email": "buero@efg-erfurt.de"
              }
          }`}
        </script>
      </Helmet>
      <CenteredContainer>
        <div className="flex flex-wrap">
          <div className="flex flex-col bg-efg-yellow z-10 mx-2 sm:mx-4 shadow-sm self-end rounded w-full lg:w-144 xl:w-192">
            <div className="h-full w-full rounded-t">
              <ImageQuery
                property="pages.welcome.imgUrl"
                alt="Header"
                rounded="top"
              />
            </div>
            <div className="p-4 md:p-6">
              <ContentQuery markdown property="pages.welcome.note" />
            </div>
          </div>
          <div className="pb-8 flex-1">
            <div className="flex flex-col gap-8 my-16 mx-2 items-center justify-center">
              <div className="max-w-sm leading-6 flex justify-between">
                <ContentQuery markdown property="pages.welcome.content" />
              </div>
              <div className="grid grid-cols-1 sm:grid-cols-3 gap-8 sm:gap-4">
                <CenteredColumn>
                  <div className="m-3">
                    <Icon ariaLabel="Gottesdienst" icon="church" size="2x" />
                  </div>
                  <ContentQuery property="components.welcome.services">
                    {property => (
                      <Link to="/event?id=10">
                        <Button background="transparent">{property}</Button>
                      </Link>
                    )}
                  </ContentQuery>
                </CenteredColumn>
                <CenteredColumn>
                  <div className="m-3">
                    <Icon ariaLabel="Café" icon="coffee" size="2x" />
                  </div>
                  <ContentQuery property="components.welcome.cafe">
                    {property => (
                      <Link to="/cafe">
                        <Button background="transparent">{property}</Button>
                      </Link>
                    )}
                  </ContentQuery>
                </CenteredColumn>
                <CenteredColumn>
                  <div className="m-3">
                    <Icon
                      ariaLabel="Kleingruppen"
                      icon="user-friends"
                      size="2x"
                    />
                  </div>
                  <ContentQuery property="components.welcome.smallGroups">
                    {property => (
                      <Link to="/groups">
                        <Button background="transparent">{property}</Button>
                      </Link>
                    )}
                  </ContentQuery>
                </CenteredColumn>
              </div>
            </div>
          </div>
        </div>
      </CenteredContainer>
      <CenteredContainer>
        <AsyncBackground overlap top="down" bottom="down">
          <PlainCardGroup>
            <Card title="Aktuelles">
              <ContentQuery markdown property="pages.welcome.latest" />
              <LatestPosts />
            </Card>
            <div className="grid grid-cols-1 gap-4">
              <Card title="Zeitung">
                <ZeitungPreview />
              </Card>
              <Card title="Aufnahmen">
                <ContentQuery markdown property="pages.welcome.sermon" />
                <LatestSermons />
              </Card>
            </div>
            <Card title="Termine">
              <ContentQuery markdown property="pages.welcome.events" />
              <UpcomingEvents />
            </Card>
          </PlainCardGroup>
        </AsyncBackground>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default StartPage;
