import React from "react";
import { ContentQuery } from "../../components/i18n/ContentQuery";
import {
  CenteredContainer,
  CenteredRow
} from "../../components/layout/CenteredContent";
import { LocalizedPage } from "../LocalizedPage";

const LoadingPage = () => (
  <LocalizedPage title="pages.loading.title">
    <CenteredContainer>
      <CenteredRow>
        <ContentQuery markdown property="pages.loading.content" />
      </CenteredRow>
    </CenteredContainer>
  </LocalizedPage>
);

export default LoadingPage;
