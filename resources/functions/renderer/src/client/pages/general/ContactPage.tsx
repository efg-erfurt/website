import React from "react";
import { Field, Form as FinalForm } from "react-final-form";
import { Button } from "../../components/form/Button";
import { Notification } from "../../components/errors/Notification";
import { InputAdapter } from "../../components/form/Input";
import type { FormErrorsMap } from "../../components/form/util";
import { CenteredContainer } from "../../components/layout/CenteredContent";
import { Section } from "../../components/layout/Section";
import { LocalizedPage } from "../LocalizedPage";
import { Spacer } from "../../components/layout/Spacer";
import { TextareaAdapter } from "../../components/form/Textarea";
import { SelectAdapter } from "../../components/form/Select";
import { gql, useMutation } from "@apollo/client";
import { useContentsQuery } from "../../components/i18n/ContentQuery";
import { MediaRow } from "../../components/media/MediaRow";
import {
  SendMessageMutation,
  SendMessageMutationVariables
} from "../../../types/generated";
import { useSearchParams } from "react-router-dom";

const sendMessageFn = gql`
  mutation sendMessage($message: SendMessageInput!) {
    sendMessage(message: $message)
  }
`;

type ContactForm = {
  name: string;
  mail: string;
  destination: string;
  message: string;
};

type Topic = {
  label: string;
  key: string;
};

type TopicGroup = {
  label: string;
  key: string;
  items: Array<Topic>;
};

const topics: Array<TopicGroup> = [
  {
    label: "Gemeindeleitung",
    key: "gl",
    items: [
      {
        label: "Büro",
        key: "buero"
      },
      { label: "Pastor", key: "pastor" },
      { label: "Gemeindeleiter", key: "gemeindeleiter" }
    ]
  },
  {
    label: "Bereiche",
    key: "bereiche",
    items: [{ label: "Gebet", key: "gebet" }]
  }
];

const isEmailValid = (mail: string) =>
  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail);

const supportedTopics = topics
  .map(n => n.items.map(i => i.key))
  .reduce((a, b) => [...a, ...b], []);

const ContactPage = () => {
  const [searchParams] = useSearchParams();
  const topicParam = searchParams.get("topic");
  const selectedTopic =
    supportedTopics.findIndex(t => t === topicParam) >= 0
      ? topicParam!
      : topics[0].items[0].key;
  const {
    "pages.contact.submit": submit,
    "pages.contact.name": name,
    "pages.contact.mail": mail,
    "pages.contact.recipient": recipient,
    "pages.contact.message": message,
    "pages.contact.errors.messageEmpty": messageEmpty,
    "pages.contact.errors.recipientEmpty": recipientEmpty,
    "pages.contact.errors.nameEmpty": nameEmpty,
    "pages.contact.errors.mailEmpty": mailEmpty,
    "pages.contact.errors.mailInvalid": mailInvalid
  } = useContentsQuery([
    "pages.contact.content",
    "pages.contact.submit",
    "pages.contact.name",
    "pages.contact.mail",
    "pages.contact.recipient",
    "pages.contact.message",
    "pages.contact.errors.messageEmpty",
    "pages.contact.errors.recipientEmpty",
    "pages.contact.errors.nameEmpty",
    "pages.contact.errors.mailEmpty",
    "pages.contact.errors.mailInvalid"
  ]);
  const [sendMessage, { error }] = useMutation<
    SendMessageMutation,
    SendMessageMutationVariables
  >(sendMessageFn);
  const onSubmit = async (args: ContactForm) => {
    await sendMessage({
      variables: {
        message: {
          name: args.name,
          destination: args.destination,
          message: args.message,
          mail: args.mail
        }
      }
    });
  };

  return (
    <LocalizedPage
      title="pages.contact.title"
      description="pages.contact.content"
    >
      <CenteredContainer>
        <Section>
          <div className="flex justify-evenly mb-8">
            <div className="max-w-3xl w-full">
              <MediaRow
                property="pages.contact.content"
                imageSrc="paper-plane"
                imagePos="right"
              />
              <Spacer size="small" />
              <FinalForm<ContactForm>
                onSubmit={onSubmit}
                initialValues={{
                  destination: selectedTopic
                }}
                validate={values => {
                  const errors: FormErrorsMap<ContactForm> = {};
                  if (values.destination !== "gebet" && !values.name) {
                    errors.name = nameEmpty;
                  }
                  if (values.destination !== "gebet" && !values.mail) {
                    errors.mail = mailEmpty;
                  } else if (!!values.mail && !isEmailValid(values.mail)) {
                    errors.mail = mailInvalid;
                  }
                  if (!values.destination) {
                    errors.destination = recipientEmpty;
                  }
                  if (!values.message) {
                    errors.message = messageEmpty;
                  }
                  return errors;
                }}
                render={({
                  values,
                  handleSubmit,
                  submitting,
                  pristine,
                  hasValidationErrors,
                  submitSucceeded
                }) => (
                  <>
                    {submitSucceeded && (
                      <>
                        {error ? (
                          <Notification type="error">
                            {error.message}. Bitte wende dich an
                            <a href="mailto:buero@efg-erfurt.de">
                              buero@efg-erfurt.de
                            </a>
                            .
                          </Notification>
                        ) : (
                          <Notification type="success">
                            Nachricht erfolgreich versendet
                          </Notification>
                        )}
                        <Spacer size="small" />
                      </>
                    )}
                    <form onSubmit={handleSubmit}>
                      <Field<string>
                        id="destination"
                        name="destination"
                        component={SelectAdapter}
                        label={recipient}
                        fullWidth
                        required
                      >
                        {topics.map(t => (
                          <optgroup key={t.key} label={t.label}>
                            {t.items.map(i => (
                              <option key={i.key} value={i.key}>
                                {i.label}
                              </option>
                            ))}
                          </optgroup>
                        ))}
                      </Field>
                      <div className="grid md:grid-cols-2 grid-cols-1 md:gap-4 gap-0">
                        <Field<string>
                          id="name"
                          name="name"
                          component={InputAdapter}
                          label={name}
                          icon="user"
                          fullWidth
                          required={values.destination !== "gebet"}
                        />
                        <Field<string>
                          id="mail"
                          name="mail"
                          component={InputAdapter}
                          label={mail}
                          icon="envelope"
                          fullWidth
                          required={values.destination !== "gebet"}
                        />
                      </div>
                      <Field<string>
                        id="message"
                        name="message"
                        component={TextareaAdapter}
                        fullWidth
                        required
                        label={message}
                        type="textarea"
                      />
                      <Button
                        color="primary"
                        type="submit"
                        loading={submitting}
                        disabled={pristine || hasValidationErrors}
                      >
                        {submit}
                      </Button>
                    </form>
                  </>
                )}
              />
            </div>
          </div>
        </Section>
      </CenteredContainer>
    </LocalizedPage>
  );
};

export default ContactPage;
