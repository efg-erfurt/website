import { gql, useQuery } from "@apollo/client";
import React, { PropsWithChildren, useEffect, useState } from "react";
import { RobotsValue } from "../../../config";
import {
  GetMonitorQuery,
  GetMonitorQueryVariables,
  Visibility
} from "../../../types/generated";
import { Error } from "../../components/errors/Error";
import { useContentQuery } from "../../components/i18n/ContentQuery";
import { Spinner } from "../../components/media/Spinner";
import { MinimalPage } from "../LocalizedPage";
import { useSearchParams } from "react-router-dom";

if (process.env.WP_TARGET === "web" || process.env.WP_TARGET === `"web"`) {
  require("./custom.scss");
}

const eventMode = "events";
const contentMode = "content";

const eventPagesCount = 3;
const displayDuration = 10;

const getMonitorFn = gql`
  query getMonitor($visibility: Visibility!) {
    monitor {
      pages(visibility: $visibility)
    }
  }
`;

const ScreenPage = () => {
  const [searchParams] = useSearchParams();
  const mode = searchParams.get("mode");
  const type = searchParams.get("type");
  const { data, loading, error } = useQuery<
    GetMonitorQuery,
    GetMonitorQueryVariables
  >(getMonitorFn, {
    variables: {
      visibility:
        type === "internal" ? Visibility.Internal : Visibility.External
    }
  });

  if (loading) {
    return <Spinner />;
  } else if (!data || error) {
    return <Error error={error} />;
  }

  const eventPages = new Array(eventPagesCount).fill(1).map(() => eventMode);
  const allPages =
    mode === contentMode
      ? data.monitor.pages
      : mode === eventMode
      ? eventPages
      : [...data.monitor.pages, ...eventPages];

  return <PageSwitcher pages={allPages} />;
};

const PageSwitcher = ({ pages }: { pages: Array<string> }) => {
  const [currentPage, setCurrentPage] = useState(0);
  const eventsUrl = useContentQuery("general.info.calendarUrl");
  useEffect(() => {
    setTimeout(() => {
      setCurrentPage(currentPage + 1 === pages.length ? 0 : currentPage + 1);
    }, displayDuration * 1000);
  }, [currentPage]);

  const page = pages[currentPage];

  return (
    <MinimalPage
      title="general.info.title"
      robots={RobotsValue.NO_INDEX_NO_FOLLOW}
    >
      {page !== eventMode ? (
        <SimplePage>
          <img className="w-full" src={`/public/assets/${page}`} alt="Screen" />
        </SimplePage>
      ) : (
        <div className="h-screen w-screen">
          <object data={eventsUrl} className="w-full h-full">
            <embed src={eventsUrl} className="w-full h-full" />
            Veranstaltungen konnten nicht geladen werden
          </object>
        </div>
      )}
    </MinimalPage>
  );
};

const SimplePage = ({ children }: PropsWithChildren<unknown>) => (
  <div className="h-full w-full">{children}</div>
);

export default ScreenPage;
