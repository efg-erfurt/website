import { Languages, Language, defaultLanguage } from "../commons";

const preferredLanguageKey = "PREFERRED_LANG";

const getLanguageFromCode = (code?: string) =>
  Object.values(Languages).find(n => n.code === code) || null;

const saveLanguagePreference = (language: Language) => {
  if (!window.localStorage) {
    return;
  }

  window.localStorage.setItem(preferredLanguageKey, language.code);
};

const getLanguageFromLocalstorage: () => Language | null = () => {
  if (!window.localStorage) {
    return null;
  }

  const existingItem = window.localStorage.getItem(preferredLanguageKey);
  if (existingItem !== null) {
    return getLanguageFromCode(existingItem);
  }

  return null;
};

export const getLanguage = (code?: string): Language => {
  const languageFromUrl = getLanguageFromCode(code);
  const languageFromLocalstorage = getLanguageFromLocalstorage();

  const usedLanguage =
    languageFromUrl !== null
      ? languageFromUrl
      : languageFromLocalstorage !== null
      ? languageFromLocalstorage
      : defaultLanguage;
  if (usedLanguage !== languageFromLocalstorage) {
    saveLanguagePreference(usedLanguage);
  }

  return usedLanguage;
};
