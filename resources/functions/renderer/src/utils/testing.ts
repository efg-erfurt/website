import { APIGatewayProxyEventV2 } from "aws-lambda";

export const isJestTest = () => process.env.NODE_ENV === "test";

export const buildApiGwEvent = (
  path: string,
  options?: {
    method?: string;
    queryParams?: string;
  }
): APIGatewayProxyEventV2 => ({
  headers: {},
  requestContext: {
    http: {
      method: options?.method || "GET",
      path: path,
      protocol: "https",
      sourceIp: "::",
      userAgent: ""
    },
    accountId: "",
    stage: "",
    apiId: "",
    domainName: "",
    requestId: "",
    routeKey: "",
    time: "",
    timeEpoch: 0,
    domainPrefix: ""
  },
  isBase64Encoded: false,
  rawPath: path,
  rawQueryString: options?.queryParams || "",
  routeKey: "",
  version: "",
  body: ""
});
