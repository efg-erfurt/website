export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /**
   * A datetime with timezone offset.
   *
   * The input is a string in RFC3339 format, e.g. "2022-01-12T04:00:19.12345Z"
   * or "2022-01-12T04:00:19+03:00". The output is also a string in RFC3339
   * format, but it is always normalized to the UTC (Z) offset, e.g.
   * "2022-01-12T04:00:19.12345Z".
   */
  DateTime: { input: any; output: any; }
};

export type Content = {
  __typename?: 'Content';
  content: Scalars['String']['output'];
  date: Scalars['DateTime']['output'];
  key: Scalars['String']['output'];
  language: Scalars['String']['output'];
};

export type CreatePostInput = {
  content: Scalars['String']['input'];
  imageUrl?: InputMaybe<Scalars['String']['input']>;
  language: Scalars['String']['input'];
  tags: Array<Scalars['String']['input']>;
  title: Scalars['String']['input'];
};

export type CreateSermonInput = {
  audioUrl?: InputMaybe<Scalars['String']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  language: Scalars['String']['input'];
  speaker: Scalars['String']['input'];
  tags: Array<Scalars['String']['input']>;
  talkDate: Scalars['DateTime']['input'];
  textUrl?: InputMaybe<Scalars['String']['input']>;
  title: Scalars['String']['input'];
  translationUrl?: InputMaybe<Scalars['String']['input']>;
  videoUrl?: InputMaybe<Scalars['String']['input']>;
};

export type Event = {
  __typename?: 'Event';
  category: EventCategory;
  date: Scalars['DateTime']['output'];
  description?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  place?: Maybe<Scalars['String']['output']>;
};

export type EventCategory = {
  __typename?: 'EventCategory';
  backgroundColor: Scalars['String']['output'];
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  textColor: Scalars['String']['output'];
};

export type FillPropertiesResult = {
  __typename?: 'FillPropertiesResult';
  imported: Scalars['Int']['output'];
  total: Scalars['Int']['output'];
};

export type Group = {
  __typename?: 'Group';
  ages?: Maybe<Scalars['String']['output']>;
  /** @deprecated Field no longer supported */
  canBeContacted: Scalars['Boolean']['output'];
  contactUrl?: Maybe<Scalars['String']['output']>;
  day?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  imageUrl?: Maybe<Scalars['String']['output']>;
  leaders: Array<User>;
  name: Scalars['String']['output'];
  note: Scalars['String']['output'];
  place?: Maybe<Scalars['String']['output']>;
  time?: Maybe<Scalars['String']['output']>;
};

export enum Language {
  English = 'ENGLISH',
  German = 'GERMAN'
}

export type Match = {
  __typename?: 'Match';
  content: Scalars['String']['output'];
  selection: Array<MatchSelection>;
};

export type MatchSelection = {
  __typename?: 'MatchSelection';
  from: Scalars['Int']['output'];
  highlight: Scalars['Boolean']['output'];
  to: Scalars['Int']['output'];
};

export type Monitor = {
  __typename?: 'Monitor';
  pages: Array<Scalars['String']['output']>;
};


export type MonitorPagesArgs = {
  visibility?: Visibility;
};

export type Mutation = {
  __typename?: 'Mutation';
  clearCache: Scalars['Boolean']['output'];
  createPost: Post;
  createSermon: Sermon;
  deletePost: Scalars['Boolean']['output'];
  deleteSermon: Scalars['Boolean']['output'];
  editMonitor: Scalars['Boolean']['output'];
  fillProperties: FillPropertiesResult;
  sendMessage: Scalars['Boolean']['output'];
  updateContent: Scalars['Boolean']['output'];
  updateMonitor: Scalars['Boolean']['output'];
  updatePost: Post;
  updateSermon: Sermon;
};


export type MutationCreatePostArgs = {
  post: CreatePostInput;
};


export type MutationCreateSermonArgs = {
  sermon: CreateSermonInput;
};


export type MutationDeletePostArgs = {
  id: Scalars['String']['input'];
};


export type MutationDeleteSermonArgs = {
  id: Scalars['String']['input'];
};


export type MutationEditMonitorArgs = {
  newSlides: Array<Scalars['String']['input']>;
  visibility: Visibility;
};


export type MutationFillPropertiesArgs = {
  language: Language;
};


export type MutationSendMessageArgs = {
  message: SendMessageInput;
};


export type MutationUpdateContentArgs = {
  content: Scalars['String']['input'];
  key: Scalars['String']['input'];
  language: Language;
};


export type MutationUpdateMonitorArgs = {
  language: Language;
};


export type MutationUpdatePostArgs = {
  post: UpdatePostInput;
};


export type MutationUpdateSermonArgs = {
  sermon: UpdateSermonInput;
};

export type PageResult = {
  __typename?: 'PageResult';
  matches: Array<Match>;
  path: Scalars['String']['output'];
  score: Scalars['Float']['output'];
  title: Scalars['String']['output'];
};

export type PaginatedPosts = {
  __typename?: 'PaginatedPosts';
  content: Array<Post>;
  count: Scalars['Int']['output'];
  page: Scalars['Int']['output'];
  totalCount: Scalars['Int']['output'];
  totalPages: Scalars['Int']['output'];
};

export type PaginatedSermons = {
  __typename?: 'PaginatedSermons';
  content: Array<Sermon>;
  count: Scalars['Int']['output'];
  page: Scalars['Int']['output'];
  totalCount: Scalars['Int']['output'];
  totalPages: Scalars['Int']['output'];
};

export type PaginationInput = {
  page?: Scalars['Int']['input'];
  size?: Scalars['Int']['input'];
};

export type Post = {
  __typename?: 'Post';
  content: Scalars['String']['output'];
  creator: User;
  date: Scalars['DateTime']['output'];
  id: Scalars['String']['output'];
  imageUrl?: Maybe<Scalars['String']['output']>;
  language: Scalars['String']['output'];
  publishDate: Scalars['DateTime']['output'];
  summary?: Maybe<Scalars['String']['output']>;
  tags: Array<Scalars['String']['output']>;
  title: Scalars['String']['output'];
};

export type QueryRoot = {
  __typename?: 'QueryRoot';
  content?: Maybe<Content>;
  contents: Array<Content>;
  currentUser?: Maybe<User>;
  eventCategories: Array<EventCategory>;
  eventCategory?: Maybe<EventCategory>;
  events: Array<Event>;
  groups: Array<Group>;
  latestPosts: PaginatedPosts;
  latestSermons: PaginatedSermons;
  monitor: Monitor;
  post?: Maybe<Post>;
  search: SearchResult;
  sermons: PaginatedSermons;
  signups: Array<Signup>;
  user?: Maybe<User>;
};


export type QueryRootContentArgs = {
  key: Scalars['String']['input'];
  language: Language;
};


export type QueryRootContentsArgs = {
  keys: Array<Scalars['String']['input']>;
  language: Language;
};


export type QueryRootEventCategoryArgs = {
  id: Scalars['String']['input'];
};


export type QueryRootEventsArgs = {
  categoryId?: InputMaybe<Scalars['String']['input']>;
  end?: InputMaybe<Scalars['DateTime']['input']>;
  maxEntries?: Scalars['Int']['input'];
  start: Scalars['DateTime']['input'];
};


export type QueryRootGroupsArgs = {
  id: Scalars['String']['input'];
};


export type QueryRootLatestPostsArgs = {
  language?: InputMaybe<Language>;
  pagination: PaginationInput;
  tag?: InputMaybe<Scalars['String']['input']>;
};


export type QueryRootLatestSermonsArgs = {
  language: Language;
  pagination: PaginationInput;
};


export type QueryRootPostArgs = {
  id: Scalars['String']['input'];
};


export type QueryRootSearchArgs = {
  language: Language;
  query: Scalars['String']['input'];
  startDate?: InputMaybe<Scalars['DateTime']['input']>;
};


export type QueryRootSermonsArgs = {
  language: Language;
  pagination: PaginationInput;
  speaker?: InputMaybe<Scalars['String']['input']>;
};


export type QueryRootSignupsArgs = {
  groupsId: Scalars['String']['input'];
};


export type QueryRootUserArgs = {
  id: Scalars['String']['input'];
};

export type SearchResult = {
  __typename?: 'SearchResult';
  events: Array<Event>;
  id: Scalars['String']['output'];
  language: Scalars['String']['output'];
  pages: Array<PageResult>;
  posts: Array<Post>;
  query: Scalars['String']['output'];
  sermons: Array<Sermon>;
};

export type SendMessageInput = {
  destination: Scalars['String']['input'];
  mail?: InputMaybe<Scalars['String']['input']>;
  message: Scalars['String']['input'];
  name?: InputMaybe<Scalars['String']['input']>;
};

export type Sermon = {
  __typename?: 'Sermon';
  audioUrl?: Maybe<Scalars['String']['output']>;
  comment?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  language: Scalars['String']['output'];
  publishDate: Scalars['DateTime']['output'];
  speaker: Scalars['String']['output'];
  tags: Array<Scalars['String']['output']>;
  talkDate: Scalars['DateTime']['output'];
  textUrl?: Maybe<Scalars['String']['output']>;
  title: Scalars['String']['output'];
  translationUrl?: Maybe<Scalars['String']['output']>;
  videoUrl?: Maybe<Scalars['String']['output']>;
};

export type Signup = {
  __typename?: 'Signup';
  day?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  imageUrl?: Maybe<Scalars['String']['output']>;
  name: Scalars['String']['output'];
  note: Scalars['String']['output'];
  slotsLeft?: Maybe<Scalars['Int']['output']>;
  slotsTotal?: Maybe<Scalars['Int']['output']>;
  slotsUsed: Scalars['Int']['output'];
  time?: Maybe<Scalars['String']['output']>;
};

export type UpdatePostInput = {
  content: Scalars['String']['input'];
  id: Scalars['String']['input'];
  imageUrl?: InputMaybe<Scalars['String']['input']>;
  language: Scalars['String']['input'];
  tags: Array<Scalars['String']['input']>;
  title: Scalars['String']['input'];
};

export type UpdateSermonInput = {
  audioUrl?: InputMaybe<Scalars['String']['input']>;
  comment?: InputMaybe<Scalars['String']['input']>;
  id: Scalars['String']['input'];
  language: Scalars['String']['input'];
  speaker: Scalars['String']['input'];
  tags: Array<Scalars['String']['input']>;
  talkDate: Scalars['DateTime']['input'];
  textUrl?: InputMaybe<Scalars['String']['input']>;
  title: Scalars['String']['input'];
  translationUrl?: InputMaybe<Scalars['String']['input']>;
  videoUrl?: InputMaybe<Scalars['String']['input']>;
};

export type User = {
  __typename?: 'User';
  firstName: Scalars['String']['output'];
  id: Scalars['String']['output'];
  imageUrl?: Maybe<Scalars['String']['output']>;
  lastName: Scalars['String']['output'];
  name: Scalars['String']['output'];
};

export enum Visibility {
  External = 'EXTERNAL',
  Internal = 'INTERNAL'
}

export type GetUserQueryVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type GetUserQuery = { __typename?: 'QueryRoot', user?: { __typename?: 'User', id: string, firstName: string, lastName: string, imageUrl?: string | null } | null };

export type DeleteSermonMutationVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type DeleteSermonMutation = { __typename?: 'Mutation', deleteSermon: boolean };

export type GetLatestSermonsQueryVariables = Exact<{
  language: Language;
  pagination: PaginationInput;
}>;


export type GetLatestSermonsQuery = { __typename?: 'QueryRoot', latestSermons: { __typename?: 'PaginatedSermons', totalPages: number, totalCount: number, page: number, count: number, content: Array<{ __typename?: 'Sermon', id: string, title: string, publishDate: any, talkDate: any, speaker: string, comment?: string | null, audioUrl?: string | null, videoUrl?: string | null, textUrl?: string | null, translationUrl?: string | null, language: string, tags: Array<string> }> } };

export type UpdateContentMutationVariables = Exact<{
  key: Scalars['String']['input'];
  content: Scalars['String']['input'];
  language: Language;
}>;


export type UpdateContentMutation = { __typename?: 'Mutation', updateContent: boolean };

export type GetEventsWithMaxEntriesQueryVariables = Exact<{
  start: Scalars['DateTime']['input'];
  end: Scalars['DateTime']['input'];
  maxEntries: Scalars['Int']['input'];
}>;


export type GetEventsWithMaxEntriesQuery = { __typename?: 'QueryRoot', events: Array<{ __typename?: 'Event', id: string, name: string, date: any, place?: string | null, category: { __typename?: 'EventCategory', id: string, name: string, textColor: string, backgroundColor: string } }> };

export type GetEventsQueryVariables = Exact<{
  start: Scalars['DateTime']['input'];
  end?: InputMaybe<Scalars['DateTime']['input']>;
}>;


export type GetEventsQuery = { __typename?: 'QueryRoot', events: Array<{ __typename?: 'Event', id: string, name: string, date: any, place?: string | null, description?: string | null, category: { __typename?: 'EventCategory', id: string, name: string, backgroundColor: string, textColor: string } }> };

export type GetGroupsQueryVariables = Exact<{
  groupsId: Scalars['String']['input'];
}>;


export type GetGroupsQuery = { __typename?: 'QueryRoot', groups: Array<{ __typename?: 'Group', id: string, name: string, note: string, imageUrl?: string | null, place?: string | null, ages?: string | null, time?: string | null, day?: string | null, canBeContacted: boolean, leaders: Array<{ __typename?: 'User', id: string, firstName: string, lastName: string, name: string, imageUrl?: string | null }> }> };

export type GetContentQueryVariables = Exact<{
  key: Scalars['String']['input'];
  language: Language;
}>;


export type GetContentQuery = { __typename?: 'QueryRoot', content?: { __typename?: 'Content', key: string, content: string } | null };

export type GetContentsQueryVariables = Exact<{
  keys: Array<Scalars['String']['input']> | Scalars['String']['input'];
  language: Language;
}>;


export type GetContentsQuery = { __typename?: 'QueryRoot', contents: Array<{ __typename?: 'Content', key: string, content: string }> };

export type GetCurrentUserQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCurrentUserQuery = { __typename?: 'QueryRoot', currentUser?: { __typename?: 'User', id: string, firstName: string, lastName: string, name: string, imageUrl?: string | null } | null };

export type GetLatestPostsQueryVariables = Exact<{
  language: Language;
  tag?: InputMaybe<Scalars['String']['input']>;
  pagination: PaginationInput;
}>;


export type GetLatestPostsQuery = { __typename?: 'QueryRoot', latestPosts: { __typename?: 'PaginatedPosts', totalPages: number, totalCount: number, page: number, count: number, content: Array<{ __typename?: 'Post', id: string, title: string, publishDate: any, summary?: string | null, content: string, imageUrl?: string | null, tags: Array<string>, date: any, language: string, creator: { __typename?: 'User', id: string, firstName: string, lastName: string, name: string } }> } };

export type SearchQueryVariables = Exact<{
  query: Scalars['String']['input'];
  language: Language;
}>;


export type SearchQuery = { __typename?: 'QueryRoot', search: { __typename?: 'SearchResult', id: string, query: string, language: string, posts: Array<{ __typename?: 'Post', id: string, publishDate: any, summary?: string | null, content: string, date: any, title: string, language: string, tags: Array<string>, imageUrl?: string | null, creator: { __typename?: 'User', id: string, firstName: string, lastName: string, name: string } }>, pages: Array<{ __typename?: 'PageResult', title: string, path: string, matches: Array<{ __typename?: 'Match', content: string, selection: Array<{ __typename?: 'MatchSelection', from: number, to: number, highlight: boolean }> }> }>, events: Array<{ __typename?: 'Event', id: string, name: string, place?: string | null, date: any, category: { __typename?: 'EventCategory', id: string, backgroundColor: string, textColor: string, name: string } }>, sermons: Array<{ __typename?: 'Sermon', id: string, title: string, publishDate: any, talkDate: any, language: string, speaker: string, comment?: string | null, audioUrl?: string | null, videoUrl?: string | null, textUrl?: string | null, translationUrl?: string | null, tags: Array<string> }> } };

export type GetSignupsQueryVariables = Exact<{
  groupsId: Scalars['String']['input'];
}>;


export type GetSignupsQuery = { __typename?: 'QueryRoot', signups: Array<{ __typename?: 'Signup', id: string, name: string, note: string, imageUrl?: string | null, time?: string | null, day?: string | null, slotsLeft?: number | null, slotsTotal?: number | null, slotsUsed: number }> };

export type ClearCacheMutationVariables = Exact<{ [key: string]: never; }>;


export type ClearCacheMutation = { __typename?: 'Mutation', clearCache: boolean };

export type FillPropertiesMutationVariables = Exact<{
  language: Language;
}>;


export type FillPropertiesMutation = { __typename?: 'Mutation', fillProperties: { __typename?: 'FillPropertiesResult', imported: number, total: number } };

export type UpdateMonitorMutationVariables = Exact<{
  language: Language;
}>;


export type UpdateMonitorMutation = { __typename?: 'Mutation', updateMonitor: boolean };

export type GetSermonsQueryVariables = Exact<{
  language: Language;
  speaker?: InputMaybe<Scalars['String']['input']>;
  pagination: PaginationInput;
}>;


export type GetSermonsQuery = { __typename?: 'QueryRoot', sermons: { __typename?: 'PaginatedSermons', totalPages: number, totalCount: number, page: number, count: number, content: Array<{ __typename?: 'Sermon', id: string, title: string, comment?: string | null, publishDate: any, talkDate: any, audioUrl?: string | null, textUrl?: string | null, translationUrl?: string | null, videoUrl?: string | null, tags: Array<string>, speaker: string, language: string }> } };

export type CreateSermonMutationVariables = Exact<{
  sermon: CreateSermonInput;
}>;


export type CreateSermonMutation = { __typename?: 'Mutation', createSermon: { __typename?: 'Sermon', id: string } };

export type UpdateSermonMutationVariables = Exact<{
  sermon: UpdateSermonInput;
}>;


export type UpdateSermonMutation = { __typename?: 'Mutation', updateSermon: { __typename?: 'Sermon', id: string } };

export type GetEventCategoryQueryVariables = Exact<{
  categoryId: Scalars['String']['input'];
  end: Scalars['DateTime']['input'];
  start: Scalars['DateTime']['input'];
}>;


export type GetEventCategoryQuery = { __typename?: 'QueryRoot', eventCategory?: { __typename?: 'EventCategory', id: string, name: string } | null, events: Array<{ __typename?: 'Event', id: string, name: string, date: any, place?: string | null, category: { __typename?: 'EventCategory', id: string, name: string, textColor: string, backgroundColor: string } }> };

export type SendMessageMutationVariables = Exact<{
  message: SendMessageInput;
}>;


export type SendMessageMutation = { __typename?: 'Mutation', sendMessage: boolean };

export type GetMonitorQueryVariables = Exact<{
  visibility: Visibility;
}>;


export type GetMonitorQuery = { __typename?: 'QueryRoot', monitor: { __typename?: 'Monitor', pages: Array<string> } };

export type CreatePostMutationVariables = Exact<{
  post: CreatePostInput;
}>;


export type CreatePostMutation = { __typename?: 'Mutation', createPost: { __typename?: 'Post', id: string } };

export type GetPostQueryVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type GetPostQuery = { __typename?: 'QueryRoot', post?: { __typename?: 'Post', id: string, title: string, summary?: string | null, content: string, publishDate: any, date: any, imageUrl?: string | null, language: string, tags: Array<string>, creator: { __typename?: 'User', id: string, firstName: string, lastName: string, name: string } } | null };

export type DeletePostMutationVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type DeletePostMutation = { __typename?: 'Mutation', deletePost: boolean };

export type UpdatePostMutationVariables = Exact<{
  post: UpdatePostInput;
}>;


export type UpdatePostMutation = { __typename?: 'Mutation', updatePost: { __typename?: 'Post', id: string } };
