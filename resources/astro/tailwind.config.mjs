const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      colors: {
        "efg-yellow": "#fecb2f",
        "efg-blue-bright": "#6da9c0",
        "efg-blue-dark": "#1d5778",
        "efg-orange": "#ed6500"
      },
      width: {
        128: "32rem"
      },
      fontFamily: {
        sans: [
          "Open Sans Variable",
          "Open Sans",
          ...defaultTheme.fontFamily.sans
        ],
        serif: ["Vollkorn Variable", ...defaultTheme.fontFamily.serif],
        konzert: ["EB Garamond Variable", ...defaultTheme.fontFamily.serif]
      }
    }
  },
  plugins: []
};
