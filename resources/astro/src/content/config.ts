import { defineCollection, reference, z } from "astro:content";

const zeitung = defineCollection({
  type: "data",
  schema: z.object({
    title: z.string(),
    vorwort: z.string(),
    pubDate: z.coerce.date(),
    heroImage: z.string(),
    pdfDownload: z.string()
  })
});

const article = defineCollection({
  type: "content",
  schema: z.object({
    author: z.string(),
    title: z.string(),
    summary: z.string().max(500),
    subTitle: z.string().optional(),
    image: z.string().optional(),
    zeitung: reference("zeitung"),
    category: reference("category")
  })
});

const category = defineCollection({
  type: "data",
  schema: z.object({
    title: z.string()
  })
});

export const collections = { zeitung, article, category };
