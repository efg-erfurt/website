---
title: Verletzlichkeit
zeitung: "82"
author: Jeannette
category: persoenliches
image: /zeitungen/82-path.jpg
summary: Zu diesem Wort fällt mir, ehrlich gesagt, zuerst "verletzlich sein" ein. Auch mit empfindlich, sensibel oder dünnhäutig sein zu übersetzen. Schnell geht mir etwas nahe, was ich erlebe, was zu mir gesagt wird oder vielleicht auch gerade, was nicht gesagt wird oder nicht gesehen wird. Es hinterlässt Spuren und oft braucht es Zeit, bis es heilt.
---
Zu diesem Wort fällt mir, ehrlich gesagt, zuerst "verletzlich sein" ein. Auch mit empfindlich, sensibel oder dünnhäutig sein zu übersetzen. Schnell geht mir etwas nahe, was ich erlebe, was zu mir gesagt wird oder vielleicht auch gerade, was nicht gesagt wird oder nicht gesehen wird. Es hinterlässt Spuren und oft braucht es Zeit, bis es heilt.

Aber hier soll es viel mehr um das "verletzlich Machen" gehen. "Machen" bedeutet aktiv sein und bewusst handeln. Freiwillig? Wirklich? Mich verletzlich zu machen, bedeutet, meine Gefühle zu zeigen, zu erzählen, was aus meinem Innersten kommt. Ich gebe preis, was mich wirklich berührt, was ich spüre, und was ich denke. Ich mache mein Herz auf und gewähre Einblicke. Ehrlich sein zu sich selbst und zum Gegenüber. Doch geht das nicht einfach so auf Knopfdruck und auch nicht mit jedem oder jeder.

Es gibt verschiedene Voraussetzungen, die ich sehe. Da ist zuerst einmal das eigene Erkennen von Gefühlen und das In-Worte-Fassen. Mir kreisen oft tausend Gedanken durch den Kopf, die ich nur schwierig ordnen kann. Und dann auch noch jemanden anderen mit hineinnehmen, wo ich es doch selbst kaum verstehe? Mir helfen dabei Bilder, die das Gewirr "übersetzen" und so wieder zu einfachen Worten werden. Ich habe es auch einige Zeit lang niedergeschrieben. Auch das hilft zu formulieren und es macht den Kopf frei, weil ich meine Schublade im Kopf leeren konnte. Interessanterweise werden auf diese Weise manche Dinge klarer.

Und es braucht Mut. Mut, über meinen Schatten zu springen. Mut, auf einen anderen zuzugehen. Mut, aus mir herauszukommen. Mut… Aber ohne Vertrauen geht das alles nicht. Es braucht einen Vertrauensvorschuss für mein Gegenüber. Ich lasse mich darauf ein in dem Vertrauen, dass ich ernst genommen werde und dass meine Worte nicht weitergetragen werden. Vertrauen heißt: mir sicher sein. Aber wo sind sie, diese Vertrauensorte? Wo kann man Vertrauensmenschen finden? Unser Leben besteht nicht nur aus Erfolgsgeschichten, es gelingt uns nicht alles, was wir uns vorgenommen haben, unsere Wünsche gehen nicht immer in Erfüllung. Manchmal scheint genau das Gegenteil zu passieren. Fragen stehen im Raum oder Zweifel. Und wohin dann mit dem ganzen "Müll"?

Mein Traum von Gemeinde ist, dass wir genau hier diesen Ort bzw. diese Menschen finden. Ohne Angst, sich zu blamieren, wünsche ich mir, dass wir uns mitteilen, wo es gerade schwierig, mühselig oder sogar unerträglich ist.

Als einen sicheren Ort erlebe ich meinen Hauskreis. Ich kann sowohl diejenige sein, die etwas loszuwerden hat, als auch die, die zuhören kann. Wir tragen unsere Nöte, freuen uns über Erreichtes oder Überwundenes oder halten Dinge gemeinsam aus. Ich möchte dich ermutigen, eine Kleingruppe zu suchen und dort Leben zu teilen.

Eine gute Nachricht ist, dass es bald wieder das Gebet nach dem Gottesdienst geben wird. Hier wirst du die Möglichkeit haben, Dinge am Kreuz abzulegen oder Sorgen auszusprechen. Manchmal nutzt Gott solch ein Gebet aber auch, um etwas anzuschieben. Diesen Weg musst du dann aber auch nicht allein gehen. Seelsorgerliche Begleitung kannst du bei den Pastoren und Ältesten erfragen. Ich bin mir sicher, dass wir es lernen können, mutig zu sein und aufrichtig und so zu echtem Frieden und Freiheit gelangen.

Gott segne uns auf dem Weg.