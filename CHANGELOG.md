# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.2.0](https://gitlab.com/efg-erfurt/website/compare/v2.1.0...v2.2.0) (2021-09-12)


### Features

* save monitor references in properties ([92cc5d2](https://gitlab.com/efg-erfurt/website/commit/92cc5d2c3e1330769b3653d2fd62c883f1c6684a))


### Bug Fixes

* **deps:** update dependency core-js to v3.17.2 ([9d441f5](https://gitlab.com/efg-erfurt/website/commit/9d441f5d13c915644b332399c3816ea3ac0da72c))
* **deps:** update dependency core-js to v3.17.3 ([0d89ec1](https://gitlab.com/efg-erfurt/website/commit/0d89ec112405ed60b62ae6f7188bed343ad14c51))
* fixed invalid text contents, downgraded react-markdown ([2d6a6c8](https://gitlab.com/efg-erfurt/website/commit/2d6a6c8e5e1e86d138f6c5d2c8defdbf9fd95966))

## [2.1.0](https://gitlab.com/efg-erfurt/website/compare/v1.47.5...v2.1.0) (2021-09-01)


### Features

* removed x-ray ([abb3d69](https://gitlab.com/efg-erfurt/website/commit/abb3d695d936676cfb1c0ad3000afe821ce69f78))
* small improvements ([922c304](https://gitlab.com/efg-erfurt/website/commit/922c3048bd02360258c59941d738edb4d392d60a))


### Bug Fixes

* **deps:** update dependency analytics to ^0.7.0 ([0048182](https://gitlab.com/efg-erfurt/website/commit/004818278ad33d70dc63ddc99a60c59c51346d45))
* **deps:** update dependency class-transformer to ^0.4.0 ([5fda083](https://gitlab.com/efg-erfurt/website/commit/5fda0837871809fbc67c63982feca6afb2c5db7c))
* **deps:** update dependency core-js to v3.10.2 ([303a053](https://gitlab.com/efg-erfurt/website/commit/303a0531c499762931e6a6e64654780c3ce3ae8f))
* **deps:** update dependency core-js to v3.11.0 ([2de8330](https://gitlab.com/efg-erfurt/website/commit/2de8330c8ebeaaac49bc6881ef38215c0bd5d6de))
* **deps:** update dependency core-js to v3.11.1 ([498cfe8](https://gitlab.com/efg-erfurt/website/commit/498cfe8cf7401542132acc788e79111c95b2dc90))
* **deps:** update dependency core-js to v3.11.2 ([e52c025](https://gitlab.com/efg-erfurt/website/commit/e52c025316b196ad861f623b50de2fab3b988a91))
* **deps:** update dependency core-js to v3.11.3 ([5cd5ab6](https://gitlab.com/efg-erfurt/website/commit/5cd5ab63d3eea71a04948ddbdffa54210830e616))
* **deps:** update dependency core-js to v3.12.1 ([05b8327](https://gitlab.com/efg-erfurt/website/commit/05b8327a01210898a0c5c508511cc2c7fc3750b7))
* **deps:** update dependency core-js to v3.13.1 ([062e543](https://gitlab.com/efg-erfurt/website/commit/062e543d63e33ff4c31782fcd13cc74f465b675f))
* **deps:** update dependency core-js to v3.16.0 ([9df5442](https://gitlab.com/efg-erfurt/website/commit/9df54426172b8a62abdf7d4415697d3f917e2761))
* **deps:** update dependency core-js to v3.16.1 ([4a8df48](https://gitlab.com/efg-erfurt/website/commit/4a8df48afb7cebcc8b10b1ee7de4fc341d2123a2))
* **deps:** update dependency core-js to v3.16.3 ([69f842f](https://gitlab.com/efg-erfurt/website/commit/69f842fd9ed90358b4c6dc76452330564fdafd08))
* **deps:** update dependency core-js to v3.16.4 ([72fe936](https://gitlab.com/efg-erfurt/website/commit/72fe936ccb8020cb45c97b65c4c1274c788b97db))
* **deps:** update dependency dotenv to v10 ([b8ae586](https://gitlab.com/efg-erfurt/website/commit/b8ae586049d03a2ba342c2815ff8903b3613ee18))
* **deps:** update dependency dotenv to v9 ([47fe998](https://gitlab.com/efg-erfurt/website/commit/47fe998e36cdf0b0ea81a16ad7869acb7dc12189))
* **deps:** update dependency fastify to v3.14.2 ([605ac5f](https://gitlab.com/efg-erfurt/website/commit/605ac5f394fa783102a27c7c30149c510cae741f))
* **deps:** update dependency fastify to v3.15.0 ([a8efdad](https://gitlab.com/efg-erfurt/website/commit/a8efdade830da044889b7f57caf191f66a3e8d9a))
* **deps:** update dependency fastify to v3.15.1 ([d9be964](https://gitlab.com/efg-erfurt/website/commit/d9be96446840fb2ca6e73bf044d55d1c074786c8))
* **deps:** update dependency fastify to v3.17.0 ([6106cc2](https://gitlab.com/efg-erfurt/website/commit/6106cc28b2412c473dab4c912f8ec0fe4fda0a20))
* **deps:** update dependency fastify-static to v4 ([4f6b4f0](https://gitlab.com/efg-erfurt/website/commit/4f6b4f09ae5f5fd27b83464013b951e65fc1daf0))
* **deps:** update dependency htmlparser2 to v7 ([a346892](https://gitlab.com/efg-erfurt/website/commit/a346892ca076e456181e76774c51f8421e58630a))
* **deps:** update dependency mjml to ^4.9.0 ([832c90c](https://gitlab.com/efg-erfurt/website/commit/832c90cae65a6379c4affb08fe1e7874f906785c))
* **deps:** update dependency mjml-react to ^2.0.0 ([2c599f4](https://gitlab.com/efg-erfurt/website/commit/2c599f424f3c76a51783464d625074168a006f24))
* **deps:** update dependency mjml-react to v2 ([15c8756](https://gitlab.com/efg-erfurt/website/commit/15c8756295cd384bd4758c84b8a23be990c2b6b0))
* **deps:** update dependency pg to ^8.6.0 ([74e4943](https://gitlab.com/efg-erfurt/website/commit/74e494371694358f97c68701bfe4b9b930662f88))
* **deps:** update dependency react-bulma-components to v4 ([0940a27](https://gitlab.com/efg-erfurt/website/commit/0940a27dc52fc7d383bd39c958bca626e80215d0))
* **deps:** update dependency react-final-form to ^6.5.2 ([3eb4f65](https://gitlab.com/efg-erfurt/website/commit/3eb4f6524895a6cc60507da5b222c0ba947b8fca))
* **deps:** update dependency react-mde to ^11.1.0 ([065239f](https://gitlab.com/efg-erfurt/website/commit/065239f529260fddf145b4788d22d2eddcdb22f4))
* **deps:** update dependency sitemap to v7 ([39b07c2](https://gitlab.com/efg-erfurt/website/commit/39b07c23cdbed653206a49a455e07380c8a45a80))
* **deps:** update dependency tailwindcss to ^2.1.1 ([18f883f](https://gitlab.com/efg-erfurt/website/commit/18f883f9b45510d546dbbc4441966bba10a74203))
* **deps:** update dependency ua-parser-js to ^0.7.24 ([2c25c27](https://gitlab.com/efg-erfurt/website/commit/2c25c27b4304a7b70383f5bff3de137f03acbb5b))
* **deps:** update dependency webpack-node-externals to v3 ([a0ee6c1](https://gitlab.com/efg-erfurt/website/commit/a0ee6c1bc4247d48d277949a6944db3a2f08a2c0))
* don't emit schema ([29e3e81](https://gitlab.com/efg-erfurt/website/commit/29e3e81f3edeebd72884d673ba3860d4df7679db))
* fixed markdown rendering ([d1ab0d4](https://gitlab.com/efg-erfurt/website/commit/d1ab0d494c16c0e7ef1130300774a4917be4373e))
* fixed renovate config ([5e1de93](https://gitlab.com/efg-erfurt/website/commit/5e1de930390d92a84bfda18379f4cef801cac351))
* fixed static asset delivery ([4e540ae](https://gitlab.com/efg-erfurt/website/commit/4e540ae8b9c03a38b00d66cda9b783c1d5bc94da))
* moved package ([e4f1fcf](https://gitlab.com/efg-erfurt/website/commit/e4f1fcfee065e298998e08b9f87180fec280bbac))
* use fastify static again ([6aaec4d](https://gitlab.com/efg-erfurt/website/commit/6aaec4d1c6b4a6104a08cefa09ddccd7f9e3588f))
