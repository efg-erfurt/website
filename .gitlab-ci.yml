variables:
  FF_USE_FASTZIP: "true"
  ARTIFACT_COMPRESSION_LEVEL: "fastest"
  CACHE_COMPRESSION_LEVEL: "fastest"
  TRANSFER_METER_FREQUENCY: "5s"

stages:
  - setup
  - dependencies
  - build
  - test
  - lint
  - audit

.setup:
  stage: setup
  image: docker:24
  needs: []
  services:
    - docker:24-dind
  script:
    - cd ci/builders
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_NAME:$IMAGE_TAG  -f $IMAGE_FILE .
    - docker push $IMAGE_NAME:$IMAGE_TAG
  rules:
    # run only manually for newer ci releases
    - when: never

.api_base:
  image: registry.gitlab.com/efg-erfurt/website/ci/rust:v1.0
  needs:
    - job: "🏗️ setup:rust"
      optional: true
  variables:
    CARGO_REGISTRIES_CRATES_IO_PROTOCOL: "sparse"
    CARGO_HOME: ${CI_PROJECT_DIR}/.cargo
    CARGO_INCREMENTAL: 0
  script:
    - cd resources/functions/api
  rules: !reference [.mr-or-branch, rules]

.renderer_base:
  image: registry.gitlab.com/efg-erfurt/website/ci/node:v1.0
  needs:
    - job: "🏗️ setup:node"
      optional: true
  before_script:
    - pnpm config set store-dir .pnpm-store
  script:
    - cd resources/functions/renderer
    - pnpm install --frozen-lockfile
  rules: !reference [.mr-or-branch, rules]

.renderer_cache:
  cache: &renderer_cache
    key:
      files:
        - resources/functions/renderer/pnpm-lock.yaml
    paths:
      - resources/functions/renderer/.pnpm-store

.api_cache:
  cache: &api_cache
    key:
      files:
        - resources/functions/api/Cargo.lock
    paths:
      - .cargo/bin
      - .cargo/registry/index
      - .cargo/registry/cache
      - resources/functions/api/target/debug/deps
      - resources/functions/api/target/debug/build

.renderer_cache_write:
  cache:
    <<: *renderer_cache
    policy: pull-push

.renderer_cache_read:
  cache:
    <<: *renderer_cache
    policy: pull

.api_cache_write:
  cache:
    <<: *api_cache
    policy: pull-push

.api_cache_read:
  cache:
    <<: *api_cache
    policy: pull

.mr-or-branch:
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_BRANCH == "main"
    - if: "$CI_COMMIT_TAG =~ /^v.*/"
      when: never

# Setup Environment

"🏗️ setup:node":
  extends:
    - .setup
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
    IMAGE_NAME: registry.gitlab.com/efg-erfurt/website/ci/node
    IMAGE_TAG: v1.0
    IMAGE_FILE: Dockerfile.node

"🏗️ setup:rust":
  extends:
    - .setup
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
    IMAGE_NAME: registry.gitlab.com/efg-erfurt/website/ci/rust
    IMAGE_TAG: v1.0
    IMAGE_FILE: Dockerfile.rust

# Dependencies

"📦 dependencies:renderer":
  stage: dependencies
  extends:
    - .renderer_base
    - .renderer_cache_write
  script:
    - !reference [.renderer_base, script]

# Build

"🛠️ build:api":
  stage: build
  extends:
    - .api_base
    - .api_cache_write
  script:
    - !reference [.api_base, script]
    - cargo check

"🛠️ build:renderer":
  stage: build
  extends:
    - .renderer_base
    - .renderer_cache_read
  needs:
    - job: "📦 dependencies:renderer"
  script:
    - !reference [.renderer_base, script]
    - pnpm build

# Test

"🧪 test:api":
  stage: test
  extends:
    - .api_base
    - .api_cache_read
  needs:
    - job: "🛠️ build:api"
  script:
    - !reference [.api_base, script]
    - cargo nextest run --profile ci
  artifacts:
    reports:
      junit:
        - resources/functions/api/target/nextest/ci/junit.xml

"🧪 test:renderer":
  stage: test
  extends:
    - .renderer_base
    - .renderer_cache_read
  needs:
    - job: "📦 dependencies:renderer"
  script:
    - !reference [.renderer_base, script]
    - pnpm test -- --ci
  artifacts:
    reports:
      junit:
        - resources/functions/renderer/junit.xml
      coverage_report:
        coverage_format: cobertura
        path: resources/functions/renderer/coverage/cobertura-coverage.xml
    when: always
  coverage: '/All files[^|]*\|[^|]*\s+([\d\.]+)/'

# Lint and Format

"💄 lint:api":
  stage: lint
  extends:
    - .api_base
    - .api_cache_read
  needs:
    - job: "🛠️ build:api"
  script:
    - !reference [.api_base, script]
    - cargo clippy -- --no-deps
    - cargo fmt -- --check

# Audit

"🛡️ audit:api":
  stage: audit
  extends:
    - .api_base
    - .api_cache_read
  needs:
    - job: "🛠️ build:api"
  script:
    - !reference [.api_base, script]
    - cargo audit
  allow_failure: true

"🛡️ audit:renderer":
  stage: audit
  extends:
    - .renderer_base
    - .renderer_cache_read
  needs:
    - job: "📦 dependencies:renderer"
  script:
    - !reference [.renderer_base, script]
    - pnpm audit
  allow_failure: true
