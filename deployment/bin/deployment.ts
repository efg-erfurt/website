#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "aws-cdk-lib";
import { DeploymentStack } from "../lib/deployment-stack";
import { RepositoryStack } from "../lib/repository-stack";

export type CustomEnv = "prod" | "dev";

const customEnv: CustomEnv = "prod";

const app = new cdk.App();

const repoStack = new RepositoryStack(app, `efg-erfurt-${customEnv}-repos`);

new DeploymentStack(app, `efg-erfurt-${customEnv}-deployment`, {
  repoStack,
  customEnv
});
