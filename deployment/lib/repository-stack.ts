import { RemovalPolicy, Stack, StackProps } from "aws-cdk-lib";
import * as ecr from "aws-cdk-lib/aws-ecr";
import * as s3 from "aws-cdk-lib/aws-s3";
import * as s3Deployment from "aws-cdk-lib/aws-s3-deployment";
import * as iam from "aws-cdk-lib/aws-iam";
import * as cloudfront from "aws-cdk-lib/aws-cloudfront";
import { Construct } from "constructs";

export class RepositoryStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const apiEcrRepo = new ecr.Repository(this, "api-repo", {
      imageScanOnPush: true,
      imageTagMutability: ecr.TagMutability.IMMUTABLE
    });
    this.apiRepoArn = apiEcrRepo.repositoryArn;
    const rendererEcrRepo = new ecr.Repository(this, "renderer-repo", {
      imageScanOnPush: true,
      imageTagMutability: ecr.TagMutability.IMMUTABLE
    });
    this.rendererRepoArn = rendererEcrRepo.repositoryArn;

    const accessIdentity = new cloudfront.OriginAccessIdentity(
      this,
      "origin-access-identity",
      {
        comment: "access for cloudfront"
      }
    );

    this.accessIdentityId = accessIdentity.originAccessIdentityId;

    const assetsBucket = new s3.Bucket(this, "assets-bucket", {
      accessControl: s3.BucketAccessControl.PRIVATE,
      removalPolicy: RemovalPolicy.RETAIN
    });
    assetsBucket.addToResourcePolicy(
      new iam.PolicyStatement({
        principals: [
          new iam.CanonicalUserPrincipal(
            accessIdentity.cloudFrontOriginAccessIdentityS3CanonicalUserId
          )
        ],
        actions: ["s3:GetObject"],
        resources: [`${assetsBucket.bucketArn}/*`]
      })
    );
    this.assetsBucketArn = assetsBucket.bucketArn;

    const distBucket = new s3.Bucket(this, "dist-bucket", {
      accessControl: s3.BucketAccessControl.PRIVATE,
      removalPolicy: RemovalPolicy.DESTROY
    });
    distBucket.addToResourcePolicy(
      new iam.PolicyStatement({
        principals: [
          new iam.CanonicalUserPrincipal(
            accessIdentity.cloudFrontOriginAccessIdentityS3CanonicalUserId
          )
        ],
        actions: ["s3:GetObject"],
        resources: [`${distBucket.bucketArn}/*`]
      })
    );
    this.distBucketArn = distBucket.bucketArn;

    const astroBucket = new s3.Bucket(this, "zeitungen-bucket", {
      accessControl: s3.BucketAccessControl.PRIVATE,
      removalPolicy: RemovalPolicy.DESTROY
    });
    astroBucket.addToResourcePolicy(
      new iam.PolicyStatement({
        principals: [
          new iam.CanonicalUserPrincipal(
            accessIdentity.cloudFrontOriginAccessIdentityS3CanonicalUserId
          )
        ],
        actions: ["s3:GetObject"],
        resources: [`${astroBucket.bucketArn}/*`]
      })
    );
    this.astroBucketArn = astroBucket.bucketArn;

    new s3Deployment.BucketDeployment(this, "DeployServerRoot", {
      sources: [s3Deployment.Source.asset("../resources/server-root")],
      destinationBucket: distBucket,
      cacheControl: [
        s3Deployment.CacheControl.fromString("public,max-age=31536000")
      ],
      prune: false
    });
    new s3Deployment.BucketDeployment(this, "DeployDist", {
      sources: [
        s3Deployment.Source.asset("../resources/functions/renderer/dist")
      ],
      destinationKeyPrefix: "public",
      destinationBucket: distBucket,
      cacheControl: [
        s3Deployment.CacheControl.fromString("public,max-age=31536000")
      ],
      prune: false
    });
    new s3Deployment.BucketDeployment(this, "DeployZeitungen", {
      sources: [s3Deployment.Source.asset("../resources/astro/dist")],
      destinationBucket: astroBucket,
      cacheControl: [
        s3Deployment.CacheControl.fromString("public,max-age=31536000")
      ],
      prune: false
    });
  }

  apiRepoArn = "";
  rendererRepoArn = "";
  assetsBucketArn = "";
  distBucketArn = "";
  astroBucketArn = "";
  accessIdentityId = "";
}
