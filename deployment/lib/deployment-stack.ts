import { CfnOutput, Stack, StackProps } from "aws-cdk-lib";
import * as s3 from "aws-cdk-lib/aws-s3";
import { Construct } from "constructs";
import { RepositoryStack } from "./repository-stack";
import { createApiFunction } from "./functions/api";
import { createRendererFunction } from "./functions/renderer";
import { createCloudfront } from "./cloudfront";
import { createApiGateway } from "./api-gateway";
import { createTables } from "./dynamodb";
import { CustomEnv } from "../bin/deployment";

type DeploymentStackProps = StackProps & {
  repoStack: RepositoryStack;
  customEnv: CustomEnv;
};

export class DeploymentStack extends Stack {
  private customEnv: CustomEnv;

  constructor(scope: Construct, id: string, props: DeploymentStackProps) {
    super(scope, id, props);
    this.customEnv = props.customEnv;

    const {
      repoStack: {
        assetsBucketArn,
        distBucketArn,
        astroBucketArn,
        accessIdentityId,
        rendererRepoArn,
        apiRepoArn
      }
    } = props;

    const assetsBucket = s3.Bucket.fromBucketArn(
      this,
      "assets-bucket2",
      assetsBucketArn
    );

    const tables = createTables(this);
    const apiFunction = createApiFunction(this, {
      ...tables,
      assetsBucket: assetsBucket
    });
    const rendererFunction = createRendererFunction(this);

    const httpApi = createApiGateway(this, {
      apiRepoArn: apiRepoArn,
      rendererRepoArn: rendererRepoArn,
      apiFunction,
      rendererFunction
    });

    const cf = createCloudfront(this, {
      assetsBucketArn,
      distBucketArn,
      astroBucketArn,
      accessIdentityId,
      httpApi
    });

    new CfnOutput(this, "apiGwUrl", {
      value: httpApi.url!,
      exportName: `apiGwUrl-${this.customEnv}`
    });
    new CfnOutput(this, "cfUrl", {
      value: cf.distributionDomainName,
      exportName: `cfUrl-${this.customEnv}`
    });
  }

  getCustomEnv() {
    return this.customEnv;
  }

  isProd() {
    return this.customEnv === "prod";
  }
}
