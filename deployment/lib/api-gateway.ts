import * as lambda from "aws-cdk-lib/aws-lambda";
import * as apiGw from "aws-cdk-lib/aws-apigatewayv2";
import * as apiGwInteg from "aws-cdk-lib/aws-apigatewayv2-integrations";
import { Stack } from "aws-cdk-lib";

export const createApiGateway = (
  stack: Stack,
  opts: {
    apiRepoArn: string;
    rendererRepoArn: string;
    apiFunction: lambda.IFunction;
    rendererFunction: lambda.IFunction;
  }
) => {
  const { apiFunction, rendererFunction } = opts;
  const httpApi = new apiGw.HttpApi(stack, "lambda-api", {
    corsPreflight: { allowOrigins: ["*"] }
  });
  httpApi.addRoutes({
    path: "/{proxy+}",
    methods: [apiGw.HttpMethod.GET],
    integration: new apiGwInteg.HttpLambdaIntegration(
      "renderer-integration",
      rendererFunction
    )
  });
  httpApi.addRoutes({
    path: "/api/{proxy+}",
    methods: [apiGw.HttpMethod.POST, apiGw.HttpMethod.GET],
    integration: new apiGwInteg.HttpLambdaIntegration(
      "api-integration",
      apiFunction
    )
  });
  return httpApi;
};
