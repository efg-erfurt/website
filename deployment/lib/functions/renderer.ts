import * as lambda from "aws-cdk-lib/aws-lambda";
import { Duration } from "aws-cdk-lib";
import { sharedLambdaProps } from "./shared";
import { DeploymentStack } from "../deployment-stack";

export const createRendererFunction = (stack: DeploymentStack) => {
  return new lambda.DockerImageFunction(stack, "renderer-function", {
    code: lambda.DockerImageCode.fromImageAsset(
      "../resources/functions/renderer"
    ),
    timeout: Duration.seconds(20),
    environment: {
      CDN_URL: "d1ho7rt58l9xhb.cloudfront.net",
      ORIGIN: "www.efg-erfurt.de",
      PROTOCOL: "https:",
      STAGE: stack.getCustomEnv()
    },
    tracing: lambda.Tracing.ACTIVE,
    ...sharedLambdaProps
  });
};
