import * as lambda from "aws-cdk-lib/aws-lambda";
import * as iam from "aws-cdk-lib/aws-iam";
import * as s3 from "aws-cdk-lib/aws-s3";
import * as dynamodb from "aws-cdk-lib/aws-dynamodb";
import { CfnParameter, Duration } from "aws-cdk-lib";
import { sharedLambdaProps } from "./shared";
import { DeploymentStack } from "../deployment-stack";

export const createApiFunction = (
  stack: DeploymentStack,
  opts: {
    contentsDb: dynamodb.ITable;
    postsDb: dynamodb.ITable;
    sermonsDb: dynamodb.ITable;
    cacheDb: dynamodb.ITable;
    assetsBucket: s3.IBucket;
  }
) => {
  const { contentsDb, postsDb, sermonsDb, cacheDb, assetsBucket } = opts;

  const dynamoDbPolicy = new iam.PolicyStatement();
  dynamoDbPolicy.addActions("dynamodb:*");
  dynamoDbPolicy.addResources(
    contentsDb.tableArn,
    postsDb.tableArn,
    sermonsDb.tableArn,
    cacheDb.tableArn
  );
  const s3Policy = new iam.PolicyStatement();
  s3Policy.addActions("s3:*");
  s3Policy.addResources(`${assetsBucket.bucketArn}/*`);

  const ctTechUser = new CfnParameter(stack, "ctTechUser", {
    type: "String",
    description: "ChurchTools Tech User"
  });
  const ctTechPw = new CfnParameter(stack, "ctTechPw", {
    type: "String",
    description: "ChurchTools Tech Password",
    noEcho: true
  });
  const smtpMail = new CfnParameter(stack, "smtpMail", {
    type: "String",
    description: "SMTP Mail"
  });
  const smtpHost = new CfnParameter(stack, "smtpHost", {
    type: "String",
    description: "SMTP Host"
  });
  const smtpPw = new CfnParameter(stack, "smtpPw", {
    type: "String",
    description: "SMTP PW",
    noEcho: true
  });
  const smtpUser = new CfnParameter(stack, "smtpUser", {
    type: "String",
    description: "SMTP User"
  });

  return new lambda.DockerImageFunction(stack, "api-function", {
    code: lambda.DockerImageCode.fromImageAsset("../resources/functions/api"),
    timeout: Duration.seconds(20),
    environment: {
      STAGE: stack.getCustomEnv(),
      RUST_BACKTRACE: "1",
      RUST_LOG: "info",
      CT_USER: ctTechUser.valueAsString,
      CT_PW: ctTechPw.valueAsString,
      SMTP_MAIL: smtpMail.valueAsString,
      SMTP_HOST: smtpHost.valueAsString,
      SMTP_USER: smtpUser.valueAsString,
      SMTP_PW: smtpPw.valueAsString,
      TABLE_CONTENT: contentsDb.tableName,
      TABLE_POST: postsDb.tableName,
      TABLE_SERMON: sermonsDb.tableName,
      TABLE_CACHE: cacheDb.tableName,
      BUCKET_PUBLIC: assetsBucket.bucketName
    },
    tracing: lambda.Tracing.ACTIVE,
    initialPolicy: [dynamoDbPolicy, s3Policy],
    ...sharedLambdaProps
  });
};
