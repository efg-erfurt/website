import * as lambda from "aws-cdk-lib/aws-lambda";

export const sharedLambdaProps = {
  architecture: lambda.Architecture.ARM_64,
  memorySize: 2048
};
