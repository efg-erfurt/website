import * as s3 from "aws-cdk-lib/aws-s3";
import * as cloudfront from "aws-cdk-lib/aws-cloudfront";
import * as certificatemanager from "aws-cdk-lib/aws-certificatemanager";
import * as cfOrigins from "aws-cdk-lib/aws-cloudfront-origins";
import * as apiGw from "aws-cdk-lib/aws-apigatewayv2";
import { Duration, Fn } from "aws-cdk-lib";
import { DeploymentStack } from "./deployment-stack";

export const createCloudfront = (
  stack: DeploymentStack,
  opts: {
    assetsBucketArn: string;
    distBucketArn: string;
    astroBucketArn: string;
    accessIdentityId: string;
    httpApi: apiGw.IHttpApi;
  }
): cloudfront.IDistribution => {
  const {
    accessIdentityId,
    assetsBucketArn,
    distBucketArn,
    astroBucketArn,
    httpApi
  } = opts;

  const assetsBucket = s3.Bucket.fromBucketArn(
    stack,
    "assets-bucket",
    assetsBucketArn
  );
  const distBucket = s3.Bucket.fromBucketArn(
    stack,
    "dist-bucket",
    distBucketArn
  );
  const zeitungenBucket = s3.Bucket.fromBucketArn(
    stack,
    "zeitungen-bucket",
    astroBucketArn
  );
  const accessIdentity =
    cloudfront.OriginAccessIdentity.fromOriginAccessIdentityId(
      stack,
      "accessidentity",
      accessIdentityId
    );

  const sharedCachePolicyProps = {
    enableAcceptEncodingGzip: true,
    enableAcceptEncodingBrotli: true,
    cookieBehavior: cloudfront.CacheCookieBehavior.none(),
    headerBehavior: cloudfront.CacheHeaderBehavior.none(),
    queryStringBehavior: cloudfront.CacheQueryStringBehavior.all()
  };
  const functionCachePolicy = new cloudfront.CachePolicy(
    stack,
    "function-cache-policy",
    {
      maxTtl: Duration.days(1),
      minTtl: Duration.minutes(15),
      defaultTtl: Duration.minutes(15),
      ...sharedCachePolicyProps
    }
  );

  const assetsCachePolicy = new cloudfront.CachePolicy(
    stack,
    "assets-cache-policy",
    {
      maxTtl: Duration.days(365),
      minTtl: Duration.days(30),
      defaultTtl: Duration.days(30),
      ...sharedCachePolicyProps
    }
  );

  const zeitungenCachePolicy = new cloudfront.CachePolicy(
    stack,
    "zeitungen-cache-policy",
    {
      maxTtl: Duration.days(365),
      minTtl: Duration.days(1),
      defaultTtl: Duration.days(30),
      ...sharedCachePolicyProps
    }
  );

  const assetsBehavior: cloudfront.BehaviorOptions = {
    origin: cfOrigins.S3BucketOrigin.withOriginAccessIdentity(assetsBucket, {
      originAccessIdentity: accessIdentity
    }),
    viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
    cachePolicy: assetsCachePolicy,
    allowedMethods: cloudfront.AllowedMethods.ALLOW_GET_HEAD_OPTIONS,
    cachedMethods: cloudfront.CachedMethods.CACHE_GET_HEAD_OPTIONS
  };
  const distBehavior: cloudfront.BehaviorOptions = {
    origin: cfOrigins.S3BucketOrigin.withOriginAccessIdentity(distBucket, {
      originAccessIdentity: accessIdentity
    }),
    viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
    cachePolicy: assetsCachePolicy,
    allowedMethods: cloudfront.AllowedMethods.ALLOW_GET_HEAD_OPTIONS,
    cachedMethods: cloudfront.CachedMethods.CACHE_GET_HEAD_OPTIONS
  };
  const astroBehavior: cloudfront.BehaviorOptions = {
    origin: cfOrigins.S3BucketOrigin.withOriginAccessIdentity(zeitungenBucket, {
      originAccessIdentity: accessIdentity
    }),
    functionAssociations: [
      {
        function: new cloudfront.Function(stack, "astro-redirect", {
          functionName: "astroRedirect",
          code: cloudfront.FunctionCode.fromInline(`function handler(event) {
  var request = event.request;
  var uri = request.uri;
  if (uri.endsWith('/')) {
    request.uri += 'index.html';
  }
  else if (!uri.includes('.')) {
    request.uri += '/index.html';
  }
  return request;
}`)
        }),
        eventType: cloudfront.FunctionEventType.VIEWER_REQUEST
      }
    ],
    viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
    cachePolicy: zeitungenCachePolicy,
    allowedMethods: cloudfront.AllowedMethods.ALLOW_GET_HEAD_OPTIONS,
    cachedMethods: cloudfront.CachedMethods.CACHE_GET_HEAD_OPTIONS
  };
  const httpApiDomainName = Fn.parseDomainName(httpApi.apiEndpoint);
  const httpApiBehavior: cloudfront.BehaviorOptions = {
    origin: new cfOrigins.HttpOrigin(httpApiDomainName),
    viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
    cachePolicy: functionCachePolicy,
    allowedMethods: cloudfront.AllowedMethods.ALLOW_ALL,
    cachedMethods: cloudfront.CachedMethods.CACHE_GET_HEAD,
    responseHeadersPolicy:
      cloudfront.ResponseHeadersPolicy
        .CORS_ALLOW_ALL_ORIGINS_WITH_PREFLIGHT_AND_SECURITY_HEADERS,
    originRequestPolicy: cloudfront.OriginRequestPolicy.CORS_CUSTOM_ORIGIN
  };

  const certificate = certificatemanager.Certificate.fromCertificateArn(
    stack,
    "efg-certificate",
    "arn:aws:acm:us-east-1:639143552896:certificate/4cfce5d2-aa1a-4cf9-b955-a0e24cad596f"
  );

  return new cloudfront.Distribution(stack, "cdn", {
    defaultBehavior: httpApiBehavior,
    additionalBehaviors: {
      "api/*": httpApiBehavior,
      "public/assets/*": assetsBehavior,
      "_astro/*": astroBehavior,
      "zeitungen/*": astroBehavior,
      "public/*": distBehavior,
      "favicon.ico": distBehavior,
      "sitemap.xml": distBehavior,
      "robots.txt": distBehavior
    },
    certificate,
    domainNames: stack.getCustomEnv() ? ["www.efg-erfurt.de"] : [],
    minimumProtocolVersion: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2021,
    priceClass: cloudfront.PriceClass.PRICE_CLASS_100,
    defaultRootObject: "index.html"
  });
};
