import { RemovalPolicy } from "aws-cdk-lib";
import * as dynamodb from "aws-cdk-lib/aws-dynamodb";
import { DeploymentStack } from "./deployment-stack";

const generateSharedDbProps = (stack: DeploymentStack) => ({
  billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
  pointInTimeRecovery: true,
  removalPolicy: stack.isProd() ? RemovalPolicy.RETAIN : RemovalPolicy.DESTROY
});

export const createTables = (stack: DeploymentStack) => {
  const contentsDb = new dynamodb.Table(
    stack,
    `content-${stack.getCustomEnv()}`,
    {
      partitionKey: { name: "id", type: dynamodb.AttributeType.STRING },
      ...generateSharedDbProps(stack)
    }
  );
  const postsDb = new dynamodb.Table(stack, `posts-${stack.getCustomEnv()}`, {
    partitionKey: { name: "id", type: dynamodb.AttributeType.STRING },
    ...generateSharedDbProps(stack)
  });
  const cacheDb = new dynamodb.Table(stack, `cache-${stack.getCustomEnv()}`, {
    partitionKey: { name: "id", type: dynamodb.AttributeType.STRING },
    timeToLiveAttribute: "ttl",
    ...generateSharedDbProps(stack)
  });
  const sermonsDb = new dynamodb.Table(
    stack,
    `sermons-${stack.getCustomEnv()}`,
    {
      partitionKey: { name: "id", type: dynamodb.AttributeType.STRING },
      ...generateSharedDbProps(stack)
    }
  );

  return { contentsDb, postsDb, sermonsDb, cacheDb };
};
