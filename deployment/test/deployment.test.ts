import * as cdk from "aws-cdk-lib";
import { Template } from "aws-cdk-lib/assertions";
import test from "node:test";
import * as repo from "../lib/repository-stack";

test("RepositoryStack", () => {
  // given
  const app = new cdk.App();

  // when
  const stack = new repo.RepositoryStack(app, "test");

  // then
  const template = Template.fromStack(stack);
  template.hasResourceProperties("AWS::S3::Bucket", {
    VisibilityTimeout: 300
  });
});
